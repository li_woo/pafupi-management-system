<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once 'pafupi.php';
require_once 'Security_check.php';
//require_once  APPPATH.'libraries/ofslib/OFSLib.php';
//require_once APPPATH.'libraries/PHPExcel.php';
class Account extends CI_Controller {

    /**
     * A class that handles pafupi account functions
     */
    public $active;
    public $role_id;
    public $name;
    public $current_page;
    public $ip_address;
    public function __construct()
    {
        parent::__construct();
        $this->load->library('blade');
        $this->load->library('excel');
        //$this->load->library('OFSLib');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('pagination');
        $this->blade->set('base_url',BASEURL);
        $this->load->model('account_m_model','account');
        //$this->session->userdata('fullname');
        $this->load->library('session');
        $this->active ='act_account';
        $this->load->model('authentication');
        //$this->ip_address      = Network::get_ip_address();
        Security_check::check_login();
        $this->username = $this->session->userdata('username');
        if(empty($this->username))
            $this->username = false;
        $this->current_page ='';
        /*if($this->session->userdata('role_id') !=1){
            pafupi::index();
        }*/


    }

    /**
     * 	shows a view where the user can generate accounts - the user chooses the number of pafupi accounts to be created and the branch which the accounts will belong
     *
     * @authour Lewis Msasa <lmsasajnr@gmail.com>
     */
    public function createOTPView(){
        $branches = $this->account->getBranches();
        $this->name = 'create OTP books here';
        $this->active ='act_pin';
        $this->current_page='account_otp';
        $this->blade->render('create_one_time_pin', array('title' => 'create OTPs','type'=>'otp'));
    }
    public function createRefView(){
        $branches = $this->account->getBranches();
        $this->name = 'create Referral books here';
        $this->active ='act_pin';
        $this->current_page='account_otp';
        $this->blade->render('create_one_time_pin', array('title' => 'create OTPs','type'=>'ref'));
    }
    public function createOTP(){
        $this->session->unset_userdata('book_creating');
        if(!$this->session->userdata('book_creating')){
            $this->session->set_userdata('book_creating',1);
        $num_accounts = $this->security->xss_clean($this->input->post('number'));
        $action_type = $this->security->xss_clean($this->input->post('action_type'));
        $num = 0;
        ignore_user_abort(true);
        set_time_limit(0);
        //ob_start();
        //usleep(1500000);
       // echo json_encode("The books are being generated..you can do other things as you wait");
        // now force PHP to output to the browser...
        //$size = ob_get_length();
        //header("Content-Length: $size");
        //header('Connection: close');
        //ob_end_flush();
        //ob_flush();
        //flush();
        //if (session_id()) session_write_close();
        for($i=0;$i<$num_accounts;$i++){
;
            if($action_type =='otp'){
            $result = $this->account->makeBooklet(0);

            }
            elseif($action_type=='ref')
            $result = $this->account->makeCoupon(0);

            if($result){

                $num++;
            }


        }
        //usleep(120000);

        //$this->session->unset_userdata('book_creating');
        $this->session->set_userdata('book_finished',$num." booklets created");
        //$this->session->unset_userdata('book_creating');
        echo json_encode($num." booklets created");
        }
        else{
            echo json_encode("Another process creating booklets");
        }
    }
    public function booKFinished(){
        set_time_limit(0);
       // $this->session->unset_userdata('book_creating');
        $note = $this->session->userdata('book_finished');
        $creating = $this->session->userdata('book_creating');
        while($creating !="" || !$creating){
            if(!$note){
                sleep(1);
                continue;
            }
            else{
                $this->session->unset_userdata('book_finished');
                $this->session->unset_userdata('book_creating');
                echo json_encode($note);
                break;
            }
        }
    }
    public function createAccountView(){

        $branches = $this->account->getBranches();
        $this->name = 'generate pafupi accounts here';
        $this->active = 'act_pin';
        $this->current_page='account_preopen';
        $this->blade->set('branches',$branches)->render('create_account', array('title' => 'create Pafupi Accounts'));
    }
    public function createCardView(){
        $branches = $this->account->getBranches();
        $this->name = 'generate ATM Cards here';
        $this->active = 'act_pin';
        $this->current_page='account_preopen';
        $this->blade->set('branches',$branches)->render('create_card', array('title' => 'create ATM Cards'));
    }

    public function createCardPost(){
        $num = $this->security->xss_clean($this->input->post('number'));
        $branch_id = $this->security->xss_clean($this->input->post('branch'));
        $num = $this->account->createDummyCard($num,$branch_id);
        echo json_encode($num);
    }
    public function inventoryView(){
        $branches = $this->account->getBranches();
        $this->name = 'Inventory management';
        $this->active = 'act_inventory';
        $this->current_page='inventory';
        $this->blade->render('inventory', array('title' => 'inventory management'));
    }
    public function accountView(){
        $branches = $this->account->getBranches();
        $this->name = 'pafupi accounts';
        $this->current_page='account';
        $this->blade->set('branches',$branches)->render('account', array('title' => 'Pafupi Accounts'));
    }
    /**
     * send the request to T24 by calling sentToT24() to create the number of accounts specified by the user in the form submited from the create_account view
     *
     * for every success response from T24 on account creation, the function inserts the created account into a pafupi accounts table
     * A booklet is also generated which has 50 pins by calling the createBooklet() on each successfully created account.
     *
     * for every failure from T24 on account creation, the function logs the result into the log database table
     *
     *
     *
     * @authour Lewis Msasa <lmsasajnr@gmail.com>
     */
    public function dummyAccount($num_account){
        $data = array();
        for($i=0;$i<$num_account;$i++){
            $data[$i]['account_number'] = '0025'.mt_rand(0,9).mt_rand(0,9).mt_rand(0,9).mt_rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9);
            $data[$i]['customer_id'] = substr( $data[$i]['account_number'],3,6);
        }
        return $data;
    }
    public function createAccount(){
        $num_accounts = $this->security->xss_clean($this->input->post('number'));
        $branch_id = $this->security->xss_clean($this->input->post('branch'));
        $randoms = array();
        //send to t24
        $data = array();
        //$data = $this->sendToT24($num_accounts,$branch_id);
        $data = $this->dummyAccount($num_accounts);
        $num = 0;
        if($data){
            for($i=0; $i<count($data);$i++){
                if($data[$i]){
                    $data[$i]['branch_id'] = $branch_id;
                    //change
                    //$data[$i] = array('branch_id' => $branch_id, 'account_number' =>'00251711'.rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9),'opened_by' =>'05050');
                    $d = $this->account->saveAccount($data[$i]);
                    if($d)
                        $num++;
                }
                else{

                }
            }
        }
        if($num){
            echo json_encode($num." out of ".$num_accounts." account(s) created");
        }
        else{
            echo json_encode(false);
        }
    }
    public function viewTolink($user_id=false){
        $feedback = false;
        $search_feedback = false;
        $action = 'linking';
        $branches = $this->account->getBranches();
        $this->blade->set('branches',$branches);
        $user_id    =  $this->session->userdata('user_id');
        if($this->security->xss_clean($this->input->get('act'))=='linking'){
            $selected_accounts = $this->security->xss_clean($this->input->post('selected_accounts'));
            $feedback = $this->account->linkBooks(false);

            //$feedback = $this->account->removeAccount($selected_accounts);
        }
        if($this->security->xss_clean($this->input->post('search'))){
            $param = $this->security->xss_clean($this->input->post('param'));
            $data = $this->account->search_precarded(false,false,$param);
            $this->name =  "view Pre-carded accounts";
            $search_feedback = "search for ".$param." ". $data['total']." Pre-carded accounts found";
            $num_rows = $this->pagination($data['total']);
            $data["total_rows"] = $num_rows;
            $this->active = 'act_link';

            $num_rows = $this->pagination(count($data));
            //$data['users']            = $num_rows;
            $config['base_url']       = BASEURL.'pafupi/main/view_to_link';
            $config['total_rows']     = $num_rows;
            $config['per_page']       = 5;
            $config['num_links']      = 3;
            $config['last_link']      = FALSE;
            $config['first_link']     = FALSE;
            $config['uri_segment']    = 4;
            $config['next_link']      = 'Next';
            $config['next_tag_open']  = "<td class='pagination-item-outer-rght'>";
            $config['next_tag_close'] = "</td'>";
            $config['prev_link']      = 'Previous';
            $config['prev_tag_open']  = "<td class='pagination-item-outer-lft'>";
            $config['prev_tag_close'] = "</td>";
            $config['num_tag_open']   = "<td class='pagination-item'>";
            $config['num_tag_close']  = "</td>";
            $config['cur_tag_open']   = "<td class='pagination-item-current'>";
            $config['cur_tag_close']  = "</td>";

            $this->pagination->initialize($config);
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $data['result']= array_slice($data,$page,$config['per_page']);


            $this->blade->set('accounts',$data['result'])->render('view_prelinked', array('title' => 'view Pafupi Pre-carded Accounts','feedback'=>$feedback,'search_feedback'=>$search_feedback,'action'=>$action));
            return;

        }
        else{
            $this->name =  "Link otp and referral books to pre-opened pafupi accounts  ";
            $accounts   =  $this->account->get_all_to_link_accounts(false,false,true);
            $num_rows   =  $this->pagination(count($accounts));
            $data["total_rows"] = $num_rows;

            $this->active = 'act_link';

           // $data['users']            = $num_rows;
            $config['base_url']       = BASEURL.'pafupi/main/view_to_link';
            $config['total_rows']     = $num_rows;
            $config['per_page']       = 5;
            $config['num_links']      = 3;
            $config['last_link']      = FALSE;
            $config['first_link']     = FALSE;
            $config['uri_segment']    = 4;
            $config['next_link']      = 'Next';
            $config['next_tag_open']  = "<td class='pagination-item-outer-rght'>";
            $config['next_tag_close'] = "</td'>";
            $config['prev_link']      = 'Previous';
            $config['prev_tag_open']  = "<td class='pagination-item-outer-lft'>";
            $config['prev_tag_close'] = "</td>";
            $config['num_tag_open']   = "<td class='pagination-item'>";
            $config['num_tag_close']  = "</td>";
            $config['cur_tag_open']   = "<td class='pagination-item-current'>";
            $config['cur_tag_close']  = "</td>";

            $this->pagination->initialize($config);
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $data['result']     = $this->account-> get_all_to_link_accounts($config['per_page'],$page);

            $this->blade->set('accounts',$data['result'])->render('view_prelinked', array('title' => 'Link Accounts','feedback'=>$feedback,'search_feedback'=>$search_feedback,'action'=>$action));
        }
    }
    public function viewToCard($user_id=false){
        $feedback = false;
        $search_feedback = false;
        $action = 'carding';
        $branches = $this->account->getBranches();
        $this->blade->set('branches',$branches);
        $user_id    =  $this->session->userdata('user_id');
        if($this->security->xss_clean($this->input->get('act'))=='carding'){
            //$selected_accounts = $this->security->xss_clean($this->input->post('selected_accounts'));
            $feedback = $this->account->saveCardNumbers(false);


        }
        if($this->security->xss_clean($this->input->post('search'))){
            $param = $this->security->xss_clean($this->input->post('param'));
            $data = $this->account->search_precarded(false,false,$param);
            $this->name =  "view Pre-carded accounts";
            $search_feedback = "search for ".$param." ". $data['total']." Pre-carded accounts found";
            $num_rows = $data['total'];
            $this->active = 'act_link';
            $this->blade->set('accounts',$data['result'])->render('view_prelinked', array('title' => 'view Pafupi Pre-carded Accounts','feedback'=>$feedback,'search_feedback'=>$search_feedback,'action'=>$action));
            return;

        }
        else{
            $this->name =  "link atm cards to pre-opened pafupi accounts";
            $accounts   =  $this->account->get_all_to_card_accounts(false,false,true);
            $num_rows   =  $this->pagination(count($accounts));
            $data["total_rows"] = $num_rows;

            $this->active = 'act_link';

           // $data['users']            = $num_rows;
            $config['base_url']       = BASEURL.'pafupi/main/view_to_card';
            $config['total_rows']     = $num_rows;
            $config['per_page']       = 5;
            $config['num_links']      = 3;
            $config['last_link']      = FALSE;
            $config['first_link']     = FALSE;
            $config['uri_segment']    = 4;
            $config['next_link']      = 'Next';
            $config['next_tag_open']  = "<td class='pagination-item-outer-rght'>";
            $config['next_tag_close'] = "</td'>";
            $config['prev_link']      = 'Previous';
            $config['prev_tag_open']  = "<td class='pagination-item-outer-lft'>";
            $config['prev_tag_close'] = "</td>";
            $config['num_tag_open']   = "<td class='pagination-item'>";
            $config['num_tag_close']  = "</td>";
            $config['cur_tag_open']   = "<td class='pagination-item-current'>";
            $config['cur_tag_close']  = "</td>";

            $this->pagination->initialize($config);
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $data['result']     = $this->account->get_all_to_card_accounts($config['per_page'],$page);

            $this->blade->set('accounts',$data['result'])->render('view_prelinked', array('title' => 'Link Accounts','feedback'=>$feedback,'search_feedback'=>$search_feedback,'action'=>$action));
        }
    }
    public function viewPrelinked($user_id=false){
        $feedback = false;
        $search_feedback = false;
        $branches = $this->account->getBranches();
        $this->blade->set('branches',$branches);
        $user_id    =  $this->session->userdata('user_id');
        if($this->security->xss_clean($this->input->get('link'))){
            $selected_accounts = $this->security->xss_clean($this->input->post('selected_accounts'));
            $feedback = $this->account->linkBooks($selected_accounts);

            //$feedback = $this->account->removeAccount($selected_accounts);
        }
        if($this->security->xss_clean($this->input->post('link_card'))){
            $selected_accounts = $this->security->xss_clean($this->input->post('selected_accounts'));
            $feedback = $this->account->saveCardNumbers($selected_accounts);

            //$feedback = $this->account->removeAccount($selected_accounts);
        }
        if($this->security->xss_clean($this->input->get('act'))=='linking'){
            //$selected_accounts = $this->security->xss_clean($this->input->post('selected_accounts'));
            $feedback = $this->account->saveCardNumbers(false);
            var_dump($feedback);exit;
            //$feedback = $this->account->removeAccount($selected_accounts);
        }
        if($this->security->xss_clean($this->input->post('search'))){
            $param = $this->security->xss_clean($this->input->post('param'));
            $data = $this->account->search_precarded(false,false,$param);
            $this->name =  "view Pre-carded accounts";
            $search_feedback = "search for ".$param." ". $data['total']." Pre-carded accounts found";
            $num_rows = $data['total'];
            $this->active = 'act_link';
            $this->blade->set('accounts',$data['result'])->render('view_prelinked', array('title' => 'view Pafupi Pre-carded Accounts','feedback'=>$feedback,'search_feedback'=>$search_feedback));
            return;

        }
        else{
            $this->name =  "view Pre-carded accounts";
            $accounts   =  $this->account->getPreAccount($user_id);
            $num_rows   =  count($accounts);
            $data["total_rows"] = $num_rows;

            $this->active = 'act_link';

            //$data['users']            = $num_rows;
            $config['base_url']       = BASEURL.'pafupi/main/view_preopen';
            $config['total_rows']     = $num_rows;
            $config['per_page']       = 5;
            $config['num_links']      = 3;
            $config['last_link']      = FALSE;
            $config['first_link']     = FALSE;
            $config['uri_segment']    = 4;
            $config['next_link']      = 'Next';
            $config['next_tag_open']  = "<td class='pagination-item-outer-rght'>";
            $config['next_tag_close'] = "</td'>";
            $config['prev_link']      = 'Previous';
            $config['prev_tag_open']  = "<td class='pagination-item-outer-lft'>";
            $config['prev_tag_close'] = "</td>";
            $config['num_tag_open']   = "<td class='pagination-item'>";
            $config['num_tag_close']  = "</td>";
            $config['cur_tag_open']   = "<td class='pagination-item-current'>";
            $config['cur_tag_close']  = "</td>";

            $this->pagination->initialize($config);
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $data['result']     = $this->account->get_all_prelinked_accounts($config['per_page'],$page);

            $this->blade->set('accounts',$data['result'])->render('view_prelinked', array('title' => 'view Pafupi Pre-carded Accounts','feedback'=>$feedback,'search_feedback'=>$search_feedback));
        }
    }
    public function viewLinked($user_id=false){
        $feedback = false;
        $search_feedback = false;
        $branches = $this->account->getBranches();
        $this->blade->set('branches',$branches);
        $user_id    =  $this->session->userdata('user_id');
        if($this->security->xss_clean($this->input->post('link_card'))){
            $selected_accounts = $this->security->xss_clean($this->input->post('selected_accounts'));
            //$feedback = $this->account->saveCardNumbers($selected_accounts);

            //$feedback = $this->account->removeAccount($selected_accounts);
        }
        if($this->security->xss_clean($this->input->post('search'))){
            $param = $this->security->xss_clean($this->input->post('param'));
            //$data = $this->account->search_linked(false,false,$param);
            $data = $this->account->search_carded_linked($param);
            $this->name =  "download linked starterpacks here";
            $search_feedback = "search for ".$param." ". $data['total']." Pin-linked account(s) found";
            //$num_rows = $data['total'];
            $this->active = 'act_sp';

            $num_rows = $this->pagination(count($data));
            //$data['users']            = $num_rows;
            $config['base_url']       = BASEURL.'pafupi/main/view_linked';
            $config['total_rows']     = $num_rows;
            $config['per_page']       = 5;
            $config['num_links']      = 3;
            $config['last_link']      = FALSE;
            $config['first_link']     = FALSE;
            $config['uri_segment']    = 4;
            $config['next_link']      = 'Next';
            $config['next_tag_open']  = "<td class='pagination-item-outer-rght'>";
            $config['next_tag_close'] = "</td'>";
            $config['prev_link']      = 'Previous';
            $config['prev_tag_open']  = "<td class='pagination-item-outer-lft'>";
            $config['prev_tag_close'] = "</td>";
            $config['num_tag_open']   = "<td class='pagination-item'>";
            $config['num_tag_close']  = "</td>";
            $config['cur_tag_open']   = "<td class='pagination-item-current'>";
            $config['cur_tag_close']  = "</td>";

            $this->pagination->initialize($config);
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $data['result']= array_slice($data,$page,$config['per_page']);

            $this->blade->set('accounts',$data['result'])->render('view_linked', array('title' => 'view Pafupi pin-linked Accounts','feedback'=>$feedback,'search_feedback'=>$search_feedback));
            return;

        }
        else{
            $this->name =  "download  starterpacks here";
            //$accounts   =  $this->account->getLinkedAccount($user_id);
            $accounts = $this->account->getCardedLinked();
            $num_rows   =  count($accounts);
            $data["total_rows"] = $num_rows;

            $this->active = 'act_sp';

            //$data['users']            = $num_rows;
            $config['base_url']       = BASEURL.'pafupi/main/view_linked';
            $config['total_rows']     = $num_rows;
            $config['per_page']       = 5;
            $config['num_links']      = 3;
            $config['last_link']      = FALSE;
            $config['first_link']     = FALSE;
            $config['uri_segment']    = 4;
            $config['next_link']      = 'Next';
            $config['next_tag_open']  = "<td class='pagination-item-outer-rght'>";
            $config['next_tag_close'] = "</td'>";
            $config['prev_link']      = 'Previous';
            $config['prev_tag_open']  = "<td class='pagination-item-outer-lft'>";
            $config['prev_tag_close'] = "</td>";
            $config['num_tag_open']   = "<td class='pagination-item'>";
            $config['num_tag_close']  = "</td>";
            $config['cur_tag_open']   = "<td class='pagination-item-current'>";
            $config['cur_tag_close']  = "</td>";

            $this->pagination->initialize($config);
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            //$data['result']     = $this->account->get_all_linked_accounts($config['per_page'],$page);
            $data['result'] = $this->account->get_all_carded_linked($config['per_page'],$page);
            $this->blade->set('accounts',$data['result'])->render('view_linked', array('title' => 'view Pafupi Pin-linked Accounts','feedback'=>$feedback,'search_feedback'=>$search_feedback));
        }
    }
    public function viewDownloadOTP($user_id=false){
        $feedback = false;
        $search_feedback = true;
        $branches = $this->account->getBranches();
        $this->blade->set('branches',$branches);
        $user_id    =  $this->session->userdata('user_id');



        $data = $this->account->get_all_otp_books();
        $num_rows = $this->pagination(count($data));
        $data['users']            = $num_rows;
        $config['base_url']       = BASEURL.'pafupi/main/view_download_otp';
        $config['total_rows']     = $num_rows;
        $config['per_page']       = 5;
        $config['num_links']      = 3;
        $config['last_link']      = FALSE;
        $config['first_link']     = FALSE;
        $config['uri_segment']    = 4;
        $config['next_link']      = 'Next';
        $config['next_tag_open']  = "<td class='pagination-item-outer-rght'>";
        $config['next_tag_close'] = "</td'>";
        $config['prev_link']      = 'Previous';
        $config['prev_tag_open']  = "<td class='pagination-item-outer-lft'>";
        $config['prev_tag_close'] = "</td>";
        $config['num_tag_open']   = "<td class='pagination-item'>";
        $config['num_tag_close']  = "</td>";
        $config['cur_tag_open']   = "<td class='pagination-item-current'>";
        $config['cur_tag_close']  = "</td>";

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data['result']= array_slice($data,$page,$config['per_page']);
        //$data['result']     = $this->account->get_all_prelinked_accounts($config['per_page'],$page);
        $this->name =  "Download OTP Books here";
        //$search_feedback = "search for ".$param." ". $data['total']." Pin-linked account(s) found";
        //$num_rows = $data['total'];
        $this->active = 'act_down';
        $this->blade->set('books',$data['result'])->render('download_otp', array('title' => 'Download Pins','feedback'=>$feedback,'search_feedback'=>$search_feedback));




    }
    public function viewDownloadCard($user_id=false){
        $feedback = false;
        $search_feedback = true;
        //$branches = $this->account->getBranches();
        //$this->blade->set('branches',$branches);
        $user_id    =  $this->session->userdata('user_id');
        $data = $this->account->getDummyCard();
        $this->name =  "Download ATM cards here";
        //$search_feedback = "search for ".$param." ". $data['total']." Pin-linked account(s) found";
        //$num_rows = $data['total'];
        $this->active = 'act_down';
        $num_rows = $this->pagination(count($data));
        //$data['users']            = $num_rows;
        $config['base_url']       = BASEURL.'pafupi/main/view_download_card';
        $config['total_rows']     = $num_rows;
        $config['per_page']       = 5;
        $config['num_links']      = 3;
        $config['last_link']      = FALSE;
        $config['first_link']     = FALSE;
        $config['uri_segment']    = 4;
        $config['next_link']      = 'Next';
        $config['next_tag_open']  = "<td class='pagination-item-outer-rght'>";
        $config['next_tag_close'] = "</td'>";
        $config['prev_link']      = 'Previous';
        $config['prev_tag_open']  = "<td class='pagination-item-outer-lft'>";
        $config['prev_tag_close'] = "</td>";
        $config['num_tag_open']   = "<td class='pagination-item'>";
        $config['num_tag_close']  = "</td>";
        $config['cur_tag_open']   = "<td class='pagination-item-current'>";
        $config['cur_tag_close']  = "</td>";

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data['result']= array_slice($data,$page,$config['per_page']);
        $branches = $this->account->getBranches();
        $this->blade->set('accounts',$data['result'])->set('branches',$branches)->render('download_card', array('title' => 'Download Cards','feedback'=>$feedback,'search_feedback'=>$search_feedback));




    }
    public function viewDownloadReferal($user_id=false){
        $feedback = false;
        $search_feedback = true;
        $branches = $this->account->getBranches();
        $this->blade->set('branches',$branches);
        $user_id    =  $this->session->userdata('user_id');



        $data = $this->account->get_all_referal_books();
        $this->name =  "Download Referral Books here";
        //$search_feedback = "search for ".$param." ". $data['total']." Pin-linked account(s) found";
        //$num_rows = $data['total'];
        $this->active = 'act_down';

        $num_rows = $this->pagination(count($data));
        //$data['users']            = $num_rows;
        $config['base_url']       = BASEURL.'pafupi/main/view_download_referal';
        $config['total_rows']     = $num_rows;
        $config['per_page']       = 5;
        $config['num_links']      = 3;
        $config['last_link']      = FALSE;
        $config['first_link']     = FALSE;
        $config['uri_segment']    = 4;
        $config['next_link']      = 'Next';
        $config['next_tag_open']  = "<td class='pagination-item-outer-rght'>";
        $config['next_tag_close'] = "</td'>";
        $config['prev_link']      = 'Previous';
        $config['prev_tag_open']  = "<td class='pagination-item-outer-lft'>";
        $config['prev_tag_close'] = "</td>";
        $config['num_tag_open']   = "<td class='pagination-item'>";
        $config['num_tag_close']  = "</td>";
        $config['cur_tag_open']   = "<td class='pagination-item-current'>";
        $config['cur_tag_close']  = "</td>";

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data['result']= array_slice($data,$page,$config['per_page']);

        $this->blade->set('referals',$data['result'])->render('download_referal', array('title' => 'Download Referrals','feedback'=>$feedback,'search_feedback'=>$search_feedback));




    }
    public function viewCarded($user_id=false){
        $feedback = false;
        $search_feedback =false;
        if($this->security->xss_clean($this->input->post('delete'))){
            $selected_accounts = $this->security->xss_clean($this->input->post('selected_accounts'));

            $feedback = $this->account->removeAccount($selected_accounts);
        }
        if($this->security->xss_clean($this->input->post('search'))){
            $param = $this->security->xss_clean($this->input->post('param'));
            $data = $this->account->search_carded($param);
            $this->name =  "view carded accounts";
            $search_feedback =  "search for ".$param." ".$data['total']." Carded accounts found";

            $num_rows = $this->pagination(count($data));
            //$data['users']            = $num_rows;
            $config['base_url']       = BASEURL.'pafupi/main/view_carded';
            $config['total_rows']     = $num_rows;
            $config['per_page']       = 5;
            $config['num_links']      = 3;
            $config['last_link']      = FALSE;
            $config['first_link']     = FALSE;
            $config['uri_segment']    = 4;
            $config['next_link']      = 'Next';
            $config['next_tag_open']  = "<td class='pagination-item-outer-rght'>";
            $config['next_tag_close'] = "</td'>";
            $config['prev_link']      = 'Previous';
            $config['prev_tag_open']  = "<td class='pagination-item-outer-lft'>";
            $config['prev_tag_close'] = "</td>";
            $config['num_tag_open']   = "<td class='pagination-item'>";
            $config['num_tag_close']  = "</td>";
            $config['cur_tag_open']   = "<td class='pagination-item-current'>";
            $config['cur_tag_close']  = "</td>";

            $this->pagination->initialize($config);
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $data['result']= array_slice($data,$page,$config['per_page']);



            $this->blade->set('accounts',$data['result'])->render('view_carded', array('title' => 'view Pafupi Carded Accounts','feedback'=>$feedback,'search_feedback'=>$search_feedback));
            return;
        }
        $this->name =  "view carded accounts";
        $user_id    =  $this->session->userdata('user_id');
        $accounts   =  $this->account->getCardAccounts($user_id);
        $num_rows   =  count($accounts);
        $data["total_rows"] = $num_rows;

        $this->active = 'act_account';

        //$data['users']            = $num_rows;
        $config['base_url']       = BASEURL.'pafupi/main/view_carded';
        $config['total_rows']     = $num_rows;
        $config['per_page']       = 5;
        $config['num_links']      = 3;
        $config['last_link']      = FALSE;
        $config['first_link']     = FALSE;
        $config['uri_segment']    = 4;
        $config['next_link']      = 'Next';
        $config['next_tag_open']  = "<td class='pagination-item-outer-rght'>";
        $config['next_tag_close'] = "</td'>";
        $config['prev_link']      = 'Previous';
        $config['prev_tag_open']  = "<td class='pagination-item-outer-lft'>";
        $config['prev_tag_close'] = "</td>";
        $config['num_tag_open']   = "<td class='pagination-item'>";
        $config['num_tag_close']  = "</td>";
        $config['cur_tag_open']   = "<td class='pagination-item-current'>";
        $config['cur_tag_close']  = "</td>";

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data['result']     = $this->account->get_all_carded_accounts($config['per_page'],$page);

        $this->blade->set('accounts',$data['result'])->render('view_carded', array('title' => 'view Pafupi Carded Accounts','feedback'=>$feedback,'search_feedback'=>$search_feedback));
    }
    public function viewWithCardAccounts($user_id=false){
        //$user_id = '05050';
        $this->name = 'view accounts with cards';
        $user_id = $this->session->userdata('user_id');
        $accounts = $this->account->getWithCardAccounts($user_id);
        //var_dump($accounts);
        $this->blade->set('accounts',$accounts)->render('view_real_card', array('title' => 'view Pafupi Accounts with Card'));
    }
    public function viewCardAccounts($user_id=false){
        //$user_id = '05050';
        $this->name = 'view accounts with card accounts';
        $user_id = $this->session->userdata('user_id');
        $accounts = $this->account->getCardAccounts($user_id);

        $num_rows = $this->pagination(count($accounts));
        //$data['users']            = $num_rows;
        $config['base_url']       = BASEURL.'pafupi/main/view_carded';
        $config['total_rows']     = $num_rows;
        $config['per_page']       = 5;
        $config['num_links']      = 3;
        $config['last_link']      = FALSE;
        $config['first_link']     = FALSE;
        $config['uri_segment']    = 4;
        $config['next_link']      = 'Next';
        $config['next_tag_open']  = "<td class='pagination-item-outer-rght'>";
        $config['next_tag_close'] = "</td'>";
        $config['prev_link']      = 'Previous';
        $config['prev_tag_open']  = "<td class='pagination-item-outer-lft'>";
        $config['prev_tag_close'] = "</td>";
        $config['num_tag_open']   = "<td class='pagination-item'>";
        $config['num_tag_close']  = "</td>";
        $config['cur_tag_open']   = "<td class='pagination-item-current'>";
        $config['cur_tag_close']  = "</td>";

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data['result']= array_slice($data,$page,$config['per_page']);


        $this->blade->set('accounts',$accounts)->render('view_cardaccounts', array('title' => 'view Pafupi Accounts with Card data'));
    }
    public function createCardExcel($user_id=false){

        $user_id = $this->session->userdata('user_id');
        $accounts = $this->account->getCardAccounts($user_id);

        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Pre-linked account');
        //set auto column width
        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        //set headers
        $this->excel->getActiveSheet()->setCellValue('A1', 'Account No');
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('B1', 'Card No');
        $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('C1', 'Branch');
        $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('D1', 'Date created');
        $this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
        //change the font size
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(12);
        $cell = 2;
        foreach($accounts as $account){
            //$this->excel->getActiveSheet()->setCellValue('A'.$cell, $account['acc_no']);
            $this->excel->getActiveSheet()->getCell('A'.$cell)->setValueExplicit($account['acc_no'], PHPExcel_Cell_DataType::TYPE_STRING);
            //$this->excel->getActiveSheet()->setCellValue('B'.$cell, $account['card_no']);
            $this->excel->getActiveSheet()->getCell('B'.$cell)->setValueExplicit($account['card_no'], PHPExcel_Cell_DataType::TYPE_STRING);
            //$this->excel->getActiveSheet()->setCellValue('C'.$cell, $account['branch_name']);
            $this->excel->getActiveSheet()->getCell('C'.$cell)->setValueExplicit($account['branch_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            $this->excel->getActiveSheet()->setCellValue('D'.$cell, date('d-m-Y',strtotime($account['date_created'])));
            //$this->excel->getActiveSheet()->getCell('D'.$cell)->setValueExplicit(date('d-m-Y',strtotime($account['date_created'])), PHPExcel_Cell_DataType::TYPE_STRING);
            $cell++;
        }

        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);


        $filename='pre-card-accounts_'.$user_id.'.xls';  //file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('files/card_accounts/'.$filename);
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }
    public function createWithCardExcel($user_id=false){
        //$user_id = '05050';
        $user_id = $this->session->userdata('user_id');
        $name = $this->session->userdata('fullname');
        $accounts = $this->account->getWithCardAccounts($user_id);
        $this->excel->getProperties()->setCreator($name)
            ->setLastModifiedBy($this->session->userdata('fullname'))
            ->setTitle("Pre-linked account")
            ->setSubject("Prelinked account")
            ->setDescription("view prelinked accounts by ".$name)
            ->setKeywords("office 2007 openxml php")
            ->setCategory("accounts");
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Pre-linked account');
        //set auto column width
        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        //set headers
        $this->excel->getActiveSheet()->setCellValue('A1', 'Starterpack ID');
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('B1', 'Account No');
        $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('C1', 'Account status');
        $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('D1', 'Card No');
        $this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('E1', 'Branch');
        $this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('F1', 'Date created');
        $this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('G1', 'Opened by');
        $this->excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
        //change the font size
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(12);
        $cell = 2;
        foreach($accounts as $account){
            //$this->excel->getActiveSheet()->setCellValue('A'.$cell, $account['starter_id']);
            $this->excel->getActiveSheet()->getCell('A'.$cell)->setValueExplicit($account['starter_id'], PHPExcel_Cell_DataType::TYPE_STRING);
            //$this->excel->getActiveSheet()->setCellValue('B'.$cell, $account['acc_no']);
            $this->excel->getActiveSheet()->getCell('B'.$cell)->setValueExplicit($account['acc_no'], PHPExcel_Cell_DataType::TYPE_STRING);
            //$this->excel->getActiveSheet()->setCellValue('C'.$cell, $account['acc_status']);
            $this->excel->getActiveSheet()->getCell('C'.$cell)->setValueExplicit($account['acc_status'], PHPExcel_Cell_DataType::TYPE_STRING);
            //$this->excel->getActiveSheet()->setCellValue('D'.$cell, $account['card_no']);
            $this->excel->getActiveSheet()->getCell('D'.$cell)->setValueExplicit($account['card_no'], PHPExcel_Cell_DataType::TYPE_STRING);
            //$this->excel->getActiveSheet()->setCellValue('E'.$cell, $account['branch_name']);
            $this->excel->getActiveSheet()->getCell('E'.$cell)->setValueExplicit($account['branch_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            //$this->excel->getActiveSheet()->setCellValue('F'.$cell, date('d-m-Y',strtotime($account['date_created'])));
            $this->excel->getActiveSheet()->getCell('F'.$cell)->setValueExplicit(date('d-m-Y',strtotime($account['date_created'])), PHPExcel_Cell_DataType::TYPE_STRING);
            //$this->excel->getActiveSheet()->setCellValue('G'.$cell, $account['opened_by']);
            $this->excel->getActiveSheet()->getCell('G'.$cell)->setValueExplicit($account['opened_by'], PHPExcel_Cell_DataType::TYPE_STRING);
            $cell++;
        }
        //make the font become bold
        //$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        //merge cell A1 until D1
        //$this->excel->getActiveSheet()->mergeCells('A1:D1');
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);


        $filename='with-card-accounts_'.$user_id.'.xls';  //file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('files/card_accounts/'.$filename);
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }
    public function createExcel($user_id=false){

        $user_id = $this->session->userdata('user_id');
        $accounts = $this->account->getPreAccount($user_id);
        $name = $this->session->userdata('fullname');
        //$accounts = $this->account->getWithCardAccounts($user_id);
        $this->excel->getProperties()->setCreator($name)
            ->setLastModifiedBy($this->session->userdata('fullname'))
            ->setTitle("Pre-linked account")
            ->setSubject("Prelinked account")
            ->setDescription("view prelinked accounts by ".$name)
            ->setKeywords("office 2007 openxml php")
            ->setCategory("accounts");
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Pre-linked account');
        //set auto column width
        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        //set headers
        $this->excel->getActiveSheet()->setCellValue('A1', 'Account No');
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('B1', 'Branch');
        $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('C1', 'Date created');
        $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('D1', 'Created By');
        $this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
        //change the font size
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(12);
        $cell = 2;
        foreach($accounts as $account){
            //$this->excel->getActiveSheet()->setCellValue('A'.$cell, $account['acc_no']);
            $this->excel->getActiveSheet()->getCell('A'.$cell)->setValueExplicit($account['acc_no'], PHPExcel_Cell_DataType::TYPE_STRING);
            //$this->excel->getActiveSheet()->setCellValue('B'.$cell, $account['branch_name']);
            $this->excel->getActiveSheet()->getCell('B'.$cell)->setValueExplicit($account['branch_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            //$this->excel->getActiveSheet()->setCellValue('C'.$cell, date('d-m-Y',strtotime($account['date_created'])));
            $this->excel->getActiveSheet()->getCell('C'.$cell)->setValueExplicit(date('d-m-Y',strtotime($account['date_created'])), PHPExcel_Cell_DataType::TYPE_STRING);
            //$this->excel->getActiveSheet()->setCellValue('C'.$cell, date('d-m-Y',strtotime($account['date_created'])));
            $this->excel->getActiveSheet()->getCell('C'.$cell)->setValueExplicit($account['opened_by'], PHPExcel_Cell_DataType::TYPE_STRING);
            $cell++;
        }

        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);


        $filename='pre-linked-accounts_'.$user_id.'.xls';  //file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('files/prelinked_accounts/'.$filename);
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }
    public function createPinsExcel($user_id=false){
        $pins =$this->account->getBookPins();

        $user_id = $this->session->userdata('user_id');

        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Pre-linked account');
        //set auto column width
        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);

        //set headers
        $this->excel->getActiveSheet()->setCellValue('A1', 'Book No');
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('B1', 'OTP');
        $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
        //change the font size
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(12);
        $cell = 2;
        foreach($pins as $pin){
            //$this->excel->getActiveSheet()->setCellValue('A'.$cell, $account['acc_no']);
            $this->excel->getActiveSheet()->getCell('A'.$cell)->setValueExplicit($pin['book_no'], PHPExcel_Cell_DataType::TYPE_STRING);
            //$this->excel->getActiveSheet()->setCellValue('B'.$cell, $account['branch_name']);
            $this->excel->getActiveSheet()->getCell('B'.$cell)->setValueExplicit($pin['trans_pin'], PHPExcel_Cell_DataType::TYPE_STRING);

            $cell++;
        }

        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);


        $filename='bookpins_'.$user_id.'.xls';  //file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('files/book_pins/'.$filename);
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }
    public function uploadCardFile(){
        if($_FILES){
            //$user_id = '05050';
            $user_id = $this->session->userdata('user_id');
            $name = $_FILES['userfile']['name'];
            $this->current_page = 'upload_file';
            $name = strtr($name, 'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ', 'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');

            // replace characters other than letters, numbers and . by _
            $name = preg_replace('/([^.a-z0-9]+)/i', '_', $name);
            $config['upload_path'] = 'files/card_fin/';
            $config['allowed_types'] = 'xls|xlsx';
            $config['max_size']	= '10000';
            $config['file_name'] = $name;
            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload())
            {
                $error = array('error' => $this->upload->display_errors('',''));
                //insert into upload table
                $dat = array('upload_name' => $name, 'uploaded_by' => $user_id, 'error_type' => 'error uploading');
                $uploaded = $this->account->saveUploadData($dat);
                echo json_encode(array($error));
            }
            else
            {
                $d = array();
                $data = $this->upload->data();
                $name = $data['file_name'];
                $this->current_page = 'upload_file';
                //insert into upload table
                $dat = array('upload_name' => $name, 'uploaded_by' => $user_id, 'error_type' => 'success');
                $uploaded = $this->account->saveUploadData($dat);
                $feedback = $this->readCardExcel($name);
                $info = new stdClass();
                $info->name = $name;
                $info->size = $data['file_size'];
                $info->type = $data['file_type'];
                $info->deleteUrl = "";
                $info->url = 'files/card_fin/'.$name;
                array_push($d,$info);
                array_push($d,$feedback);
                echo json_encode($d);

            }

        }
        else{
            $this->name = 'upload fincard excel document';
            $this->current_page = 'upload_file';
            $this->blade->render('upload_fincard', array('title' => 'upload file'));
        }
    }
    public function readCardExcel($filename=false){
        $objReader = PHPExcel_IOFactory::createReader('Excel5');
        $objReader->setReadDataOnly(true);
        $data = array();
        $errors = array(); $errors[0]['error'] = " no errors" ;
        $filename = $filename?$filename:"card_numbers.xls";
        try{
            $objPHPExcel = $objReader->load("files/card_fin/".$filename);
        }
        catch(PHPExcel_Reader_Exception $e) {
            $errors[0]['error'] =  $e->getMessage().'-->Error reading file: ';

            array_push($data,$errors);
            return $data;
        }
        $objWorksheet = $objPHPExcel->getActiveSheet();
        $num = 0;
        //echo '<table>' . "\n";
        foreach ($objWorksheet->getRowIterator() as $row) {
            //echo '<tr>' . "\n";
            try{
                if($row->getRowIndex() == 1) continue;
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(false);

                foreach ($cellIterator as $cell) {
                    try{
                        if($cell->getColumn() == 'A')
                            $data[$num]['acc_no'] = $cell->getValue();
                        if($cell->getColumn() == 'B')
                            $data[$num]['card_no'] = $cell->getValue();
                    }
                    catch(PHPExcel_Reader_Exception $e) {
                        $errors[$num][$cell->getColumn()]['cell'] = $e->getMessage().'-->Error reading cell: '.$cell->getCoordinate();
                    }

                }
                $num++;
            }
            catch(PHPExcel_Reader_Exception $e) {
                $errors[$num]['row'] = $e->getMessage().'-->Error reading row: '.$cell->getRow();
            }
            // echo '</tr>' . "\n";
        }
        //echo '</table>' . "\n";
        $errors[$num++]['database'] = $this->account->saveCardNumbers($data);
        array_push($data,$errors);
        return $data;
        //var_dump($data);
    }
    public function uploadManuFile(){
        if($_FILES){
            $user_id = $this->session->userdata('user_id');
            $name = $_FILES['userfile']['name'];

            $name = strtr($name, 'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ', 'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');

            // replace characters other than letters, numbers and . by _
            $name = preg_replace('/([^.a-z0-9]+)/i', '_', $name);
            $config['upload_path'] = 'files/card_man/';
            $config['allowed_types'] = 'xls|xlsx';
            $config['max_size']	= '10000';
            $config['file_name'] = $name;
            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload())
            {
                $error = array('error' => $this->upload->display_errors('',''));
                //insert into upload table
                $dat = array('upload_name' => $name, 'uploaded_by' => $user_id, 'error_type' => 'error uploading file');
                $uploaded = $this->account->saveUploadData($dat);
                echo json_encode(array($error));
            }
            else
            {
                $d = array();
                $data = $this->upload->data();
                $name = $data['file_name'];
                $feedback = $this->readManuExcel($name);
                //insert into upload table
                $dat = array('upload_name' => $name, 'uploaded_by' => $user_id, 'error_type' => 'success');
                $uploaded = $this->account->saveUploadData($dat);
                $info = new stdClass();
                $info->name = $name;
                $info->size = $data['file_size'];
                $info->type = $data['file_type'];
                $info->deleteUrl = "";
                $info->url = 'files/card_man/'.$name;
                array_push($d,$info);
                array_push($d,$feedback);
                echo json_encode($d);

            }

        }
        else{
            $this->current_page = 'upload_file';
            $this->name = 'upload excel from manufacture';
            $this->blade->render('upload_manu', array('title' => 'upload file'));
        }
    }
    public function readManuExcel($filename=false){
        $objReader = PHPExcel_IOFactory::createReader('Excel5');
        $objReader->setReadDataOnly(true);
        $data = array();
        $errors = array(); $errors[0]['error'] = "errors" ;
        $toSave = array();
        try{
            $filename = $filename?$filename:"card_numbers.xls";
            $objPHPExcel = $objReader->load("files/card_man/".$filename);
        }
        catch(PHPExcel_Reader_Exception $e) {
            $errors[0]['error'] =  $e->getMessage().'-->Error reading file: ';
            array_push($data,$errors);
            return $data;
        }
        $objWorksheet = $objPHPExcel->getActiveSheet();
        $num = 0;
        //echo '<table>' . "\n";
        foreach ($objWorksheet->getRowIterator() as $row) {
            //echo '<tr>' . "\n";
            try{
                if($row->getRowIndex() == 1) continue;
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(false);

                foreach ($cellIterator as $cell) {
                    try{

                        if($cell->getColumn() == 'A'){
                            $data[$num]['acc_no'] = $cell->getValue();
                            $toSave[$num]['acc_no'] = $cell->getValue();
                        }
                        if($cell->getColumn() == 'B'){
                            $data[$num]['card_no'] = $cell->getValue();
                        }
                        if($cell->getColumn() == 'C'){
                            $data[$num]['starter_id'] = $cell->getValue();
                            $toSave[$num]['starter_id'] = $cell->getValue();
                        }
                    }
                    catch(PHPExcel_Reader_Exception $e) {
                        $errors[$num][$cell->getColumn()]['cell'] = $e->getMessage().'-->Error reading cell: '.$cell->getCoordinate();
                    }

                }
                $num++;
            }
            catch(PHPExcel_Reader_Exception $e) {
                $errors[$num]['row'] = $e->getMessage().'-->Error reading row: '.$cell->getRow();
            }
            // echo '</tr>' . "\n";
        }
        //echo '</table>' . "\n";
        $errors[$num++]['database'] = $this->account->saveStarterIds($toSave);
        array_push($data,$errors);
        return $data;
    }
    public function lockAccount(){
        if($this->session->userdata('role_id')==1){
            $feedback = false;
            $search_feedback =false;
            if($this->security->xss_clean($this->input->post('lock'))){

                $done =  $this->account->lockAccount($this->security->xss_clean($this->input->post('selected_accounts')));
                $feedback = $done;
            }
            elseif($this->security->xss_clean($this->input->post('delete'))){
                $selected_accounts = $this->security->xss_clean($this->input->post('selected_accounts'));

                $feedback = $this->account->removeAccount($selected_accounts);
            }
            if($this->security->xss_clean($this->input->post('search'))){
                $param = $this->security->xss_clean($this->input->post('param'));
                $data = $this->account->search_unlocked($param);
                $this->name =  "lock starterpacks from being fully opened";
                $this->active = 'act_inventory';
                $search_feedback = "search for ".$param." ".$data['total']." accounts ready to lock found";

                $num_rows = $this->pagination(count($data));
                //$data['users']            = $num_rows;
                $config['base_url']       = BASEURL.'pafupi/main/lock_account';
                $config['total_rows']     = $num_rows;
                $config['per_page']       = 5;
                $config['num_links']      = 3;
                $config['last_link']      = FALSE;
                $config['first_link']     = FALSE;
                $config['uri_segment']    = 4;
                $config['next_link']      = 'Next';
                $config['next_tag_open']  = "<td class='pagination-item-outer-rght'>";
                $config['next_tag_close'] = "</td'>";
                $config['prev_link']      = 'Previous';
                $config['prev_tag_open']  = "<td class='pagination-item-outer-lft'>";
                $config['prev_tag_close'] = "</td>";
                $config['num_tag_open']   = "<td class='pagination-item'>";
                $config['num_tag_close']  = "</td>";
                $config['cur_tag_open']   = "<td class='pagination-item-current'>";
                $config['cur_tag_close']  = "</td>";

                $this->pagination->initialize($config);
                $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
                $data['result']= array_slice($data,$page,$config['per_page']);



                $this->blade->set('accounts',$data['result'])->render('lock_accounts', array('title' => 'lock Pafupi Accounts','feedback'=>$feedback,'search_feedback'=>$search_feedback));
                return;

            }

            $this->name ="lock accounts";
            $this->active = 'act_inventory';
            $accounts = $this->account->getNonLockedAccounts();
            $branches = $this->account->getBranches();
            $num_rows   =  $this->pagination(count($accounts));
            $data["total_rows"] = $num_rows;

            //$data['users']            = $num_rows;
            $config['base_url']       = BASEURL.'pafupi/main/lock_account';
            $config['total_rows']     = $num_rows;
            $config['per_page']       = 5;
            $config['num_links']      = 3;
            $config['last_link']      = FALSE;
            $config['first_link']     = FALSE;
            $config['uri_segment']    = 4;
            $config['next_link']      = 'Next';
            $config['next_tag_open']  = "<td class='pagination-item-outer-rght'>";
            $config['next_tag_close'] = "</td'>";
            $config['prev_link']      = 'Previous';
            $config['prev_tag_open']  = "<td class='pagination-item-outer-lft'>";
            $config['prev_tag_close'] = "</td>";
            $config['num_tag_open']   = "<td class='pagination-item'>";
            $config['num_tag_close']  = "</td>";
            $config['cur_tag_open']   = "<td class='pagination-item-current'>";
            $config['cur_tag_close']  = "</td>";

            $this->pagination->initialize($config);
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $data['result']     = $this->account->get_all_nonlocked_accounts($config['per_page'],$page);

            $this->blade->set('accounts',$data['result'])->set('branches',$branches)->render('lock_accounts', array('title' => 'lock Pafupi Accounts','feedback'=>$feedback,'search_feedback'=>$search_feedback));

        }
        else{
            pafupi::index();
        }
    }
    public function unlockAccount(){
        if($this->session->userdata('role_id')==1){
            $feedback = false;
            $search_feedback = false;
            if($this->security->xss_clean($this->input->post('unlock'))){

                $done =  $this->account->unlockAccount($this->security->xss_clean($this->input->post('selected_accounts')));
                $feedback = $done;
            }
            elseif($this->security->xss_clean($this->input->post('delete'))){
                $selected_accounts = $this->security->xss_clean($this->input->post('selected_accounts'));

                $feedback = $this->account->removeAccount($selected_accounts);
            }
            if($this->security->xss_clean($this->input->post('search'))){
                $param = $this->security->xss_clean($this->input->post('param'));
                $data = $this->account->search_locked($param);
                $this->name =  "unlock accounts";
                $this->active = 'act_inventory';
                $search_feedback =  "search for ".$param." ".$data['total']." accounts ready to unlock found";

                $num_rows = $this->pagination(count($data));
                //$data['users']            = $num_rows;
                $config['base_url']       = BASEURL.'pafupi/main/unlock_account';
                $config['total_rows']     = $num_rows;
                $config['per_page']       = 5;
                $config['num_links']      = 3;
                $config['last_link']      = FALSE;
                $config['first_link']     = FALSE;
                $config['uri_segment']    = 4;
                $config['next_link']      = 'Next';
                $config['next_tag_open']  = "<td class='pagination-item-outer-rght'>";
                $config['next_tag_close'] = "</td'>";
                $config['prev_link']      = 'Previous';
                $config['prev_tag_open']  = "<td class='pagination-item-outer-lft'>";
                $config['prev_tag_close'] = "</td>";
                $config['num_tag_open']   = "<td class='pagination-item'>";
                $config['num_tag_close']  = "</td>";
                $config['cur_tag_open']   = "<td class='pagination-item-current'>";
                $config['cur_tag_close']  = "</td>";

                $this->pagination->initialize($config);
                $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
                $data['result']= array_slice($data,$page,$config['per_page']);



                $this->blade->set('accounts',$data['result'])->render('unlock_accounts', array('title' => 'unlock Pafupi Accounts','feedback'=>$feedback,'search_feedback'=>$search_feedback));
                return;

            }
            $this->name ="unlock accounts";
            $this->active = 'act_inventory';
            $accounts = $this->account->getLockedAccounts();
            $branches = $this->account->getBranches();
            $num_rows   =  $this->pagination(count($accounts));
            $data["total_rows"] = $num_rows;



            //$data['users']            = $num_rows;
            $config['base_url']       = BASEURL.'pafupi/main/unlock_account';
            $config['total_rows']     = $num_rows;
            $config['per_page']       = 5;
            $config['num_links']      = 3;
            $config['last_link']      = FALSE;
            $config['first_link']     = FALSE;
            $config['uri_segment']    = 4;
            $config['next_link']      = 'Next';
            $config['next_tag_open']  = "<td class='pagination-item-outer-rght'>";
            $config['next_tag_close'] = "</td'>";
            $config['prev_link']      = 'Previous';
            $config['prev_tag_open']  = "<td class='pagination-item-outer-lft'>";
            $config['prev_tag_close'] = "</td>";
            $config['num_tag_open']   = "<td class='pagination-item'>";
            $config['num_tag_close']  = "</td>";
            $config['cur_tag_open']   = "<td class='pagination-item-current'>";
            $config['cur_tag_close']  = "</td>";

            $this->pagination->initialize($config);
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $data['result']     = $this->account->get_all_locked_accounts($config['per_page'],$page);
            $this->blade->set('accounts',$data['result'])->set('branches',$branches)->render('unlock_accounts', array('title' => 'unlock Pafupi Accounts','feedback'=>$feedback,'search_feedback'=>$search_feedback));

        }
        else{
            pafupi::index();
        }
    }
    public function viewDispatched(){
        $feedback =false;
        $search_feedback =false;
        if($this->session->userdata('role_id')==1){

            if($this->security->xss_clean($this->input->post('search'))){
                $param = $this->security->xss_clean($this->input->post('param'));
                $data = $this->account->search_dispatched($param);
                $this->name ="dispatched accounts";
                $this->active = 'act_inventory';
                $search_feedback =  "search for ".$param." ".$data['total']." accounts  found";

                $num_rows = $this->pagination(count($data));
                //$data['users']            = $num_rows;
                $config['base_url']       = BASEURL.'pafupi/main/dispatched_account';
                $config['total_rows']     = $num_rows;
                $config['per_page']       = 5;
                $config['num_links']      = 3;
                $config['last_link']      = FALSE;
                $config['first_link']     = FALSE;
                $config['uri_segment']    = 4;
                $config['next_link']      = 'Next';
                $config['next_tag_open']  = "<td class='pagination-item-outer-rght'>";
                $config['next_tag_close'] = "</td'>";
                $config['prev_link']      = 'Previous';
                $config['prev_tag_open']  = "<td class='pagination-item-outer-lft'>";
                $config['prev_tag_close'] = "</td>";
                $config['num_tag_open']   = "<td class='pagination-item'>";
                $config['num_tag_close']  = "</td>";
                $config['cur_tag_open']   = "<td class='pagination-item-current'>";
                $config['cur_tag_close']  = "</td>";

                $this->pagination->initialize($config);
                $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
                $data['result']= array_slice($data,$page,$config['per_page']);


                $this->blade->set('accounts',$data['result'])->render('dispatched_accounts', array('title' => 'Dispatched Accounts','feedback'=>$feedback,'search_feedback'=>$search_feedback));
                return;

            }




            $this->name ="dispatched accounts";
            $this->active = 'act_inventory';
            $accounts = $this->account->getDispatchedAccounts();
            $branches = $this->account->getBranches();
            $num_rows   =  $this->pagination(count($accounts));
            $data["total_rows"] = $num_rows;


            //$data['users']            = $num_rows;
            $config['base_url']       = BASEURL.'pafupi/main/dispatched_account';
            $config['total_rows']     = $num_rows;
            $config['per_page']       = 5;
            $config['num_links']      = 3;
            $config['last_link']      = FALSE;
            $config['first_link']     = FALSE;
            $config['uri_segment']    = 4;
            $config['next_link']      = 'Next';
            $config['next_tag_open']  = "<td class='pagination-item-outer-rght'>";
            $config['next_tag_close'] = "</td'>";
            $config['prev_link']      = 'Previous';
            $config['prev_tag_open']  = "<td class='pagination-item-outer-lft'>";
            $config['prev_tag_close'] = "</td>";
            $config['num_tag_open']   = "<td class='pagination-item'>";
            $config['num_tag_close']  = "</td>";
            $config['cur_tag_open']   = "<td class='pagination-item-current'>";
            $config['cur_tag_close']  = "</td>";

            $this->pagination->initialize($config);
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $data['result']     = $this->account->get_all_dispatched_accounts($config['per_page'],$page);
            $this->blade->set('accounts',$data['result'])->set('branches',$branches)->render('dispatched_accounts', array('title' => 'dispatched Accounts','feedback'=>$feedback,'search_feedback'=>$search_feedback));

        }
        else{
            pafupi::index();
        }

    }
    /*public function viewStarterpacks(){
        if($this->session->userdata('role_id')==1){
            $this->name ="dispatched accounts";
            $this->active = 'act_inventory';
            $accounts = $this->account->getStarterPacks();
            $branches = $this->account->getBranches();
            $num_rows   =  $this->pagination(count($accounts));
            $data["total_rows"] = $num_rows;


            $data['users']            = $num_rows;
            $config['base_url']       = BASEURL.'pafupi/main/manage_starterpacks';
            $config['total_rows']     = $num_rows;
            $config['per_page']       = 3;
            $config['num_links']      = 3;
            $config['last_link']      = FALSE;
            $config['first_link']     = FALSE;
            $config['uri_segment']    = 4;
            $config['next_link']      = 'Next';
            $config['next_tag_open']  = "<td class='pagination-item-outer-rght'>";
            $config['next_tag_close'] = "</td'>";
            $config['prev_link']      = 'Previous';
            $config['prev_tag_open']  = "<td class='pagination-item-outer-lft'>";
            $config['prev_tag_close'] = "</td>";
            $config['num_tag_open']   = "<td class='pagination-item'>";
            $config['num_tag_close']  = "</td>";
            $config['cur_tag_open']   = "<td class='pagination-item-current'>";
            $config['cur_tag_close']  = "</td>";

            $this->pagination->initialize($config);
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $data['result']     = $this->account->get_all_starterpack_accounts($config['per_page'],$page);
            $this->blade->set('accounts',$data['result'])->set('branches',$branches)->render('starterpack_accounts', array('title' => 'dispatch Pafupi Accounts'));

        }
        else{
            pafupi::index();
        }

    }*/
    public function dispatchAccount(){
        if($this->session->userdata('role_id')==1){
            $feedback = false;
            $search_feedback = false;
            if($this->security->xss_clean($this->input->post('dispatch'))){

                $done =  $this->account->dispatchAccount($this->security->xss_clean($this->input->post('selected_accounts')));

                $feedback = $done;


            }
            elseif($this->security->xss_clean($this->input->post('delete'))){
                $selected_accounts = $this->security->xss_clean($this->input->post('selected_accounts'));

                $feedback = $this->account->removeAccount($selected_accounts);
            }
            if($this->security->xss_clean($this->input->post('search'))){
                $param = $this->security->xss_clean($this->input->post('param'));
                $data = $this->account->search_undispatched($param);
                $this->name =  "dispatch packaged starterpacks to branches";
                $this->active = 'act_inventory';
                $search_feedback =  "search for ".$param." ".$data['total']." accounts ready to dispatch found";


                $num_rows = $this->pagination(count($data));
                //$data['users']            = $num_rows;
                $config['base_url']       = BASEURL.'pafupi/main/dispatch_account';
                $config['total_rows']     = $num_rows;
                $config['per_page']       = 5;
                $config['num_links']      = 3;
                $config['last_link']      = FALSE;
                $config['first_link']     = FALSE;
                $config['uri_segment']    = 4;
                $config['next_link']      = 'Next';
                $config['next_tag_open']  = "<td class='pagination-item-outer-rght'>";
                $config['next_tag_close'] = "</td'>";
                $config['prev_link']      = 'Previous';
                $config['prev_tag_open']  = "<td class='pagination-item-outer-lft'>";
                $config['prev_tag_close'] = "</td>";
                $config['num_tag_open']   = "<td class='pagination-item'>";
                $config['num_tag_close']  = "</td>";
                $config['cur_tag_open']   = "<td class='pagination-item-current'>";
                $config['cur_tag_close']  = "</td>";

                $this->pagination->initialize($config);
                $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
                $data['result']= array_slice($data,$page,$config['per_page']);



                $this->blade->set('accounts',$data['result'])->render('dispatch_accounts', array('title' => 'dispatch Pafupi Accounts','feedback'=>$feedback,'search_feedback'=>$search_feedback));
                return;

            }
            $this->name =  "dispatch packaged starterpacks to branches";
            $this->active = 'act_inventory';
            $accounts = $this->account->getNotDispatchedAccounts();
            $branches = $this->account->getBranches();
            $num_rows   =  count($accounts);
            $data["total_rows"] = $num_rows;


           // $data['users']            = $num_rows;
            $config['base_url']       = BASEURL.'pafupi/main/dispatch_account';
            $config['total_rows']     = $num_rows;
            $config['per_page']       = 5;
            $config['num_links']      = 3;
            $config['last_link']      = FALSE;
            $config['first_link']     = FALSE;
            $config['uri_segment']    = 4;
            $config['next_link']      = 'Next';
            $config['next_tag_open']  = "<td class='pagination-item-outer-rght'>";
            $config['next_tag_close'] = "</td'>";
            $config['prev_link']      = 'Previous';
            $config['prev_tag_open']  = "<td class='pagination-item-outer-lft'>";
            $config['prev_tag_close'] = "</td>";
            $config['num_tag_open']   = "<td class='pagination-item'>";
            $config['num_tag_close']  = "</td>";
            $config['cur_tag_open']   = "<td class='pagination-item-current'>";
            $config['cur_tag_close']  = "</td>";

            $this->pagination->initialize($config);
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $data['result']     = $this->account->get_all_nondispatched_accounts($config['per_page'],$page);

            $this->blade->set('accounts',$data['result'])->set('branches',$branches)->render('dispatch_accounts', array('title' => 'dispatch Pafupi Accounts','feedback'=>$feedback,'search_feedback'=>$search_feedback));

        }
        else{
            pafupi::index();
        }

    }
    public function regeneratePins($data=false){
        $feedback = false;
        $search_feedback = false;
        if($this->security->xss_clean($this->input->post('regen'))){

            $data = array($this->security->xss_clean($this->input->post('acc_no')));
            $user_id = $this->session->userdata('user_id');
            $result =$this->account->regeneratePins($data);
            echo json_encode($result);
        }
        elseif($this->security->xss_clean($this->input->post('search'))){
            $param = $this->security->xss_clean($this->input->post('param'));
            $data = $this->account->search_with_customer($param);

            $this->name ="regenerate otp book for selected account";
            $this->active = 'act_inventory';
            $search_feedback =  "search for ".$param." ".$data['total']." accounts found";
            $this->blade->set('accounts',$data['result'])->render('regenerate_accounts', array('title' => 'regenerate Accounts','feedback'=>$feedback,'search_feedback'=>$search_feedback));
            return;
        }
        else{
            $this->name ="regenerate otp book for selected account";
            $this->active = 'act_inventory';
            $accounts = $this->account->getAccountsWithCustomer();
            $branches = $this->account->getBranches();
            $num_rows   =  count($accounts);
            $data["total_rows"] = $num_rows;


            //$data['users']            = $num_rows;
            $config['base_url']       = BASEURL.'pafupi/main/regenerate_pins';
            $config['total_rows']     = $num_rows;
            $config['per_page']       = 5;
            $config['num_links']      = 3;
            $config['last_link']      = FALSE;
            $config['first_link']     = FALSE;
            $config['uri_segment']    = 4;
            $config['next_link']      = 'Next';
            $config['next_tag_open']  = "<td class='pagination-item-outer-rght'>";
            $config['next_tag_close'] = "</td'>";
            $config['prev_link']      = 'Previous';
            $config['prev_tag_open']  = "<td class='pagination-item-outer-lft'>";
            $config['prev_tag_close'] = "</td>";
            $config['num_tag_open']   = "<td class='pagination-item'>";
            $config['num_tag_close']  = "</td>";
            $config['cur_tag_open']   = "<td class='pagination-item-current'>";
            $config['cur_tag_close']  = "</td>";

            $this->pagination->initialize($config);
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $data    = $this->account->getAccountsWithCustomer($config['per_page'],$page);
            //var_dump($data['result']);exit;
            $this->blade->set('accounts',$data['result'])->set('branches',$branches)->render('regenerate_accounts', array('title' => 'regenerate Accounts','feedback'=>$feedback,'search_feedback'=>$search_feedback));

        }

    }

    //Branch admin
    public function viewBranchStarter(){
        $feedback = false;
        $search_feedback = false;
        if($this->security->xss_clean($this->input->post('search'))){
            $param = $this->security->xss_clean($this->input->post('param'));
            $this->name ="assigned starterpacks";
            $this->active = 'act_account';
            $branch_id = $this->session->userdata('branch');
            $data  = $this->account->search_assigned($branch_id,$param);
            $search_feedback =  "search for ".$param." ".$data['total']." accounts found";


            $num_rows = $this->pagination(count($data));
            //$data['users']            = $num_rows;
            $config['base_url']       = BASEURL.'pafupi/main/view_assigned_starterpacks';
            $config['total_rows']     = $num_rows;
            $config['per_page']       = 5;
            $config['num_links']      = 3;
            $config['last_link']      = FALSE;
            $config['first_link']     = FALSE;
            $config['uri_segment']    = 4;
            $config['next_link']      = 'Next';
            $config['next_tag_open']  = "<td class='pagination-item-outer-rght'>";
            $config['next_tag_close'] = "</td'>";
            $config['prev_link']      = 'Previous';
            $config['prev_tag_open']  = "<td class='pagination-item-outer-lft'>";
            $config['prev_tag_close'] = "</td>";
            $config['num_tag_open']   = "<td class='pagination-item'>";
            $config['num_tag_close']  = "</td>";
            $config['cur_tag_open']   = "<td class='pagination-item-current'>";
            $config['cur_tag_close']  = "</td>";

            $this->pagination->initialize($config);
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $data['result']= array_slice($data,$page,$config['per_page']);



            $this->blade->set('accounts',$data['result'])->render('assigned_accounts', array('title' => 'Assigned starter packs','feedback'=>$feedback,'search_feedback'=>$search_feedback));
            return;
        }
        $this->name ="assigned starterpacks";
        $this->active = 'act_account';
        $branch_id = $this->session->userdata('branch');
        $accounts  = $this->account->getAssignedByBranch($branch_id);
        $branches = $this->account->getBranches();
        $num_rows   =  $this->pagination(count($accounts));
        $data["total_rows"] = $num_rows;

        //$data['users']            = $num_rows;
        $config['base_url']       = BASEURL.'pafupi/main/view_assigned_starterpacks';
        $config['total_rows']     = $num_rows;
        $config['per_page']       = 5;
        $config['num_links']      = 3;
        $config['last_link']      = FALSE;
        $config['first_link']     = FALSE;
        $config['uri_segment']    = 4;
        $config['next_link']      = 'Next';
        $config['next_tag_open']  = "<td class='pagination-item-outer-rght'>";
        $config['next_tag_close'] = "</td'>";
        $config['prev_link']      = 'Previous';
        $config['prev_tag_open']  = "<td class='pagination-item-outer-lft'>";
        $config['prev_tag_close'] = "</td>";
        $config['num_tag_open']   = "<td class='pagination-item'>";
        $config['num_tag_close']  = "</td>";
        $config['cur_tag_open']   = "<td class='pagination-item-current'>";
        $config['cur_tag_close']  = "</td>";

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data     = $this->account->get_all_assigned_by_branch($branch_id,$config['per_page'],$page);
        $this->blade->set('accounts',$data['result'])->render('assigned_accounts', array('title' => 'Assigned starter packs','feedback'=>$feedback,'search_feedback'=>$search_feedback));

    }
    public function assignStarterPack(){
        $feedback = false;
        $search_feedback = false;
        $branch_id = $this->session->userdata('branch');
        $ossc = $this->account->getOSSC($branch_id);
        if($this->security->xss_clean($this->input->post('search'))){
            $param = $this->security->xss_clean($this->input->post('param'));
            $this->name ="assigned starterpacks";
            $this->active = 'act_account';
            $branch_id = $this->session->userdata('branch');
            $data  = $this->account->search_dispatched_not_assigned($branch_id,$param);
            $search_feedback =  "search for ".$param." ".$data['total']." accounts found";

            $num_rows = $this->pagination(count($data));
            //$data['users']            = $num_rows;
            $config['base_url']       = BASEURL.'pafupi/main/assign_starterpack';
            $config['total_rows']     = $num_rows;
            $config['per_page']       = 5;
            $config['num_links']      = 3;
            $config['last_link']      = FALSE;
            $config['first_link']     = FALSE;
            $config['uri_segment']    = 4;
            $config['next_link']      = 'Next';
            $config['next_tag_open']  = "<td class='pagination-item-outer-rght'>";
            $config['next_tag_close'] = "</td'>";
            $config['prev_link']      = 'Previous';
            $config['prev_tag_open']  = "<td class='pagination-item-outer-lft'>";
            $config['prev_tag_close'] = "</td>";
            $config['num_tag_open']   = "<td class='pagination-item'>";
            $config['num_tag_close']  = "</td>";
            $config['cur_tag_open']   = "<td class='pagination-item-current'>";
            $config['cur_tag_close']  = "</td>";

            $this->pagination->initialize($config);
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $data['result']= array_slice($data,$page,$config['per_page']);


            $this->blade->set('accounts',$data['result'])->render('assign_starterpacks', array('title' => 'Assign starter packs','feedback'=>$feedback,'search_feedback'=>$search_feedback));
            return;
        }
        elseif($this->security->xss_clean($this->input->post('assign'))){
            if($this->security->xss_clean($this->input->post('bulk_assign'))){
                $accounts = $this->security->xss_clean($this->input->post('selected_accounts'));
                $ossc = $this->security->xss_clean($this->input->post('ossc'));

                $num = 0;
                for($i=0;$i<count($accounts);$i++){
                    $data['acc_no'] = $accounts[$i];
                    $data['ossc'] = $ossc[$i];
                    $done = $this->account->assignStarterPack($data);
                    if($done){
                        $feedback = ($num+1)." starterpack(s) successfully assigned to ".$data['ossc'];
                        $num++;
                    }
                    else{
                        //$feedback = "starterpack NOT successfully assigned to".$data['ossc'];
                    }
                }
            }
            else{
                $data['acc_no'] = $this->security->xss_clean($this->input->post('acc_no'));
                $data['ossc'] = $this->security->xss_clean($this->input->post('ossc'));

                $done = $this->account->assignStarterPack($data);
                if($done){
                    $feedback = "starterpack successfully assigned to ".$data['ossc'];
                }
                else{
                    $feedback = "starterpack NOT successfully assigned to".$data['ossc'];
                }
                // echo $feedback;exit;
            }
        }

        $this->name ="assign starterpacks";
        $this->active = 'act_account';
        //$branch_id = $this->session->userdata('branch');
        $num_rows  = $this->account-> get_all_dispatched_not_assigned_by_branch($branch_id,false,false,true);
        $ossc = $this->account->getOSSC($branch_id);
        //$num_rows   =  $this->pagination(count($accounts));
        $data["total_rows"] = $num_rows;

        //$data['users']            = $num_rows;
        $config['base_url']       = BASEURL.'pafupi/main/assign_starterpack';
        $config['total_rows']     = $num_rows;
        $config['per_page']       = 5;
        $config['num_links']      = 3;
        $config['last_link']      = FALSE;
        $config['first_link']     = FALSE;
        $config['uri_segment']    = 4;
        $config['next_link']      = 'Next';
        $config['next_tag_open']  = "<td class='pagination-item-outer-rght'>";
        $config['next_tag_close'] = "</td'>";
        $config['prev_link']      = 'Previous';
        $config['prev_tag_open']  = "<td class='pagination-item-outer-lft'>";
        $config['prev_tag_close'] = "</td>";
        $config['num_tag_open']   = "<td class='pagination-item'>";
        $config['num_tag_close']  = "</td>";
        $config['cur_tag_open']   = "<td class='pagination-item-current'>";
        $config['cur_tag_close']  = "</td>";

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data     = $this->account->get_all_dispatched_not_assigned_by_branch($branch_id,$config['per_page'],$page);

        $this->blade->set('accounts',$data['result'])->set('ossc',$ossc)->render('assign_starterpacks', array('title' => 'Assign starter packs','feedback'=>$feedback,'search_feedback'=>$search_feedback));
    }
    public function receiveStarterPack(){
        $feedback = false;
        $search_feedback = false;
        if($this->security->xss_clean($this->input->post('search'))){
            $param = $this->security->xss_clean($this->input->post('param'));
            $this->name ="receive starterpacks";
            $this->active = 'act_account';
            $branch_id = $this->session->userdata('branch');
            $data  = $this->account->search_dispatched_not_received($branch_id,$param);
            $search_feedback =  "search for ".$param." ".$data['total']." accounts found";

            $num_rows = $this->pagination(count($data));
            //$data['users']            = $num_rows;
            $config['base_url']       = BASEURL.'pafupi/main/receive_starterpack';
            $config['total_rows']     = $num_rows;
            $config['per_page']       = 5;
            $config['num_links']      = 3;
            $config['last_link']      = FALSE;
            $config['first_link']     = FALSE;
            $config['uri_segment']    = 4;
            $config['next_link']      = 'Next';
            $config['next_tag_open']  = "<td class='pagination-item-outer-rght'>";
            $config['next_tag_close'] = "</td'>";
            $config['prev_link']      = 'Previous';
            $config['prev_tag_open']  = "<td class='pagination-item-outer-lft'>";
            $config['prev_tag_close'] = "</td>";
            $config['num_tag_open']   = "<td class='pagination-item'>";
            $config['num_tag_close']  = "</td>";
            $config['cur_tag_open']   = "<td class='pagination-item-current'>";
            $config['cur_tag_close']  = "</td>";

            $this->pagination->initialize($config);
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $data['result']= array_slice($data,$page,$config['per_page']);


            $this->blade->set('accounts',$data['result'])->render('receive_starterpacks', array('title' => 'Receive starter packs','feedback'=>$feedback,'search_feedback'=>$search_feedback));
            return;
        }
        elseif($this->security->xss_clean($this->input->post('receive'))){
            $data = $this->security->xss_clean($this->input->post('selected_accounts'));

            $feedback = $this->account->receiveStarterPack($data);

        }
        $this->name ="receive starterpacks";
        $this->active = 'act_account';
        $branch_id = $this->session->userdata('branch');
        $num_rows  = $this->account-> get_all_dispatched_not_received_by_branch($branch_id,false,false,true);
        $branches = $this->account->getBranches();
        //$num_rows   =  $this->pagination(count($accounts));
        $data["total_rows"] = $num_rows;

        //$data['users']            = $num_rows;
        $config['base_url']       = BASEURL.'pafupi/main/receive_starterpack';
        $config['total_rows']     = $num_rows;
        $config['per_page']       = 5;
        $config['num_links']      = 3;
        $config['last_link']      = FALSE;
        $config['first_link']     = FALSE;
        $config['uri_segment']    = 4;
        $config['next_link']      = 'Next';
        $config['next_tag_open']  = "<td class='pagination-item-outer-rght'>";
        $config['next_tag_close'] = "</td'>";
        $config['prev_link']      = 'Previous';
        $config['prev_tag_open']  = "<td class='pagination-item-outer-lft'>";
        $config['prev_tag_close'] = "</td>";
        $config['num_tag_open']   = "<td class='pagination-item'>";
        $config['num_tag_close']  = "</td>";
        $config['cur_tag_open']   = "<td class='pagination-item-current'>";
        $config['cur_tag_close']  = "</td>";

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data     = $this->account->get_all_dispatched_not_received_by_branch($branch_id,$config['per_page'],$page);

        $this->blade->set('accounts',$data['result'])->render('receive_starterpacks', array('title' => 'Receive starter packs','feedback'=>$feedback,'search_feedback'=>$search_feedback));
    }
    public function sendCardToT24($branch_id){
        return true;
    }
    public function sendToT24($num_of_accounts,$branch){
        $data = array();
        $this->load->library('../controllers/Account2');
        $data = $this->account2->makeAccount($num_of_accounts,$branch);
        return $data;
    }
    private function pagination($total_rows)
    {
        //fixes an issue with pagination in CI :)
        if($total_rows > 1)
        {

            $total_rows = $total_rows + 1;

        }

        else
            $total_rows + 0;


        return $total_rows;

    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */