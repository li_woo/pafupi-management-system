<?php
/** ----------------------------------------------------------------------------------------------------------
 *	@file <Security_check.php>
 *   @version 1.0
 *	@description : This controller handles authentication on the system
 *	@author : Vitu Mhone <vmhone@nbsmw.com>
 * -------------------------------------------------------------------------------------------------------------*/
?>

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once 'pafupi.php';
//require_once 'login.php';

class Security_check extends CI_Controller {

    public function __construct()
    {

        //invoke the parent constructor
        parent::__construct();

        //$this->check_login();

    }

    public function check_login()
    {

        $this->load->library('session');
        $this->load->helper('url');

        $username = $this->session->userdata('username');

        if(empty($username))
        {

            //if the user is not logged in, redirect to the login page
            redirect('pafupi/index');

        }


    }

    public function check_admin_privilege()
    {
        //this method checks whether the user is indeed an administrator
        $role_id = $this->session->userdata('role_id');

        if($role_id ==  2 || $role_id == 3)
        {

            //registration clerks (2) and registration supervisors
            //should not be granted access, redirect to the access denied page
            redirect('Security_check/access_denied');

        }

    }

    public function check_reg_clerk_privilege()
    {

        //this method ensures that registration supervisors do not have access to
        //the modules only intended for registration clerks

        $role_id = $this->session->userdata('role_id');

        if($role_id == 1 || $role_id == 3)
        {
            redirect('Security_check/access_denied');
        }


    }

    public function check_reg_supervisor_privilege()
    {

        //this method ensures that registration clerks do not have access to
        //the modules only intended for registration supervisors

        $role_id = $this->session->userdata('role_id');

        if($role_id == 1 || $role_id == 2)
        {

            redirect('Security_check/access_denied');

        }

    }

    public function access_denied()
    {

        $data=array('name'=>'Access_Denied','active'=>'home');
        $data['menu_notifications'] = 0;
        $this->load->view('header');
        $this->load->view('menu',$data);
        $this->load->view('redirects/access_denied');
        $this->load->view('footer');

    }

}

/* End of file Security_check.php */
/* Location: ./application/controllers/Security_check.php */