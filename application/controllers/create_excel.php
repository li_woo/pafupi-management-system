<?php

//require_once 'pafupi.php';
//require_once 'Security_check.php';
class create_excel  extends CI_Controller {
    public $role_id;
    public function __construct()
    {
        parent::__construct();

        $this->load->library('excel');
        $this->load->library('session');
        $this->load->library('blade');
        $this->load->library('branch_helper','branch');
        $this->blade->set('base_url',BASEURL);
        $this->load->library('session');
        $this->active ='act_account';
        //$this->load->model('authentication');
        $this->role_id = $this->session->userdata('role_id');
        $this->load->model('account_m_model','account');
        //$this->load->model('authentication');
    }
    public function createDat($branch_id=false){
        $user_id = $this->session->userdata('user_id');
        $branch = false;
        //$branch_id = $this->security->xss_clean($this->input->get('branch'));
        if($branch_id){

            $branch = $this->branch_helper->get_branch($branch_id);
            $branch_district = strtoupper($this->branch_helper->get_branch_district($branch_id));
        }

        $accounts = $this->account->getDummyCard($branch_id);
        $filename="cards_$branch_id".'_'.date('dmy').'.dat';
        $myfile = fopen($filename, "w");
        if(count($accounts) > 0){

        $txt = "1,".substr($branch_id,-2)."_".date('dmy')."0831".","."628120".","."628120,".substr($branch_id,-2).",10".",".date('dmY').
               ",,"."1,2,3,4,5";
        fwrite($myfile, $txt);
        fwrite($myfile, "\n");

        $num = 1;
        foreach($accounts as $account){

            if($num%26==0){

                fwrite($myfile, "\n\n");
                $txt = "1,".substr($account['branch_id'],-2)."_".date('dmy')."0831".","."628120".","."628120,".substr($account['branch_id'],-2).",10".",".date('dmY').
                    ",,"."1,2,3,4,5";
                fwrite($myfile, $txt);
                fwrite($myfile, "\n");
                $num =1;



            }
            $downloaded = $this->account->updateDummyCard($account['date_created']);
            if($downloaded){
            $txt = "2,SAVINGS,PAFUPI,,".strtoupper($this->branch_helper->get_name($account['branch_id'])).",".
                    strtoupper($this->branch_helper->get_branch_district($account['branch_id'])).",,,NONE,PAFUPI.jpg,".$account['account_id']."|10|454|15000|15000|".",,,";
            fwrite($myfile, $txt);
            fwrite($myfile, "\n");
            $num++;
            }
            else{
                continue;
            }

          }
        }
        else{
            fwrite($myfile, "No cards data");
        }
        if (file_exists($filename)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($filename));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filename));
            readfile($filename);
            exit;
        }

    }
    public function createExcel($branch_id=false){

        $user_id = $this->session->userdata('user_id');
        $branch = false;
        $branch_id = $this->security->xss_clean($this->input->get('branch'));
        if($branch_id){
            $accounts = $this->account->getPreAccount(false,false,$branch_id);
            $branch = $this->branch_helper->get_branch($branch_id);
            $branch_district = strtoupper($this->branch_helper->get_branch_district($branch_id));
        }//var_dump($accounts);exit;
        else
            $accounts = $this->account->getPreAccount($user_id);
        //$branch = $this->authentication->get_branch($branch_id);
        $name = $this->session->userdata('fullname');
        // $accounts = $this->account->getWithCardAccounts($user_id);
        $this->excel->getProperties()->setCreator($name)
            ->setLastModifiedBy($this->session->userdata('fullname'))
            ->setTitle("Pre-linked account")
            ->setSubject("Prelinked account")
            ->setDescription("view prelinked accounts of ".$branch." ")
            ->setKeywords("office 2007 openxml php")
            ->setCategory("accounts");
        ///workbook number 1

        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('account cards');
        //set auto column width
        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        //set headers
        $this->excel->getActiveSheet()->setCellValue('A1', '1');

        $this->excel->getActiveSheet()->setCellValue('B1', substr($branch_id,-2)."_".date('dmy')."0831");
        //$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('C1', '628120');
        //$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('D1', '628120');
        //$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getCell('E1')->setValueExplicit(substr($branch_id,-2), PHPExcel_Cell_DataType::TYPE_STRING);
        //$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('F1', '10');
        //$this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('G1', date('dmY'));
        //$this->excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('H1', '');
        //$this->excel->getActiveSheet()->getStyle('H1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('I1', '1');
        //$this->excel->getActiveSheet()->getStyle('I1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('J1', '2');
        //$this->excel->getActiveSheet()->getStyle('J1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('K1', '3');
        //$this->excel->getActiveSheet()->getStyle('K1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('L1', '4');
        //$this->excel->getActiveSheet()->getStyle('L1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('M1', '5');
        //$this->excel->getActiveSheet()->getStyle('L1')->getFont()->setBold(true);
        //change the font size
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(12);
        $cell = 2;
        $num = 1;
        foreach($accounts as $account){

            if($num%26==0){

                $this->excel->getActiveSheet()->setCellValue('A'.$cell, '1');
                $this->excel->getActiveSheet()->setCellValue('B'.$cell, substr($branch_id,-2)."_".date('dmy')."0831");
                $this->excel->getActiveSheet()->setCellValue('C'.$cell, '628120');
                $this->excel->getActiveSheet()->setCellValue('D'.$cell, '628120');
                $this->excel->getActiveSheet()->getCell('E'.$cell)->setValueExplicit(substr($branch_id,-2), PHPExcel_Cell_DataType::TYPE_STRING);
                $this->excel->getActiveSheet()->setCellValue('F'.$cell, '10');
                $this->excel->getActiveSheet()->setCellValue('G'.$cell, date('dmY'));
                $this->excel->getActiveSheet()->setCellValue('H'.$cell, '');
                $this->excel->getActiveSheet()->setCellValue('I'.$cell, '1');
                $this->excel->getActiveSheet()->setCellValue('J'.$cell, '2');
                $this->excel->getActiveSheet()->setCellValue('K'.$cell, '3');
                $this->excel->getActiveSheet()->setCellValue('L'.$cell, '4');
                $this->excel->getActiveSheet()->setCellValue('M'.$cell, '5');
                $num =1;
                $cell++;


            }
            $this->excel->getActiveSheet()->setCellValue('A'.$cell,'2');
            $this->excel->getActiveSheet()->setCellValue('B'.$cell,'SAVINGS'); 
            $this->excel->getActiveSheet()->setCellValue('C'.$cell,'PAFUPI');
            $this->excel->getActiveSheet()->setCellValue('D'.$cell,'');
            $this->excel->getActiveSheet()->setCellValue('E'.$cell,strtoupper($this->branch_helper->get_name($branch_id)));
            $this->excel->getActiveSheet()->setCellValue('F'.$cell,strtoupper($this->branch_helper->get_branch_district($branch_id)));
            $this->excel->getActiveSheet()->setCellValue('G'.$cell,'');
            $this->excel->getActiveSheet()->setCellValue('H'.$cell,'');
            $this->excel->getActiveSheet()->setCellValue('I'.$cell,'NONE');
            $this->excel->getActiveSheet()->setCellValue('J'.$cell,'PAFUPI.jpg');
            $this->excel->getActiveSheet()->getCell('K'.$cell)->setValueExplicit($account['acc_no']."|10|454|15000|15000|", PHPExcel_Cell_DataType::TYPE_STRING);
            $this->excel->getActiveSheet()->setCellValue('L'.$cell,'');
            $this->excel->getActiveSheet()->setCellValue('L'.$cell,'');
            $num++;
            /*$this->excel->getActiveSheet()->getCell('A'.$cell)->setValueExplicit($account['acc_no'], PHPExcel_Cell_DataType::TYPE_STRING);
            $this->excel->getActiveSheet()->getCell('B'.$cell)->setValueExplicit($account['branch_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            $this->excel->getActiveSheet()->getCell('C'.$cell)->setValueExplicit($account['book_no'], PHPExcel_Cell_DataType::TYPE_STRING);
            $this->excel->getActiveSheet()->getCell('D'.$cell)->setValueExplicit($account['referal_no'], PHPExcel_Cell_DataType::TYPE_STRING);
            $this->excel->getActiveSheet()->getCell('E'.$cell)->setValueExplicit(date('d-m-Y',strtotime($account['date_created'])), PHPExcel_Cell_DataType::TYPE_STRING);
            $this->excel->getActiveSheet()->getCell('F'.$cell)->setValueExplicit($account['FullName'], PHPExcel_Cell_DataType::TYPE_STRING);*/
            $cell++;
        }

        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

        //workbook number 2
        $pins = $this->account->getAccountBookPins();

        $this->excel->createSheet();
        $this->excel->getSheet(1);
        $this->excel->setActiveSheetIndex(1);
        $this->excel->getActiveSheet()->setTitle('Pins');

        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);

        //$this->excel->getActiveSheet()->getCell('A1')->setValueExplicit($acc_no,PHPExcel_Cell_DataType::TYPE_STRING);
        //set headers
        $this->excel->getActiveSheet()->setCellValue('A2', 'Book No');
        $this->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('B2', 'OTP');
        $this->excel->getActiveSheet()->getStyle('B2')->getFont()->setBold(true);
        //change the font size
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(12);
        $cell = 3;
        foreach($pins as $pin){
            //$this->excel->getActiveSheet()->setCellValue('A'.$cell, $account['acc_no']);
            $this->excel->getActiveSheet()->getCell('A'.$cell)->setValueExplicit($pin['book_no'], PHPExcel_Cell_DataType::TYPE_STRING);
            //$this->excel->getActiveSheet()->setCellValue('B'.$cell, $account['branch_name']);
            $this->excel->getActiveSheet()->getCell('B'.$cell)->setValueExplicit($pin['trans_pin'], PHPExcel_Cell_DataType::TYPE_STRING);

            $cell++;
        }

        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

        //worksheet 3
        $this->excel->createSheet();
        $this->excel->getSheet(2);
        $this->excel->setActiveSheetIndex(2);
        $this->excel->getActiveSheet()->setTitle('Referals');

        $pins = $this->account->getReferalPins();
        $user_id = $this->session->userdata('user_id');



        $this->excel->getActiveSheet()->setTitle('Referals');

        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);

        //$this->excel->getActiveSheet()->getCell('A1')->setValueExplicit($acc_no,PHPExcel_Cell_DataType::TYPE_STRING);
        //set headers
        $this->excel->getActiveSheet()->setCellValue('A2', 'Referal No');
        $this->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('B2', 'Pins');
        $this->excel->getActiveSheet()->getStyle('B2')->getFont()->setBold(true);
        //change the font size
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(12);
        $cell = 3;
        foreach($pins as $pin){
            //$this->excel->getActiveSheet()->setCellValue('A'.$cell, $account['acc_no']);
            $this->excel->getActiveSheet()->getCell('A'.$cell)->setValueExplicit($pin['referal_no'], PHPExcel_Cell_DataType::TYPE_STRING);
            //$this->excel->getActiveSheet()->setCellValue('B'.$cell, $account['branch_name']);
            $this->excel->getActiveSheet()->getCell('B'.$cell)->setValueExplicit($pin['coupon_no'], PHPExcel_Cell_DataType::TYPE_STRING);

            $cell++;
        }

        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

        $this->excel->setActiveSheetIndex(0);

        $filename="cards_$branch_id".'_'.date('dmy').'.xls';  //file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('files/prelinked_accounts/'.$filename);
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }
    public function createReferalPinsExcel($user_id=false){

        $acc_no = false;
        if($acc_no = $this->security->xss_clean($this->input->get('account_no'))){

        }
            $pins = $this->account->getReferalPins($acc_no);
            $user_id = $this->session->userdata('user_id');

            $this->excel->setActiveSheetIndex(0);

            $this->excel->getActiveSheet()->setTitle('Referal 00'.$acc_no);
            $this->excel->setActiveSheetIndex(0);
            $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
            $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);

            $this->excel->getActiveSheet()->getCell('A1')->setValueExplicit($acc_no,PHPExcel_Cell_DataType::TYPE_STRING);
            //set headers
            $this->excel->getActiveSheet()->setCellValue('A2', 'Referal No');
            $this->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->setCellValue('B2', 'Pins');
            $this->excel->getActiveSheet()->getStyle('B2')->getFont()->setBold(true);
            //change the font size
            $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(12);
            $cell = 3;
            foreach($pins as $pin){
                //$this->excel->getActiveSheet()->setCellValue('A'.$cell, $account['acc_no']);
                $this->excel->getActiveSheet()->getCell('A'.$cell)->setValueExplicit($pin['referal_no'], PHPExcel_Cell_DataType::TYPE_STRING);
                //$this->excel->getActiveSheet()->setCellValue('B'.$cell, $account['branch_name']);
                $this->excel->getActiveSheet()->getCell('B'.$cell)->setValueExplicit($pin['coupon_no'], PHPExcel_Cell_DataType::TYPE_STRING);

                $cell++;
            }

            $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);


            $filename='referalpins_'.$user_id.'.xls';  //file name
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache

            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format
            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            $objWriter->save('files/referal_pins/'.$filename);
            //force user to download the Excel file without writing it to server's HD
            $objWriter->save('php://output');

    }
    public function createPinsExcel($acc_no = false){

        if($acc_no || $acc_no = $this->security->xss_clean($this->input->get('acc_no'))){

            $pins = $this->account->getAccountBookPins($acc_no);
            $user_id = $this->session->userdata('user_id');

            $this->excel->setActiveSheetIndex(0);

            $this->excel->getActiveSheet()->setTitle('Pins 00'.$acc_no);
              $this->excel->setActiveSheetIndex(0);



        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);

            $this->excel->getActiveSheet()->getCell('A1')->setValueExplicit($acc_no,PHPExcel_Cell_DataType::TYPE_STRING);
        //set headers
        $this->excel->getActiveSheet()->setCellValue('A2', 'Book No');
        $this->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('B2', 'OTP');
        $this->excel->getActiveSheet()->getStyle('B2')->getFont()->setBold(true);
        //change the font size
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(12);
        $cell = 3;
        foreach($pins as $pin){
            //$this->excel->getActiveSheet()->setCellValue('A'.$cell, $account['acc_no']);
            $this->excel->getActiveSheet()->getCell('A'.$cell)->setValueExplicit($pin['book_no'], PHPExcel_Cell_DataType::TYPE_STRING);
            //$this->excel->getActiveSheet()->setCellValue('B'.$cell, $account['branch_name']);
            $this->excel->getActiveSheet()->getCell('B'.$cell)->setValueExplicit($pin['trans_pin'], PHPExcel_Cell_DataType::TYPE_STRING);

            $cell++;
        }

        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);


        $filename='otp'.time().'.xls';  //file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('files/otp/'.$filename);
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
        }
        else{
            $pins =$this->account->getBookPins();

        $user_id = $this->session->userdata('user_id');

        $this->excel->setActiveSheetIndex(0);

        $this->excel->getActiveSheet()->setTitle('Pins');

        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);

        //set headers
        $this->excel->getActiveSheet()->setCellValue('A1', 'Book No');
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('B1', 'OTP');
        $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
        //change the font size
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(12);
        $cell = 2;
        foreach($pins as $pin){
            //$this->excel->getActiveSheet()->setCellValue('A'.$cell, $account['acc_no']);
            $this->excel->getActiveSheet()->getCell('A'.$cell)->setValueExplicit($pin['book_no'], PHPExcel_Cell_DataType::TYPE_STRING);
            //$this->excel->getActiveSheet()->setCellValue('B'.$cell, $account['branch_name']);
            $this->excel->getActiveSheet()->getCell('B'.$cell)->setValueExplicit($pin['trans_pin'], PHPExcel_Cell_DataType::TYPE_STRING);

            $cell++;
        }

        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);


        $filename='otp'.time().'.xls';  //file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('files/otp/'.$filename);
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
        }
    }
    public function createReferalExcel(){
        $referals = $this->account->getReferalPins();

        $this->excel->setActiveSheetIndex(0);
        //pins worksheet
        $this->excel->getActiveSheet()->setTitle('OTPs');
        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);

        $this->excel->getActiveSheet()->setTitle('Referals');


        $user_id = $this->session->userdata('user_id');



        $this->excel->getActiveSheet()->setTitle('Referals');

        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);

        //$this->excel->getActiveSheet()->getCell('A1')->setValueExplicit($acc_no,PHPExcel_Cell_DataType::TYPE_STRING);
        //set headers
        $this->excel->getActiveSheet()->setCellValue('A1', 'Referal No');
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('B1', 'Coupons');
        $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
        //change the font size
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(12);
        $cell = 2;
        foreach($referals as $referal){
            //$this->excel->getActiveSheet()->setCellValue('A'.$cell, $account['acc_no']);
            $this->excel->getActiveSheet()->getCell('A'.$cell)->setValueExplicit($referal['referal_no'], PHPExcel_Cell_DataType::TYPE_STRING);
            //$this->excel->getActiveSheet()->setCellValue('B'.$cell, $account['branch_name']);
            $this->excel->getActiveSheet()->getCell('B'.$cell)->setValueExplicit($referal['coupon_no'], PHPExcel_Cell_DataType::TYPE_STRING);

            $cell++;
        }

        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

        $this->excel->setActiveSheetIndex(0);

        $filename='referals_'.time().'.xls';  //file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache


        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('files/referals/'.$filename);
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }
    public function createOTPReferalExcel(){
        $this->excel->setActiveSheetIndex(0);
        $pins = $this->account->getAccountBookPins();
        $referals = $this->account->getReferalPins();

        $this->excel->setActiveSheetIndex(0);
        //pins worksheet
        $this->excel->getActiveSheet()->setTitle('OTPs');
        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);

        $this->excel->getActiveSheet()->setCellValue('A1', 'Book No');
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('B1', 'OTP');
        $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
        $cell = 2;
        foreach($pins as $pin){

            $this->excel->getActiveSheet()->getCell('A'.$cell)->setValueExplicit($pin['book_no'], PHPExcel_Cell_DataType::TYPE_STRING);
            $this->excel->getActiveSheet()->getCell('B'.$cell)->setValueExplicit($pin['trans_pin'], PHPExcel_Cell_DataType::TYPE_STRING);

            $cell++;
        }

        $this->excel->createSheet();
        $this->excel->getSheet(1);
        $this->excel->setActiveSheetIndex(1);
        $this->excel->getActiveSheet()->setTitle('Referals');


        $user_id = $this->session->userdata('user_id');



        $this->excel->getActiveSheet()->setTitle('Referals');

        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);

        //$this->excel->getActiveSheet()->getCell('A1')->setValueExplicit($acc_no,PHPExcel_Cell_DataType::TYPE_STRING);
        //set headers
        $this->excel->getActiveSheet()->setCellValue('A1', 'Referal No');
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('B1', 'Coupons');
        $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
        //change the font size
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(12);
        $cell = 2;
        foreach($referals as $referal){
            //$this->excel->getActiveSheet()->setCellValue('A'.$cell, $account['acc_no']);
            $this->excel->getActiveSheet()->getCell('A'.$cell)->setValueExplicit($referal['referal_no'], PHPExcel_Cell_DataType::TYPE_STRING);
            //$this->excel->getActiveSheet()->setCellValue('B'.$cell, $account['branch_name']);
            $this->excel->getActiveSheet()->getCell('B'.$cell)->setValueExplicit($referal['coupon_no'], PHPExcel_Cell_DataType::TYPE_STRING);

            $cell++;
        }

        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

        $this->excel->setActiveSheetIndex(0);
        //end added



        $filename='otp_'.$user_id.'.xls';  //file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache


        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('files/otp/'.$filename);
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');

    }
    public function createCardExcel($user_id=false){

        $user_id = $this->session->userdata('user_id');
        $accounts = $this->account->getCardAccounts($user_id);

        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Carded account');
        //set auto column width
        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        //set headers
        $this->excel->getActiveSheet()->setCellValue('A1', 'Account No');
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('B1', 'Branch');
        $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('C1', 'Booklet No');
        $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('D1', 'Referal No');
        $this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('E1', 'ATM No');
        $this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
        //change the font size
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(12);
        $cell = 2;
        foreach($accounts as $account){
            //$this->excel->getActiveSheet()->setCellValue('A'.$cell, $account['acc_no']);
            $this->excel->getActiveSheet()->getCell('A'.$cell)->setValueExplicit($account['acc_no'], PHPExcel_Cell_DataType::TYPE_STRING);
            //$this->excel->getActiveSheet()->setCellValue('B'.$cell, $account['card_no']);
            $this->excel->getActiveSheet()->getCell('B'.$cell)->setValueExplicit($account['branch_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            //$this->excel->getActiveSheet()->setCellValue('C'.$cell, $account['branch_name']);
            $this->excel->getActiveSheet()->getCell('C'.$cell)->setValueExplicit($account['booklet_no'], PHPExcel_Cell_DataType::TYPE_STRING);
            $this->excel->getActiveSheet()->getCell('D'.$cell)->setValueExplicit($account['referal_no'],PHPExcel_Cell_DataType::TYPE_STRING);
            $this->excel->getActiveSheet()->getCell('E'.$cell)->setValueExplicit($account['card_no'],PHPExcel_Cell_DataType::TYPE_STRING);
            //$this->excel->getActiveSheet()->getCell('D'.$cell)->setValueExplicit(date('d-m-Y',strtotime($account['date_created'])), PHPExcel_Cell_DataType::TYPE_STRING);
            $cell++;
        }
        //added things
        //workbook number 2
        $pins = $this->account->getAccountBookPins();

        $this->excel->createSheet();
        $this->excel->getSheet(1);
        $this->excel->setActiveSheetIndex(1);
        $this->excel->getActiveSheet()->setTitle('Pins');

        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);

        //$this->excel->getActiveSheet()->getCell('A1')->setValueExplicit($acc_no,PHPExcel_Cell_DataType::TYPE_STRING);
        //set headers
        $this->excel->getActiveSheet()->setCellValue('A2', 'Book No');
        $this->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('B2', 'OTP');
        $this->excel->getActiveSheet()->getStyle('B2')->getFont()->setBold(true);
        //change the font size
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(12);
        $cell = 3;
        foreach($pins as $pin){
            //$this->excel->getActiveSheet()->setCellValue('A'.$cell, $account['acc_no']);
            $this->excel->getActiveSheet()->getCell('A'.$cell)->setValueExplicit($pin['book_no'], PHPExcel_Cell_DataType::TYPE_STRING);
            //$this->excel->getActiveSheet()->setCellValue('B'.$cell, $account['branch_name']);
            $this->excel->getActiveSheet()->getCell('B'.$cell)->setValueExplicit($pin['trans_pin'], PHPExcel_Cell_DataType::TYPE_STRING);

            $cell++;
        }

        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

        //worksheet 3
        $this->excel->createSheet();
        $this->excel->getSheet(2);
        $this->excel->setActiveSheetIndex(2);
        $this->excel->getActiveSheet()->setTitle('Referals');

        $pins = $this->account->getReferalPins();
        $user_id = $this->session->userdata('user_id');



        $this->excel->getActiveSheet()->setTitle('Referals');

        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);

        //$this->excel->getActiveSheet()->getCell('A1')->setValueExplicit($acc_no,PHPExcel_Cell_DataType::TYPE_STRING);
        //set headers
        $this->excel->getActiveSheet()->setCellValue('A2', 'Referal No');
        $this->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('B2', 'Pins');
        $this->excel->getActiveSheet()->getStyle('B2')->getFont()->setBold(true);
        //change the font size
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(12);
        $cell = 3;
        foreach($pins as $pin){
            //$this->excel->getActiveSheet()->setCellValue('A'.$cell, $account['acc_no']);
            $this->excel->getActiveSheet()->getCell('A'.$cell)->setValueExplicit($pin['referal_no'], PHPExcel_Cell_DataType::TYPE_STRING);
            //$this->excel->getActiveSheet()->setCellValue('B'.$cell, $account['branch_name']);
            $this->excel->getActiveSheet()->getCell('B'.$cell)->setValueExplicit($pin['coupon_no'], PHPExcel_Cell_DataType::TYPE_STRING);

            $cell++;
        }

        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

        $this->excel->setActiveSheetIndex(0);
        //end added



        $filename='carded-accounts_'.$user_id.'.xls';  //file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache


        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('files/card_accounts/'.$filename);
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }
    public function createWithCardExcel($user_id=false){
        //$user_id = '05050';
        $user_id = $this->session->userdata('user_id');
        $name = $this->session->userdata('fullname');
        $accounts = $this->account->getWithCardAccounts($user_id);
        $this->excel->getProperties()->setCreator($name)
            ->setLastModifiedBy($this->session->userdata('fullname'))
            ->setTitle("Pre-linked account")
            ->setSubject("Prelinked account")
            ->setDescription("view prelinked accounts by ".$name)
            ->setKeywords("office 2007 openxml php")
            ->setCategory("accounts");
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Pre-linked account');
        //set auto column width
        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        //set headers
        $this->excel->getActiveSheet()->setCellValue('A1', 'Starterpack ID');
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('B1', 'Account No');
        $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('C1', 'Account status');
        $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('D1', 'Card No');
        $this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('E1', 'Branch');
        $this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('F1', 'Date created');
        $this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('G1', 'Opened by');
        $this->excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
        //change the font size
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(12);
        $cell = 2;
        foreach($accounts as $account){
            //$this->excel->getActiveSheet()->setCellValue('A'.$cell, $account['starter_id']);
            $this->excel->getActiveSheet()->getCell('A'.$cell)->setValueExplicit($account['starter_id'], PHPExcel_Cell_DataType::TYPE_STRING);
            //$this->excel->getActiveSheet()->setCellValue('B'.$cell, $account['acc_no']);
            $this->excel->getActiveSheet()->getCell('B'.$cell)->setValueExplicit($account['acc_no'], PHPExcel_Cell_DataType::TYPE_STRING);
            //$this->excel->getActiveSheet()->setCellValue('C'.$cell, $account['acc_status']);
            $this->excel->getActiveSheet()->getCell('C'.$cell)->setValueExplicit($account['acc_status'], PHPExcel_Cell_DataType::TYPE_STRING);
            //$this->excel->getActiveSheet()->setCellValue('D'.$cell, $account['card_no']);
            $this->excel->getActiveSheet()->getCell('D'.$cell)->setValueExplicit($account['card_no'], PHPExcel_Cell_DataType::TYPE_STRING);
            //$this->excel->getActiveSheet()->setCellValue('E'.$cell, $account['branch_name']);
            $this->excel->getActiveSheet()->getCell('E'.$cell)->setValueExplicit($account['branch_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            //$this->excel->getActiveSheet()->setCellValue('F'.$cell, date('d-m-Y',strtotime($account['date_created'])));
            $this->excel->getActiveSheet()->getCell('F'.$cell)->setValueExplicit(date('d-m-Y',strtotime($account['date_created'])), PHPExcel_Cell_DataType::TYPE_STRING);
            //$this->excel->getActiveSheet()->setCellValue('G'.$cell, $account['opened_by']);
            $this->excel->getActiveSheet()->getCell('G'.$cell)->setValueExplicit($account['opened_by'], PHPExcel_Cell_DataType::TYPE_STRING);
            $cell++;
        }

        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);


        $filename='with-card-accounts_'.$user_id.'.xls';  //file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache


        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('files/card_accounts/'.$filename);
        $objWriter->save('php://output');
    }
    public function createCardLinkedExcel($user_id=false){

        $user_id = $this->session->userdata('user_id');
        $accounts = $this->account->getCardedLinked();

        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('card-linked account');
        //set auto column width
        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        //set headers
        $this->excel->getActiveSheet()->setCellValue('A1', 'Account No');
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);

        $this->excel->getActiveSheet()->setCellValue('B1', 'OTP Book No');
        $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('C1', 'Referal Book No');
        $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('D1', 'SP No');
        $this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('E1', 'Card No');
        $this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);

        //change the font size
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(12);
        $cell = 2;
        foreach($accounts as $account){

            $this->excel->getActiveSheet()->getCell('A'.$cell)->setValueExplicit($account['acc_no'], PHPExcel_Cell_DataType::TYPE_STRING);

            //$this->excel->getActiveSheet()->getCell('B'.$cell)->setValueExplicit($account['branch_name'], PHPExcel_Cell_DataType::TYPE_STRING);

            $this->excel->getActiveSheet()->getCell('B'.$cell)->setValueExplicit($account['book_no'], PHPExcel_Cell_DataType::TYPE_STRING);
            $this->excel->getActiveSheet()->getCell('C'.$cell)->setValueExplicit($account['referal_no'],PHPExcel_Cell_DataType::TYPE_STRING);
            $this->excel->getActiveSheet()->getCell('D'.$cell)->setValueExplicit($account['starter_id'],PHPExcel_Cell_DataType::TYPE_STRING);
            $this->excel->getActiveSheet()->getCell('E'.$cell)->setValueExplicit($account['card_no'],PHPExcel_Cell_DataType::TYPE_STRING);
            //$this->excel->getActiveSheet()->getCell('G'.$cell)->setValueExplicit($account['FullName'],PHPExcel_Cell_DataType::TYPE_STRING);

            $cell++;
        }



        $filename='linked-carded-accounts_'.time().'.xls';  //file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache


        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('files/card_linked_accounts/'.$filename);
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }
    public function uploadManuFile(){
        $errors = false;
        $this->active = 'act_sp';
        if($_FILES){
            $user_id = $this->session->userdata('user_id');
            $name = $_FILES['userfile']['name'];

            $name = strtr($name, 'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ', 'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');

            // replace characters other than letters, numbers and . by _
            $name = preg_replace('/([^.a-z0-9]+)/i', '_', $name);
            $config['upload_path'] = 'files/card_linked_accounts/';
            $config['allowed_types'] = 'xls|xlsx';
            $config['max_size']	= '10000';
            $config['file_name'] = $name;
            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload())
            {
                $error = array('error' => $this->upload->display_errors('',''));
                //insert into upload table
                $dat = array('upload_name' => $name, 'uploaded_by' => $user_id, 'error_type' => 'error uploading file');
                $uploaded = $this->account->saveUploadData($dat);
                $errors = $error;

            }
            else
            {
                $d = array();

                $data = $this->upload->data();
                $name = $data['file_name'];
                $feedback = $this->readManuExcel($name);
                //insert into upload table
                $dat = array('upload_name' => $name, 'uploaded_by' => $user_id, 'error_type' => 'success');
                $uploaded = $this->account->saveUploadData($dat);
                $info = new stdClass();
                $info->name = $name;
                $info->size = $data['file_size'];
                $info->type = $data['file_type'];
                $info->deleteUrl = "";
                $info->url = 'files/card_linked_accounts/'.$name;
                array_push($d,$info);
                array_push($d,$feedback);
                $errors = array_pop($feedback);
                //echo json_encode($d);

            }
            $this->current_page = 'upload_file';
            $this->name = 'validate packaged starterpacks from manufacturer here ';
            $this->blade->render('manu_upload', array('title' => 'upload file','errors' => $errors));

        }
        else{
            $this->current_page = 'upload_file';
            $this->name = 'validate packaged starterpacks from manufacturer here ';
            $this->blade->render('manu_upload', array('title' => 'upload file','errors' => $errors));
        }
    }

    public function  checkOTPFields($objWorksheet){
        $num = 0;
        $field1 = 0; $field2 = 0; $field3= 0;
        //echo '<table>' . "\n";
        foreach ($objWorksheet->getRowIterator() as $row) {
            //echo '<tr>' . "\n";
            try{
                if($row->getRowIndex() == 1){

                    continue;}
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(false);

                foreach ($cellIterator as $cell) {
                    try{

                        if($cell->getColumn() == 'A'){

                            if($cell->getValue() !=" ")
                                $field1++;
                        }
                        if($cell->getColumn() == 'B'){

                            if($cell->getValue() !=" ")
                                $field2++;
                        }

                    }
                    catch(PHPExcel_Reader_Exception $e) {
                        $errors[$num][$cell->getColumn()]['cell'] = $e->getMessage().'-->Error reading cell: '.$cell->getCoordinate();
                    }

                }
                $num++;
            }
            catch(PHPExcel_Reader_Exception $e) {
                $errors[$num]['row'] = $e->getMessage().'-->Error reading row: '.$cell->getRow();
            }

        }
        if(($field1 == $field2)){
            return true;
        }
        else{
            return false;
        }

    }
    public function checkOTPHeaders($row){

        $cellIterator = $row->getCellIterator();
        $cellIterator->setIterateOnlyExistingCells(false);
        $num = 0;
        try{
            foreach ($cellIterator as $cell) {
                if($cell->getColumn() == 'A'){
                    $headerA = $cell->getValue();
                    if($headerA !='Book No'){

                        return false;
                    }
                }
                if($cell->getColumn() == 'B'){
                    $headerB = $cell->getValue();
                    if($headerB !='Pin No'){

                        return false;
                    }
                }

                $num++;
            }
        }
        catch(PHPExcel_Reader_Exception $e) {
            $errors[$num]['cell'] = $e->getMessage().'-->Error reading cell: ';
            return false;
        }
        return true;
    }
    /*public function readOTPExcel($filename=false){

        $objReader = PHPExcel_IOFactory::createReader('Excel5');
        $objReader->setReadDataOnly(true);
        $data = array();
        $errors = array(); $errors[0]['error'] = "" ;
        $toSave = array();
        try{
            $filename = $filename?$filename:"otp.xls";
            $objPHPExcel = $objReader->load("files/otp/".$filename);

        }
        catch(PHPExcel_Reader_Exception $e) {
            $errors[0]['error'] =  $e->getMessage().'-->Error reading file: ';
            array_push($data,$errors);
            return $data;
        }
        $objPHPExcel->setActiveSheetIndex(0);
        $objWorksheet = $objPHPExcel->getActiveSheet();
        $num_rows =  $objWorksheet->getHighestRow();
        $high_columns = $objWorksheet->getHighestColumn();
        if($high_columns != 'B'){
            $errors[0]['error'] = "the file has more/less columns than needed";
            array_push($data,$errors);
            return $data;
        }
        //$this->countRows($objWorksheet);
        $fields = $this->checkOTPFields($objWorksheet);
        if(!$fields){
            $errors[0]['error'] =  'the file has mismatched number of fields';
            array_push($data,$errors);
            return $data;
        }
        $num = 0;

        //echo '<table>' . "\n";
        foreach ($objWorksheet->getRowIterator() as $row) {
            //echo '<tr>' . "\n";
            try{
                if($row->getRowIndex() == 1){
                    $headers = true;//$this->checkOTPHeaders($row);
                    if(!$headers){
                        $errors[$num]['row']=" the file has wrong headers";
                        array_push($data,$errors);
                        return $data;
                    }

                    continue;}
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(false);
                $num_cell = 1;
                foreach ($cellIterator as $cell) {
                    try{

                        if($cell->getColumn() == 'A'){
                            if(!ctype_digit (trim($cell->getValue()))){
                                $data[$num]['book_no'] = $cell->getValue();
                                $toSave[$num]['book_no'] = $cell->getValue();
                            }
                            else{
                                $errors[$num_cell]['cell'] = "book number not numeric at A".$num_cell;
                                $num--;
                                continue;
                            }

                        }
                        if($cell->getColumn() == 'B'){
                            if(ctype_digit (trim($cell->getValue()))){
                                $data[$num]['trans_pin'] = $cell->getValue();
                                $toSave[$num]['trans_pin'] = $cell->getValue();
                            }
                            else{
                                $errors[$num_cell]['cell'] = "referal number not numeric";
                                unset($data[$num]['book_no']);
                                unset($toSave[$num]['book_no']);
                                $num--;
                                continue;
                            }
                        }


                    }
                    catch(PHPExcel_Reader_Exception $e) {
                        $errors[$num_cell]['cell'] = $e->getMessage().'-->Error reading cell: '.$cell->getCoordinate();
                    }
                    $num_cell++;
                }
                $num++;
            }
            catch(PHPExcel_Reader_Exception $e) {
                $errors[$num]['row'] = $e->getMessage().'-->Error reading row: '.$cell->getRow();
            }

        }
        $total_rows = $num_rows;
        //////////////////////////////////////////////////////////////////
        $objPHPExcel->setActiveSheetIndex(1);
        $objWorksheet = $objPHPExcel->getActiveSheet();
        $num_rows =  $objWorksheet->getHighestRow();
        $high_columns = $objWorksheet->getHighestColumn();
        if($high_columns != 'B'){
            $errors[0]['error'] = "the file has more/less columns than needed";
            array_push($data,$errors);
            return $data;
        }
        /*
        $fields = $this->checkOTPFields($objWorksheet);
        if(!$fields){
            $errors[0]['error'] =  'the file has mismatched number of fields';
            array_push($data,$errors);
            return $data;
        }
        $num_read = $num;
        $num = 0;
        $data2 = array();
        //echo '<table>' . "\n";
        foreach ($objWorksheet->getRowIterator() as $row) {
            //echo '<tr>' . "\n";
            try{
                if($row->getRowIndex() == 1){
                    $headers = true;//$this->checkOTPHeaders($row);

                    if(!$headers){
                        $errors[$num]['row']=" the file has wrong headers";
                        array_push($data,$errors);
                        return $data;
                    }

                    continue;}
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(false);
                $num_cell = 1;
                foreach ($cellIterator as $cell) {
                    try{

                        if($cell->getColumn() == 'A'){

                            if(!ctype_digit (trim($cell->getValue()))){
                                $data[$num]['referal_no'] = $cell->getValue();
                                $toSave[$num]['referal_no'] = $cell->getValue();
                            }
                            else{
                                $errors[$num_cell]['cell'] = "book number not numeric at A".$num_cell;
                                $num--;
                                continue;
                            }

                        }
                        if($cell->getColumn() == 'B'){
                            if(ctype_digit (trim($cell->getValue()))){
                                $data[$num]['referal_coupons'] = $cell->getValue();
                                $toSave[$num]['referal_coupons'] = $cell->getValue();
                            }
                            else{
                                $errors[$num_cell]['cell'] = "referal number not numeric";
                                unset($data[$num]['referal_no']);
                                unset($toSave[$num]['referal_no']);
                                $num--;
                                continue;
                            }
                        }


                    }
                    catch(PHPExcel_Reader_Exception $e) {
                        $errors[$num_cell]['cell'] = $e->getMessage().'-->Error reading cell: '.$cell->getCoordinate();
                    }
                    $num_cell++;
                }
                $num++;
            }
            catch(PHPExcel_Reader_Exception $e) {
                $errors[$num]['row'] = $e->getMessage().'-->Error reading row: '.$cell->getRow();
            }

        }
        //get books and validate number
        $dut = array();
        $valid_books = array();
        for($i=0;$i<count($data);$i++){
            $dut[$i] = @$data[$i]['book_no'];
        }
        $count_books = @array_count_values($dut);
        $books = array_keys($count_books);
        for($i=0;$i<count($books);$i++){
            if($count_books[$books[$i]] == 50){
                array_push($valid_books,$books[$i]);
            }
            else{

            }
        }
        //get referal books and validate number
        $ref = array();
        $valid_refs = array();
        for($i=0;$i<count($data);$i++){
            $ref[$i] = @$data[$i]['referal_no'];
        }
        $count_refs = @array_count_values($ref);
        $refs = array_keys($count_refs);
        for($i=0;$i<count($refs);$i++){
            if($count_refs[$refs[$i]] == 10){
                array_push($valid_refs,$refs[$i]);
            }
            else{

            }
        }


        $db = $this->account->updateOTP($data,$valid_books,$valid_refs);
        $numbers = array_pop($db);
        $errors['percent'] =  @$numbers['books']." otp books out of ". count($count_books)
                               ." read ----".@$numbers['refs']." referal books out of ". count($count_refs)." read";
        //$errors['database'] = $db;
        array_push($data,$errors);
        return $data;
    }
    public function uploadOTPFile(){
        $errors = false;
        $this->active = 'act_pin';
        if($_FILES){
            $user_id = $this->session->userdata('user_id');
            $name = $_FILES['userfile']['name'];

            $name = strtr($name, 'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ', 'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');

            // replace characters other than letters, numbers and . by _
            $name = preg_replace('/([^.a-z0-9]+)/i', '_', $name);
            $config['upload_path'] = 'files/otp/';
            $config['allowed_types'] = 'xls|xlsx';
            $config['max_size']	= '10000';
            $config['file_name'] = $name;
            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload())
            {
                $error = array('error' => $this->upload->display_errors('',''));
                //insert into upload table
                $dat = array('upload_name' => $name, 'uploaded_by' => $user_id, 'error_type' => 'error uploading file');
                $uploaded = $this->account->saveUploadData($dat);
                $errors = $error;

            }
            else
            {
                $d = array();

                $data = $this->upload->data();
                $name = $data['file_name'];
                $feedback = $this->readOTPExcel($name);
                //insert into upload table
                $dat = array('upload_name' => $name, 'uploaded_by' => $user_id, 'error_type' => 'success');
                $uploaded = $this->account->saveUploadData($dat);
                $info = new stdClass();
                $info->name = $name;
                $info->size = $data['file_size'];
                $info->type = $data['file_type'];
                $info->deleteUrl = "";
                $info->url = 'files/otp/'.$name;
                array_push($d,$info);
                array_push($d,$feedback);
                $errors = array_pop($feedback);
                //echo json_encode($d);

            }
            $this->current_page = 'upload_file';
            $this->name = 'upload OTP excel';
            $this->blade->render('otp_upload', array('title' => 'upload file','errors' => $errors));

        }
        else{
            $this->current_page = 'upload_file';
            $this->name = 'upload OTP excel';
            $this->blade->render('otp_upload', array('title' => 'upload file','errors' => $errors));
        }
    }*/
    public function checkManuHeaders($row){

        $cellIterator = $row->getCellIterator();
        $cellIterator->setIterateOnlyExistingCells(false);
        $num = 0;
        try{
            foreach ($cellIterator as $cell) {
                if($cell->getColumn() == 'A'){
                    $headerA = $cell->getValue();
                    if($headerA !='Account No'){

                       return false;
                    }
                }
                if($cell->getColumn() == 'B'){
                    $headerB = $cell->getValue();
                    if($headerB !='Card No'){

                        return false;
                    }
                }
                if($cell->getColumn() == 'C'){
                    $headerC = $cell->getValue();
                    if($headerC !='Starter No'){

                        return false;
                    }
                }
                $num++;
            }
        }
        catch(PHPExcel_Reader_Exception $e) {
            $errors[$num]['cell'] = $e->getMessage().'-->Error reading cell: ';
            return false;
        }
       return true;
    }
    public function  checkManuFields($objWorksheet){
        $num = 0;
        $field1 = 0; $field2 = 0; $field3= 0; $field4= 0; $field5= 0; $field6= 0; $field7= 0;
        //echo '<table>' . "\n";
        foreach ($objWorksheet->getRowIterator() as $row) {
            //echo '<tr>' . "\n";
            try{
                if($row->getRowIndex() == 1){

                    continue;}
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(false);

                foreach ($cellIterator as $cell) {
                    try{

                        if($cell->getColumn() == 'A'){

                            if($cell->getValue() !=" ")
                                $field1++;
                        }
                        if($cell->getColumn() == 'B'){

                            if($cell->getValue() !=" ")
                                $field2++;
                        }
                        if($cell->getColumn() == 'C'){

                            if($cell->getValue() !="")
                                $field3++;
                        }
                        if($cell->getColumn() == 'D'){

                            if($cell->getValue() !="")
                                $field4++;
                        }
                        if($cell->getColumn() == 'E'){

                            if($cell->getValue() !="")
                                $field5++;
                        }

                    }
                    catch(PHPExcel_Reader_Exception $e) {
                        $errors[$num][$cell->getColumn()]['cell'] = $e->getMessage().'-->Error reading cell: '.$cell->getCoordinate();
                    }

                }
                $num++;
            }
            catch(PHPExcel_Reader_Exception $e) {
                $errors[$num]['row'] = $e->getMessage().'-->Error reading row: '.$cell->getRow();
            }

        }
        $fields_num = array($field1,$field2,$field3,$field4,$field5);
        for($i=0;$i<count($fields_num);$i++){
            $temp = $fields_num[$i];
            for($j=0;$j<count($fields_num);$j++){
                if($temp != $fields_num[$j]){
                    // echo $temp." ".$fields_num[$j]." ".$j;exit;
                    return false;
                }
            }
        }

        /*if(($field1 == $field2) && ($field2 == $field3)){
           return true;
        }
        else{
           return false;
        }*/
        return true;

    }
    public function countRows($objWorksheet){
        $num  = 0;
        foreach ($objWorksheet->getRowIterator() as $row) {
          $num++;
        }
        return $num;
    }
    public function readManuExcel($filename=false){

        $objReader = PHPExcel_IOFactory::createReader('Excel5');
        $objReader->setReadDataOnly(true);
        $data = array();
        $errors = array(); $errors[0]['error'] = "" ;
        $toSave = array();
        try{
            $filename = $filename?$filename:"starter_numbers.xls";
            $objPHPExcel = $objReader->load("files/card_linked_accounts/".$filename);
        }
        catch(PHPExcel_Reader_Exception $e) {
            $errors[0]['error'] =  $e->getMessage().'-->Error reading file: ';
            array_push($data,$errors);
            return $data;
        }
        $objWorksheet = $objPHPExcel->getActiveSheet();
        $num_rows =  $objWorksheet->getHighestRow();
        $high_columns = $objWorksheet->getHighestColumn();
        if($high_columns != 'E'){
            $errors[0]['error'] = "the file has more/less columns than needed";
            array_push($data,$errors);
            return $data;
        }
        //$this->countRows($objWorksheet);
        $fields = $this->checkManuFields($objWorksheet);
        if(!$fields){
            $errors[0]['error'] =  'the file has mismatched number of fields';
            array_push($data,$errors);
            return $data;
        }
        $num = 0;
        //echo '<table>' . "\n";
        foreach ($objWorksheet->getRowIterator() as $row) {
            //echo '<tr>' . "\n";
            try{
                if($row->getRowIndex() == 1){
                    $headers = true;//$this->checkManuHeaders($row);
                    if(!$headers){
                        $errors[$num]['row']=" the file has wrong headers";
                        array_push($data,$errors);
                        return $data;
                    }

                    continue;}
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(false);
                $num_cell = 1;
                foreach ($cellIterator as $cell) {
                    try{

                        if($cell->getColumn() == 'A'){
                            if(ctype_digit (trim($cell->getValue()))){
                                $data[$num]['acc_no'] = $cell->getValue();
                                $toSave[$num]['acc_no'] = $cell->getValue();
                            }
                            else{
                                $errors[$num_cell]['cell'] = "account number not numeric at A".$num_cell;
                                $num--;
                                continue;
                            }

                        }
                        if($cell->getColumn() == 'B'){
                            if(!ctype_digit (trim($cell->getValue()))){
                                $data[$num]['book_no'] = $cell->getValue();
                            }
                            else{
                                $errors[$num_cell]['cell'] = "OTP book number numeric";
                                unset($data[$num]['acc_no']);
                                unset($toSave[$num]['acc_no']);
                                $num--;
                                continue;
                            }
                        }
                        if($cell->getColumn() == 'C'){
                            if(!ctype_digit (trim($cell->getValue()))){
                                $data[$num]['referal_no'] = $cell->getValue();
                                $toSave[$num]['referal_no'] = $cell->getValue();
                            }
                            else{
                                $errors[$num_cell]['cell'] = "Refearal  number  numeric";
                                unset($data[$num]['acc_no']);
                                unset($toSave[$num]['acc_no']);
                                unset($data[$num]['book_no']);
                                unset($data[$num]['referal_no']);
                                unset($toSave[$num]['referal_no']);
                                $num--;
                                continue;
                            }
                        }
                        if($cell->getColumn() == 'D'){
                            if(!ctype_digit (trim($cell->getValue()))){
                                // $data[$num]['branch_id'] =  $this->authentication->get_branch_code($cell->getValue());
                                //$toSave[$num]['branch_id'] =  $this->authentication->get_branch_code($cell->getValue());
                                $data[$num]['starter_id'] = $cell->getValue();
                                $toSave[$num]['starter_id'] = $cell->getValue();
                            }
                            else{
                                $errors[$num_cell]['cell'] = "starter pack number  numeric";
                                unset($data[$num]['acc_no']);
                                unset($toSave[$num]['acc_no']);
                                unset($data[$num]['book_no']);
                                unset($data[$num]['referal_no']);
                                unset($toSave[$num]['starter_id']);
                                unset($data[$num]['starter_id']);

                                $num--;
                                continue;
                            }
                        }
                        if($cell->getColumn() == 'E'){
                            if(ctype_digit (trim($cell->getValue()))){
                                $data[$num]['card_no'] = $cell->getValue();
                                $toSave[$num]['card_no'] = $cell->getValue();
                            }
                            else{
                                $errors[$num_cell]['cell'] = "card number not numeric";
                                unset($data[$num]['acc_no']);
                                unset($toSave[$num]['acc_no']);
                                unset($data[$num]['book_no']);
                                unset($data[$num]['referal_no']);
                                unset($toSave[$num]['referal_no']);
                                unset($data[$num]['starter_id']);
                                unset($toSave[$num]['starter_id']);
                                unset($data[$num]['card_no']);
                                unset($toSave[$num]['card_no']);
                                $num--;
                                continue;
                            }
                        }

                    }
                    catch(PHPExcel_Reader_Exception $e) {
                        $errors[$num_cell]['cell'] = $e->getMessage().'-->Error reading cell: '.$cell->getCoordinate();
                    }
                    $num_cell++;
                }
                $num++;
            }
            catch(PHPExcel_Reader_Exception $e) {
                $errors[$num]['row'] = $e->getMessage().'-->Error reading row: '.$cell->getRow();
            }

        }
        $errors['read'] =  $num .' rows out of '. $num_rows.' read';
        $errors['percent'] = (($num/($num_rows-1))*100); 
        $db = $this->account->checkStarterPack($data);

        $errors['percent'] =  $db;


        array_push($data,$errors);
        return $data;
    }


    public function checkCardHeaders($row){

        $cellIterator = $row->getCellIterator();
        $cellIterator->setIterateOnlyExistingCells(false);
        $num = 0;
        try{

            foreach ($cellIterator as $cell) {
                if($cell->getColumn() == 'A'){
                    $headerA = $cell->getValue();
                    if($headerA !="Account No"){
                        return false;
                    }

                }
                if($cell->getColumn() == 'B'){
                    $headerB = $cell->getValue();
                    if($headerA !="Card No"){
                        return false;
                    }
                }
               $num++;
            }
        }
        catch(PHPExcel_Reader_Exception $e) {
            $errors[$num]['cell'] = $e->getMessage().'-->Error reading cell: ';
            return false;
        }
        return false;
    }
    public function  checkCardFields($objWorksheet){
        $num = 0;
        $field1 = 0; $field2 = 0;
        //echo '<table>' . "\n";
        foreach ($objWorksheet->getRowIterator() as $row) {
            //echo '<tr>' . "\n";
            try{
                if($row->getRowIndex() == 1){

                    continue;}
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(false);

                foreach ($cellIterator as $cell) {
                    try{

                        if($cell->getColumn() == 'A'){

                            if($cell->getValue() !=" ")
                                $field1++;
                        }
                        if($cell->getColumn() == 'B'){

                            if($cell->getValue() !=" ")
                                $field2++;
                        }

                    }
                    catch(PHPExcel_Reader_Exception $e) {
                        $errors[$num][$cell->getColumn()]['cell'] = $e->getMessage().'-->Error reading cell: '.$cell->getCoordinate();
                    }

                }
                $num++;
            }
            catch(PHPExcel_Reader_Exception $e) {
                $errors[$num]['row'] = $e->getMessage().'-->Error reading row: '.$cell->getRow();
            }

        }
        if(($field1 == $field2)){
            return true;
        }
        else{
            return false;
        }

    }
    public function uploadCardFile(){
        $errors = false;
        $feedback = false;
        if($_FILES){

            //$user_id = '05050';
            $user_id = $this->session->userdata('user_id');
            $name = $_FILES['userfile']['name'];
            $this->current_page = 'upload_file';
            $name = strtr($name, 'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ', 'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');

            // replace characters other than letters, numbers and . by _
            $name = preg_replace('/([^.a-z0-9]+)/i', '_', $name);
            $config['upload_path'] = 'files/card_fin/';
            $config['allowed_types'] = 'xls|xlsx';
            $config['max_size']	= '10000';
            $config['file_name'] = $name;
            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload())
            {
                $error = array('error' => $this->upload->display_errors('',''));
                //insert into upload table
                $dat = array('upload_name' => $name, 'uploaded_by' => $user_id, 'error_type' => 'error uploading');
                $uploaded = $this->account->saveUploadData($dat);
                //echo json_encode(array($error));
                $errors = $error;
            }
            else
            {
                $d = array();
                $data = $this->upload->data();
                $name = $data['file_name'];
                $this->current_page = 'upload_file';
                //insert into upload table
                $dat = array('upload_name' => $name, 'uploaded_by' => $user_id, 'error_type' => 'success');
                $uploaded = $this->account->saveUploadData($dat);
                $feedback = $this->readCardExcel($name);
                $info = new stdClass();
                $info->name = $name;
                $info->size = $data['file_size'];
                $info->type = $data['file_type'];
                $info->deleteUrl = "";
                $info->url = 'files/card_fin/'.$name;
                array_push($d,$info);
                array_push($d,$feedback);
                $errors = array_pop($feedback);
                //echo json_encode($d);
                //var_dump($errors);exit;

            }

            $this->name = 'upload fincard excel document';
            $this->current_page = 'upload_file';
            $this->blade->render('card_upload', array('title' => 'upload file','errors' =>$errors,'feedback' =>$feedback));


        }
        else{
            $this->name = 'upload fincard excel document';
            $this->current_page = 'upload_file';
            $this->blade->render('card_upload', array('title' => 'upload file','errors' =>$errors,'feedback' =>$feedback));
        }
    }
    public function readCardExcel($filename=false){
        $objReader = PHPExcel_IOFactory::createReader('Excel5');
        $objReader->setReadDataOnly(true);
        $data = array();
        $errors = array();$errors[0]['error'] = "";
        $filename = $filename?$filename:"card_numbers.xls";
        try{
            $objPHPExcel = $objReader->load("files/card_fin/".$filename);
        }
        catch(PHPExcel_Reader_Exception $e) {
            $errors[0]['error'] =  $e->getMessage().'-->Error reading file: ';

            array_push($data,$errors);
            return $data;
        }
        $objWorksheet = $objPHPExcel->getActiveSheet();
        $num_rows =  $objWorksheet->getHighestRow();
        $high_columns = $objWorksheet->getHighestColumn();
        if($high_columns != 'B'){
            $errors[0]['error'] = "the file has more/less columns than needed";
            array_push($data,$errors);
            return $data;
        }
        //$this->countRows($objWorksheet);
        $fields = $this->checkCardFields($objWorksheet);
        if(!$fields){
            $errors[0]['error'] =  'the file has mismatched number of fields';
            array_push($data,$errors);
            return $data;
        }
        $num = 0;
        //echo '<table>' . "\n";
        foreach ($objWorksheet->getRowIterator() as $row) {
            //echo '<tr>' . "\n";
            try{
                if($row->getRowIndex() == 1){
                    $headers = $this->checkCardHeaders($row);
                    if($headers){
                        $errors[$num]['row']=" the file has wrong headers";
                        array_push($data,$errors);
                        return $data;
                    }

                    continue;}
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(false);
                 $num_cell = 1;
                foreach ($cellIterator as $cell) {
                    try{
                        if($cell->getColumn() == 'A'){

                            if(ctype_digit (trim($cell->getValue()))){

                                $data[$num]['acc_no'] = $cell->getValue();
                            }
                            else{
                                $errors[$num_cell]['cell'] = "account number not numeric for A".$num_cell;
                                $num--;
                                continue;
                            }
                        }
                        if($cell->getColumn() == 'B'){

                            if(ctype_digit(trim($cell->getValue()))){

                                $data[$num]['card_no'] = $cell->getValue();
                            }
                            else{
                                $errors[$num_cell]['cell'] = "card number not numeric for B".$num_cell;
                                unset( $data[$num]['acc_no']);
                                $num--;
                                continue;
                            }
                        }
                    }
                    catch(PHPExcel_Reader_Exception $e) {
                        $errors[$num_cell]['cell'] = $e->getMessage().'-->Error reading cell: '.$cell->getCoordinate();
                    }
                    $num_cell++;
                }
                $num++;
            }
            catch(PHPExcel_Reader_Exception $e) {
                $errors[$num]['row'] = $e->getMessage().'-->Error reading row: '.$cell->getRow();
            }
            // echo '</tr>' . "\n";
        }
        //echo '</table>' . "\n";
        $errors[1]['error'] =  $num .' rows out of '. ($num_rows-1).' read';
        $errors['percent'] = (($num/($num_rows-1))*100);
        $errors['database'] = $this->account->saveCardNumbers($data);
        array_push($data,$errors);
        return $data;
        var_dump( $errors['database']);
        var_dump($data);exit;
    }
    public function readReferalExcel($filename=false){

        $objReader = PHPExcel_IOFactory::createReader('Excel5');
        $objReader->setReadDataOnly(true);
        $data = array();
        $errors = array(); $errors[0]['error'] = "" ;
        $toSave = array();
        try{
            $filename = $filename?$filename:"referals.time()".".xls";
            $objPHPExcel = $objReader->load("files/referals/".$filename);

        }
        catch(PHPExcel_Reader_Exception $e) {
            $errors[0]['error'] =  $e->getMessage().'-->Error reading file: ';
            array_push($data,$errors);
            return $data;
        }

        //////////////////////////////////////////////////////////////////
        $objPHPExcel->setActiveSheetIndex(0);
        $objWorksheet = $objPHPExcel->getActiveSheet();
        $num_rows =  $objWorksheet->getHighestRow();
        $high_columns = $objWorksheet->getHighestColumn();
        if($high_columns != 'B'){
            $errors[0]['error'] = "the file has more/less columns than needed";
            array_push($data,$errors);
            return $data;
        }

        $num = 0;
        $data = array();
        //echo '<table>' . "\n";
        foreach ($objWorksheet->getRowIterator() as $row) {
            //echo '<tr>' . "\n";
            try{
                if($row->getRowIndex() == 1){
                    $headers = true;//$this->checkOTPHeaders($row);

                    if(!$headers){
                        $errors[$num]['row']=" the file has wrong headers";
                        array_push($data,$errors);
                        return $data;
                    }

                    continue;}
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(false);
                $num_cell = 1;
                foreach ($cellIterator as $cell) {
                    try{

                        if($cell->getColumn() == 'A'){

                            if(!ctype_digit (trim($cell->getValue()))){
                                $data[$num]['referal_no'] = $cell->getValue();

                            }
                            else{
                                $errors[$num_cell]['cell'] = "book number not numeric at A".$num_cell;
                                $num--;
                                continue;
                            }

                        }
                        if($cell->getColumn() == 'B'){
                            if(ctype_digit (trim($cell->getValue()))){
                                $data[$num]['referal_coupons'] = $cell->getValue();

                            }
                            else{
                                $errors[$num_cell]['cell'] = "referal number not numeric";
                                unset($data[$num]['referal_no']);
                                $num--;
                                continue;
                            }
                        }


                    }
                    catch(PHPExcel_Reader_Exception $e) {
                        $errors[$num_cell]['cell'] = $e->getMessage().'-->Error reading cell: '.$cell->getCoordinate();
                    }
                    $num_cell++;
                }
                $num++;
            }
            catch(PHPExcel_Reader_Exception $e) {
                $errors[$num]['row'] = $e->getMessage().'-->Error reading row: '.$cell->getRow();
            }

        }

        //get referal books and validate number
        $ref = array();
        $valid_refs = array();
        for($i=0;$i<count($data);$i++){
            $ref[$i] = @$data[$i]['referal_no'];
        }
        $count_refs = @array_count_values($ref);
        $refs = array_keys($count_refs);
        for($i=0;$i<count($refs);$i++){
            if($count_refs[$refs[$i]] == 10){
                array_push($valid_refs,$refs[$i]);
            }
            else{

            }
        }


        $db = $this->account->updateReferals($data,$valid_refs);
        $numbers = array_pop($db);
        $errors['percent'] = @$numbers['refs']." referal books out of ". count($count_refs)." read";
        //$errors['database'] = $db;
        array_push($data,$errors);
        return $data;
    }
    public function uploadReferalFile(){
            $errors = false;
            $this->active = 'act_link';
            if($_FILES){
                $user_id = $this->session->userdata('user_id');
                $name = $_FILES['userfile']['name'];

                $name = strtr($name, 'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ', 'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');

                // replace characters other than letters, numbers and . by _
                $name = preg_replace('/([^.a-z0-9]+)/i', '_', $name);
                $config['upload_path'] = 'files/referals/';
                $config['allowed_types'] = 'xls|xlsx';
                $config['max_size']	= '10000';
                $config['file_name'] = $name;
                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload())
                {
                    $error = array('error' => $this->upload->display_errors('',''));
                    //insert into upload table
                    $dat = array('upload_name' => $name, 'uploaded_by' => $user_id, 'error_type' => 'error uploading file');
                    $uploaded = $this->account->saveUploadData($dat);
                    $errors = $error;

                }
                else
                {
                    $d = array();

                    $data = $this->upload->data();
                    $name = $data['file_name'];
                    $feedback = $this->readReferalExcel($name);
                    //insert into upload table
                    $dat = array('upload_name' => $name, 'uploaded_by' => $user_id, 'error_type' => 'success');
                    $uploaded = $this->account->saveUploadData($dat);
                    $info = new stdClass();
                    $info->name = $name;
                    $info->size = $data['file_size'];
                    $info->type = $data['file_type'];
                    $info->deleteUrl = "";
                    $info->url = 'files/referals/'.$name;
                    array_push($d,$info);
                    array_push($d,$feedback);
                    $errors = array_pop($feedback);
                    //echo json_encode($d);

                }
                $this->current_page = 'upload_file';
                $this->name = 'validate referal books printed by manufacturer here';
                $this->blade->render('referal_upload', array('title' => 'upload file','errors' => $errors));

            }
            else{
                $this->current_page = 'upload_file';
                $this->name = 'validate referal books printed by manufacturer here';
                $this->blade->render('referal_upload', array('title' => 'upload file','errors' => $errors));
            }
        }
    public function readOTPExcel($filename=false){

        $objReader = PHPExcel_IOFactory::createReader('Excel5');
        $objReader->setReadDataOnly(true);
        $data = array();
        $errors = array(); $errors[0]['error'] = "" ;
        $toSave = array();
        try{
            $filename = $filename?$filename:"otp".time().".xls";
            $objPHPExcel = $objReader->load("files/otp/".$filename);

        }
        catch(PHPExcel_Reader_Exception $e) {
            $errors[0]['error'] =  $e->getMessage().'-->Error reading file: ';
            array_push($data,$errors);
            return $data;
        }
        $objPHPExcel->setActiveSheetIndex(0);
        $objWorksheet = $objPHPExcel->getActiveSheet();
        $num_rows =  $objWorksheet->getHighestRow();
        $high_columns = $objWorksheet->getHighestColumn();
        if($high_columns != 'B'){
            $errors[0]['error'] = "the file has more/less columns than needed";
            array_push($data,$errors);
            return $data;
        }
        //$this->countRows($objWorksheet);
        $fields = $this->checkOTPFields($objWorksheet);
        if(!$fields){
            $errors[0]['error'] =  'the file has mismatched number of fields';
            array_push($data,$errors);
            return $data;
        }
        $num = 0;


        foreach ($objWorksheet->getRowIterator() as $row) {

            try{
                if($row->getRowIndex() == 1){
                    $headers = true;//$this->checkOTPHeaders($row);
                    if(!$headers){
                        $errors[$num]['row']=" the file has wrong headers";
                        array_push($data,$errors);
                        return $data;
                    }

                    continue;}
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(false);
                $num_cell = 1;
                foreach ($cellIterator as $cell) {
                    try{

                        if($cell->getColumn() == 'A'){
                            if(!ctype_digit (trim($cell->getValue()))){
                                $data[$num]['book_no'] = $cell->getValue();

                            }
                            else{
                                $errors[$num_cell]['cell'] = "book number not numeric at A".$num_cell;
                                $num--;
                                continue;
                            }

                        }
                        if($cell->getColumn() == 'B'){
                            if(ctype_digit (trim($cell->getValue()))){
                                $data[$num]['trans_pin'] = $cell->getValue();

                            }
                            else{
                                $errors[$num_cell]['cell'] = "referal number not numeric";
                                unset($data[$num]['book_no']);

                                $num--;
                                continue;
                            }
                        }


                    }
                    catch(PHPExcel_Reader_Exception $e) {
                        $errors[$num_cell]['cell'] = $e->getMessage().'-->Error reading cell: '.$cell->getCoordinate();
                    }
                    $num_cell++;
                }
                $num++;
            }
            catch(PHPExcel_Reader_Exception $e) {
                $errors[$num]['row'] = $e->getMessage().'-->Error reading row: '.$cell->getRow();
            }

        }
        $total_rows = $num_rows;

        //get books and validate number
        $dut = array();
        $valid_books = array();
        for($i=0;$i<count($data);$i++){
            $dut[$i] = @$data[$i]['book_no'];
        }
        $count_books = @array_count_values($dut);
        $books = array_keys($count_books);
        for($i=0;$i<count($books);$i++){
            if($count_books[$books[$i]] == 50){
                array_push($valid_books,$books[$i]);
            }
            else{

            }
        }

        $db = $this->account->updateOTP($data,$valid_books);
        $numbers = array_pop($db);
        $errors['percent'] =  @$numbers['books']." otp books out of ". count($count_books)." read";
        //$errors['database'] = $db;
        array_push($data,$errors);
        return $data;
    }
    public function uploadOTPFile(){
        $errors = false;
        $this->active = 'act_link';
        if($_FILES){
            $user_id = $this->session->userdata('user_id');
            $name = $_FILES['userfile']['name'];

            $name = strtr($name, 'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ', 'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');

            // replace characters other than letters, numbers and . by _
            $name = preg_replace('/([^.a-z0-9]+)/i', '_', $name);
            $config['upload_path'] = 'files/otp/';
            $config['allowed_types'] = 'xls|xlsx';
            $config['max_size']	= '10000';
            $config['file_name'] = $name;
            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload())
            {
                $error = array('error' => $this->upload->display_errors('',''));
                //insert into upload table
                $dat = array('upload_name' => $name, 'uploaded_by' => $user_id, 'error_type' => 'error uploading file');
                $uploaded = $this->account->saveUploadData($dat);
                $errors = $error;

            }
            else
            {
                $d = array();

                $data = $this->upload->data();
                $name = $data['file_name'];
                $feedback = $this->readOTPExcel($name);
                //insert into upload table
                $dat = array('upload_name' => $name, 'uploaded_by' => $user_id, 'error_type' => 'success');
                $uploaded = $this->account->saveUploadData($dat);
                $info = new stdClass();
                $info->name = $name;
                $info->size = $data['file_size'];
                $info->type = $data['file_type'];
                $info->deleteUrl = "";
                $info->url = 'files/otp/'.$name;
                array_push($d,$info);
                array_push($d,$feedback);
                $errors = array_pop($feedback);
                //echo json_encode($d);

            }
            $this->current_page = 'upload_file';
            $this->name = 'validate books printed by manufacturer here';
            $this->blade->render('otp_upload', array('title' => 'upload file','errors' => $errors));

        }
        else{
            $this->current_page = 'upload_file';
            $this->name = 'validate books printed by manufacturer here';
            $this->blade->render('otp_upload', array('title' => 'upload file','errors' => $errors));
        }
    }
    public function uploadCardLinkedFile(){
        $errors = false;
        $this->active = 'act_sp';
        if($_FILES){
            $user_id = $this->session->userdata('user_id');
            $name = $_FILES['userfile']['name'];

            $name = strtr($name, 'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ', 'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');

            // replace characters other than letters, numbers and . by _
            $name = preg_replace('/([^.a-z0-9]+)/i', '_', $name);
            $config['upload_path'] = 'files/card_linked_accounts/';
            $config['allowed_types'] = 'xls|xlsx';
            $config['max_size']	= '10000';
            $config['file_name'] = $name;
            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload())
            {
                $error = array('error' => $this->upload->display_errors('',''));
                //insert into upload table
                $dat = array('upload_name' => $name, 'uploaded_by' => $user_id, 'error_type' => 'error uploading file');
                $uploaded = $this->account->saveUploadData($dat);
                $errors = $error;

            }
            else
            {
                $d = array();

                $data = $this->upload->data();
                $name = $data['file_name'];
                $feedback = $this->readCardLinkedExcel($name);
                //insert into upload table
                $dat = array('upload_name' => $name, 'uploaded_by' => $user_id, 'error_type' => 'success');
                $uploaded = $this->account->saveUploadData($dat);
                $info = new stdClass();
                $info->name = $name;
                $info->size = $data['file_size'];
                $info->type = $data['file_type'];
                $info->deleteUrl = "";
                $info->url = 'files/card_linked_accounts/'.$name;
                array_push($d,$info);
                array_push($d,$feedback);
                $errors = array_pop($feedback); var_dump($errors);exit;
                //echo json_encode($d);

            }
            $this->current_page = 'upload_file';
            $this->name = 'upload Referals excel';
            $this->blade->render('card_linked_upload', array('title' => 'upload file','errors' => $errors));

        }
        else{
            $this->current_page = 'upload_file';
            $this->name = 'upload Starterpack excel';
            $this->blade->render('card_linked_upload', array('title' => 'upload file','errors' => $errors));
        }
    }
    public function readCardLinkedExcel($filename=false){
        $objReader = PHPExcel_IOFactory::createReader('Excel5');
        $objReader->setReadDataOnly(true);
        $data = array();
        $errors = array();$errors[0]['error'] = "";
        $filename = $filename?$filename:"linked-carded-accounts_".time().".xls";
        try{
            $objPHPExcel = $objReader->load("files/card_linked_accounts/".$filename);
        }
        catch(PHPExcel_Reader_Exception $e) {
            $errors[0]['error'] =  $e->getMessage().'-->Error reading file: ';

            array_push($data,$errors);
            return $data;
        }
        $objWorksheet = $objPHPExcel->getActiveSheet();
        $num_rows =  $objWorksheet->getHighestRow();
        $high_columns = $objWorksheet->getHighestColumn();
        if($high_columns != 'E'){
            $errors[0]['error'] = "the file has more/less columns than needed";
            array_push($data,$errors);
            return $data;
        }
        //$this->countRows($objWorksheet);
        $fields = $this->checkCardFields($objWorksheet);
        if(!$fields){
            $errors[0]['error'] =  'the file has mismatched number of fields';
            array_push($data,$errors);
            return $data;
        }
        $num = 0;
        //echo '<table>' . "\n";
        foreach ($objWorksheet->getRowIterator() as $row) {
            //echo '<tr>' . "\n";
            try{
                if($row->getRowIndex() == 1){
                    $headers = true;//$this->checkCardHeaders($row);
                    if(!$headers){
                        $errors[$num]['row']=" the file has wrong headers";
                        array_push($data,$errors);
                        return $data;
                    }

                    continue;}
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(false);
                $num_cell = 1;
                foreach ($cellIterator as $cell) {
                    try{
                        if($cell->getColumn() == 'A'){

                            if(ctype_digit (trim($cell->getValue()))){

                                $data[$num]['acc_no'] = $cell->getValue();
                            }
                            else{
                                $errors[$num_cell]['cell'] = "account number not numeric for A".$num_cell;
                                $num--;
                                continue;
                            }
                        }
                        if($cell->getColumn() == 'B'){

                            if(!ctype_digit(trim($cell->getValue()))){

                                $data[$num]['book_no'] = $cell->getValue();
                            }
                            else{
                                $errors[$num_cell]['cell'] = "book number  numeric for B".$num_cell;
                                unset( $data[$num]['acc_no']);
                                $num--;
                                continue;
                            }
                        }
                        if($cell->getColumn() == 'C'){

                            if(!ctype_digit(trim($cell->getValue()))){

                                $data[$num]['referal_no'] = $cell->getValue();
                            }
                            else{
                                $errors[$num_cell]['cell'] = "book number  numeric for C".$num_cell;
                                unset( $data[$num]['acc_no']);
                                unset( $data[$num]['book_no']);
                                $num--;
                                continue;
                            }
                        }
                        if($cell->getColumn() == 'D'){

                            if(!ctype_digit(trim($cell->getValue()))){

                                $data[$num]['starterpack_id'] = $cell->getValue();
                            }
                            else{
                                $errors[$num_cell]['cell'] = "Starterpack id number  numeric for D".$num_cell;
                                unset( $data[$num]['acc_no']);
                                unset( $data[$num]['referal_no']);
                                unset( $data[$num]['book_no']);
                                $num--;
                                continue;
                            }
                        }
                        if($cell->getColumn() == 'E'){

                            if(!ctype_digit(trim($cell->getValue()))){

                                $data[$num]['card_no'] = $cell->getValue();
                            }
                            else{
                                $errors[$num_cell]['cell'] = "book number  numeric for D".$num_cell;
                                unset( $data[$num]['acc_no']);
                                unset( $data[$num]['card_no']);
                                unset( $data[$num]['referal_no']);
                                unset( $data[$num]['book_no']);
                                $num--;
                                continue;
                            }
                        }
                    }
                    catch(PHPExcel_Reader_Exception $e) {
                        $errors[$num_cell]['cell'] = $e->getMessage().'-->Error reading cell: '.$cell->getCoordinate();
                    }
                    $num_cell++;
                }
                $num++;
            }
            catch(PHPExcel_Reader_Exception $e) {
                $errors[$num]['row'] = $e->getMessage().'-->Error reading row: '.$cell->getRow();
            }

        }
        //echo '</table>' . "\n";
        $errors[1]['error'] =  $num .' rows out of '. ($num_rows-1).' read';
        $errors['percent'] = (($num/($num_rows-1))*100);
        $errors['database'] = $this->account->checkStarterPack($data);
        array_push($data,$errors); var_dump($data);exit;
        return $data;

    }
}
