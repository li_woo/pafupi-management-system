<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once 'pafupi.php';
require_once APPPATH.'libraries/Common_Tasks.php';
require_once APPPATH.'libraries/Messages.php';
require_once 'Security_check.php';
class User extends CI_Controller {
    public $active;
    public $role_id;
    public $name;
    public $username;
    public $current_page;
    public function __construct(){
        parent::__construct();
        $this->load->library('blade');

        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model('user_model','user');
        $this->load->helper('url');
        //load the authentication model
        $this->load->model('authentication');
        $this->blade->set('base_url',BASEURL);
        $this->username = $this->session->userdata('username');
        if(empty($this->username))
            $this->username = false;
    }
    public function  index(){
        $this->name = 'profile management';
        $this->current_page='profile';
        $this->active ='act_profile';

        $this->role_id = 1;
        $this->blade->render('profile', array('title' => 'Home'));
    }
    private function create_session($username)
    {

        /* this method creates session variables upon successful login */

        //get user details
        $user_details = $this->authentication->get_details($username);

        //get the branch code for the branch
        $branch_name = $this->authentication->get_branch($user_details['Branch']);

        $this->session->set_userdata('username',$user_details['Username']);
        $this->session->set_userdata('role_id',$user_details['UserRoleID']);
        $this->session->set_userdata('user_id',$user_details['UserID']);
        $this->session->set_userdata('login_flag','Online');
        $this->session->set_userdata('fullname',$user_details['FullName']);
        $this->session->set_userdata('last_login',$user_details['LastLogin']);
        $this->session->set_userdata('last_pwd_change',$user_details['LastPasswordChange']);
        $this->session->set_userdata('branch',$user_details['Branch']);
        $this->session->set_userdata('role',$user_details['RoleName']);
        $this->session->set_userdata('branch_name',$branch_name);
        $this->session->set_userdata('notif',1);

    }
    public function authenticate_user()
    {
        $this->active = 'act_profile';

        $this->form_validation->set_rules('username','Username','required|trim|apha');

        if($this->form_validation->run() == FALSE)
        {

            $feedback = Messages::get_message('ERROR','MISSING_FIELDS');
            pafupi::index($feedback);

        }

        else
        {

            //get the username and password from the form
            $username = trim($this->input->post('username'));
            $password = $this->input->post('password');

            if(preg_match('/[^a-z0-9\-\_\.]+/i',$username))
            {
                // this block will execute when an invalid username is entered
                $feedback = "Wrong username format";
                pafupi::index($feedback);
            }

            else
            {

                //this method authenticates the user
                $result = $this->authentication->check_login($username,$password);

                if($result == 'A')
                {

                    //initialise the session
                    $this->create_session($username);

                    //check if its the first login or not
                    $login_check = $this->authentication->first_login($username);

                    if($login_check == false)
                    {

                        //this is not a first login
                        //now load the home page
                       redirect('pafupi/index');
                       // echo "S";

                    }

                    else
                    {

                        //first login, prompt the user to change their password
                        $this->session->set_userdata('status',2);
                        //pafupi::main('password','act_password','Change_Password',NULL,NULL,'first_login');
                        // echo "C";
                        $feedback ="Please reset your password";
                        pafupi::main('password_reset',$feedback);


                    }
                }

                elseif($result == 'L')
                {
                    //login details were correct but the account is locked out
                    $feedback = Messages::get_message('ERROR','LOCKOUT');
                    pafupi::index($feedback);
                    //echo "L";
                }

                elseif($result == 'I')
                {
                    //login details were correct but the account is inactive
                     $feedback = Messages::get_message('ERROR','INACTIVE');
                     pafupi::index($feedback);
                     //echo "I";
                }

                elseif($result == 'E')
                {

                    //initialise the session
                    $this->create_session($username);

                    //expired password, prompt the user to change their password
                    //EazyMobile::main('home','act_home','Home',NULL,NULL,NULL,NULL);
                    //echo "E";
                    $feedback ="Password expired.Please reset your password";
                    pafupi::main('password_reset',$feedback);

                }

                elseif($result == 'W')
                {

                    //if the wrong password was provided
                   // EazyMobile::index(Message::get_message('ERROR','BAD_LOGIN'));
                    $feedback = Messages::get_message('ERROR','BAD_LOGIN');
                    pafupi::index($feedback);

                }
            }
        }
    }
    public function redirect_login() {

        pafupi::main('home','act_home');

    }
    public function changePassword($username=false){
        Security_check::check_login();
        $this->name = 'Password Reset';
        $username = $this->session->userdata('username');
        $user_id = $this->session->userdata('user_id');
        $fullname = $this->session->userdata('fullname');
        $this->blade->render('password_reset', array('title' => 'Password Reset'));
    }
    public function reset_user_password(){

       $data = $this->security->xss_clean($this->input->post());
       $result = $this->user->reset_password($data['user_id'],$data['password']);
       echo json_encode($result);

    }
    public function change_password_action()
    {
        Security_check::check_login();
        //get the username
        $username = $this->session->userdata('username');

        //this method allows the user to change their password
        //first things first. the usual, validation
        $this->form_validation->set_rules('current_password','Current Password','required');
        $this->form_validation->set_rules('new_password','New Password','required|min_length[6]');
        $this->form_validation->set_rules('confirm_password','Confirm New Password','required|matches[new_password]|callback_password_check');

        if($this->form_validation->run() == FALSE)
        {

           //$this->session->set_userdata('password_change',true);
            //EazyMobile::main('password','act_password','Change_Password');
            $this->form_validation->set_error_delimiters('','');
            $error_msg = validation_errors();// var_dump($error_msg);exit;
            pafupi::main('password_reset',$error_msg);
            //redirect('pafupi/main/password_reset/'.$error_msg);
        }

        else {

            //now we can call the method in the model to change the user's password
            $result = $this->authentication->change_password($username);

            if($result == 0)
            {
                $this->session->set_userdata('password_change',true);
                $error_msg = Messages::get_message('ERROR','PASSWORD_FAIL');
                pafupi::main('password_reset',$error_msg);
               // redirect('pafupi/main/password_reset/'.$error_msg);

            }

            else if($result == 2)
            {

                $this->session->set_userdata('password_change',true);
                $error_msg = Messages::get_message('ERROR','PASSWORD_SAME');
                pafupi::main('password_reset',$error_msg);
                //redirect('pafupi/main/password_reset/'.$error_msg);


            }

            else if($result == 1)
            {
                $this->session->set_userdata('password_change',true);
                $feedback = "Password successfully changed";
                $this->logout($feedback);
            }

        }


    }
    public function logout($feedback=false){
        //call the method that changes the login flag in the model
        $this->authentication->flag_change($this->session->userdata('username'));

        //update the last_login field
        $this->authentication->update_login($this->session->userdata('username'));

        //destroy the session
        $this->session->sess_destroy();

        //$this->Workflow_tasks_3->purge_workflow();

        //disconnect from the database
        $this->db->close();

        //redirect to the login page
        //redirect('pafupi/index/'.$feedback);
        pafupi::index($feedback);
    }
    public function show_errors(){
        $this->blade->render('errors', array('title' => 'Error'));
    }

}