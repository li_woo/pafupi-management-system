<?php

require_once APPPATH."controllers/user.php";
require_once 'Security_check.php';
require_once 'pafupi.php';
class support_staff extends User {
     public $active;
     public $role_id;
     public $name;
     public $current_page;
     public function __construct(){
         parent::__construct();
         $this->load->library('pagination');
         $this->load->library('blade');
         $this->load->library('session');
         $this->blade->set('base_url',BASEURL);
         $this->load->model('account_m_model','account');
         $this->load->model('user_model','user');
         $this->load->helper('url');
         $this->load->model('authentication');
         $this->role_id = $this->session->userdata('role_id');
         if(empty($this->role_id))
             $this->role_id = false;
         Security_check::check_login();

         //$this->load->library('../controllers/Security_check');
         //$this->Security_check->check_login();

     }
    public function registerUser($data=false){
        if($this->role_id == 1){
        if(!$data){
        $this->current_page='create_user';
        $this->name = 'Create user';
        $this->active = 'act_profile';
        $branches = $this->account->getBranches();
        $roles = $this->user->getRoles();
        $this->blade->set('branches',$branches)->set('roles',$roles)->render('create_user', array('title' => 'create user'));
        }
        else{
          //echo json_encode($data);
          $data =$this->user->createUser($data);
           echo json_encode($data);
           }
        }
       else{
           pafupi::index();
        }
    }
    public function deleteUser(){
        if(@$_POST['selected_users']){
            if($this->role_id == 1){
            $data = $this->security->xss_clean($this->input->post());
            $result = $this->user->delete_users($data['selected_users']);
            echo json_encode($result);
            }
            else{
                echo json_encode(false);
            }
        }
        elseif($this->security->xss_clean($this->input->post('search'))){

        }

        else{
        if($this->role_id == 1){
        $num_rows = $this->pagination($this->user->get_no_users());
        $data["total_rows"] = $num_rows;
        $this->active = 'act_profile';
        $this->name ='select users to delete';
        $data['users']            = $num_rows;
        $config['base_url']       = BASEURL.'pafupi/main/delete_user';
        $config['total_rows']     = $num_rows;
        $config['per_page']       = 3;
        $config['num_links']      = 3;
        $config['last_link']      = FALSE;
        $config['first_link']     = FALSE;
        $config['uri_segment']    = 4;
        $config['next_link']      = 'Next';
        $config['next_tag_open']  = "<td class='pagination-item-outer-rght'>";
        $config['next_tag_close'] = "</td'>";
        $config['prev_link']      = 'Previous';
        $config['prev_tag_open']  = "<td class='pagination-item-outer-lft'>";
        $config['prev_tag_close'] = "</td>";
        $config['num_tag_open']   = "<td class='pagination-item'>";
        $config['num_tag_close']  = "</td>";
        $config['cur_tag_open']   = "<td class='pagination-item-current'>";
        $config['cur_tag_close']  = "</td>";

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data['result']     = $this->user->get_all_users($config['per_page'],$page);

        //var_dump($data['result']);
        $this->blade->set('users',$data['result'])->render('delete_user', array('title' => 'delete users'));
        }
        else{
            pafupi::index();
         }
        }
    }
    public function lockUser(){
        $search_feedback = false;
        $feedback = false;
        if(@$_POST['selected_users']){
            if($this->role_id == 1){
            $data = $this->security->xss_clean($this->input->post());
            $result = $this->user->lock_users($data['selected_users']);
            echo json_encode($result);
            }
            else{
                echo json_encode(false);
            }
        }
        elseif($this->security->xss_clean($this->input->post('search'))){
           $parameter = $this->security->xss_clean($this->input->post('param'));
           $data = $this->user->search_unlock($parameter);
           $this->name =  "lock users";
           $this->active = 'act_profile';
           $search_feedback = "search for ".$search." ".$data['total']." users found";

           $this->blade->set('users',$data['result'])->render('lock_user', array('title' => 'lock users','feedback'=>$feedback,'search_feedback'=>$search_feedback));
           return;
        }
        else{
            if($this->role_id == 1){
                $num_rows = $this->pagination($this->user->get_unlocked_users());
                $this->active = 'act_profile';
                $this->name ='select users to lock';
                $data['users']            = $num_rows;
                $config['base_url']       = BASEURL.'pafupi/main/lock_user';
                $config['total_rows']     = $num_rows;
                $config['per_page']       = 3;
                $config['num_links']      = 3;
                $config['last_link']      = FALSE;
                $config['first_link']     = FALSE;
                $config['uri_segment']    = 4;
                $config['next_link']      = 'Next';
                $config['next_tag_open']  = "<td class='pagination-item-outer-rght'>";
                $config['next_tag_close'] = "</td'>";
                $config['prev_link']      = 'Previous';
                $config['prev_tag_open']  = "<td class='pagination-item-outer-lft'>";
                $config['prev_tag_close'] = "</td>";
                $config['num_tag_open']   = "<td class='pagination-item'>";
                $config['num_tag_close']  = "</td>";
                $config['cur_tag_open']   = "<td class='pagination-item-current'>";
                $config['cur_tag_close']  = "</td>";

                $this->pagination->initialize($config);
                $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

                $data['result']     = $this->user->get_all_unlocked_users($config['per_page'],$page);
                $data["total_rows"] = $num_rows;
                //var_dump($data['result']);
                $this->blade->set('users',$data['result'])->render('lock_user', array('title' => 'lock users','feedback'=>$feedback,'search_feedback'=>$search_feedback));
            }
            else{
                pafupi::index();
            }

        }

    }
    public function unlockUser(){

        $search_feedback = false;
        $feedback =false;
        if($this->security->xss_clean($this->input->post('selected_users'))){

            if($this->role_id == 1){
            $data = $this->security->xss_clean($this->input->post());
            $result = $this->user->unlock_users($data['selected_users']);
            echo json_encode($result);
            }
            else{
                echo json_encode(false);
            }
        }
        elseif($this->security->xss_clean($this->input->post('search'))){
            $parameter = $this->security->xss_clean($this->input->post('param'));
            $data = $this->user->search_unlock($parameter);
            $this->name =  "lock users";
            $this->active = 'act_profile';
            $search_feedback =  "search for ".$search." ".$data['total']." users found";

            $this->blade->set('users',$data['result'])->render('unlock_users', array('title' => 'lock users','feedback'=>$feedback,'search_feedback'=>$search_feedback));
            return;
        }
        else{
            if($this->role_id == 1){
            $num_rows = $this->pagination($this->user->get_locked_users());
            $this->active = 'act_profile';
            $this->name ='select users to unlock';
            $data['users']            = $num_rows;
            $config['base_url']       = BASEURL.'pafupi/main/unlock_user';
            $config['total_rows']     = $num_rows;
            $config['per_page']       = 3;
            $config['num_links']      = 3;
            $config['last_link']      = FALSE;
            $config['first_link']     = FALSE;
            $config['uri_segment']    = 4;
            $config['next_link']      = 'Next';
            $config['next_tag_open']  = "<td class='pagination-item-outer-rght'>";
            $config['next_tag_close'] = "</td'>";
            $config['prev_link']      = 'Previous';
            $config['prev_tag_open']  = "<td class='pagination-item-outer-lft'>";
            $config['prev_tag_close'] = "</td>";
            $config['num_tag_open']   = "<td class='pagination-item'>";
            $config['num_tag_close']  = "</td>";
            $config['cur_tag_open']   = "<td class='pagination-item-current'>";
            $config['cur_tag_close']  = "</td>";

            $this->pagination->initialize($config);
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

            $data['result']     = $this->user->get_all_locked_users($config['per_page'],$page);
            $data["total_rows"] = $num_rows;
            //var_dump($data['result']);
                $this->blade->set('users',$data['result'])->render('unlock_users', array('title' => 'unlock users','feedback'=>$feedback,'search_feedback'=>$search_feedback));
            }
            else{
                pafupi::index();
            }
        }
    }
    public function modifyUser(){
        $search_feedback = false;
        $feedback = false;
        if(@$_POST['selected_users']){
            if($this->role_id == 1){
            $data = $this->security->xss_clean($this->input->post());
            $result = $this->user->unlock_users($data['selected_users']);
            echo json_encode($result);
           }
           else{
               echo json_encode(false);
           }

        }
        elseif($this->security->xss_clean($this->input->post('search'))){
            $search = $this->security->xss_clean($this->input->post('param'));
            //$search_type = $this->security->xss_clean($this->input->post('search_type'));
            $data =$this->user->search_modify($search);
            $this->active = 'act_profile';
            $this->current_page='modify_user';
            $this->name ='select user to edit';
            $search_feedback =  "search for ".$search." ".$data['total']." users found";

            $this->blade->set('users',$data['result'])->render('modify_users', array('title' => 'modify users','feedback'=>$feedback,'search_feedback'=>$search_feedback));
            return;

        }

        else{
            if($this->role_id == 1){
            $num_rows = $this->pagination($this->user->get_no_users());
            $this->active = 'act_profile';
            $this->current_page='modify_user';
            $this->name ='select user to edit';
            $data['users']            = $num_rows;
            $config['base_url']       = BASEURL.'pafupi/main/modify_user';
            $config['total_rows']     = $num_rows;
            $config['per_page']       = 3;
            $config['num_links']      = 3;
            $config['last_link']      = FALSE;
            $config['first_link']     = FALSE;
            $config['uri_segment']    = 4;
            $config['next_link']      = 'Next';
            $config['next_tag_open']  = "<td class='pagination-item-outer-rght'>";
            $config['next_tag_close'] = "</td'>";
            $config['prev_link']      = 'Previous';
            $config['prev_tag_open']  = "<td class='pagination-item-outer-lft'>";
            $config['prev_tag_close'] = "</td>";
            $config['num_tag_open']   = "<td class='pagination-item'>";
            $config['num_tag_close']  = "</td>";
            $config['cur_tag_open']   = "<td class='pagination-item-current'>";
            $config['cur_tag_close']  = "</td>";

            $this->pagination->initialize($config);
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

            $data['result']     = $this->user->get_all_users($config['per_page'],$page);;
            $data["total_rows"] = $num_rows;
            //var_dump($data['result']);
            $branches = $this->account->getBranches();
            $roles = $this->user->getRoles();
            $this->blade->set('users',$data['result'])->set('roles',$roles)->set('branches',$branches)->render('modify_users', array('title' => 'edit users','feedback'=>$feedback,'search_feedback'=>$search_feedback));
            }
            else{
                pafupi::index();
            }
        }
    }
    public function getUser($user_id=false){
        if(!$user_id)
        $user_id = $this->input->post('UserID');
        $users = $this->user->get_user_info($user_id);
        if(!empty($users)){
        $user = explode(' ',$users[0]['FullName']);
        $users[0]['fname'] = $user[0];
        $users[0]['lname'] = $user[1];
        }
        echo json_encode($users);

    }
    public function searchUser(){
        $search = $this->security->xss_clean($this->input->post('search_string'));
        $search_type = $this->security->xss_clean($this->input->post('search_type'));

        if($search_type == 'L'){
            //lock account we can unlock
           $result = $this->user->search_unlock($search);
           echo json_encode($result);
        }
        elseif($search_type=='E'){

            $result =$this->user->search_modify($search);
            echo json_encode($result);

        }
        elseif($search_type=='U'){
            $result = $this->user->search_lock($search);
            echo json_encode($result);

        }
    }
    public function saveUser(){
        $user_id = $this->security->xss_clean($this->input->post('user_id'));
        $data = $this->security->xss_clean($this->input->post());
        //echo $user_id;
        $result = $this->user->update_user($user_id,$data);
        if($result)
         $this->getUser($user_id);
        else
             echo json_encode(false);
    }
    private function pagination($total_rows)
    {
        //fixes an issue with pagination in CI :)
        if($total_rows > 1)
        {

            $total_rows = $total_rows + 1;

        }

        else
            $total_rows + 0;


        return $total_rows;

    }
} 