<?php
/**
 * Created by PhpStorm.
 * User: tnm180
 * Date: 6/20/14
 * Time: 6:22 PM
 */
require_once APPPATH."controllers/user.php";
require_once 'Security_check.php';
require_once 'pafupi.php';
class branch_admin extends User {
    public $active;
    public $role_id;
    public $name;
    public $current_page;
    public function __construct(){
        parent::__construct();

        $this->load->library('pagination');
        $this->load->library('blade');
        $this->load->library('session');
        $this->blade->set('base_url',BASEURL);
        $this->load->model('account_m_model','account');
        $this->load->model('user_model','user');
        $this->load->model('customer_model','customer');
        $this->load->helper('url');
        $this->load->model('authentication');
        $this->role_id = $this->session->userdata('role_id');
        if(empty($this->role_id))
            $this->role_id = false;
        else{
            if($this->role_id == 2){

            }
            else{
                 Pafupi::index();
            }
        }
        Security_check::check_login();

    }
    public function viewHome(){
        if($this->role_id == 2){

        }
        else{

        }
    }
    public function uploadImages($cust_id=false){
        $errors = false;
        if($_FILES){
            $action_type = $this->security->xss_clean($this->input->post('type'));
            $cust_id = $this->security->xss_clean($this->input->post('cust_id'));
            $type = end(explode('.',$_FILES['userfile']['name']));

            if($action_type = 'ID'){
              $name = $cust_id."_ID.".$type;
            }
            elseif($action_type = 'FACE'){
                $name = $cust_id."_FACE.".$type;
            }
            else{
                $name = $cust_id."_ID.".$type;
            }
            $user_id = $this->session->userdata('user_id');
            //Format the name
            //$name = $_FILES['userfile']['name'];

            $name = strtr($name, 'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ', 'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');

            // replace characters other than letters, numbers and . by _
            $name = preg_replace('/([^.a-z0-9]+)/i', '_', $name);

            //Your upload directory, see CI user guide
            $config['upload_path'] = 'files/customer_kyc';

            $config['allowed_types'] = 'gif|jpg|png|JPG|GIF|PNG';
            $config['max_size'] = '2048';
            $config['file_name'] = $name;

            //Load the upload library

            $db_errors= array();

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload())
            {
                $error = array('error' => $this->upload->display_errors('',''));
                //insert into upload table
                $dat = array('upload_name' => $name, 'uploaded_by' => $user_id, 'error_type' => 'error uploading file');
                $uploaded = $this->account->saveUploadData($dat);
                $errors = $error;
            }
            else
            {
                $d = array();
                $data = $this->upload->data();
                $name = $data['file_name'];


                $config['image_library'] = 'gd2';
                $config['new_image'] ='files/customer_kyc/'.$name;
                $config['source_image'] = 'files/customer_kyc/'.$name;
                $config['maintain_ratio'] = TRUE;
                if($action_type = "FACE"){
                    $config['width'] = 150;
                    $config['height'] = 150;
                }
                else{
                    $config['width'] = 150;
                    $config['height'] = 300;
                }

                $this->load->library('image_lib');
                $this->image_lib->initialize($config);
                $resized = $this->image_lib->resize();
                $this->image_lib->clear();

                //insert into upload table
                $dat = array('upload_name' => $name, 'uploaded_by' => $user_id, 'error_type' => 'success');
                $uploaded = $this->account->saveUploadData($dat);

                $info = new stdClass();
                $info->name = $name;
                $info->size = $data['file_size'];
                $info->type = $data['file_type'];
                $info->deleteUrl = "";
                $info->url = 'files/customer_kyc/'.$name;
                if($action_type = "ID"){
                    //$feedback = $this->customer->saveIDPhoto($cust_id,$info->url);
                }
                else{
                    //$feedback = $this->customer->saveFacePhoto($cust_id,$info->url);
                }

                //array_push($d,$info);
                //array_push($d,$feedback);
                $errors = array();//array_pop($feedback);
                //echo json_encode($d);

            }
            $this->current_page = 'upload_image';
            $this->name = 'upload image for customer';
            $this->blade->render('manu_upload', array('title' => 'upload file','errors' => $errors,'cust_id'=>$cust_id));

        }
        else{
            $this->current_page = 'upload_image';
            $this->name = 'upload image for customer';
            $this->blade->render('kyc_image_upload', array('title' => 'upload file','errors' => $errors,'cust_id'=>$cust_id));
        }
    }
    public function viewCustomers(){
        $feedback = false;
        $search_feedback = false;
        $user_id    =  $this->session->userdata('user_id');

        if($this->security->xss_clean($this->input->post('search'))){
            $param = $this->security->xss_clean($this->input->post('param'));
            $data = $this->customer->getCustomer(false,$param);
            $this->name =  "view customers";
            $search_feedback = "search for ".$param." ". $data['total']." customers found";
            $num_rows = $data['total'];
            $this->active = 'act_kyc';

            $data['users']            = $num_rows;
            $config['base_url']       = BASEURL.'pafupi/main/view_customers';
            $config['total_rows']     = $num_rows;
            $config['per_page']       = 6;
            $config['num_links']      = 3;
            $config['last_link']      = FALSE;
            $config['first_link']     = FALSE;
            $config['uri_segment']    = 4;
            $config['next_link']      = 'Next';
            $config['next_tag_open']  = "<td class='pagination-item-outer-rght'>";
            $config['next_tag_close'] = "</td'>";
            $config['prev_link']      = 'Previous';
            $config['prev_tag_open']  = "<td class='pagination-item-outer-lft'>";
            $config['prev_tag_close'] = "</td>";
            $config['num_tag_open']   = "<td class='pagination-item'>";
            $config['num_tag_close']  = "</td>";
            $config['cur_tag_open']   = "<td class='pagination-item-current'>";
            $config['cur_tag_close']  = "</td>";
            $config['anchor_class']   = "pagina";

            $this->pagination->initialize($config);
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $data = $this->customer->getCustomer(false,$param);
            $this->blade->set('customers',$data['result'])->render('view_customers', array('title' => 'view customers','feedback'=>$feedback,'search_feedback'=>$search_feedback));
            return;

        }
        else{
            $this->name =  "view customers";
            $num_rows   =  $this->pagination($this->customer->get_all_opened_customers(false,false,true));
            $data["total_rows"] = $num_rows;

            $this->active = 'act_kyc';

            $data['users']            = $num_rows;
            $config['base_url']       = BASEURL.'pafupi/main/view_customers';
            $config['total_rows']     = $num_rows;
            $config['per_page']       = 6;
            $config['num_links']      = 3;
            $config['last_link']      = FALSE;
            $config['first_link']     = FALSE;
            $config['uri_segment']    = 4;
            $config['next_link']      = 'Next';
            $config['next_tag_open']  = "<td class='pagination-item-outer-rght'>";
            $config['next_tag_close'] = "</td'>";
            $config['prev_link']      = 'Previous';
            $config['prev_tag_open']  = "<td class='pagination-item-outer-lft'>";
            $config['prev_tag_close'] = "</td>";
            $config['num_tag_open']   = "<td class='pagination-item'>";
            $config['num_tag_close']  = "</td>";
            $config['cur_tag_open']   = "<td class='pagination-item-current'>";
            $config['cur_tag_close']  = "</td>";

            $this->pagination->initialize($config);
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $data     = $this->customer-> get_all_opened_customers($config['per_page'],$page);

            $this->blade->set('customers',$data['result'])->render('view_customers', array('title' => 'view customers','feedback'=>$feedback,'search_feedback'=>$search_feedback));
        }
    }
    private function pagination($total_rows)
    {
        //fixes an issue with pagination in CI :)
        if($total_rows > 1)
        {

            $total_rows = $total_rows + 1;

        }

        else
            $total_rows + 0;


        return $total_rows;

    }
} 