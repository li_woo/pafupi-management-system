<?php
/**
 * Created by PhpStorm.
 * User: tnm180
 * Date: 6/30/14
 * Time: 2:49 PM
 */

class trying extends CI_Controller {
    public function __construct()
    {
        parent::__construct();

        $this->load->library('excel');
        $this->load->library('session');

        $this->load->model('account_m_model','account');
    }
    public function createExcel($user_id=false){

        $user_id = $this->session->userdata('user_id');
        $accounts = $this->account->getPreAccount($user_id);
        $name = $this->session->userdata('fullname');
       // $accounts = $this->account->getWithCardAccounts($user_id);
        $this->excel->getProperties()->setCreator($name)
            ->setLastModifiedBy($this->session->userdata('fullname'))
            ->setTitle("Pre-linked account")
            ->setSubject("Prelinked account")
            ->setDescription("view prelinked accounts by ".$name)
            ->setKeywords("office 2007 openxml php")
            ->setCategory("accounts");

        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Pre-linked account');
        //set auto column width
        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        //set headers
        $this->excel->getActiveSheet()->setCellValue('A1', 'Account No');
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('B1', 'Branch');
        $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('C1', 'Date created');
        $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('D1', 'Created By');
        $this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
        //change the font size
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(12);
        $cell = 2;
        foreach($accounts as $account){

            //$this->excel->getActiveSheet()->setCellValue('A'.$cell, $account['acc_no']);
            $this->excel->getActiveSheet()->getCell('A'.$cell)->setValueExplicit($account['acc_no'], PHPExcel_Cell_DataType::TYPE_STRING);
            //$this->excel->getActiveSheet()->setCellValue('B'.$cell, $account['branch_name']);
            $this->excel->getActiveSheet()->getCell('B'.$cell)->setValueExplicit($account['branch_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            //$this->excel->getActiveSheet()->setCellValue('C'.$cell, date('d-m-Y',strtotime($account['date_created'])));
            $this->excel->getActiveSheet()->getCell('C'.$cell)->setValueExplicit(date('d-m-Y',strtotime($account['date_created'])), PHPExcel_Cell_DataType::TYPE_STRING);
            //$this->excel->getActiveSheet()->setCellValue('C'.$cell, date('d-m-Y',strtotime($account['date_created'])));
            $this->excel->getActiveSheet()->getCell('C'.$cell)->setValueExplicit($account['opened_by'], PHPExcel_Cell_DataType::TYPE_STRING);
            $cell++;
        }
        //make the font become bold
        //$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        //merge cell A1 until D1
        //$this->excel->getActiveSheet()->mergeCells('A1:D1');
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);


        $filename='pre-linked-accounts_'.$user_id.'.xls';  //file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('files/prelinked_accounts/'.$filename);
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

} 