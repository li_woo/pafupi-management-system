<?php
/**
 * Created by PhpStorm.
 * User: tnm180
 * Date: 7/1/14
 * Time: 8:30 PM
 */

require_once  APPPATH.'libraries/OFSLib.php';
//require_once APPPATH.'libraries/PHPExcel.php';
class Account2 extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        //$this->load->library('ofslib');
        $this->load->model('authentication');


    }
    public function fin(){
        $db = $this->config->item('db_connection');
        $sql = 'SHOW TABLES()';
        $stmt = $db->prepare($sql);
        $stmt->execute();
        var_dump($stmt->fetchAll());

    }
    public function getty(){
        echo substr("00251711102285",3,6);
        echo date('Y-m-d',strtotime("2014-07-09 + 30 days"));


        /*$account->request->fields->add('CUSTOMER', CIF Here); - use the customer number provided from first OFS request
        $account->request->fields->add('CATEGORY', 6075); - set it to 6075
        $account->request->fields->add('ACCOUNT.TITLE', Account Title,1)
        $account->request->fields->add('SHORT.TITLE', Short Title)
        $account->request->fields->add('POSITION.TYPE', 'TR') -> leave that to TR
        $account->request->fields->add('ACCOUNT.OFFICER', xx) e.g. 25 for ginnery corner, 70 for capital city etc etc - get the number from digit 3 and 4 of the acc number
        $account->request->fields->add('CURRENCY', 'MWK') set it to MWK
        $account->request->fields->add('REGION', Region) - set it to S, C or N (northern, central or southern depending on branch location)*/



        /* $customer->request->operation = 'ACCOUNT';

         $customer->request->options->function_type = FunctionType::SEE;
         $customer->request->options->processing_flag = ProcessingFlag::PROCESS;
         $customer->request->options->authorisers = Authoriser::ZERO_AUTHORISERS;

         $customer->request->user->username = 'SMSBANKING01';
         $customer->request->user->password = '654321';
         $customer->request->user->company = 'MW0010025';

         $customer->request->transaction_id = '0025723978017';

         //dump($customer->request->to_ofs()->message);
         $customer->request->execute();

         $customer->response->to_hash();
         dump($customer->response);

        $masm_payment = New Transaction();

          // add an operation for the transaction
          $masm_payment->request->operation = 'FUNDS.TRANSFER';

          $masm_payment->request->user->information = array(
                  OFSUser::USERNAME   => $this->config->item('ofs_user'),
                  OFSUser::PASSWORD   => $this->config->item('ofs_password'),
                  OFSUser::COMPANY    => $this->config->item('ofs_master_company')
          );

          // add OFS options
          $masm_payment->request->options->set_by_hash(array(
                  Transaction_Option::VERSION_NAME    => 'MASMPAYMENT',
                  Transaction_Option::FUNCTION_TYPE   => FunctionType::INPUT,
                  Transaction_Option::PROCESSING_FLAG => ProcessingFlag::PROCESS,
                  Transaction_Option::AUTHORISERS     => Authoriser::SELF_AUTHORISER,
                  Transaction_Option::GTS_CONTROL     => GTSControl::HOLD_ON_ERROR_AND_APPROVE_ON_OVERRIDE
                )
           );

        */
    }
    public function makeAccount($num_of_accounts=1,$branch='MW0010025'){
        $result = array();
        $branch_officer_code = substr($branch,-2); //echo $branch_officer_code;exit;
        try{
        OFSConnector::connect("http://10.51.3.10:8080/axis/services/OFSBridgeService2?wsdl");
        OFSConnector::set_default_channel("PSPP");
        }
        catch(Exception $e){
            return false;
        }

        $num = 0;
        for($i = 0 ; $i<$num_of_accounts;$i++){


        $dummy_cif = $this->createDummyCustomer();
        if($dummy_cif ==NULL){
            continue;
        }
        $trans_id = $this->createCustomer($dummy_cif);
        if($trans_id){

        $result[$num]['customer_id'] = $trans_id;

        $account = new Transaction();
        $account->request->operation = 'ACCOUNT';
        $account->request->options->set_by_hash(array(

                Transaction_Option::FUNCTION_TYPE   => FunctionType::INPUT,
                Transaction_Option::PROCESSING_FLAG => ProcessingFlag::PROCESS,
                Transaction_Option::AUTHORISERS     => Authoriser::SELF_AUTHORISER,
                Transaction_Option::GTS_CONTROL     => GTSControl::HOLD_ON_ERROR_AND_APPROVE_ON_OVERRIDE
            )
        );

        $account->request->user->username = 'SMSBANKING01';
        $account->request->user->password = '654321';
        $account->request->user->company = $branch;
        $region = $this->authentication->get_branch_region($branch);
        $account->request->transaction_id = $dummy_cif;
        $account->request->fields->add('CUSTOMER',$trans_id); //use the customer number provided from first OFS request
        $account->request->fields->add('CATEGORY', 6075); //set it to 6075
        $account->request->fields->add('ACCOUNT.TITLE.1', "PAFUPI DEFAULT ACCOUNT",1);
        $account->request->fields->add('SHORT.TITLE', "PAFUPI");
        $account->request->fields->add('POSITION.TYPE', 'TR'); //-> leave that to TR
        $account->request->fields->add('ACCOUNT.OFFICER',$branch_officer_code); //e.g. 25 for ginnery corner, 70 for capital city etc etc - get the number from digit 3 and 4 of the acc number
        $account->request->fields->add('CURRENCY', 'MWK'); //set it to MWK
        $account->request->fields->add('REGION', $region);// - set it to S, C or N (northern, central or southern depending on branch location)
        $account->request->to_ofs();

        $account->request->execute();
        //dump($account->response->message);
        $account->response->to_hash();
            if($account->response->transaction_id){
                $result[$num]['account_number'] = $account->response->transaction_id ;
                $num++;
            }
            else{
              continue;
            }
        }
        else{
           continue;
        }

       }
       if(empty($result))
           return false;
       else
       return $result;
    }
    public function createCustomer($dummy_cif){
        $customer = new Transaction();
        $customer->request->operation = 'CUSTOMER';
        $customer->request->options->set_by_hash(array(
                Transaction_Option::VERSION_NAME   => 'PAFUPI',
                Transaction_Option::FUNCTION_TYPE   => FunctionType::INPUT,
                Transaction_Option::PROCESSING_FLAG => ProcessingFlag::PROCESS,
                Transaction_Option::AUTHORISERS     => Authoriser::SELF_AUTHORISER,

            )
        );

        $customer->request->user->username = 'SMSBANKING01';
        $customer->request->user->password = '654321';
        $customer->request->user->company = 'MW0010001';

        $salutation = "MR";$initial = "P"; $surname = "PAFUPI"; $first_name = "PAFUPI";
        $type_of_id = "PASSPORT"; $id_number = "NONE"; $expiry_date = "20141031"; $legal_entity ="INDIVIDUALS & HOUSEHOLDS"; $date_of_birth ="19900501";
        $gender = "M"; $marital_status = "M"; $spouse_name = "NONE"; $cellphone ="NONE"; $residential_address ="NONE";
        $country= "MALAWI"; $village = "NONE"; $traditional_authority = "NONE"; $district = "NONE"; $permanent_addresss = "NONE";
        $postal_address = 5; $nationality = "MW"; $residential_status = "R"; $source_of_income = "NONE";
        $total_annual_income =0.0; $occupation = "NONE";  $next_of_kin ="NONE";  $next_of_kin_address ="NONE";
        $industry =10 ; $target = 7; $employer = "NONE"; $employer_address = "NONE"; $references = "NONE";
        //$account_officer = $branch_officer_code;
        $sector = 1000;
        $next_of_kin_cellphone ="NONE"; $permanent_address ="NONE";

        $customer->request->fields->add('SHORT.NAME','PAFUPI');
        $customer->request->fields->add('MNEMONIC','XX'.$dummy_cif);
        $customer->request->fields->add('TITLE.1', $salutation);
        $customer->request->fields->add('INITIAL', $initial);
        $customer->request->fields->add('CU.SURNAME', $surname);
        $customer->request->fields->add('FIRST.NAME', $first_name);
        $customer->request->fields->add('LOCAL.REF', $type_of_id, 13);
        $customer->request->fields->add('LOCAL.REF', $id_number, 47);
        $customer->request->fields->add('EXPIRY.DATE', $expiry_date);
        $customer->request->fields->add('CIF.TYPE', $legal_entity);
        $customer->request->fields->add('BIRTH.INCORP.DATE', $date_of_birth);
        $customer->request->fields->add('LOCAL.REF', $gender, 11);
        $customer->request->fields->add('MARITAL.STATUS1', $marital_status, 11);
        $customer->request->fields->add('SPOUSE.NAME', $spouse_name);
        $customer->request->fields->add('CELLPHONE', $cellphone);
        $customer->request->fields->add('NAME.2', $residential_address, 1);
        $customer->request->fields->add('COUNTRY', $country, 1);
        $customer->request->fields->add('CIF.VILLAGE', $village);
        $customer->request->fields->add('CIF.TA', $traditional_authority);
        $customer->request->fields->add('CIF.DISTRICT', $district);
        $customer->request->fields->add('STREET', $permanent_address, 1);
        $customer->request->fields->add('LOCAL.REF', $postal_address, 1);
        $customer->request->fields->add('NATIONALITY', $nationality);
        // $customer->request->fields->add('RESIDENCE', $residence);
        $customer->request->fields->add('LOCAL.REF', $residential_status, 14);
        $customer->request->fields->add('INCOME.SOURCE', $source_of_income);
        $customer->request->fields->add('LOCAL.REF', $total_annual_income, 22);
        $customer->request->fields->add('OCCUPATION', $occupation, 1);
        $customer->request->fields->add('SECTOR', $industry);
        // $customer->request->fields->add('INDUSTRY', $sector);
        $customer->request->fields->add('TARGET', $target);
        $customer->request->fields->add('EMPLOYER', $employer);
        $customer->request->fields->add('LOCAL.REF', $employer_address, 16);
        $customer->request->fields->add('LOCAL.REF', $next_of_kin, 34);
        $customer->request->fields->add('LOCAL.REF', $next_of_kin_address, 4);
        $customer->request->fields->add('REL.CELL.NO', $next_of_kin_cellphone);
        $customer->request->fields->add('LOCAL.REF', $references, 23);
        $customer->request->fields->add('ACCOUNT.OFFICER','01');
        $customer->request->fields->add('SECTOR', $sector);
        $customer->request->fields->add('NAME.1','PAFUPI');
        $customer->request->transaction_id = $dummy_cif;




        $customer->request->execute();
        //dump($customer->response->message);
        $customer->response->to_hash();
        //echo $customer->response->transaction_id;
        if($customer->response->has_error){
            return false;
        }
        else{
            return $customer->response->transaction_id;
        }

    }
    public function createDummyCustomer(){
        $dummy_customer = new Transaction();

        $dummy_customer->request->operation = 'CUSTOMER';
        $dummy_customer->request->options->set_by_hash(array(

                Transaction_Option::FUNCTION_TYPE   => FunctionType::INPUT,
                Transaction_Option::PROCESSING_FLAG => ProcessingFlag::VALIDATE,
                Transaction_Option::AUTHORISERS     => Authoriser::SELF_AUTHORISER,
                //Transaction_Option::GTS_CONTROL     => GTSControl::HOLD_ON_ERROR_AND_APPROVE_ON_OVERRIDE
            )
        );

        $dummy_customer->request->user->username = 'SMSBANKING01';
        $dummy_customer->request->user->password = '654321';
        $dummy_customer->request->user->company = 'MW0010001';

        $dummy_customer->request->execute();

        $dummy_customer->response->to_hash();

        return $dummy_customer->response->transaction_id;
    }

} 