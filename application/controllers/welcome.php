<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 public function __construct()
	{
			parent::__construct();
			$this->load->library('blade');
			$this->load->library('excel');
			$this->blade->set('base_url',BASEURL);
			$this->load->model('account_model','account');
	}	
	public function index()
	{
		$this->blade->set('foo', 'bar')
				->set('an_array', array(1, 2, 3, 4))
				->append('an_array', 5)
				->set_data(array('more' => 'data', 'other' => 'data'))
				->render('test', array('message' => 'Hello World!'));
	}
		/**
	 * 	shows a view where the user can generate accounts - the user chooses the number of pafupi accounts to be created and the branch which the accounts will belong
	 *
	 * @authour Lewis Msasa <lmsasajnr@gmail.com>	 
	 */
	public function createAccountView(){
	   $branches = $this->account->getBranches();
	   $this->blade->set('branches',$branches)->render('create_account', array('message' => 'Hello World!'));
	}
		/**
	 * send the request to T24 by calling sentToT24() to create the number of accounts specified by the user in the form submited from the create_account view
	 *
	 * for every success response from T24 on account creation, the function inserts the created account into a pafupi accounts table
	 * A booklet is also generated which has 50 pins by calling the createBooklet() on each successfully created account.
	 *
	 * for every failure from T24 on account creation, the function logs the result into the log database table
	 *
	 * 
	 * 
	 * @authour Lewis Msasa <lmsasajnr@gmail.com>
	 */
	public function createAccount(){
	   $num_accounts = $this->security->xss_clean($this->input->post('number'));
	   $branch_id = $this->security->xss_clean($this->input->post('branch'));
	   $randoms = array();
	   //send to t24
	   for($i=0; $i<$num_accounts;$i++){
	     $data = $this->sendToT24($branch_id);
		 if($data){
		   $data = array('branch_id' => $branch_id, 'acc_no' =>'00251711'.rand(0,10).rand(0,10).rand(0,10).rand(0,10).rand(0,10),'opened_by' =>'05050');
		   $d = $this->account->saveAccount($data); 
		   array_push($randoms,$d);
		 }
		 else{
		 }
	   }
	   echo json_encode($randoms);
	}
	public function createExcel(){
	   $user_id = '05050';
	   $accounts = $this->account->getPreAccount($user_id);
	   $this->excel->setActiveSheetIndex(0);
		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('Pre-linked account');
		//set auto column width
		$this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		//set headers
		$this->excel->getActiveSheet()->setCellValue('A1', 'Account No');
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->setCellValue('B1', 'Branch');	
        $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);		
		$this->excel->getActiveSheet()->setCellValue('C1', 'Date created');
		$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
		//change the font size
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(12);
		$cell = 2;
		foreach($accounts as $account){
		  $this->excel->getActiveSheet()->setCellValue('A'.$cell, $account['acc_no']);
		  $this->excel->getActiveSheet()->setCellValue('B'.$cell, $account['branch_name']);
		  $this->excel->getActiveSheet()->setCellValue('C'.$cell, date('d-m-Y',strtotime($account['date_created'])));
		  $cell++;
		}
		//make the font become bold
		//$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		//merge cell A1 until D1
		//$this->excel->getActiveSheet()->mergeCells('A1:D1');
		//set aligment to center for that merged cell (A1 to D1)
		$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		
		 
		$filename='pre-linked-accounts_'.$user_id.'.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
					 
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5'); 
        $objWriter->save('files/prelinked_accounts/'.$filename);		
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}
	public function sendToT24($branch_id){
	  return true;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */