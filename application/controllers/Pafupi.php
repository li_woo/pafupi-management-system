<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once APPPATH.'libraries/Network.php';
require_once 'Security_check.php';
class Pafupi extends CI_Controller {
    public $active;
    public $role_id;
    public $name;
    public $current_page;
    public $username;
    public $locked_users ;
    public $unlocked_users;
    public $expired_profiles ;
    public $ip_address  ;
    public $total_users;
    public $carded_accounts;
    public $received;
    public $assigned;
    public $fully_opened;
    public $expiry;
    public $count_dispatched;
    public $total_preopen;
    public function  __construct(){
        parent::__construct();
        $this->load->library('blade');
        $this->load->library('session');
        $this->blade->set('base_url',BASEURL);
        $this->load->model('account_m_model','account');
        $this->load->model('user_model','user');
        $this->load->helper('url');
        //$this->role_id = 1;
        $this->username = $this->session->userdata('username');
        if(empty($this->username))
            $this->username = false;




    }
    public function index($feedback=false)
    {

        //echo  date('y',time());exit;
        $this->name = 'home';
        $this->username = $this->session->userdata('username');
        if(empty($this->username))
            $this->username = false;
        if($this->username){
            if($this->session->userdata('role_id')==1){
                $this->name ='Welcome - '.$this->session->userdata('fullname');
                //$this->expiry = $this->contextualTime(strtotime(date("M j Y g:iA",strtotime($this->session->userdata('last_pwd_change').' + 90 days'))));
                $this->locked_users     = $this->user->get_locked_users();
                $this->unlocked_users   = $this->user->get_unlocked_users();
                $this->expired_profiles = $this->user->get_expired_passwords();
                $this->ip_address      = Network::get_ip_address();
                $this->total_users      = $this->user->get_total_users();
                $this->carded_accounts = $this->account->getCardedCount(true);
                $this->fully_accounts = $this->account->getFullyCount(true);
                $this->locked_users = $this->user->get_locked_users(true);
                $this->total_preopen = count($this->account->getPreAccount());
            }
            elseif($this->session->userdata('role_id')==2){
                $this->ip_address      = Network::get_ip_address();
                $this->expiry = $this->contextualTime(time(),strtotime(date("M j Y g:iA",strtotime($this->session->userdata('last_pwd_change').' + 90 days'))));
                $branch_id =  $this->session->userdata('branch');
                $this->received = $this->account->getReceivedByBranch($branch_id,true,false);
                $this->assigned = $this->account->getAssignedByBranch($branch_id,true,false);
                $this->fully_opened = $this->account->getFullyByBranch($branch_id,true,false);
                //$this->count_dispatched = $this->account->getDispatchedByBranch($branch_id,false,true);


            }
        }
        else
            $this->name='Login';

        $this->current_page = 'home';
        $this->blade->render('home', array('title' => 'Home','feedback'=> $feedback));
    }
    public function main($page='home',$feedback=false){

        Security_check::check_login();
        if(!$page)
            $this->current_page='home';
        else
            $this->current_page=$page;
        //account routes
        if($this->current_page=='home'){
            $this->active = 'act_home';
            $this->index();
        }
        elseif($this->current_page =='account'){
            //loads account controller
            $this->load->library('../controllers/account');
            $this->active = 'act_account';
            $this->account-> accountView();
        }
        elseif($this->current_page =='account_preopen'){
            //loads account controller
            $this->load->library('../controllers/account');
            $this->active = 'act_account';
            $this->account->createAccountView();
        }
        elseif($this->current_page =='otp_creation'){
            //loads account controller
            $this->load->library('../controllers/account');
            $this->active = 'act_pin';
            $this->account->createOTPView();
        }
        elseif($this->current_page =='ref_creation'){
            //loads account controller
            $this->load->library('../controllers/account');
            $this->active = 'act_pin';
            $this->account->createRefView();
        }
        elseif($this->current_page =='upload_otp'){
            //loads account controller
            $this->load->library('../controllers/create_excel');
            $this->active = 'act_link';
            $this->create_excel->uploadOTPFile();
        }
        elseif($this->current_page =='upload_referal'){
            //loads account controller
            $this->load->library('../controllers/create_excel');
            $this->active = 'act_link';
            $this->create_excel->uploadReferalFile();
        }
         elseif($this->current_page =='upload_card_linked'){
            //loads account controller
            $this->load->library('../controllers/create_excel');
            $this->active = 'act_sp';
            $this->create_excel->uploadCardLinkedFile();
        }
        elseif($this->current_page =='otp_creation_post'){
            //loads account controller
            $this->load->library('../controllers/account');
            $this->active = 'act_account';
            $this->account->createOTP();
        }
        elseif($this->current_page=='view_preopen'){
            //loads account controller

            $this->load->library('../controllers/account');
            $this->active = 'act_account';
            $this->account->viewPrelinked();
        }
        elseif($this->current_page=='view_to_link'){
            //loads account controller

            $this->load->library('../controllers/account');
            $this->active = 'act_account';
            $this->account->viewTolink();
        }
        elseif($this->current_page=='view_to_card'){
            //loads account controller

            $this->load->library('../controllers/account');
            $this->active = 'act_account';
            $this->account->viewToCard();
        }
        elseif($this->current_page=='view_linked'){
            //loads account controller

            $this->load->library('../controllers/account');
            $this->active = 'act_account';
            $this->account->viewLinked();
        }
        elseif($this->current_page=='view_download_otp'){
            //loads account controller

            $this->load->library('../controllers/account');
            $this->active = 'act_pin';
            $this->account->viewDownloadOTP();
        }
        elseif($this->current_page=='view_download_card'){
            //loads account controller

            $this->load->library('../controllers/account');
            $this->active = 'act_pin';
            $this->account->viewDownloadCard();
        }
        elseif($this->current_page=='view_download_referal'){
            //loads account controller

            $this->load->library('../controllers/account');
            $this->active = 'act_pin';
            $this->account->viewDownloadReferal();
        }
        elseif($this->current_page=='view_carded'){
            //loads account controller

            $this->load->library('../controllers/account');
            $this->active = 'act_account';
            $this->account->viewCarded();
        }
        elseif($this->current_page=='create_account_post'){
            $this->load->library('../controllers/account');
            $this->active = 'act_account';
            $this->account->createAccount();
        }
        elseif($this->current_page=='upload_card_file'){
            $this->load->library('../controllers/create_excel');
            $this->active = 'act_account';
            $this->current_page = 'upload_file';
            $this->create_excel->uploadCardFile();
        }
        elseif($this->current_page=='upload_manu_file'){
            $this->load->library('../controllers/create_excel');
            $this->active = 'act_sp';
            $this->create_excel->uploadManuFile();

        }
        elseif($this->current_page=='regenerate_pins'){
            $this->load->library('../controllers/account');
            $this->active = 'act_account';
            $this->account->regeneratePins();
        }
        elseif($this->current_page=='download_prelinked_account_excel'){
            $this->load->library('../controllers/create_excel');
            $this->active = 'act_account';
            $this->create_excel->createExcel();
        }
        elseif($this->current_page=='download_prelinked_account_card_data_excel'){
            $this->load->library('../controllers/account');
            $this->active = 'act_account';
            $this->account->createCardExcel();
        }
        elseif($this->current_page=='download_prelinked_account_card_excel'){
            $this->load->library('../controllers/account');
            $this->active = 'act_account';
            $this->account->createWithCardExcel();

        }
        elseif($this->current_page=='download_book_pins_excel'){
            $this->load->library('../controllers/account');
            $this->active = 'act_account';
            $this->account->createPinsExcel();
        }
        elseif($this->current_page=='dispatch_account'){
            $this->load->library('../controllers/account');
            $this->active = 'act_inventory';
            $this->account->dispatchAccount();
        }
        elseif($this->current_page=='dispatched_account'){
            $this->load->library('../controllers/account');
            $this->active = 'act_account';
            $this->account->viewDispatched();
        }
        elseif($this->current_page=='view_dispatch_ready'){
            $this->load->library('../controllers/account');
            $this->active = 'act_account';
            $this->account->viewWithCardAccounts();
        }
        //user
        elseif($this->current_page=='profile'){
            $this->load->library('../controllers/user');
            $this->active = 'act_profile';
            $this->user->index();
        }
        elseif($this->current_page=='create_user'){

            $this->load->library('../controllers/support_staff');
            $this->active = 'act_profile';
            if($_POST)
                $this->support_staff->registerUser($this->security->xss_clean($this->input->post()));
            else
                $this->support_staff->registerUser();
        }
        /*elseif($this->current_page=='delete_user'){
            $this->load->library('../controllers/support_staff');
            $this->active = 'act_profile';
            $this->support_staff->deleteUser();
          }*/
        elseif($this->current_page=='lock_user'){
            $this->load->library('../controllers/support_staff');
            $this->active = 'act_profile';
            $this->support_staff->lockUser();
        }
        elseif($this->current_page=='unlock_user'){
            $this->load->library('../controllers/support_staff');
            $this->active = 'act_profile';
            $this->support_staff->UnlockUser();
        }
        elseif($this->current_page=='modify_user'){
            $this->load->library('../controllers/support_staff');
            $this->active = 'act_profile';
            $this->support_staff->modifyUser();
        }
        elseif($this->current_page=='get_user'){
            $this->load->library('../controllers/support_staff');
            $this->active = 'act_profile';
            $this->support_staff->getUser();
        }
        elseif($this->current_page=='search_user'){
            $this->load->library('../controllers/support_staff');
            $this->active = 'act_profile';
            $this->support_staff->searchUser();
        }
        elseif($this->current_page=='save_user'){
            $this->load->library('../controllers/support_staff');
            $this->active = 'act_profile';
            $this->support_staff->saveUser();
        }
        elseif($this->current_page=='loginy'){
            $this->load->library('../controllers/user');
            $this->active = 'act_profile';
            $this->user->authenticate_user();
        }
        else if($this->current_page == 'logout')
        {

            $this->load->library('../controllers/user');
            $this->active = 'act_profile';
            $this->user->logout();

        }
        else if($this->current_page == 'password_reset_action')
        {

            $this->load->library('../controllers/user');
            $this->active = 'act_password';
            $this->user->change_password_action();

        }
        else if($this->current_page == 'reset_user_password'){
            $this->load->library('../controllers/user');
            $this->active = 'act_profile';
            $this->user->reset_user_password();
        }
        else if($this->current_page == 'password_reset')
        {
            //$this->load->library('../controllers/user');
            $this->active = 'act_password';
            //$this->user->changePassword();
            $this->name = 'Password Reset';
            $username = $this->session->userdata('username');
            $user_id = $this->session->userdata('user_id');
            $fullname = $this->session->userdata('fullname');

            $this->blade->render('password_reset', array('title' => 'Password Reset','feedback' => $feedback));
        }
        elseif($this->current_page =='dispatch_account'){
            //loads account controller
            $this->load->library('../controllers/account');
            $this->active = 'act_inventory';
            $this->account->dispatchAccount();
        }
        elseif($this->current_page =='lock_account'){
            //loads account controller
            $this->load->library('../controllers/account');
            $this->active = 'act_inventory';
            $this->account->lockAccount();
        }
        elseif($this->current_page =='manage_starterpacks'){
            //loads account controller
            $this->load->library('../controllers/account');
            $this->active = 'act_inventory';
            $this->account->viewStarterpacks();
        }
        elseif($this->current_page =='unlock_account'){
            //loads account controller
            $this->load->library('../controllers/account');
            $this->active = 'act_inventory';
            $this->account->unlockAccount();
        }
        elseif($this->current_page =='inventory'){
            //loads account controller
            $this->load->library('../controllers/account');
            $this->active = 'act_inventory';
            $this->account->inventoryView();
        }
        elseif($this->current_page =='view_assigned_starterpacks'){
            $this->load->library('../controllers/account');
            $this->active = 'act_account';
            $this->account->viewBranchStarter();
        }
        elseif($this->current_page =='assign_starterpack'){
            $this->load->library('../controllers/account');
            $this->active = 'act_account';
            $this->account->assignStarterPack();
        }
        elseif($this->current_page =='receive_starterpack'){
            $this->load->library('../controllers/account');
            $this->active = 'act_account';
            $this->account->receiveStarterPack();
        }
        elseif($this->current_page =='branch_admin'){
            //loads account controller
            $this->load->library('../controllers/branch_admin');
            $this->active = 'act_home';
            $this->branch_admin->viewHome();
        }
        elseif($this->current_page =='upload_kyc_image'){
            //loads account controller
            $this->load->library('../controllers/branch_admin');
            $this->active = 'act_kyc';
            $cust_id = $this->security->xss_clean($this->input->get('cust_id'));
            $this->branch_admin->uploadImages($cust_id);
        }
        elseif($this->current_page =='view_customers'){
            //loads account controller
            $this->load->library('../controllers/branch_admin');
            $this->active = 'act_kyc';
            $this->branch_admin->viewCustomers();
        }
        elseif($this->current_page =='create_card_view'){
            //loads account controller
            $this->load->library('../controllers/account');
            $this->active = 'act_pin';
            $this->account->createCardView();
        }
        elseif($this->current_page =='create_card_post'){
            //loads account controller
            $this->load->library('../controllers/account');
            $this->active = 'act_pin';
            $this->account->createCardPost();
        }

    }
    function contextualTime($small_ts, $large_ts=false) {
        if(!$large_ts) $large_ts = time();

        $n = $large_ts - $small_ts;

        if($n <= 1) return 'less than 1 second ago';
        if($n < (60)) return $n . ' seconds ago';
        if($n < (60*60)) { $minutes = round($n/60); return 'about ' . $minutes . ' minute' . ($minutes > 1 ? 's' : '') . ' ago'; }
        if($n < (60*60*16)) { $hours = round($n/(60*60)); return 'about ' . $hours . ' hour' . ($hours > 1 ? 's' : '') . ' ago'; }
        if($n < (time() - strtotime('yesterday'))) return 'yesterday';
        if($n < (60*60*24)) { $hours = round($n/(60*60)); return 'about ' . $hours . ' hour' . ($hours > 1 ? 's' : '') . ' ago'; }
        if($n < (60*60*24*6.5)) return 'about ' . round($n/(60*60*24)) . ' days ago';
        if($n < (time() - strtotime('last week'))) return 'last week';
        if(round($n/(60*60*24*7)) == 1) return 'about a week ago';
        if($n < (60*60*24*7*3.5)) return 'about ' . round($n/(60*60*24*7)) . ' weeks ago';
        if($n < (time() - strtotime('last month'))) return 'last month';
        if(round($n/(60*60*24*7*4)) == 1) return 'about a month ago';
        if($n < (60*60*24*7*4*11.5)) return 'about ' . round($n/(60*60*24*7*4)) . ' months ago';
        if($n < (time() - strtotime('last year'))) return 'last year';
        if(round($n/(60*60*24*7*52)) == 1) return 'about a year ago';
        if($n >= (60*60*24*7*4*12)) return 'about ' . round($n/(60*60*24*7*52)) . ' years ago';
        return false;
    }
}