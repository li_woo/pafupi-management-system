<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Common_Tasks {
	
  /**
  * Method attempts to check which browser
  * the user is using  
  *
  * @param void
  * 
  * @return string
  * @access public
  * @author Vitu Mhone <vmhone@nbsmw.com>
  */
  
  public function get_browser() {
  	
	$_user_agent = $_SERVER['HTTP_USER_AGENT'];
	$_browser	 = NULL;

    if(preg_match('/MSIE/i',$_user_agent) && (!preg_match('/opera/i',$_user_agent)))
    {
        $_browser = 'IE';
    }
    elseif(preg_match('/Firefox/i',$_user_agent))
    {
        $_browser = 'Firefox';
    }
	elseif(preg_match('/Chrome/i',$_user_agent))
    {
        $_browser = 'Chrome';
    }
    elseif(preg_match('/Safari/i',$_user_agent) && (preg_match('/Webkit/',$_user_agent)))
    {
        $_browser = 'Safari';
    }
    elseif(preg_match('/Flock/i',$_user_agent))
    {
        $_browser = 'Flock';
    }
    elseif(preg_match('/Opera/i',$_user_agent))
    {
        $_browser = 'Opera';
    }
	elseif(preg_match('/Trident/i',$_user_agent))
    {
        $_browser = 'IE';
    }
	else
	{
		$_browser = 'Unknown';
	}
	
    return $_browser;
	
  }

  /**
  * Method attempts to check which browser version
  * the user is using  
  *
  * @param void
  * 
  * @return string
  * @access public
  * @author Vitu Mhone <vmhone@nbsmw.com>
  */
  
  public function get_browser_version() {
  	
	$_user_agent = strtolower($_SERVER['HTTP_USER_AGENT']); 
	
	if(preg_match('/.+(?:rv|it|ra|ie)[\/: ]([\d.]+)/', $_user_agent, $_matches)) {
        	 
        $_version = $_matches[1]; 
		
    } 
    
    else {
        	 
        $_version = 'Unknown'; 
		
    }
	
	return $_version;
	
  }
  
  /**
  * Method attempts to check which platform
  * the user is using  
  *
  * @param void
  * 
  * @return string
  * @access public
  * @author Vitu Mhone <vmhone@nbsmw.com>
  */
  
  public function get_platform() {
  	
	$_user_agent = strtolower($_SERVER['HTTP_USER_AGENT']);
	
	//now get the platform the user is using
	if(preg_match('/linux/',$_user_agent)) {
		
		$_platform = 'linux';	
		
	}
	
	elseif(preg_match('/windows|win32/',$_user_agent)) {
		
		$_platform = 'windows';
		
	}

	elseif(preg_match('/mac/',$_user_agent)) {
		
		$_platform = 'mac';
		
	}

	elseif(preg_match('/unix/',$_user_agent)) {
		
		$_platform = 'unix';
		
	}
	
	elseif(preg_match('/webos/i',$_user_agent)) {
		
		$_platform = 'mobile';
		
	}
	
	else {
		
		$_platform = 'unrecognised';
		
	}
	
	return $_platform;
	
  }
  
  /**
  * Method generates a date in the format Month,Day,Year, Time. E.g.  Nov 2 2013 9:40AM
  *
  * @param void
  * 
  * @return string todays date 
  * @access public
  * @author Vitu Mhone <vmhone@nbsmw.com>
  */
  
  public function generate_string_date() {
	  

	$_date = date('M j Y g:iA ');
    
	return $_date;  
	  
  }
  
  /**
  * Method generates a date in the format YYYY-MM-DD HH:MM:SS for the datetime column in MSSQL
  *
  * @param void
  * 
  * @return string todays date 
  * @access public
  * @author Vitu Mhone <vmhone@nbsmw.com>
  */
  
  
  public function generate_date()
  {
    
    $_date = date('Y-m-d H:i:s');
    
    return $_date;
    
  }
          
  /**
  * Method gets a company code given an account number 
  *
  * @param void
  * 
  * @return string Company Code
  * @access public
  * @author Vitu Mhone <vmhone@nbsmw.com>
  */
  
  public function get_company_code($_account_number) {
	  
	  
	  $company_code = substr($_account_number,0,4);
		
	  //now lets generate a company code from the account number
	  /*
		
	  the company codes are below
		
	  Blantyre        : MW0010020
	  Capital City    : MW0010070
	  Ginnery Corner  : MW0010025
	  Haille Selassie : MW0010080 
	  Head Office     : MW0010001
	  Karonga         : MW0010065
	  Kasungu         : MW0010075
	  Limbe           : MW0010040
	  Mangochi        : MW0010045
	  Mchinji         : MW0010052
	  Mulanje         : MW0010055
	  Mzuzu           : MW0010030
	  Old Town        : MW0010050
	  Zomba           : MW0010060
		
		*/
		
		switch($company_code)
		{
			
			case '0020':
						$company_code = 'MW0010020';
						break; 
			
			case '0070':
						$company_code = 'MW0010070';
						break;
			
			case '0025':
						$company_code = 'MW0010025';
						break;
						
			case '0080':
						$company_code = 'MW0010080';
						break;
			
			case '0001':
						$company_code = 'MW0010001';
						break;
						
			case '0065':
						$company_code = 'MW0010065';
						break;
						
			case '0075':
						$company_code = 'MW0010075';
						break;
						
			case '0040':
						$company_code = 'MW0010040';
						break;
						
			case '0045':
						$company_code = 'MW0010045';
						break;
			
			case '0052':
						$company_code = 'MW0010052';
						break;
						
			case '0055':
						$company_code = 'MW0010055';
						break;
			
			case '0030':
						$company_code = 'MW0010030';
						break;
			
			case '0050':
						$company_code = 'MW0010050';
						break;
			
			case '0060':
						$company_code = 'MW0010060';
						break;
						
			default:
						$company_code = 'None';
		}
		
		return $company_code;
	  
  }
  
  /**
  * Method returns a numeric month representation  
  *
  * @param void
  * 
  * @return integer Numeric month representation
  * @access public
  * @author Vitu Mhone <vmhone@nbsmw.com>
  */
  
  public function get_numeric($month) 
	
	{
		
		//this method returns a numeric representation of a month given a string
		//representation
		$numeric_month = NULL;
		
		switch($month) 
		{
			
			case 'Jan':
						$numeric_month = '01';
						break;
						
			case 'Feb':
						$numeric_month = '02';
						break;
						
			case 'Mar':
						$numeric_month = '03';
						break;
						
			case 'Apr':
						$numeric_month = '04';
						break;
			
			case 'May':
						$numeric_month = '05';
						break;
						
			case 'Jun':
						$numeric_month = '06';
						break;
						
			case 'Jul':
						$numeric_month = '07';
						break;
						
			case 'Aug':
						$numeric_month = '08';
						break;
			
			case 'Sep':
						$numeric_month = '09';
						break;
						
			case 'Oct':
						$numeric_month = '10';
						break;
						
			case 'Nov':
						$numeric_month = '11';
						break;
						
			case 'Dec':
						$numeric_month = '12';
						break;
						
			default:
						$numeric_month = 'Invalid';
						
		}
		
		return $numeric_month;
		
	}

	/**
    * Method that properly formats a customer name for display 
    *
    * @param customer name in uppercase. Usually that's the globus convention
    * 
    * @return string the human readeable customer name
    * @access public
    * @author Vitu Mhone <vmhone@nbsmw.com>
    */
  
    public function format_name($customer_name) {
	  
	  return ucwords(strtolower($customer_name)) ;
	  
    }
	
	/**
    * Formats a TNM or Airtel phone number
    * by removing the prefix +265 and returns a formatted phone number  
    * to the calling method
    *
    * @param string  The customer's phone number
    * 
    * @return string the modified phone number
    * @access public
    * @author Vitu Mhone <vmhone@nbsmw.com>
    * @throws Exception if an empty string was provided
    */
  
    public function format_phone_number($mobile_number) {
	  
	  try {
		
		if(!empty($mobile_number)) {
			
			//a bit of clean up for spaces and all that
			$mobile_number = Validation::sanitise_input($mobile_number);
			
			//remove spaces inside the string
			$mobile_number = preg_replace('/\s+/', '', $mobile_number);
			
			if(substr($mobile_number, 0, 4) == '+265') {
				
				$mobile_number = substr($mobile_number,4);
				$mobile_number = 0 . $mobile_number;
				
			}
			
			else if(substr($mobile_number,0, 3) == '265') {
				
				$mobile_number = substr($mobile_number,3);
				$mobile_number = 0 . $mobile_number;	
				
			}
					
			return $mobile_number;
			
		}  
		
		else {
			
			throw new Exception('No phone number provided');
			
		}
	  }
	
	  catch(Exception $e) {
		
		return null;
	  }  
  
  
    }
	
}

/* End of file Common_Tasks.php */
/* Location: ./application/libraries/Common_Tasks.php */