<?php

    require 'lib'.DIRECTORY_SEPARATOR.'Helpers.php';

/**
 * Contains an autoload function for OFSLib
 *
 * @API
 * @summary Contains an autoload function for OFSLib
 *
 * @category  OFSLib
 * @package   OFSLib
 * @link      https://github.com/ceekays/ofslib
 * @version   $1.0.0$
 * @license   MIT License
 * @author    Edmond C. Kachale (Malawi) <kachaleedmond@gmail.com>
 *
 */

  class OFSLibAutoloader{

    /**
     * Holds the absolute path to a class file
     *
     * @var string $_class_path
     */
    private static $_class_path;

    private static $class_list  = array("Authoriser","Enquiry","Enquiry","Enquiry_Field",
                    "Enquiry_Request","Enquiry_Response","FunctionType",
                    "GTSControl","Helpers","OFSConnector","OFSException",
                    "OFSOperator","OFSUser","ProcessingFlag","SyntaxError",
                    "Transaction","Transaction","Transaction_Field",
                    "Transaction_Option","Transaction_Request",
                    "Transaction_Response");
    /**
     * Loads an OFSLib class automatically
     *
     * @param string $class_name  the name of a class that is being called
     * @return void
     */
    public static function __autoload_ofslib( $class_name ){


      self::$_class_path  = APPPATH.'libraries/lib/' ;
      self::$_class_path .= str_replace('_','/', $class_name);


      self::$_class_path .= '.php';


      if(in_array($class_name, self::$class_list) ){
          try{

              //dump(self::$_class_path);
              if (file_exists(self::$_class_path)){
                  require realpath(self::$_class_path);
              }
              else{
                  $stacktrace = debug_backtrace();
                  $last = $stacktrace[count($stacktrace) - 1];

                  $message  = 'Unable to load <b>'.$class_name.'()</b> class ';
                  $message .= 'in '.$last['file'].' on line '.$last['line'];

                  throw new Exception($message);
              }
          } catch(Exception $e){
              echo $e->getMessage();
              exit;
          }
      }
	}
  }

spl_autoload_register( array('OFSLibAutoloader', '__autoload_ofslib'));




?>

