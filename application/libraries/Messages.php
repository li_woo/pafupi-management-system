<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * EazyMobile Messages Class
 *
 * The class is used to handle mapping of error codes to error messages
 *
 * @package    EazyMobile
 * @subpackage Libraries
 * @category   Libraries
 * @author     Vitu Mhone <vmhone@nbsmw.com>
 */

class Messages {

    /**
     * Receives a message type and message id, and calls get
     * process_message to get the actual message based on the ID
     *
     *
     * @param string $message_type The type of message, e.g. ERROR, WARN
     * @param string $message_id  The specific message ID e.g BAD_LOGIN
     *
     * @return string The error message itself
     * @access public
     * @author Vitu Mhone <vmhone@nbsmw.com>
     * @throws Exception if a message category does not exist
     */

    public function get_message($_message_type,$_message_id){

        //capitalise the message type and ID
        $_message_type = strtoupper($_message_type);
        $_message_id = strtoupper($_message_id);

        //array holding the types of messages
        $_categories = array(
            'ERROR'   =>  'ERROR_MESSAGE',
            'WARN'    =>  'WARNING_MESSAGE',
            'NOTICE'  =>  'NOTICE_MESSAGE',
            'SUCCESS' =>  'SUCCESS_MESSAGE'
        );

        try {

            if(array_key_exists($_message_type,$_categories)) {

                //process the error message corresponding to the ID
                $_error_message = Messages::process_message($_message_id,$_message_type);

                return $_error_message;

            }

            else {

                throw new Exception('Invalid Message Category');

            }

        }

        catch(Exception $e) {

            $_error = $e->getMessage() . ' ' . $e->getLine();

            trigger_error($_error);

        }

    }

    /**
     * Receives a message id and returns the message to
     * the calling method, i.e. get_message
     *
     *
     * @param string $message_id
     *
     * @return string
     * @access public
     * @author Vitu Mhone <vmhone@nbsmw.com>
     * @throws Exception if a message ID does not exist
     */

    private function process_message($_message_id,$_message_type) {

        //array holding success messages and their corresponding message ID
        $_success_msgs = array(

            'USER_ADDED'         => 'The user has created succesfully',
            'USER_REMOVED'       => 'The users you selected have been removed',
            'PASSWORD_CHANGED'   => 'Password successfully changed',
            'USER_MODIFIED'      => 'User profile successfully modified'
        );

        //array holding error messages and their corresponding message ID
        $_error_msgs = array(
            'BAD_LOGIN'       => 'Wrong username or password entered',
            'INCOMPATIBLE'    => 'Sorry, the browser you are using is not compatible with EazyMobile CMS.
			                                       Please contact helpdesk@nbsmw.com.',
            'LOCKOUT'         => 'Your profile is locked out. Please contact helpdesk@nbsmw.com',
            'INACTIVE'        => 'Your profile is inactive. Contact helpdesk@nbsmw.com',
            'GENERAL_ERROR'   => 'Something went wrong while processing your request. Try again later',
            'ACCESS_DENIED'   => 'You are not authorised to access the intended resource',
            'MISSING_FIELDS'  => 'Fill all fields before you can continue',
            'PASSWORD_FAIL'   => 'The password could not be changed, as you provided an incorrect password',
            'PASSWORD_SAME'   => 'The password could not be changed, as the old and new password are the same',
            'INVALID_CHARS'   => 'Your username contains invalid characters',
            'INVALID_ACCT'    => 'Please make sure the account number is 13 digits long',
            'INVALID_BRANCH'  => 'This account cannot be registered at this branch',
            'ACCT_NOT_NUMERIC'=> 'Please make sure that the account number is numeric',
            'NO_MOBILE'       => 'The account you are trying to register does not have a mobile number registered
                                                  registered with it in T24',
            'ACCT_NOT_FOUND'  => 'The account you are trying to register does not exist',
            'EMPTY_ACCT_FIELD'=> 'Please provide an account number',
            'INVALID_ACCOUNT' => 'Please provide a valid account number',
            'TC_SERVER_FAIL'  => 'Cannot connect to T24. If this error persists, please contact helpdesk@nbsmw.com
                                                   (Error Code 6)',
            'INVALID_PHONE'   => 'The mobile number registered with the account is not valid in GLOBUS. Please check'
        );

        //array holding warning messages and their corresponding msg IDs
        $_warning_msgs = array(

            'EXPIRED_PASSWORD' => 'Your password has expired. You should renew it before
								                       you can continue'

        );

        switch($_message_type) {

            //check the message type so we know which message array to work with
            case 'ERROR'  : $_msg_array = $_error_msgs;
                break;

            case 'WARN'   : $_msg_array = $_warning_msgs;
                break;

            case 'NOTICE' : break;

            case 'SUCCESS': $_msg_array = $_success_msgs;
                break;

            default: $_msg_array = $_error_msgs;

        }

        try {

            //check if the message ID is in the array itself
            if(array_key_exists($_message_id,$_msg_array)) {

                foreach($_msg_array as $_key=>$_message) {

                    if($_key == $_message_id) {

                        //check the array for a key that matches the message ID
                        return $_message;

                    }

                }

            }

            else {

                throw new Exception('Invalid Message ID for the category you provided');

            }

        }

        catch(Exception $e) {

            $_error = $e->getMessage() . ' ' . $e->getLine();

            trigger_error($_error);

        }

    }

}

/* End of file Message.php */
/* Location: ./application/libraries/Message.php */