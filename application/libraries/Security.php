<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 /**
 * EazyMobile Security Class
 *
 * The class is used to handle hashing and generation of one time passwords
 *
 * @package    EazyMobile
 * @subpackage Libraries
 * @category   Libraries
 * @author     Vitu Mhone <vmhone@nbsmw.com>
 */

class Security {
  
  /**
  * Hashes a clear text password using SHA-256
  * and returns the hashed password to the calling method
  *
  *
  * @param string clear text password
  * 
  * @return string
  * @access public
  * @author Vitu Mhone <vmhone@nbsmw.com>
  * @throws Exception if a blank password was provided
  */
	
  public function hash_password($_password) {
	  	  
	  try {
	  	
		if(isset($_password)) {
			
			//hash the password using sha256
	  		return base64_encode(hash('sha256',$_password,true));
			
		}
		
		else
			throw new Exception('No clear text password provided');
		
	  }
	  
	  catch (Exception $e) {
		  
		$_error = $e->getMessage() . ' ' . $e->getLine();
		
		trigger_error($_error);
		  
	  }
	  
  }
  
  /**
  * Generates a random 5 digit pin
  * and returns it to the calling method
  *
  *
  * @param void method takes no arguments
  * 
  * @return integer
  * @access public
  * @author Vitu Mhone <vmhone@nbsmw.com>
  */
  
  public function generate_pin() {
  	
	$_pin = mt_rand(11111,99999);
    
	return $_pin;
	
  }
  
  /**
  * Generates a random password 
  * and returns it to the calling method
  *
  *
  * @param integer the password length as an integer but generates an eight letter password by default
  * 
  * @return string
  * @access public
  * @author Vitu Mhone <vmhone@nbsmw.com>
  */
	
  public function generate_password($_password_length = 8) {
  	
	$_alphabet = 'abcdefghijklmnopqrstuvwxyz0123456789';
	
	//array to hold the plain text
	$_plain_text = array();
	
	$_length = strlen($_alphabet) - 1;
	
	try {
		
		if(is_numeric($_password_length) && ($_password_length >= 8)) {
			
			for($i = 0; $i < $_password_length; $i++) {
				
				//get a random character from the alphabet, assigning it to the array
				$_character =  mt_rand(0,$_length);
				$_plain_text[] = $_alphabet[$_character];  
		
			}
	
		//implode the array and return the password in clear text
		$_unhashed_pwd = implode($_plain_text);
	
		return $_unhashed_pwd;
			
		}
		
		else {
			
			throw new Exception('Invalid password length');
			
		}	
	
	}
  
  	catch(Exception $e) {
		
		$_error = $e->getMessage() . ' ' . $e->getLine();
		
		trigger_error($_error);
		
	}
	
  }
  
}

/* End of file Security.php */
/* Location: ./application/libraries/Security.php */