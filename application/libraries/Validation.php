<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 /**
 * EazyMobile Validation Class
 *
 * The class is used to handle validation and santisation of data
 *
 * @package    EazyMobile
 * @subpackage Libraries
 * @category   Libraries
 * @author     Vitu Mhone <vmhone@nbsmw.com>
 */

class Validation {
  
  /**
  * Validates and email address using regex
  * and returns a boolean value to the calling method
  *
  *
  * @param string the email address that needs to be validated
  * 
  * @return boolean
  * @access public
  * @author Vitu Mhone <vmhone@nbsmw.com>
  */
  
  public function validate_email($_email_address) {
  	
	try {
		
		if(!empty($_email_address)) {
			
			//validate the email address	
			$_regex = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/'; 
			
			if(preg_match($_regex,$_email_address)) {
				
				//if the email is valid
				return true;
				
			}
			
			else {
				
				//if the email is not valid	
				return false;
				
			}
				
		}  
		
		else {
			
			throw new Exception('An email address has not been provided');
			
		}
	}
	
	catch(Exception $e) {
		
		$_error = $e->getMessage() . ' ' . $e->getLine();
		trigger_error($_error);
	}
  
  }
  
  /**
  * Prepares a query
  * and returns it to the calling method
  *
  * @param string The SQL statement
  * 
  * @return string The prepared SQL statment
  * @access public
  * @author Vitu Mhone <vmhone@nbsmw.com>
  * @throws Exception if an empty string was provided
  */
  
  public function prepare_query($statement) 
  
  {
          
    try {

          if(!empty($statement)) {
            
             $find = 'OR';
             $replace = '';
             $result = preg_replace(strrev("/$find/"),strrev($replace),strrev($statement),1);
             return strrev($result);
          }  

          else {

              throw new Exception('No SQL statement provided');

          }
      }

      catch(Exception $e) {

          return 'Invalid Input';
      }
    
  }
  
  /**
  * Sanitises a string for safe storage
  * and returns a sanitised string to the 
  * calling method
  *
  * @param string The unsanitised string
  * @param boolean if this flag is passed, then the regex will execute, enforcing a more rigorous filter
  * 
  * @return mixed depends on if the method could sanitise the string or not
  * @access public
  * @author Vitu Mhone <vmhone@nbsmw.com>
  * @throws Exception if an empty string was provided
  */
  
  public function sanitise_input($value, $flag = false) {
	
	try {
		
		if(!empty($value)) {
			
			//sanitise the string      
			$value = str_replace("'","''",$value);
            $value = str_replace("\0","",$value);
            $value = str_replace("\b","",$value);
            $value = str_replace("\n","",$value);
            $value = str_replace("\r","",$value);
            $value = str_replace("\t","",$value);
            $value = str_replace("\Z","",$value);
            $value = str_replace("\\","",$value);
            $value = str_replace("\%","",$value);
            $value = str_replace("\_","",$value);
			$value = str_replace(';','',$value);
			$value = str_replace('+','',$value);
			$value = str_replace('*','',$value);
			$value = str_replace('=','',$value);
			$value = str_replace('%','',$value);
			$value = str_replace('--','',$value);
			$value = str_replace('/*','',$value);
			$value = str_replace('*/','',$value);
			$value = str_replace('//','',$value);
			$value = trim($value);
			$value = strip_tags($value);
			
			if(substr($value,0,3) == 'xp_') {
				$value = str_replace(subtr($value,0,3),'');
			}
			
			if($flag == true) {
		
				$value = preg_replace('/[^a-zA-Z0-9]+/', '', $value);
				
			}
            
            $value = strip_tags($value);
		
			return $value;
			
		}  
		
		else {
			
			throw new Exception('Empty string provided');
			
		}
	}
	
	catch(Exception $e) {
		
		return null;
	}
  
  }

}
