<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Branch_helper {
  public function __construct(){

  }
    public function get_branch_code($branch)
    {

        //this method returns a branch code given a branch

        //now lets generate a branch code

        /*

        The company codes are below

        Blantyre        : MW0010020
        Capital City    : MW0010070
        Ginnery Corner  : MW0010025
        Haille Selassie : MW0010080
        Head Office     : MW0010001
        Karonga         : MW0010065
        Kasungu         : MW0010075
        Limbe           : MW0010040
        Mangochi        : MW0010045
        Mchinji         : MW0010052
        Mulanje         : MW0010055
        Mzuzu           : MW0010030
        Old Town        : MW0010050
        Zomba           : MW0010060

        */

        switch($branch)
        {

            case 'Blantyre Branch':
                $company_code = 'MW0010020';
                break;

            case 'Capital City Branch':
                $company_code = 'MW0010070';
                break;

            case 'Ginnery Corner Branch':
                $company_code = 'MW0010025';
                break;

            case 'Haille Selassie Branch':
                $company_code = 'MW0010080';
                break;

            case 'Head Office':
                $company_code = 'MW0010001';
                break;

            case 'Karonga Branch':
                $company_code = 'MW0010065';
                break;

            case 'Kasungu Branch':
                $company_code = 'MW0010075';
                break;

            case 'Limbe Branch':
                $company_code = 'MW0010040';
                break;

            case 'Mangochi Branch':
                $company_code = 'MW0010045';
                break;

            case 'Mchinji Branch':
                $company_code = 'MW0010052';
                break;

            case 'Mulanje Branch':
                $company_code = 'MW0010055';
                break;

            case 'Mzuzu Branch':
                $company_code = 'MW0010030';
                break;

            case 'Old Town Branch':
                $company_code = 'MW0010050';
                break;

            case 'Zomba Branch':
                $company_code = 'MW0010060';
                break;

            default:
                $company_code = 'None';
        }

        return $company_code;


    }


    public function get_branch($branch)
{

    //this method returns a branch code given a branch

    //now lets generate a branch code

    /*

    The company codes are below

    Blantyre        : MW0010020
    Capital City    : MW0010070
    Ginnery Corner  : MW0010025
    Haille Selassie : MW0010080
    Head Office     : MW0010001
    Karonga         : MW0010065
    Kasungu         : MW0010075
    Limbe           : MW0010040
    Mangochi        : MW0010045
    Mchinji         : MW0010052
    Mulanje         : MW0010055
    Mzuzu           : MW0010030
    Old Town        : MW0010050
    Zomba           : MW0010060

    */

    switch($branch)
    {

        case 'MW0010020':
            $company_code = 'Blantyre Branch';
            break;

        case 'MW0010070':
            $company_code = 'Capital City Branch';
            break;

        case 'MW0010025':
            $company_code = 'Ginnery Corner Branch';
            break;

        case 'MW0010080':
            $company_code = 'Haille Selassie Branch';
            break;

        case 'MW0010001':
            $company_code = 'Head Office';
            break;

        case 'MW0010065':
            $company_code = 'Karonga Branch';
            break;

        case 'MW0010075':
            $company_code = 'Kasungu Branch';
            break;

        case 'MW0010040':
            $company_code = 'Limbe Branch';
            break;

        case 'MW0010045':
            $company_code = 'Mangonchi Branch';
            break;

        case 'MW0010052':
            $company_code = 'Mchinji Branch';
            break;

        case 'MW0010055':
            $company_code = 'Mulanje Branch';
            break;

        case 'MW0010030':
            $company_code = 'Mzuzu Branch';
            break;

        case 'MW0010050':
            $company_code = 'Old Town Branch';
            break;

        case 'MW0010060':
            $company_code = 'Zomba Branch';
            break;

        default:
            $company_code = 'None';
    }

    return $company_code;


}
    public function get_name($branch)
    {

        //this method returns a branch code given a branch

        //now lets generate a branch code

        /*

        The company codes are below

        Blantyre        : MW0010020
        Capital City    : MW0010070
        Ginnery Corner  : MW0010025
        Haille Selassie : MW0010080
        Head Office     : MW0010001
        Karonga         : MW0010065
        Kasungu         : MW0010075
        Limbe           : MW0010040
        Mangochi        : MW0010045
        Mchinji         : MW0010052
        Mulanje         : MW0010055
        Mzuzu           : MW0010030
        Old Town        : MW0010050
        Zomba           : MW0010060

        */

        switch($branch)
        {

            case 'MW0010020':
                $company_code = 'Blantyre';
                break;

            case 'MW0010070':
                $company_code = 'Capital City';
                break;

            case 'MW0010025':
                $company_code = 'Ginnery Corner';
                break;

            case 'MW0010080':
                $company_code = 'Haille Selassie';
                break;

            case 'MW0010001':
                $company_code = 'Head Office';
                break;

            case 'MW0010065':
                $company_code = 'Karonga';
                break;

            case 'MW0010075':
                $company_code = 'Kasungu';
                break;

            case 'MW0010040':
                $company_code = 'Limbe';
                break;

            case 'MW0010045':
                $company_code = 'Mangonchi';
                break;

            case 'MW0010052':
                $company_code = 'Mchinji Branch';
                break;

            case 'MW0010055':
                $company_code = 'Mulanje Branch';
                break;

            case 'MW0010030':
                $company_code = 'Mzuzu Branch';
                break;

            case 'MW0010050':
                $company_code = 'Old Town Branch';
                break;

            case 'MW0010060':
                $company_code = 'Zomba Branch';
                break;

            default:
                $company_code = 'None';
        }

        return $company_code;


    }
    public function get_branch_district($branch)
    {

        //this method returns a branch code given a branch

        //now lets generate a branch code

        /*

        The company codes are below

        Blantyre        : MW0010020
        Capital City    : MW0010070
        Ginnery Corner  : MW0010025
        Haille Selassie : MW0010080
        Head Office     : MW0010001
        Karonga         : MW0010065
        Kasungu         : MW0010075
        Limbe           : MW0010040
        Mangochi        : MW0010045
        Mchinji         : MW0010052
        Mulanje         : MW0010055
        Mzuzu           : MW0010030
        Old Town        : MW0010050
        Zomba           : MW0010060

        */

        switch($branch)
        {

            case 'MW0010020':
                $company_code = 'Blantyre';
                break;

            case 'MW0010070':
                $company_code = 'Lilongwe';
                break;

            case 'MW0010025':
                $company_code = 'Blantyre';
                break;

            case 'MW0010080':
                $company_code = 'Lilongwe';
                break;

            case 'MW0010001':
                $company_code = 'Blantyre';
                break;

            case 'MW0010065':
                $company_code = 'Karonga';
                break;

            case 'MW0010075':
                $company_code = 'Kasungu';
                break;

            case 'MW0010040':
                $company_code = 'Limbe';
                break;

            case 'MW0010045':
                $company_code = 'Mangonchi';
                break;

            case 'MW0010052':
                $company_code = 'Mchinji';
                break;

            case 'MW0010055':
                $company_code = 'Mulanje';
                break;

            case 'MW0010030':
                $company_code = 'Mzuzu';
                break;

            case 'MW0010050':
                $company_code = 'Lilongwe';
                break;

            case 'MW0010060':
                $company_code = 'Zomba';
                break;

            default:
                $company_code = 'None';
        }

        return $company_code;


    }
    public function get_branch_region($branch)
    {

        //this method returns a branch code given a branch

        //now lets generate a branch code

        /*

        The company codes are below

        Blantyre        : MW0010020
        Capital City    : MW0010070
        Ginnery Corner  : MW0010025
        Haille Selassie : MW0010080
        Head Office     : MW0010001
        Karonga         : MW0010065
        Kasungu         : MW0010075
        Limbe           : MW0010040
        Mangochi        : MW0010045
        Mchinji         : MW0010052
        Mulanje         : MW0010055
        Mzuzu           : MW0010030
        Old Town        : MW0010050
        Zomba           : MW0010060

        */

        switch($branch)
        {

            case 'MW0010020':
                $company_code = 'S';
                break;

            case 'MW0010070':
                $company_code = 'C';
                break;

            case 'MW0010025':
                $company_code = 'S';
                break;

            case 'MW0010080':
                $company_code = 'S';
                break;

            case 'MW0010001':
                $company_code = 'S';
                break;

            case 'MW0010065':
                $company_code = 'N';
                break;

            case 'MW0010075':
                $company_code = 'C';
                break;

            case 'MW0010040':
                $company_code = 'S';
                break;

            case 'MW0010045':
                $company_code = 'S';
                break;

            case 'MW0010052':
                $company_code = 'C';
                break;

            case 'MW0010055':
                $company_code = 'S';
                break;

            case 'MW0010030':
                $company_code = 'N';
                break;

            case 'MW0010050':
                $company_code = 'C';
                break;

            case 'MW0010060':
                $company_code = 'S';
                break;

            default:
                $company_code = 'S';
        }

        return $company_code;


    }
} 