<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 /**
 * EazyMobile Network Class
 *
 * The class is used to handle networking related logic
 *
 * @package    EazyMobile
 * @subpackage Libraries
 * @category   Libraries
 * @author     Vitu Mhone <vmhone@nbsmw.com>
 */

class Network {
  
  /**
  * Server protocal checker 
  *
  * Checks if the server is using HTTPS (443)
  * or HTTP (80)
  *
  * @param  void method takes no arguments
  * @return boolean
  * @access public
  * @author Vitu Mhone <vmhone@nbsmw.com>
  */
  
  public function server_is_ssl()
  {
  	
	 if(isset($_SERVER['HTTPS'])) {
	 	
       	if(strtolower($_SERVER['HTTPS'] == 'on'))
           	 return true;
		else if ($_SERVER['HTTPS'] == '1')
           return true;
   		} 
   		else if (isset($_SERVER['SERVER_PORT']) && ($_SERVER['SERVER_PORT'] == '443')) {
      	  return true;
        }
		else {
			return false;
		}
	
  }
    
  /**
  * Checks for connectivity of a TC server
  *
  * Attempts to establish a connection to the TC server
  * and returns the response as true or false
  *
  * @param  void method takes no arguments
  * @return boolean
  * @access public
  * @author Vitu Mhone <vmhone@nbsmw.com>
  */
  
  public function ping_server() {
	  
	  $_ip_address = '10.51.3.17';
	  
	  $_port_number = 8080;
	  
	  $_listener_port_number = 10006;
	  
	  $_time_b = time();
		 
  	  $_socket = @fSockOpen($_ip_address, $_port_number, $_errno, $_errstr, 2); 
	  
	  //check the listener as well
	  $_socket_listener = @fSockOpen($_ip_address, $_listener_port_number, $_errno, $_errstr, 2); 
	  
  	  if ((!$_socket) || (!$_socket_listener))
	  {
			
			//if the socket cannot be established for either the tc server 
			//or the listener itself
			return false; 
		
	   } 
		
	   else 
	   {
			
  			$_time_a = time();
			
			//find the latency  
  			$_latency = $_time_b - $_time_a; 
			
			if($_latency >= 5)
			
				//if more than 5 seconds, elapsed while attempting
				//to open the socket, return false
				return false;
			
			else
				return true;
		
	   }
	  
  }
  
  /**
  * Gets the IP address of a computer on a LAN
  *
  *
  * @param  void method takes no arguments
  * @return string the IP address of the connecting machine
  * @access public
  * @author Vitu Mhone <vmhone@nbsmw.com>
  */
  
  public function get_ip_address() {
	  
	  if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		
		$_ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];  
		  
	  }
	  
	  else if(isset($_SERVER['REMOTE_ADDR'])) {
	  		
		$_ip_address = $_SERVER['REMOTE_ADDR'];
			
	  }
	  
	  else {
		
		//if the IP address could not be determined  
		$_ip_address = '0.0.0.0';
		  
	  }
	 
	  return $_ip_address;
	  
  }
  
}

/* End of file Network.php */
/* Location: ./application/libraries/Network.php */