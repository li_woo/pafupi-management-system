@include('header')
@include('menu')



<div class="section">


    <div id="body">
        <div align="center">
            <form id="reset" action="{{$base_url}}/pafupi/main/password_reset_action">
                <table class="form-container" cellspacing="0" cellpadding="0" border="0">
                    <tr><td><div class="user-info-header"><p>Changing Password for : {{$fullname}}</p></div></td></tr>
                    <tr>
                        <td>
                            @if($feedback)
                            <div><p style="color: red">{{$feedback}}</p></div>
                            @endif
                            <div class="inputs">
                                <label class="label">Current Password</label><br />
                                <label class="label">New Password</label><br />
                                <label class="label">Confirm Password</label><br />
                            </div>
                            <div class="inputs">
                                <input type="hidden" name="user_id" value="{{$user_id}}" id="user_id" />
                                <input type="hidden" name="fullname" id="fullname" value="{{$fullname}}" />
                                <input type="hidden" name="username" id="username" value="{{$username}}" />
                                <input name="password" type="password"  class="custom[required]" id="password" size="30" class="textbox" /><br />
                                <input name="new_password" type="password"  class="custom[required]" id="new_password" size="30" class="textbox" /><br />
                                <input name="confirm_password" type="password" class="custom[required]" id="confirm_password" size="30" class="textbox" /><br />
                                <input type="submit" class="bluebtn" name="button" id="button" value="Change Password" />
                            </div>

                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>


</div>

@include('footer')