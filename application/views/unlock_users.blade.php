@include('header')
@include('menu')
<script>
    function generate(type,content) {
        var n = noty({
            text        : content,
            type        : type,
            dismissQueue: true,
            layout      : 'top',
            theme       : 'defaultTheme',
            maxVisible  : 10,
            buttons: [
                {addClass: 'btn btn-primary', text: 'Ok', onClick: function($noty) {
                    $noty.close();
                }
                }]
        });

    }

    function generateAll(type,data) {

        generate(type,data);
    }
    function confirm_lock(){
        var n = noty({
            text: 'Are you sure you want to unlock user(s)',
            type: 'error',
            modal: 'true',
            dismissQueue: true,
            layout: 'top',
            theme: 'default',
            buttons: [
                {addClass: 'btn btn-primary', text: 'Ok', onClick: function($noty) {

                    var dataForm =  $( "#unlock_users" ).serialize();
                    var url = "{{$base_url}}pafupi/main/unlock_user";
                    $.ajax({
                        "type":"POST",
                        "url":url,
                        "data":dataForm,
                        "dataType":"json",
                        "success":function(data){
                            if(data !=0){
                                $.each(data,function(i,item){
                                    $('tr#'+item).remove();
                                })
                                var rows=$('table').find('tr.rows').length;
                                if(rows <1){
                                    $('#select_all').hide();
                                }
                                $('.lock-disable').hide();
                                $('.note').show();
                                generateAll('success',"user(s) successfully unlocked");
                            }
                            else{
                                generateAll('error','failed to unlock user(s)');
                            }
                        },
                        "error" : function(){
                            generateAll('error','failed to unlock user(s)');
                        }
                    });

                    $noty.close();

                }
                },
                {addClass: 'btn btn-primary', text: 'Cancel', onClick: function($noty) {


                    $noty.close();
                    return false;
                }
                }
            ]
        });
    }
    $(document).ready(function (){
        $('.lock-disable').hide();$('.note').show();
        $('.case').bind('change', function (){
            if  (this.checked) {

                $('.lock-disable').show();
                $('.note').hide();
            }
            else {
                var  checkboxes = $('.pafupi_tabl').find('.case' );
                for(var i=0; i< checkboxes.length;i++){
                    if  (checkboxes[i].checked) {

                        $('.lock-disable').show();
                        $('.note').hide();
                        break;
                    }
                    else {


                        $('.lock-disable').hide();
                        $('.note').show();
                    }
                }
                //$('.delete-disabled').hide();
                //$('.note').show();
            }
        })
        //$('.lock-disable').hide();
        $('#selectall').bind('change', function (){

            var  checkboxes = $('.pafupi_tabl').find('.case' );
            if  (this.checked) {
                checkboxes.attr('checked', 'true');
                $('.lock-disable').show();
                $('.note').hide();
            }
            else {

                checkboxes.attr('checked', false);
                $('.lock-disable').hide();
                $('.note').show();
            };
        });



        $( "#unlock_users").submit(function(event){
            event.preventDefault();

            confirm_lock();




        })




        function paginate(){
            $('table.paginate tr.pp').remove();
            var rows=$('table').find('tr.rows').length;

            var no_rec_per_page=5;
            var no_pages= Math.ceil(rows/no_rec_per_page);
            var $pagenumbers=$('<tr id="pp"></tr>');

            for(i=0;i<no_pages;i++)
            {
                $('<td class="pagination-item page"><a href="#" class="clickable">'+(i+1)+'</a></td>').appendTo($pagenumbers);
            }

            $pagenumbers.insertAfter('table.paginate');

            $('.page').hover(
                function(){
                    $(this).addClass('hover pagination-item-current');
                },
                function(){
                    $(this).removeClass('hover pagination-item-current');
                }
            );
            $('.page').bind('hover', function(){

            })
            $('table').find('tr.rows').hide();
            var tr=$('table tr.rows');
            $(tr[0]).show();if(rows >= 2){$(tr[1]).show();}if(rows >= 3){$(tr[2]).show();}if(rows >= 4){$(tr[3]).show();}if(rows >= 5){$(tr[4]).show();}
            $('a.clickable').click(function(event){
                event.preventDefault();
                $('table').find('tr.rows').hide();
                for(var i=($(this).text()-1)*no_rec_per_page;
                    i<=$(this).text()*no_rec_per_page-1;
                    i++)
                {
                    $(tr[i]).show();
                }
            });
        }

@if($search_feedback)
            paginate();
 @endif
    })
</script>
<div class="section">


    <div id="body">
        <div align="center">
            <p style="color: red">
                @for ($i = 0; $i <count($feedback); $i++)
                {{$feedback[$i]}} <br />
                @endfor
            </p>
            <p style="color: red">
                @if($search_feedback)
                {{$search_feedback}}<br />
                @endif

            </p>
            <div class="pafupi_tabl" >
                @if (count($users) >=1)
                <table border="0" cellpadding="0" cellspacing="0" class="full-tbl-container">
                    <tr>
                        <td colspan="7" class="tbl-header">
                            <form id="search" action="{{$base_url}}pafupi/main/unlock_user" method="post">
                                <table>
                                    <tr>
                                        <td width="50"></td>
                                        <td width="">Select the users you would like to unlock</td>
                                        <td width="400"><input type="text" id="param" name="param" placeholder="Search user" class="validate[required]"/></td>
                                        <input type="hidden" name="search" value="s" />
                                        <td width=""><button type="submit" class="btn btn-primary">Search</button></td>
                                    </tr>
                                </table>
                            </form>
                        </td>
                    </tr>
                    <form id="unlock_users" action="{{$base_url}}pafupi/main/unlock_user" method="post">
                        <input type="hidden" name="delete" value="yes" />

                        <tr class="tbl-titles">
                            <td>&nbsp;</td>
                            <td>
                                Username
                            </td>
                            <td>
                                Full Name
                            </td>
                            <td >
                                Branch Name
                            </td>
                            <td >
                                RoleName
                            </td>
                            <td>
                                Last Login
                            </td>
                        </tr>
                        @foreach ($users as $user)
                        <tr id="{{$user['UserID']}}" class="tbl-rows rows">
                            <td>
                                <input type="checkbox" class="case" value="{{$user['UserID']}}"
                                       name="selected_users[]" id="selected_users[]" />
                            </td>
                            <td >
                                {{ $user['Username'] }}
                            </td>
                            <td >
                                {{ $user['FullName'] }}
                            </td>
                            <td >
                                {{ $user['Branch'] }}
                            </td>
                            <td>
                                {{ $user['RoleName'] }}
                            </td>
                            <td>
                                {{ date('d-m-Y',strtotime($user['LastLogin'])) }}
                            </td>

                        </tr>
                        @endforeach
                        <tr id="select_all">

                            <td colspan="1" class="tbl-footer-checks" >


                                <input type="checkbox" id="selectall" />



                            </td>
                            <td colspan="6" class="tbl-footer">
                                <div class="tbl-options">
                                    <button type="submit"  class="btn btn-danger lock-disable">Unlock Selected</button>
                                </div>
                                <div class="tbl-pagination">

                                    <table class="paginate" cellspacing="0" cellpadding="0" border="0">
                                        <tr class="pp">
                                            {{$this->pagination->create_links()}}
                                        </tr>
                                    </table>
                                </div>
                            </td>
                    </form>
                    <!--<p class="note">No other user profiles are eligible for locking. <a href="{{$base_url}}pafupi/main/create_user">Would you like to register a new user profile?</a></p> <br />-->
                </table>

                @else
                No users to show
                @endif
            </div><br />


        </div>
    </div>

</div>
@include('footer')