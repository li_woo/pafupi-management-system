@include('header')
@include('menu')
@if($errors)
<script>
    function generateA(layout,type,content) {
        var n = noty({
            text        : content,
            type        : type,
            layout      : layout,
            dismissQueue: true,
            theme       : 'defaultTheme',
            maxVisible  : 10,
            buttons: [
                {addClass: 'btn btn-primary', text: 'Ok', onClick: function($noty) {
                    $noty.close();
                }
                }

            ]
        });
        console.log('html: ' + n.options.id);
    }

    function generate(layout,type,data) {

        generateA(layout,type,data);
    }




    @if(isset($errors['percent']))
        $(document).ready(function(){
            generate('top','information',"{{$errors['percent']}}")
        })
    @endif





</script>
@endif
<div class="section">
    <div align="center">




    </div>
    <form action="{{$base_url}}pafupi/main/upload_referal" enctype="multipart/form-data" method="post">
        <table class="form-container" cellspacing="0" cellpadding="0" border="0">
            <tr><td><div class="user-info-header"><p>Upload Referal excel file</p></div></td></tr>
            <tr>
                <td>
                    <div class="inputs">
                        <label for="excel">Select excel file</label><br />

                    </div>
                    <div class="inputs">
                        <input type="file" name="userfile" multiple /><br />
                        <button type="submit" class="btn-primary submit" >Upload</button>
                    </div>
                </td>
            </tr>
        </table>
    </form>


</div>
</div>


</div>

@include('footer')