@include('header')
@include('menu')
<script>
    $(document).ready(function (){
        $('.case').bind('change', function (){
            if  (this.checked) {

                $('.lock-disable').show();

            }
            else {
                var  checkboxes = $('.pafupi_table').find('.case' );
                for(var i=0; i< checkboxes.length;i++){
                    if  (checkboxes[i].checked) {

                        $('.lock-disable').show();

                        break;
                    }
                    else {


                        $('.lock-disable').hide();

                    }
                }
                //$('.delete-disabled').hide();
                //$('.note').show();
            }
        })
        $('.lock-disable').hide();
        $('#selectall').bind('change', function (){

            var  checkboxes = $('.pafupi_table').find('.case' );
            if  (this.checked) {
                checkboxes.attr('checked', 'true');
                $('.lock-disable').show();
                $('.note').hide();
            }
            else {

                checkboxes.attr('checked', false);
                $('.lock-disable').hide();
                $('.note').show();
            };
        });
    })
</script>
<div id="section">
    <div id="body">
        <div align="center">
            <p style="color: red">
                @for ($i = 0; $i <count($feedback); $i++)
                {{$feedback[$i]}} <br />
                @endfor
            </p>
            <p style="color: red">
                @if($search_feedback)
                {{$search_feedback}}<br />
                @endif

            </p>
            @if (count($accounts) >=1)
            <div class="pafupi_table" >
                <form id="search" action="{{$base_url}}pafupi/main/manage_starterpacks" method="post">
                    <input type="text" id="param" name="param" placeholder="Search account" class="validate[required]"/>
                    <input type="hidden" name="search" value="s" />
                    <button type="submit" class="btn btn-primary">Search</button>
                </form>
                <table class="table-sort table-sort-search">

                        <tr class="tbl-titles">
                            <td>&nbsp;</td>
                            <td>
                                Account Number
                            </td>

                            <td>
                                Date Created
                            </td>
                            <td>
                                Opened By
                            </td>

                        </tr>
                        @foreach ($accounts as $account)
                        <tr>
                            <td>
                                <input type="checkbox" class="case" value="{{$account['acc_no']}}"
                                       name="selected_accounts[]" id="selected_accounts[]" />
                            </td>
                            <td >
                                {{ $account['acc_no'] }}
                            </td>

                            <td>
                                {{ date('d-m-Y',strtotime($account['date_created'])) }}
                            </td>
                            <td>
                                {{ $account['opened_by'] }}
                            </td>
                        </tr>
                        @endforeach
                        <tr id="select_all">
                            <td>&nbsp;</td>
                            <td colspan="5" >
                                <div>

                                    <span ><input type="checkbox" id="selectall" />check all</span>




                                </div><br /><br />
                                <div >

                                    <table class="paginate" cellspacing="0" cellpadding="0" border="0">
                                        <tr>
                                            {{$this->pagination->create_links()}}
                                        </tr>
                                    </table>
                                    <button type="submit"  class="btn btn-danger lock-disable">dispatch Selected</button>

                </table>
            </div><br />
            <span><a href="{{$base_url}}" class="btn btn-primary">Download excel</a></span>
            @else
            No accounts to dispatch
            @endif
        </div>
    </div>
</div>
@include('footer')