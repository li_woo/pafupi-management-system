@include('header')
@include('menu')
<script>
    function generate(type,content) {
        var n = noty({
            text        : content,
            type        : type,
            dismissQueue: true,
            layout      : 'topCenter',
            theme       : 'defaultTheme',
            maxVisible  : 10
        });

    }

    function generateAll(type,data) {

        generate(type,data);
    }
    $(document).ready(function (){
        $("form#search").validationEngine('attach', {
            onValidationComplete: function(form, status){
                $('h5#feedback').html('');
                if(status==true){
                    dataForm = form.serialize();
                    $(":submit").attr("disabled", true);
                    url = "{{$base_url}}pafupi/main/search_user";
                    $('div#feed').html('').show();
                    $.ajax({
                        "type":"POST",
                        "url":url,
                        "data":dataForm,
                        "dataType":"json",
                        "success":function(data){
                            $('h3#feedback').html("results for '"+ $('#search_string').val()+"'");
                            if(data !=0){
                                $('#search_string').text('');
                                $('table').find('tr.rows').remove();
                                $('table.paginate').remove();
                                $.each(data.result,function(i,item){
                                    $('tr.tbl-titles').after(tmpl('user_row',item));
                                });
                                paginate();
                            }
                            else
                            {


                            }
                        },
                        "error" : function(){

                        }
                    });
                    $(":submit").attr("disabled", false);
                }


            }

        });


        $('.delete-disabled').hide();$('.note').show();
        $('.case').bind('change', function (){
            if  (this.checked) {

                $('.delete-disabled').show();
                $('.note').hide();
            }
            else {
                var  checkboxes = $('.pafupi_table').find('.case' );
                for(var i=0; i< checkboxes.length;i++){
                    if  (checkboxes[i].checked) {

                        $('.delete-disabled').show();
                        $('.note').hide();
                        break;
                    }
                    else {


                        $('.delete-disabled').hide();
                        $('.note').show();
                    }
                }
                //$('.delete-disabled').hide();
                //$('.note').show();
        }
        })
        $('#selectall').bind('change', function (){

            var  checkboxes = $('.pafupi_table').find('.case' );
            if  (this.checked) {
                checkboxes.attr('checked', 'true');
                $('.delete-disabled').show();
                $('.note').hide();
            }
            else {

                checkboxes.attr('checked', false);
                $('.delete-disabled').hide();
                $('.note').show();
            };
        });
        $('.delete-disabled').bind('click',function(e){
            e.preventDefault();
            $( "#delete_action").submit(function(event){
                event.preventDefault();
                $.confirm({
                    'title'		: 'Delete Profile',
                    'message'	: 'Are you sure you want to delete user(s)?',
                    'buttons'	: {
                        'Yes'	: {
                            'class'	: 'blue',
                            'action': function(){
                                var dataForm =  $( "#delete_action" ).serialize();
                                var url = "{{$base_url}}pafupi/main/delete_user";
                                $.ajax({
                                    "type":"POST",
                                    "url":url,
                                    "data":dataForm,
                                    "dataType":"json",
                                    "success":function(data){
                                        if(data !=0){
                                        $.each(data,function(i,item){
                                            $('tr#'+item).remove();
                                        })
                                        var rows=$('table').find('tr.rows').length;
                                        if(rows <1){
                                            $('#select_all').hide();
                                        }
                                        $('.delete-disabled').hide();
                                        $('.note').show();
                                        generateAll('success',"user(s) successfully deleted");
                                        }
                                        else{
                                            generateAll('error','failed to delete user(s)');
                                        }
                                    },
                                    "error" : function(){
                                        generateAll('error','failed to delete user(s)');
                                    }
                                });
                            }
                        },
                        'No'	: {
                            'class'	: 'gray',
                            'action': function(){ }
                        }
                    }
                });

           })

         $( "#delete_action").trigger('submit');

        })



        function paginate(){
            var rows=$('table').find('tr.rows').length;

            var no_rec_per_page=5;
            var no_pages= Math.ceil(rows/no_rec_per_page);
            var $pagenumbers=$('<div id="pages" style="background-color:lightpink"></div>');
            for(i=0;i<no_pages;i++)
            {
                $('<span class="page">'+(i+1)+'</span>').appendTo($pagenumbers);
            }
            $pagenumbers.insertAfter('table');

            $('.page').hover(
                function(){
                    $(this).addClass('hover');
                },
                function(){
                    $(this).removeClass('hover');
                }
            );
            $('.page').bind('hover', function(){

            })
            $('table').find('tr.rows').hide();
            var tr=$('table tr.rows');
            $(tr[0]).show();
            $('span.page').click(function(event){
                $('table').find('tr.rows').hide();
                for(var i=($(this).text()-1)*no_rec_per_page;
                    i<=$(this).text()*no_rec_per_page-1;
                    i++)
                {
                    $(tr[i]).show();
                }
            });
        }


    })

</script>
<style>
    .page{
        font-size:20px;
        background-color:white;
    }
    .page:hover {
        cursor:pointer;
        background-color:grey;
    }
</style>


<div class="section">
    <div align="center">
    @if (count($users) >=1)

        <form id="search" action="{{$base_url}}pafupi/main/delete_user" method="post">
            <input type="text" id="search_string" name="search_string" placeholder="Search User" class="validate[required]"/>
            <input type="hidden" name="search_type" value="E" />
            <button type="submit" class="btn btn-primary">Search</button>
        </form>
        <div class="pafupi_table" >
    <table class="table-sort table-sort-search">

        <form id="delete_action" method="post">

        <tr class="tbl-titles">
            <td>&nbsp;</td>
            <td>
                Username
            </td>
            <td>
                Full Name
            </td>
            <td >
                Branch Name
            </td>
            <td >
                RoleName
            </td>
            <td>
                Last Login
            </td>


        </tr>

        @foreach ($users as $user)
        <tr class="rows" id="{{$user['UserID']}}">
            <td>
                <input type="checkbox" class="case" value="{{$user['UserID']}}"
                       name="selected_users[]" id="selected_users[]" />
            </td>
            <td >
                {{ $user['Username'] }}
            </td>
            <td >
                {{ $user['FullName'] }}
            </td>
            <td >
                {{ $user['Branch'] }}
            </td>
            <td>
                {{ $user['RoleName'] }}
            </td>
            <td>
                {{ date('d-m-Y',strtotime($user['LastLogin'])) }}
            </td>

        </tr>
        @endforeach

        <tr id="select_all">
            <td>&nbsp;</td>
            <td colspan="5" >
                <div>

                    <span ><input type="checkbox" id="selectall" />check all</span>




                </div><br /><br />
                <div >

                    <table class="paginate" cellspacing="0" cellpadding="0" border="0">
                        <tr class="pp">
                           {{$this->pagination->create_links()}}
                        </tr>
                    </table>
                    <a href="" class="btn btn-danger delete-disabled">Delete Selected</a>
                    </form>
                    <p class="note">No other user profiles are eligible for deletion. <a href="{{$base_url}}pafupi/main/create_user">Would you like to register a new user profile?</a></p> <br />

                </div>
            </td>
        </tr>
    </table>
         </div>
    @endif
     </div>
</div>
<script id="user_row" type="text/x-tmpl">
    <tr class="rows" id="{%=o.UserID%}">
                        <td>
                            <input type="checkbox" class="case" value="{%=o.UserID%}"
                                   name="selected_users[]" id="selected_users[]" />
                        </td>
                        <td >
                            {%=o.Username%}
                        </td>
                        <td >
                            {%=o.FullName%}
                        </td>
                        <td >
                            {%=o.branch_name%}
                        </td>
                        <td>
                            {%=o.RoleName%}
                        </td>
                        <td>
                            {%o.LastLogin%}
                        </td>

                    </tr>
</script>
@include('footer')