@include('header')
@include('menu')
<div class="section">



            <form id="reset" method="post" action="{{$base_url}}pafupi/main/password_reset_action">
            <table class="form-container" cellspacing="0" cellpadding="0" border="0">
                <tr><td><div class="user-info-header"><p>Reseting your Password</p></div></td></tr>
                <tr>
                    <td>
                        @if($feedback)
                          @if(validation_errors())
                          <span style="color:red">{{validation_errors()}}</span>
                          @endif
                        <script type="text/javascript">
                            $(document).ready(function(){

                                function generate(layout) {
                                    var n = noty({
                                        text: "{{$feedback}}",
                                        type: 'error',
                                        modal: 'true',
                                        template:'<p></p>',
                                        dismissQueue: true,
                                        layout: layout,
                                        theme: 'default',
                                        buttons: [
                                            {addClass: 'btn btn-primary', text: 'Ok', onClick: function($noty) {

                                                // this = button element
                                                // $noty = $noty element

                                                //noty({text: 'You clicked "Ok" button', type: 'success'});
                                                //sendForm();
                                                $noty.close();
                                            }
                                            }
                                        ]
                                    });
                                    console.log('html: '+n.options.id);
                                }

                                function generateAll() {
                                    generate('top');
                                }

                                $(document).ready(function() {

                                    generateAll();

                                });

                            });

                        </script>

                        @endif

                        <div class="inputs">
                            <label >Current Password</label>
                            <input name="current_password" type="password"  class="custom[required] textbox" id="current_password" size="30" class="textbox" /><br />
                            <label >New Password</label>
                            <input name="new_password" type="password"  class="custom[required] textbox" id="new_password" size="30" class="textbox" /><br />
                            <label >Confirm Password</label>
                            <input name="confirm_password" type="password" class="custom[required] textbox" id="confirm_password" size="30" class="textbox" /><br />
                            <input type="submit" class="btn btn-primary submit" name="button"  value="Change Password" />
                        </div>

                    </td>
                </tr>
            </table>
           </form>
        </div>
    </div>


</div>

@include('footer')