<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <link id="main" href="{{$base_url}}assets/css/main.css" rel="stylesheet" type="text/css" />
    <link id="jquery_ui" href="{{$base_url}}assets/css/jquery-ui.css"
          rel="stylesheet" type="text/css" />
    <link id="buttons" href="{{$base_url}}assets/css/buttons.css?{{mt_rand()}}" rel="stylesheet" type="text/css" />

    <link id="pines_css" href="{{$base_url}}assets/css/jquery.pnotify.default.css?{{mt_rand()}}"
          rel="stylesheet" type="text/css" />
    <link id="pines_icons" href="{{$base_url}}assets/css/jquery.pnotify.default.icons.css?{{ mt_rand()}}"
          rel="stylesheet" type="text/css" />
    <link id="pines_icons" href="{{$base_url}}assets/css/jquery.bxslider.css?{{ mt_rand()}}"
          rel="stylesheet" type="text/css" />
    <!--<script type="text/javascript" src="{{$base_url}}assets/js/jquery.min.js"></script>-->
    <script type="text/javascript" src="{{$base_url}}assets/js/progressbar.js"></script>
    <link id ="progressbar"  href="{{$base_url}}assets/css/progress-skins/jquery-ui-like/progressbar.css?{{mt_rand()}}" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="{{$base_url}}assets/images/question.png" type="image/x-icon" />

    <link href="{{$base_url}}css/table.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen" href="{{$base_url}}css/functionalities/validationEngine.jquery.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="{{$base_url}}css/functionalities/jquery.confirm.css" />


    <link type="text/css" rel="stylesheet" href="{{$base_url}}assets/css/jquery.dataTables.css"/>





    <link rel="stylesheet" href="{{$base_url}}css/functionalities/jquery-ui.css">
    <!--<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/pepper-grinder/jquery-ui.css" id="theme">-->
    <link rel="stylesheet" href="{{$base_url}}css/functionalities/demo.css">
    <link rel="stylesheet" href="{{$base_url}}css/functionalities/blueimp-gallery.min.css">
    <!--<link rel="stylesheet" href="http://blueimp.github.io/Gallery/css/blueimp-gallery.min.css">-->


    <link rel="stylesheet" href="{{$base_url}}css/functionalities/jquery.fileupload.css">
    <link rel="stylesheet" href="{{$base_url}}css/functionalities/jquery.fileupload-ui.css">


    <script src="{{$base_url}}js/jquery-1.8.2.min.js"></script>
    <script src="{{$base_url}}js/common.js"></script>
    <script type="text/javascript" src="{{$base_url}}assets/js/jquery.dataTables.min.js"></script>
    <script src="{{$base_url}}js/functionalities/jquery.validationEngine.js"></script>
    <script src="{{$base_url}}js/functionalities/jquery.confirm.js"></script>
    <script src="{{$base_url}}js/functionalities/jquery.validationEngine-en.js"></script>
    <script type="text/javascript" src="{{$base_url}}js/functionalities/tmpl.js"></script>
    <script src="{{$base_url}}/js/functionalities/jquery-ui.min.js"></script>




    <title>{{$title}}</title>




    <meta name="viewport" content="width=device-width" />

</head>

<body>
<!-- This header section is the one that contains the nbs watermark towards the right -->
@if($this->session->userdata('book_creating') != "")
<script>
    function generateReal(layout,type,content) {
        var n = noty({
            text        : content,
            type        : type,
            layout      : layout,
            dismissQueue: true,
            theme       : 'defaultTheme',
            maxVisible  : 10,
            buttons: [
                {addClass: 'btn btn-primary', text: 'Ok', onClick: function($noty) {
                    $noty.close();
                }
                }
            ]
        });
        console.log('html: ' + n.options.id);
    }

    function generateR(layout,type,data) {

        generateReal(layout,type,data);
    }
    $(document).ready(function(){
        var url =  "{{$base_url}}account/booKFinished";
    (function poll(){

        $.ajax({ url: url, success: function(data){
           //console.log(data);
           generateR('top','warning',data);
        }, dataType: "json", complete: poll, timeout: 60000 });
    })();
    });
</script>
@endif
<div id="hanger">
    <div id="header">
       @if($this->session->userdata('username') != "")
            <div id="logout"><a href="{{$base_url}}pafupi/main/logout"><p id="log">Logout</p></a></div>
        @endif
    </div>
</div>

<!-- This div is what make up the main container of the interface, in which all content is found -->
<div id="container">

    <!-- This section cintains the header WITHIN the container (for logo and system name) -->
    <div id="content-header">
        <div id="logo">
            <div id="login-logo">
                <div id="nbs">NBS Bank</div>
                <div id="slogan">Your Caring Bank</div>
            </div>
            <div id="motto">Banking Together.<br />Growing Together.<br />Stronger Everyday.<br /></div>
        </div>
        <div id="logo2">&nbsp;</div>
        <div id="sys-name"><strong>Pafupi</strong> Management System {{$this->session->userdata('books')}}</div>
    </div>

