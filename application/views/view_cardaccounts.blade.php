@include('header')
@include('menu')
<div id="container">

     
	<div id="body">
	    <div align="center">
            @if (count($accounts) >=1)
		    <div class="pafupi_table" >

                <table >
                    <tr>
                        <td>
                            Account Number
                        </td>
						<td>
                           Card Number
                        </td>
                        <td >
                            Branch Name
                        </td>
                        <td>
                           Date Created
                        </td>
						<td>
                           Opened By
                        </td>
						
                    </tr>
					@foreach ($accounts as $account)     
                    <tr>
                        <td >
                            {{ $account['acc_no'] }}
                        </td>
						 <td >
                            {{ $account['card_no'] }}
                        </td>
                        <td>
                            {{ $account['branch_name'] }}
                        </td>
                        <td>
                            {{ date('d-m-Y',strtotime($account['date_created'])) }}
                        </td>
						 <td>
                               {{ $account['opened_by'] }}
                        </td>
                    </tr>
                   @endforeach
                </table>
                <span><a href="{{$base_url}}view_card_excel" class="btn btn-primary">Download excel</a></span>
                @else
                No accounts to show
                @endif
            </div><br />


		 </div> 
	</div>    

</div>
@include('footer')