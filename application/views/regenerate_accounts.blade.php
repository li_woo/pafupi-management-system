@include('header')
@include('menu')
<script>
    function generate(type,content) {
        var n = noty({
            text        : content,
            type        : type,
            dismissQueue: true,
            layout      : 'top',
            theme       : 'defaultTheme',
            maxVisible  : 10,
            buttons: [
                {addClass: 'btn btn-primary', text: 'Ok', onClick: function($noty) {
                    $noty.close();
                }
                }]
        });

    }

    function generateAll(type,data) {

        generate(type,data);
    }

    $(document).ready(function (){
        $('.lock-disable').hide();$('.note').show();
        $('.case').bind('change', function (){
            if  (this.checked) {

                $('.lock-disable').show();
                $('.note').hide();
            }
            else {
                var  checkboxes = $('.pafupi_tabl').find('.case' );
                for(var i=0; i< checkboxes.length;i++){
                    if  (checkboxes[i].checked) {

                        $('.lock-disable').show();
                        $('.note').hide();
                        break;
                    }
                    else {


                        $('.lock-disable').hide();
                        $('.note').show();
                    }
                }
                //$('.delete-disabled').hide();
                //$('.note').show();
            }
        })
        //$('.lock-disable').hide();
        $('#selectall').bind('change', function (){

            var  checkboxes = $('.pafupi_tabl').find('.case' );
            if  (this.checked) {
                checkboxes.attr('checked', 'true');
                $('.lock-disable').show();
                $('.note').hide();
            }
            else {

                checkboxes.attr('checked', false);
                $('.lock-disable').hide();
                $('.note').show();
            };
        });



        $( "#lock_users").submit(function(event){
            event.preventDefault();

            confirm_lock();




        })

        $('button.reset').live('click',function(e){
            e.preventDefault();
            var dataForm =  $(this).attr('acc_no');

            url = "{{$base_url}}pafupi/main/regenerate_pins";

            $.ajax({
                "type":"POST",
                "url":url,
                "data":{acc_no : dataForm,regen:'regen'},
                "dataType":"json",
                "success":function(data){
                    console.log(data);
                    if(data !=0){

                        generateAll('success',"<span>pins regenerated</span><br /> <a href='{{$base_url}}get_otp/"+dataForm+"'>Download OTPs</a>");
                    }
                    else
                    {

                        generateAll('warning','pins not regenerated');
                    }
                },
                "error" : function(){
                    generateAll('error','pins not regenerated');
                }
            });
            $(":submit").attr("disabled", false);
        })


        function paginate(){
            $('table.paginate tr.pp').remove();
            var rows=$('table').find('tr.rows').length;

            var no_rec_per_page=5;
            var no_pages= Math.ceil(rows/no_rec_per_page);
            var $pagenumbers=$('<tr id="pp"></tr>');

            for(i=0;i<no_pages;i++)
            {
                $('<td class="pagination-item page"><a href="#" class="clickable">'+(i+1)+'</a></td>').appendTo($pagenumbers);
            }

            $pagenumbers.insertAfter('table.paginate');

            $('.page').hover(
                function(){
                    $(this).addClass('hover pagination-item-current');
                },
                function(){
                    $(this).removeClass('hover pagination-item-current');
                }
            );
            $('.page').bind('hover', function(){

            })
            $('table').find('tr.rows').hide();
            var tr=$('table tr.rows');
            $(tr[0]).show();if(rows >= 2){$(tr[1]).show();}if(rows >= 3){$(tr[2]).show();}if(rows >= 4){$(tr[3]).show();}if(rows >= 5){$(tr[4]).show();}
            $('a.clickable').click(function(event){
                event.preventDefault();
                $('table').find('tr.rows').hide();
                for(var i=($(this).text()-1)*no_rec_per_page;
                    i<=$(this).text()*no_rec_per_page-1;
                    i++)
                {
                    $(tr[i]).show();
                }
            });
        }

@if($search_feedback)
            paginate();
 @endif
    })
</script>
<div class="section">


    <div id="body">
        <div align="center">
            <p style="color: red">
                @for ($i = 0; $i <count($feedback); $i++)
                {{$feedback[$i]}} <br />
                @endfor
            </p>
            <p style="color: red">
                @if($search_feedback)
                {{$search_feedback}}<br />
                @endif

            </p>
            <div class="pafupi_tabl" >
                @if (count($accounts) >=1)
                <table border="0" cellpadding="0" cellspacing="0" class="full-tbl-container">
                    <tr>
                        <td colspan="8" class="tbl-header">
                            <form id="search" action="{{$base_url}}pafupi/main/regenerate_pins" method="post">
                                <table>
                                    <tr>
                                        <td width="50"></td>
                                        <td width="">Select account</td>
                                        <td width="400"><input type="text" id="param" name="param" placeholder="Search user" class="validate[required]"/></td>
                                        <input type="hidden" name="search" value="s" />
                                        <td width=""><button type="submit" class="btn btn-primary">Search</button></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                </table>
                            </form>
                        </td>

                    </tr>


                    <tr class="tbl-titles">
                        <td>
                            Account Number
                        </td>
                        <td>
                            Card Number
                        </td>
                        <td >
                            Branch Name
                        </td>
                        <td>
                            Date Created
                        </td>
                        <td>
                            Opened By
                        </td>
                        <td>
                            Dispatched By
                        </td>
                        <td>
                            AOC
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    @foreach ($accounts as $account)
                    <tr  class="tbl-rows rows">
                        <td >
                            {{ $account['acc_no'] }}
                        </td>
                        <td >
                            {{ $account['card_no'] }}
                        </td>
                        <td>
                            {{ $account['branch_name'] }}
                        </td>
                        <td>
                            {{ date('d-m-Y',strtotime($account['date_created'])) }}
                        </td>
                        <td>
                            {{ $account['FullName'] }}
                        </td>
                        <td>
                            {{ $account['dispatched_by'] }}
                        </td>
                        <td>
                            @if($account['AOC'] !=NULL)
                            {{$account['AOC']}}
                            @else
                            Not assigned
                            @endif
                        </td>
                        <td>
                            <button  acc_no="{{$account['acc_no']}}" class="btn btn-danger reset">Reset pins</button>
                        </td>
                    </tr>
                    @endforeach
                    <tr id="select_all">

                        <td colspan="1" class="tbl-footer-checks" >


                            &nbsp;



                        </td>
                        <td colspan="7" class="tbl-footer">
                            <div class="tbl-options">

                            </div>
                            <div class="tbl-pagination">

                                <table class="paginate" cellspacing="0" cellpadding="0" border="0">
                                    <tr class="pp">
                                        {{$this->pagination->create_links()}}
                                    </tr>
                                </table>
                            </div>
                        </td>

                    </tr>

                    <!--<p class="note">No other user profiles are eligible for locking. <a href="{{$base_url}}pafupi/main/create_user">Would you like to register a new user profile?</a></p> <br />-->
                </table>

                @else
                No Accounts to show
                @endif
            </div><br />


        </div>
    </div>

</div>
@include('footer')