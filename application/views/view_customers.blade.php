@include('header')
@include('menu')
<script>

    $(document).ready(function (){

        $('.case').bind('change', function (){
            if  (this.checked) {

                $('.lock-disable').show();

            }
            else {
                var  checkboxes = $('.pafupi_tabl').find('.case' );
                for(var i=0; i< checkboxes.length;i++){
                    if  (checkboxes[i].checked) {

                        $('.lock-disable').show();

                        break;
                    }
                    else {


                        $('.lock-disable').hide();

                    }
                }
                //$('.delete-disabled').hide();
                //$('.note').show();
            }
        })
        $('.lock-disable').hide();
        $('#selectall').bind('change', function (){

            var  checkboxes = $('.pafupi_tabl').find('.case' );
            if  (this.checked) {
                checkboxes.attr('checked', 'true');
                $('.lock-disable').show();
                $('.note').hide();
            }
            else {

                checkboxes.attr('checked', false);
                $('.lock-disable').hide();
                $('.note').show();
            };
        });

        //sorting
        $('.tbl-titles td').each(function(column) {
            $(this).hover(
                function(){
                    $(this).addClass('hover');

                },
                function(){
                    $(this).removeClass('hover');
                }
            );

            $(this).click(function(){
                if($(this).is('.asc'))
                {
                    $(this).removeClass('asc');
                    $(this).addClass('desc');
                    $('.tbl-titles td').find('img').remove();
                    $(this).append("<img src='{{$base_url}}assets/images/down.png' />");
                    sortdir=-1;
                }
                else
                {
                    $(this).addClass('asc');
                    $(this).removeClass('desc');
                    $('.tbl-titles td').find('img').remove();
                    $(this).append("<img src='{{$base_url}}assets/images/up.png' />");
                    sortdir=1;
                }
                $(this).siblings().removeClass('asc');
                $(this).siblings().removeClass('desc');

                var rec=$('table').find('tr.rows').get();

                rec.sort(function(a, b) {
                    var val1 = $(a).children('td').eq(column).text().toUpperCase();

                    var val2 = $(b).children('td').eq(column).text().toUpperCase();
                    return (val1 < val2) ? -sortdir : (val1 > val2) ? sortdir : 0;
                });
                $('table').find('tr.rows').remove();
                $.each(rec, function(index, row) {

                    $('tbody.main').append(row);
                    console.log(row);
                });
            });
        });


        function paginate(){
            $('table.paginate tr.pp').remove();
            var rows=$('table').find('tr.rows').length;

            var no_rec_per_page=5;
            var no_pages= Math.ceil(rows/no_rec_per_page);
            var $pagenumbers=$('<tr id="pp"></tr>');

            for(i=0;i<no_pages;i++)
            {
                $('<td class="pagination-item page"><a href="#" class="clickable">'+(i+1)+'</a></td>').appendTo($pagenumbers);
            }

            $pagenumbers.insertAfter('table.paginate');

            $('.page').hover(
                function(){
                    $(this).addClass('hover pagination-item-current');
                },
                function(){
                    $(this).removeClass('hover pagination-item-current');
                }
            );
            $('.page').bind('hover', function(){

            })
            $('table').find('tr.rows').hide();
            var tr=$('table tr.rows');
            $(tr[0]).show();if(rows >= 2){$(tr[1]).show();}if(rows >= 3){$(tr[2]).show();}if(rows >= 4){$(tr[3]).show();}if(rows >= 5){$(tr[4]).show();}
            $('a.clickable').click(function(event){
                event.preventDefault();
                $('table').find('tr.rows').hide();
                for(var i=($(this).text()-1)*no_rec_per_page;
                    i<=$(this).text()*no_rec_per_page-1;
                    i++)
                {
                    $(tr[i]).show();
                }
            });
        }

@if($search_feedback)
            paginate();
 @endif
    })
</script>
<style>
    .hover{
        cursor: pointer;
    }
    .asc{
        background:url('{{$base_url}}assets/images/up.png') no-repeat; padding-left:20px;
    }

    .desc{
        background:url('{{$base_url}}assets/images/down.png') no-repeat; padding-left:20px;
    }

</style>
<div class="section">


    <div id="body">
        <div align="center">
            <p style="color: red">
                @for ($i = 0; $i <count($feedback); $i++)
                {{$feedback[$i]}} <br />
                @endfor
            </p>
            <p style="color: red">
                @if($search_feedback)
                {{$search_feedback}}<br />
                @endif

            </p>
            <div class="pafupi_tabl" >
                @if (count($customers) >=1)
                <table border="0" cellpadding="0" cellspacing="0" class="full-tbl-container">
                    <tr>
                        <td colspan="7" class="tbl-header">
                            <form id="search" action="{{$base_url}}pafupi/main/view_customers" method="post">
                                <table>
                                    <tr>
                                        <td width="50"></td>
                                        <td width="">Select the customer</td>
                                        <td width="400"><input type="text" id="param" name="param" placeholder="Search account" class="validate[required]"/></td>
                                        <input type="hidden" name="search" value="s" />
                                        <td width=""><button type="submit" class="btn btn-primary">Search</button></td>
                                    </tr>
                                </table>
                            </form>
                        </td>
                    </tr>
                    <form id="search" action="{{$base_url}}pafupi/main/view_customers" method="post">
                        <input type="hidden" name="delete" value="yes" />

                        <tr class="tbl-titles">

                            <td>
                                Customer Number
                            </td>
                            <td >
                                Customer Name
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tbody class="table_body main">
                        @foreach ($customers as $customer)
                        <tr class="tbl-rows rows">

                            <td >
                                {{ $customer['CustomerNumber'] }}
                            </td>
                            <td>
                                {{ $customer['CustomerFirstName'] }} {{$customer['CustomerSurname']}}
                            </td>
                            <td> <a href="{{$base_url}}pafupi/main/upload_kyc_image?cust_id={{ $customer['CustomerNumber'] }}">Upload images</a></td>
                        </tr>
                        @endforeach
                        </tbody>
                        <tr id="select_all">


                            <td colspan="7" class="tbl-footer">

                                <div class="tbl-pagination">

                                    <table class="paginate" cellspacing="0" cellpadding="0" border="0">
                                        <tr class="pp">
                                            {{$this->pagination->create_links()}}
                                        </tr>
                                    </table>
                                </div>
                            </td>
                    </form>
                </table>

                @else
                No customers to show
                @endif
            </div><br />


        </div>
    </div>

</div>
@include('footer')