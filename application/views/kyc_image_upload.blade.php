@include('header')
@include('menu')
<div class="section">
    <div align="center">


        @if($errors)
        <h4>Feedback</h4>
          <span style="color: red">

                @foreach($errors as $error)
                    {{$error}}
                @endforeach


        </span>
        @endif

    </div>
    <form action="{{$base_url}}pafupi/main/upload_kyc_image" enctype="multipart/form-data" method="post">
        <table class="form-container" cellspacing="0" cellpadding="0" border="0">
            <tr><td><div class="user-info-header"><p>Upload Customer image</p></div></td></tr>
            <tr>
                <td>
                    <div class="inputs">
                        <label for="userfile">Select image file</label><br />
                        <label for="type">Select image type</label><br />
                    </div>
                    <div class="inputs">
                        <input type="file" name="userfile" multiple /><br /><br />
                        <select name="type" class="styled-select option">
                            <option value="ID">Customer ID Photo</option>
                            <option value="FACE">Customer Face Photo</option>
                        </select><br />

                        <input type="hidden" name="cust_id" value="{{$cust_id}}" />
                        <button type="submit" class="btn-primary submit" >Upload</button>
                    </div>
                </td>
            </tr>
        </table>
    </form>


</div>
</div>


</div>

@include('footer')