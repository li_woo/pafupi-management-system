@include('header')
@include('menu')
<script>
$(document).ready(function (){


    var selected_id = null;

    $("form#reset").live('submit', function (e){
        e.preventDefault();
        $('#reset_feedback').html('');
        var status = true;

        var fields = $(this).find('.res')
        for(var i=0; i< fields.length;i++){
            if( fields[i].value == ''){
                $('#reset_feedback').append("fill all fields");
                status = false;
                break;
            }
            else if($('#password').val() != $('#confirm').val()){
                $('#reset_feedback').append("passwords do not match");
                status = false;
                break;
            }
        }
        if(status==true){
            dataForm = $(this).serialize();
            // var data_d = $(this).serializeArray();
            $(":submit").attr("disabled", true);
            url = "{{$base_url}}pafupi/main/reset_user_password";

            $.ajax({
                "type":"POST",
                "url":url,
                "data":dataForm,
                "dataType":"json",
                "success":function(data){
                    console.log(data);
                    if(data !=0){
                        generateAll('success','user password successfully reset');
                        $( "#dialog" ).dialog( "close" );
                        //alert(selected_id);
                        /*$('table').find('tr#'+selected_id).remove();
                         console.log(data);
                         $.each(data,function(i,item){

                         $('tr.tbl-titles').after(tmpl('user_row',item));
                         });*/
                    }
                    else
                    {

                        generateAll('warning','user password NOT reset');
                    }
                },
                "error" : function(){
                    generateAll('error','user password NOT reset');
                }
            });
            $(":submit").attr("disabled", false);
        }




    });


    $("form#save_use").live('submit', function (e){
        e.preventDefault();
        var status = true;
        var fields = $(this).find('.ed')
        for(var i=0; i< fields.length;i++){
            if( fields[i].value == ''){
                $('#save_feedback').append("fill all fields");
                status = false;
                break;
            }
        }
        if(status==true){
            dataForm = $(this).serialize();
            // var data_d = $(this).serializeArray();
            $(":submit").attr("disabled", true);
            url = "{{$base_url}}pafupi/main/save_user";

            $.ajax({
                "type":"POST",
                "url":url,
                "data":dataForm,
                "dataType":"json",
                "success":function(data){
                    console.log(data);
                    if(data !=0){
                        generateAll('success','user successfully saved');
                        $( "#dialog" ).dialog( "close" );
                        //alert(selected_id);
                        $('table').find('tr#'+selected_id).remove();
                        console.log(data);
                        $.each(data,function(i,item){

                            $('tr.tbl-titles').after(tmpl('user_row',item));
                        });
                    }
                    else
                    {

                        generateAll('warning','user NOT edited');
                    }
                },
                "error" : function(){
                    generateAll('error','user NOT edited');
                }
            });
            $(":submit").attr("disabled", false);
        }




    });



    $("#cancel_edit").live('click', function(e){
            e.preventDefault();
            $( "#dialog" ).dialog( "close" );
        }
    )
    $("#cancel_reset").live('click', function(e){
            e.preventDefault();
            $( "#dialog" ).dialog( "close" );
        }
    )
    $('button.edit').live('click',function(e){
        $( "#dialog").html('');
        var dataForm =  $(this).attr('user_id');

        url = "{{$base_url}}pafupi/main/get_user";

        $.ajax({
            "type":"POST",
            "url":url,
            "data":{UserID : dataForm},
            "dataType":"json",
            "success":function(data){
                console.log(data);
                if(data !=0){
                    $.each(data,function(i,item){
                        $('#dialog').append(tmpl('user',item));
                    })

                }
                else
                {

                    generateAll('warning','user NOT edited');
                }
            },
            "error" : function(){
                generateAll('error','user NOT edited');
            }
        });
        $(":submit").attr("disabled", false);

        $( "#dialog" ).dialog();
    });









    $('button.reset').live('click',function(e){
        $( "#dialog").html('');
        var dataForm =  $(this).attr('user_id');

        $('#dialog').append(tmpl('user_reset',dataForm));
        $( "#dialog" ).dialog();



    })


    //sorting
    $('.tbl-titles td').each(function(column) {
        $(this).hover(
            function(){
                $(this).addClass('hover');

            },
            function(){
                $(this).removeClass('hover');
            }
        );

        $(this).click(function(){
            if($(this).is('.asc'))
            {
                $(this).removeClass('asc');
                $(this).addClass('desc');
                $('.tbl-titles td').find('img').remove();
                $(this).append("<img src='{{$base_url}}assets/images/down.png' />");
                sortdir=-1;
            }
            else
            {
                $(this).addClass('asc');
                $(this).removeClass('desc');
                $('.tbl-titles td').find('img').remove();
                $(this).append("<img src='{{$base_url}}assets/images/up.png' />");
                sortdir=1;
            }
            $(this).siblings().removeClass('asc');
            $(this).siblings().removeClass('desc');

            var rec=$('table').find('tr.rows').get();

            rec.sort(function(a, b) {
                var val1 = $(a).children('td').eq(column).text().toUpperCase();

                var val2 = $(b).children('td').eq(column).text().toUpperCase();
                return (val1 < val2) ? -sortdir : (val1 > val2) ? sortdir : 0;
            });
            $('table').find('tr.rows').remove();
            $.each(rec, function(index, row) {

                $('tbody.main').append(row);
                console.log(row);
            });
        });
    });












    function paginate(){
        $('table.paginate tr.pp').remove();
        var rows=$('table').find('tr.rows').length;

        var no_rec_per_page=5;
        var no_pages= Math.ceil(rows/no_rec_per_page);
        var $pagenumbers=$('<tr id="pp"></tr>');

        for(i=0;i<no_pages;i++)
        {
            $('<td class="pagination-item page"><a href="#" class="clickable">'+(i+1)+'</a></td>').appendTo($pagenumbers);
        }

        $pagenumbers.insertAfter('table.paginate');

        $('.page').hover(
            function(){
                $(this).addClass('hover pagination-item-current');
            },
            function(){
                $(this).removeClass('hover pagination-item-current');
            }
        );
        $('.page').bind('hover', function(){

        })
        $('table').find('tr.rows').hide();
        var tr=$('table tr.rows');
        $(tr[0]).show();if(rows >= 2){$(tr[1]).show();}if(rows >= 3){$(tr[2]).show();}if(rows >= 4){$(tr[3]).show();}if(rows >= 5){$(tr[4]).show();}
        $('a.clickable').click(function(event){
            event.preventDefault();
            $('table').find('tr.rows').hide();
            for(var i=($(this).text()-1)*no_rec_per_page;
                i<=$(this).text()*no_rec_per_page-1;
                i++)
            {
                $(tr[i]).show();
            }
        });
    }

@if($search_feedback)
        paginate();
 @endif
})
</script>
<style>
    .hover{
        cursor: pointer;
    }
    .asc{
        background:url('{{$base_url}}assets/images/up.png') no-repeat; padding-left:20px;
    }

    .desc{
        background:url('{{$base_url}}assets/images/down.png') no-repeat; padding-left:20px;
    }

</style>
<div class="section">


    <div id="body">
        <div align="center">
            <p style="color: red">
                @for ($i = 0; $i <count($feedback); $i++)
                {{$feedback[$i]}} <br />
                @endfor
            </p>
            <p style="color: red">
                @if($search_feedback)
                {{$search_feedback}}<br />
                @endif

            </p>
            <div class="pafupi_tabl" >
                @if (count($users) >=1)
                <table border="0" cellpadding="0" cellspacing="0" class="full-tbl-container">
                    <tr>
                        <td colspan="7" class="tbl-header">
                            <form id="search" action="{{$base_url}}pafupi/main/modify_user" method="post">
                                <table>
                                    <tr>
                                        <td width="50"></td>
                                        <td width="">Select the accounts you would like to see</td>
                                        <td width="400"><input type="text" id="param" name="param" placeholder="Search user" class="validate[required]"/></td>
                                        <input type="hidden" name="search" value="s" />
                                        <td width=""><button type="submit" class="btn btn-primary">Search</button></td>
                                    </tr>
                                </table>
                            </form>
                        </td>
                    </tr>


                    <tr class="tbl-titles">
                        <td>Username</td>
                        <td>Fullname</td>
                        <td>Branch/Agency</td>
                        <td>User Role</td>
                        <td>Last Login</td>
                        <td colspan="2">Action</td>
                    </tr>
                    <tbody class="main">
                    @foreach ($users as $user)
                    <tr class="tbl-rows rows" id="{{$user['UserID']}}">
                        <td>
                            @if($this->session->userdata('username') == $user['Username'])
                            {{strtolower($user['Username'])}} <p> " (You)"</p>

                            @else
                            {{strtolower($user['Username'])}};
                            @endif
                        </td>
                        <td>{{ucwords(strtolower($user['FullName']))}}</td>
                        <td>{{$user['branch_name']}}</td>
                        <td>{{$user['RoleName']}}</td>
                        <td>
                            @if($user['LastLogin'] != "")
                            {{$user['LastLogin']}}
                            @endif
                            @if($user['LastLogin'] == "")
                            N/A
                            @endif
                        </td>
                        <td>


                            <button  user_id="{{$user['UserID']}}" class="btn btn-danger reset">Reset</button>


                        </td>
                        <td>
                            <button  user_id="{{$user['UserID']}}" class="btn btn-primary edit">Modify</button>
                        </td>

                    </tr>
                    @endforeach
                    </tbody>
                    <tr id="select_all">
                        <td colspan="1" class="tbl-footer-checks" >


                            &nbsp;



                        </td>

                        <td colspan="6" class="tbl-footer">

                            <div class="tbl-pagination">

                                <table class="paginate" cellspacing="0" cellpadding="0" border="0">
                                    <tr class="pp">
                                        {{$this->pagination->create_links()}}
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                        <!--<p class="note">No other user profiles are eligible for locking. <a href="{{$base_url}}pafupi/main/create_user">Would you like to register a new user profile?</a></p> <br />-->
                </table>

                @else
                No users to show
                @endif


            </div><br />


        </div>
    </div>
    <div id="dialog" title="Edit User">

    </div>
</div>

<script id="user_row" type="text/x-tmpl">
     <tr class="rows" id="{%=o.UserID%}">

                    <td>
                        {%=o.UserName%}
                    </td>
                    <td>{%=o.FullName%}</td>
                    <td>{%=o.branch_name%}</td>
                    <td>{%=o.RoleName%}</td>
                    <td>
                       {%=o.LastLogin%}
                    </td>
                    <td>


                        <button  user_id="{%=o.UserID%}" class="btn btn-danger reset">Reset</button>


                    </td>
                    <td>
                        <button  user_id="{%=o.UserID%}" class="btn btn-primary edit">Modify</button>
                    </td>

                <tr>
</script>
<script id="user_reset" type="text/x-tmpl">
     <form id='reset' method='post'>
             <p style='color:red' align="center" id="reset_feedback"></p>
              <input type="hidden"  name="user_id" value="{%=o%}" />
              <label for="password">Password</label>
              <input type="password" id="password" class="res" name="password" />
              <label for="confirm">Confirm</label>
              <input type="password" id="confirm" class="res" name="confirm" /><br />
               <button type="submit" class="btn btn-primary">Reset</button>
               <button id="cancel_reset" class="btn btn-danger">Cancel</button>
     </form>
</script>
<script id="user" type="text/x-tmpl">
     <form id='save_user' method='post'>
      <p style='color:red' id="save_feedback"></p>
	  <input type="hidden" name="user_id" value="{%=o.UserID%}" />
	  <label for="name">First Name</label>
	  <input type="text"  name="fname" value="{%=o.fname%}" class="validate[required] ed"><br />
	  <label for="name">Surname</label>
	  <input type="text" name="lname" value="{%=o.lname%}" class="validate[required] ed"><br />
	   <label for="branch">Branch</label>
                <select name="branch" class="validate[required] ed">
                    <option value="{%=o.Branch%}">{%=o.branch_name%}</option>
                    @foreach ($branches as $branch)
                    <option value="{{ $branch['branch_id'] }}">{{ $branch['branch_name'] }}</option>
                    @endforeach
                </select><br />
        <label for="role">Role</label>
	   <select name="user_role" class="validate[required] ed">
	                <option value="{%=o.RoleID%}">{%=o.RoleName%}</option>
                    @foreach ($roles as $role)
                    <option value="{{ $role['RoleID'] }}" >{{ $role['RoleName'] }}</option>
                    @endforeach
      </select><br />
      <button type="submit" class="btn btn-primary">Save</button>
       <button id="cancel_edit" class="btn btn-danger">Cancel</button>
	 </form>
</script>
@include('footer')