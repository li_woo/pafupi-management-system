@include('header')
@include('menu')
<script>
    $(document).ready(function (){
        $('.case').bind('change', function (){
            if  (this.checked) {

                $('.lock-disable').show();

            }
            else {
                var  checkboxes = $('.pafupi_tabl').find('.case' );
                for(var i=0; i< checkboxes.length;i++){
                    if  (checkboxes[i].checked) {

                        $('.lock-disable').show();

                        break;
                    }
                    else {


                        $('.lock-disable').hide();

                    }
                }
                //$('.delete-disabled').hide();
                //$('.note').show();
            }
        })
        $('.lock-disable').hide();
        $('#selectall').bind('change', function (){

            var  checkboxes = $('.pafupi_tabl').find('.case' );
            if  (this.checked) {
                checkboxes.attr('checked', 'true');
                $('.lock-disable').show();
                $('.note').hide();
            }
            else {

                checkboxes.attr('checked', false);
                $('.lock-disable').hide();
                $('.note').show();
            };
        });


        function paginate(){
            $('table.paginate tr.pp').remove();
            var rows=$('table').find('tr.rows').length;

            var no_rec_per_page=5;
            var no_pages= Math.ceil(rows/no_rec_per_page);
            var $pagenumbers=$('<tr id="pp"></tr>');

            for(i=0;i<no_pages;i++)
            {
                $('<td class="pagination-item page"><a href="#" class="clickable">'+(i+1)+'</a></td>').appendTo($pagenumbers);
            }

            $pagenumbers.insertAfter('table.paginate');

            $('.page').hover(
                function(){
                    $(this).addClass('hover pagination-item-current');
                },
                function(){
                    $(this).removeClass('hover pagination-item-current');
                }
            );
            $('.page').bind('hover', function(){

            })
            $('table').find('tr.rows').hide();
            var tr=$('table tr.rows');
            $(tr[0]).show();if(rows >= 2){$(tr[1]).show();}if(rows >= 3){$(tr[2]).show();}if(rows >= 4){$(tr[3]).show();}if(rows >= 5){$(tr[4]).show();}
            $('a.clickable').click(function(event){
                event.preventDefault();
                $('table').find('tr.rows').hide();
                for(var i=($(this).text()-1)*no_rec_per_page;
                    i<=$(this).text()*no_rec_per_page-1;
                    i++)
                {
                    $(tr[i]).show();
                }
            });
        }

@if($search_feedback)
            paginate();
 @endif
    })
</script>
<div class="section">


    <div id="body">
        <div align="center">
            <p style="color: red">
                @for ($i = 0; $i <count($feedback); $i++)
                {{$feedback[$i]}} <br />
                @endfor
            </p>
            <p style="color: red">
                @if($search_feedback)
                {{$search_feedback}}<br />
                @endif

            </p>
            <div class="pafupi_tabl" >
                @if (count($accounts) >=1)
                <table border="0" cellpadding="0" cellspacing="0" class="full-tbl-container">
                    <tr>
                        <td colspan="7" class="tbl-header">
                            <form id="search" action="{{$base_url}}pafupi/main/view_carded" method="post">
                                <table>
                                    <tr>
                                        <td width="50"></td>
                                        <td width="">Select the accounts you would like to see</td>
                                        <td width="400"><input type="text" id="param" name="param" placeholder="Search account" class="validate[required]"/></td>
                                        <input type="hidden" name="search" value="s" />
                                        <td width=""><button type="submit" class="btn btn-primary">Search</button></td>
                                    </tr>
                                </table>
                            </form>
                        </td>
                    </tr>
                    <form id="search" action="{{$base_url}}pafupi/main/view_carded" method="post">
                        <input type="hidden" name="delete" value="yes" />

                        <tr class="tbl-titles">
                            <td>&nbsp;</td>
                            <td>
                                Account Number
                            </td>
                            <td >
                                Branch
                            </td>
                            <td >
                                Booklet No
                            </td>

                            <td>
                                Referal No
                            </td>
                            <td>
                                ATM No
                            </td>
                            <td>&nbsp;</td>

                        </tr>
                        @foreach ($accounts as $account)
                        <tr class="tbl-rows rows">
                            <td>
                                <input type="checkbox" class="case" value="{{$account['acc_no']}}"
                                       name="selected_accounts[]" id="selected_accounts[]" />
                            </td>
                            <td >
                                {{ $account['acc_no'] }}
                            </td>
                            <td>
                                {{ $account['branch_name'] }}
                            </td>
                            <td>
                                {{ $account['book_no'] }}
                            </td>
                            <td>
                                {{ $account['referal_no'] }}
                            </td>
                            <td >
                                {{ $account['card_no'] }}
                            </td>



                        </tr>
                        @endforeach
                        <tr id="select_all">

                            <td colspan="1" class="tbl-footer-checks" >


                                <input type="checkbox" id="selectall" />



                            </td>
                            <td colspan="6" class="tbl-footer">
                                <div class="tbl-options">
                                    <button type="submit"  class="btn btn-danger lock-disable">Delete Selected</button>
                                </div>
                                <div class="tbl-pagination">

                                    <table class="paginate" cellspacing="0" cellpadding="0" border="0">
                                        <tr class="pp">
                                            {{$this->pagination->create_links()}}
                                        </tr>
                                    </table>
                                </div>
                            </td>
                    </form>
                </table>
                <span><a href="{{$base_url}}view_card_excel" class="btn btn-primary">Download excel</a></span>
                @else
                No accounts to show
                @endif
            </div><br />


        </div>
    </div>

</div>
@include('footer')