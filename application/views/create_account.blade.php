@include('header')
@include('menu')

	<script>
        function generateA(layout,type,content) {
            var n = noty({
                text        : content,
                type        : type,
                layout      : layout,
                dismissQueue: true,
                theme       : 'defaultTheme',
                maxVisible  : 10,
                buttons: [
                    {addClass: 'btn btn-primary', text: 'Ok', onClick: function($noty) {
                              $noty.close();
                          }
                       }
                    ]
            });
            console.log('html: ' + n.options.id);
        }

        function generateAccount(layout,type,data) {

            generateA(layout,type,data);
        }

        function generate(layout) {
            var n = noty({
                text: 'Are you sure you want to create ' + $('#account_number').val()+' accounts',
                type: 'warning',
                modal: 'true',
                dismissQueue: true,
                layout: layout,
                theme: 'default',
                buttons: [
                    {addClass: 'btn btn-primary', text: 'Ok', onClick: function($noty) {

                        dataForm = $("form#create").serialize();

                        $(":submit").hide();
                        url = "{{$base_url}}pafupi/main/create_account_post";
                        $('div#feed').html('').show();
                        $('div#feed').html("<span style='color:red'>processing....</span>");
                        $.ajax({
                            xhr: function() {
                                var xhr = new window.XMLHttpRequest();
                                xhr.upload.addEventListener("progress", function(evt) {
                                    if (evt.lengthComputable) {
                                        var percentComplete = evt.loaded / evt.total;

                                        //Do something with upload progress here
                                        $('#progress').show();
                                        //progressBar(percentComplete, $('#progress'));
                                    }
                                }, false);

                                xhr.addEventListener("progress", function(evt) {
                                    if (evt.lengthComputable) {
                                        var percentComplete = evt.loaded / evt.total;

                                        //Do something with download progress
                                        $('#progress').show();
                                        //progressBar(percentComplete, $('#progress'));
                                    }
                                }, false);

                                return xhr;
                            },
                            "type":"POST",
                            "url":url,
                            "data":dataForm,
                            "dataType":"json",
                            "success":function(data){
                                if(data !=0){
                                    /*var result;
                                    var count = 0;
                                    $.each(data,function(i,item){

                                        $.each(data.pop(),function(i,item){
                                               count++;
                                            $.each(item.pop(),function(i,item){


                                            });
                                        });
                                    });*/
                                    result = data+" account(s) created";
                                    generateAccount('top','success',result);
                                    $(":submit").show();
                                    $('#progress').hide();
                                }
                                else
                                {
                                    generateAccount('top','error',"Account creation failed");
                                    $(":submit").show();
                                    $('#progress').hide();

                                }
                                $('div#feed').html('').hide();
                            },
                            "error" : function(e){
                                console.log(e.responseText)
							    generateAccount('top','error',"Connection error...Account creation failed..");
                                $(":submit").show();
                                $('#progress').hide();
                                $('div#feed').html('').hide();
                            }
                        });
                        $noty.close();

                    }
                    },
                    {addClass: 'btn btn-primary', text: 'Cancel', onClick: function($noty) {


                        $noty.close();
                        return false;
                    }
                    }
                ]
            });
            console.log('html: '+n.options.id);
        }
        function generateAll() {
            return generate('top');
        }

	   $(document).ready(function(){
	    var conf = false;

           $('#progress').hide();
           $("form#create").validationEngine('attach', {
				onValidationComplete: function(form, status){

					if(status==true){

                        generateAll();

                       //$(":submit").show();
					    }
					
					 
				}
	
                    });
					
	$('#feed').on('click', function(){
	      $('#feed').hide();
	})				
    function confirm(){
	     $.confirm({
			                        'title'		: 'Account creation',
			                        'message'	: 'Are you sure you want to create accounts?',
									'buttons'	: {
									'Yes'	: {
									            'class'	: 'blue',
									             'action': function(){
													 
													 conf = true;
												              
															 
																	
											                          }
										      },
									'No'	: {
									            'class'	: 'gray',
									            'action': function(){ }	
										      }
			                        }
		                          });
	
	
         }	
					
					
					
					});
	</script>


<div class="section">

     
	<div id="body">
	    <div >

		<div id="booklet"></div>
                <div><div><div id="progress" style="display: none" class="jquery-ui-like"><div></div></div>
                <form  id="create" action="{{$base_url}}pafupi/main/create_account_post" method="post">
                    <div id="feed" align="center"></div>
                 <table class="form-container" cellspacing="0" cellpadding="0" border="0">
                     <tr><td><div class="user-info-header"><p>Create Accounts</p></div></td></tr>
                     <tr>
                         <td>
                             <div class="inputs">
                                 <label for="number">Number of accounts</label><br />
                                 <label for="branch">Branch</label><br />
                             </div>
                             <div class="inputs">

                                 <select name="number" id="account_number"  class="validate[required] styled-select option">
                                     @for ($i = 1; $i <=20; $i++)
                                     <option value="{{$i*5}}">{{$i*5}}</option>
                                     @endfor
                                     <option value="1000">1000</option>
                                 </select><br />

                                 <select name="branch"  class="validate[required] styled-select option">
                                     @foreach ($branches as $branch)
                                     <option value="{{ $branch['branch_id'] }}">{{ $branch['branch_name'] }}</option>
                                     @endforeach

                                 </select><br />
                                 <button type="submit" class="btn-primary submit" >Create</button>
                             </div>

                         </td>
                     </tr>

                 </table>
                </form>

		 </div> 
	</div>
    <script id="account" type="text/x-tmpl">
	  <div style="background-color:grey" id="acc_">
	  <p>{%=o.account%}</p>
	</script>

</div>

@include('footer')