@include('header')
@include('menu')

<script>
    function generateA(layout,type,content) {
        var n = noty({
            text        : content,
            type        : type,
            layout      : layout,
            dismissQueue: true,
            theme       : 'defaultTheme',
            maxVisible  : 10,
            buttons: [
                {addClass: 'btn btn-primary', text: 'Ok', onClick: function($noty) {
                    $noty.close();
                }
                }
            ]
        });
        console.log('html: ' + n.options.id);
    }

    function generateAccount(layout,type,data) {

        generateA(layout,type,data);
    }

    function generate(layout) {
        var n = noty({
            text: 'Are you sure you want to create ' + $('#account_number').val()+' booklets',
            type: 'warning',
            modal: 'true',
            dismissQueue: true,
            layout: layout,
            theme: 'default',
            buttons: [
                {addClass: 'btn btn-primary', text: 'Ok', onClick: function($noty) {

                    dataForm = $("form#create").serialize();

                    $(":submit").hide();
                    url = "{{$base_url}}pafupi/main/otp_creation_post";
                    $('div#feed').html('').show();
                    $('div#feed').html("<span style='color:red'>processing....open another tab to do other things as you wait</span>");
                    $.ajax({
                        xhr: function() {
                            var xhr = new window.XMLHttpRequest();
                            xhr.upload.addEventListener("progress", function(evt) {
                                if (evt.lengthComputable) {
                                    var percentComplete = evt.loaded / evt.total;


                                }
                            }, false);

                            xhr.addEventListener("progress", function(evt) {
                                if (evt.lengthComputable) {
                                    var percentComplete = evt.loaded / evt.total;


                                }
                            }, false);

                            return xhr;
                        },
                        "type":"POST",
                        "url":url,
                        "data":dataForm,
                        "dataType":"json",
                        "success":function(data){
                            if(data !=0){

                                result = data;//+" "+"<a href='{{$base_url}}download_book_pins' style='color:red'>Download Pins</a>"+" "+"<a href='{{$base_url}}download_referals' style='color:red'>Download Referals</a>" //+" OTP Booklet(s) created";
                                generateAccount('top','success',result);
                                $(":submit").show();
                                $('#progress').hide();
                            }
                            else
                            {
                                generateAccount('top','error',"" +"Book creation failed");
                                $(":submit").show();
                                $('#progress').hide();

                            }
                            $('div#feed').html('').hide();
                        },
                        "error" : function(data){
                             if(data !=0){
                                 generateAccount('top','warning',data.responseText);
                             }
						     else
                            generateAccount('top','error',"Connection error...Book creation failed");
                            $(":submit").show();
                            $('#progress').hide();
                            $('div#feed').html('').hide();
                        }
                    });
                    $noty.close();

                }
                },
                {addClass: 'btn btn-primary', text: 'Cancel', onClick: function($noty) {


                    $noty.close();
                    return false;
                }
                }
            ]
        });
        console.log('html: '+n.options.id);
    }
    function generateAll() {
        return generate('top');
    }

    $(document).ready(function(){
        var conf = false;

        $('#progress').hide();
        $("form#create").validationEngine('attach', {
            onValidationComplete: function(form, status){

                if(status==true){

                    generateAll();

                    //$(":submit").show();
                }


            }

        });

        $('#feed').on('click', function(){
            $('#feed').hide();
        })






    });
</script>


<div class="section">


    <div id="body">
        <div >

            <div id="booklet"></div>

                    <form  id="create" method="post">
                        <div id="feed" align="center"></div>
                        <table class="form-container" cellspacing="0" cellpadding="0" border="0">
                            <tr><td><div class="user-info-header">
                                        <p>Choose Number of
                                            @if($type=='ref')
                                            Referrals
                                            @elseif($type='otp')
                                            OTP
                                            @endif
                                            Books to Generate</p></div></td></tr>
                            <tr>
                                <td>
                                    <div class="inputs">
                                        <label for="number">Select Amount</label><br />
                                    </div>
                                    <div class="inputs">

                                        <select name="number" id="account_number"  class="validate[required] styled-select option">
                                            @for ($i =1; $i <=20; $i++)
                                            <option value="{{$i*5}}">{{$i*5}}</option>

                                            @endfor
                                            <option value="1000">1000</option>
                                        </select><br />
                                        <input type="hidden" name="action_type" value="{{$type}}">
                                        <button type="submit" class="submit" >Create</button>
                                    </div>

                                </td>
                            </tr>

                        </table>
                    </form>

                </div>
            </div>
            <script id="account" type="text/x-tmpl">
	  <div style="background-color:grey" id="acc_">
	  <p>{%=o.account%}</p>
	</script>

        </div>

        @include('footer')