@include('header')
@include('menu')
<!--id="container"-->
<div  class="section">


    <div id="body">
        <div align="center">
            <div class="pafupi_table" >
                @if (count($accounts) >=1)
                <table >
                    <tr>
                        <td>
                          dispatch
                        </td>
                        <td>
                           Starter No
                        </td>
                        <td>
                            Account Number
                        </td>
                        <td>
                            Account Status
                        </td>
                        <td>
                            Card Number
                        </td>
                        <td >
                            Branch Name
                        </td>
                        <td>
                            Date Created
                        </td>
                        <td>
                            Opened By
                        </td>

                    </tr>
                    @foreach ($accounts as $account)
                    <tr>
                        <td><input type="checkbox" id="{{ $account['acc_no'] }}" class="dispatch"/></td>
                        <td >
                            {{ $account['starter_id'] }}
                        </td>
                        <td >
                            {{ $account['acc_no'] }}
                        </td>
                        <td >
                            {{ $account['acc_status'] }}
                        </td>
                        <td >
                            {{ $account['card_no'] }}
                        </td>
                        <td>
                            {{ $account['branch_name'] }}
                        </td>
                        <td>
                            {{ date('d-m-Y',strtotime($account['date_created'])) }}
                        </td>
                        <td>
                            {{ $account['opened_by'] }}
                        </td>
                    </tr>
                    @endforeach
                </table>

            </div><br />

            <span><input type="checkbox" name="checkall" id ="checkall" /><label>Check all</label></span>

                  <select name="action" id="action">
                      <option>With selected:</option>
                      <option value="dispatch">Dispatch</option>
                      <option value="lock">Lock</option>
                      <option value="unlock">Unlock</option>
                  </select>
            <br />
            <span><a href="{{$base_url}}real_card_excel" class="btn btn-primary">Download excel</a></span>
            @else
              No accounts to show
            @endif
        </div>
    </div>

</div>

</body>
</html>