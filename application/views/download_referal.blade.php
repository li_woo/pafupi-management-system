@include('header')
@include('menu')
<script>
    $(document).ready(function (){
        $('.case').bind('change', function (){
            if  (this.checked) {

                $('.lock-disable').show();

            }
            else {
                var  checkboxes = $('.pafupi_tabl').find('.case' );
                for(var i=0; i< checkboxes.length;i++){
                    if  (checkboxes[i].checked) {

                        $('.lock-disable').show();

                        break;
                    }
                    else {


                        $('.lock-disable').hide();

                    }
                }
                //$('.delete-disabled').hide();
                //$('.note').show();
            }
        })
        $('.lock-disable').hide();
        $('#selectall').bind('change', function (){

            var  checkboxes = $('.pafupi_tabl').find('.case' );
            if  (this.checked) {
                checkboxes.attr('checked', 'true');
                $('.lock-disable').show();
                $('.note').hide();
            }
            else {

                checkboxes.attr('checked', false);
                $('.lock-disable').hide();
                $('.note').show();
            };
        });


        function paginate(){
            $('table.paginate tr.pp').remove();
            var rows=$('table').find('tr.rows').length;

            var no_rec_per_page=5;
            var no_pages= Math.ceil(rows/no_rec_per_page);
            var $pagenumbers=$('<tr id="pp"></tr>');

            for(i=0;i<no_pages;i++)
            {
                $('<td class="pagination-item page"><a href="#" class="clickable">'+(i+1)+'</a></td>').appendTo($pagenumbers);
            }

            $pagenumbers.insertAfter('table.paginate');

            $('.page').hover(
                function(){
                    $(this).addClass('hover pagination-item-current');
                },
                function(){
                    $(this).removeClass('hover pagination-item-current');
                }
            );
            $('.page').bind('hover', function(){

            })
            $('table').find('tr.rows').hide();
            var tr=$('table tr.rows');
            $(tr[0]).show();if(rows >= 2){$(tr[1]).show();}if(rows >= 3){$(tr[2]).show();}if(rows >= 4){$(tr[3]).show();}if(rows >= 5){$(tr[4]).show();}
            $('a.clickable').click(function(event){
                event.preventDefault();
                $('table').find('tr.rows').hide();
                for(var i=($(this).text()-1)*no_rec_per_page;
                    i<=$(this).text()*no_rec_per_page-1;
                    i++)
                {
                    $(tr[i]).show();
                }
            });
        }

@if($search_feedback)
           // paginate();
 @endif
    })
</script>
<style>
    .hover{
        cursor: pointer;
    }
    .asc{
        background:url('{{$base_url}}assets/images/up.png') no-repeat; padding-left:20px;
    }

    .desc{
        background:url('{{$base_url}}assets/images/down.png') no-repeat; padding-left:20px;
    }

</style>
<div class="section">


    <div id="body">
        <div align="center">

            <div class="pafupi_tabl" >
                @if (count($referals) >=1)
                <table border="0" cellpadding="0" cellspacing="0" class="full-tbl-container">
                    <tr>
                        <td colspan="7" class="tbl-header">


                        </td>
                    </tr>


                    <tr class="tbl-titles">

                        <td>
                            Referal Book No.
                        </td>
                        <td >
                            Created By
                        </td>

                        <td>
                            Date Created
                        </td>

                    </tr>
                    <tbody class="table_body main">
                    @foreach ($referals as $referal)
                    <tr class="tbl-rows rows">

                        <td >
                            {{ $referal['referal_no'] }}
                        </td>
                        <td >
                            {{ $referal['created_by'] }}
                        </td>
                        <td >
                            {{ date('d-m-Y H:i:s',strtotime($referal['date_created'])) }}
                        </td>

                    </tr>
                    @endforeach
                    </tbody>

                    <td colspan="7" class="tbl-footer">
                        <div class="tbl-options">

                        </div>
                        <div class="tbl-pagination">

                            <table class="paginate" cellspacing="0" cellpadding="0" border="0">
                                <tr class="pp">
                                    {{$this->pagination->create_links()}}
                                </tr>
                            </table>
                        </div>
                    </td>
                    </form>
                </table>
                <span><a href="{{$base_url}}download_referals" class="btn btn-primary download">Download Referals</a></span>
                @else
                No referals to show
                @endif
            </div><br />


        </div>
    </div>

</div>
@include('footer')