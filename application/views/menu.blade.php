
@if($this->session->userdata('role_id') == 3)

@endif

@if($this->session->userdata('role_id') == 2)

@endif

<!--
/*

this script simply looks at the role ID,
and loads a menu for that particular user who is logged in

1 - Administrator
2 - Registration Clerk
3 - Registration Supervisor


*/
-->





@if($this->session->userdata('role_id') == 1)
    <!-- This is an HTML5 tag for the main menu - this is for support staff -->
    <div id="menu">

        <!-- Primary Menu Container -->
        <ul>

            <!-- >> Primary Menu Item. Make sure the first item (li) has class = first
                 >> Always put style="z-index:9;" in the first href and decrement with every
                 new primary menu item.
                 >> The first item should always have empty span tags (used for styling and
                   spacing            
             -->

            <!-- Make sure that there is an empty submenu container for every primary menuitem WITHOUT submenu items -->
            <li @if ($this->active=='act_home' || $this->active=='') class='first active' @elseif class='first' @endif >
                <a href="{{$base_url}}pafupi/main/" style="z-index:10;">
                    <span></span>home</a><ul><li>&nbsp;</li></ul>
            <li>
            @if($this->session->userdata('username'))

            <li @if ($this->active=='act_pin') class='active'  elseif @endif >
            <a href="{{$base_url}}pafupi/main/otp_creation" style="z-index:9;">
                Generate Starterpacks</a>
            <ul>
                <li><a href="{{$base_url}}pafupi/main/otp_creation">
                        Generate OTP Books</a></li>
                <li><a href="{{$base_url}}pafupi/main/ref_creation">
                        Generate Referral Books</a></li>
                <li><a href="{{$base_url}}pafupi/main/create_card_view">
                        Generate Cards</a></li>
                <li><a href="{{$base_url}}pafupi/main/account_preopen">
                       Generate Accounts</a></li>

                <!--<li><a href="{{$base_url}}pafupi/main/view_preopen">
                        Link accounts</a></li>-->
            </ul>
                <li @if ($this->active=='act_down') class='active'  elseif @endif >
                <a href="{{$base_url}}pafupi/main/view_download_otp" style="z-index:8;">
                    Download Files</a>
                <ul>
                    <li><a href="{{$base_url}}pafupi/main/view_download_otp">
                            Download OTP Books</a></li>
                    <li><a href="{{$base_url}}pafupi/main/view_download_referal">
                            Download Referral Books</a></li>
                    <li><a href="{{$base_url}}pafupi/main/view_download_card">
                            Download ATM Cards</a></li>
                    <!--<li><a href="{{$base_url}}pafupi/main/account_preopen">
                            Download Accounts</a></li>-->


                    <!--<li><a href="{{$base_url}}pafupi/main/view_preopen">
                            Link accounts</a></li>-->

            </ul>
            <li>

            <li @if ($this->active=='act_link') class='active'  elseif @endif >
            <a href="{{$base_url}}pafupi/main/upload_otp" style="z-index:7;">
                Link Starterpacks</a>
                 <ul>

                     <li><a href="{{$base_url}}pafupi/main/upload_otp">
                             Validate OTP Books</a></li>
                     <li><a href="{{$base_url}}pafupi/main/upload_referal">
                             Validate Referal Books</a></li>
                     <li><a href="{{$base_url}}pafupi/main/view_to_card">
                             Link Cards to Accounts</a></li>
                     <li><a href="{{$base_url}}pafupi/main/view_to_link">
                             Link Books to Accounts</a></li>


                 </ul>
            <li>

            <li @if ($this->active=='act_sp') class='active'  elseif @endif >
            <a href="{{$base_url}}pafupi/main/view_linked" style="z-index:6;">
                Package Starterpacks</a>
            <ul>
                <li><a href="{{$base_url}}pafupi/main/view_linked">
                        Download Linked Starterpacks</a></li>

                <li><a href="{{$base_url}}pafupi/main/upload_manu_file">
                       Validate Packaged  Starterpacks</a></li>

            </ul>
            <li>


                <!-- Secondary Menu Container -->
                <!-- // Checks if $active (passed from your main controller) is set to that page
                // If it is, it sets that main menu item to class=active
                -->


            <li @if ($this->active=='act_inventory') class='active'  elseif @endif >
            <a href="{{$base_url}}pafupi/main/dispatch_account" style="z-index:5;">
                Inventory</a>
            <ul>
                <li><a href="{{$base_url}}pafupi/main/dispatch_account">
                        Dispatch  Starterpacks</a></li>
                <li><a href="{{$base_url}}pafupi/main/lock_account">
                        Lock Starterpacks</a></li>
                <li><a href="{{$base_url}}pafupi/main/unlock_account">
                        unlock Starterpacks</a></li>
                <!--<li><a href="{{$base_url}}pafupi/main/dispatched_account">
                        View dispatched Starterpacks</a></li>-->
                <li><a href="{{$base_url}}pafupi/main/regenerate_pins">
                        Regenerate OTP Books</a></li>
            </ul>
            <li>

            <li @if ($this->active=='act_profile') class='active' @elseif @endif >

            <a href="{{$base_url}}pafupi/main/create_user"
               style="z-index:4;">Profiles</a>
            <ul>
                <li><a href="{{$base_url}}pafupi/main/create_user">
                        Register profile</a></li>
                <li><a href="{{$base_url}}pafupi/main/lock_user">
                        Lock profile</a></li>
                <li><a href="{{$base_url}}pafupi/main/unlock_user">
                        Unlock profile</a></li>
                <li><a href="{{$base_url}}pafupi/main/modify_user">
                        Modify profile</a></li>
            </ul>
            <li>

            <li @if ($this->active=='act_password') class='active'  elseif @endif >
                <a href="{{$base_url}}pafupi/main/password_reset" style="z-index:3;">
                    Change Password</a><ul><li>&nbsp;</li></ul>
            <li>


            @endif
        </ul>
    </div>
    <!-- -------------------------------------- -->
@elseif($this->session->userdata('role_id') == 2)
<!-- This is an HTML5 tag for the main menu--this is for branch admin -->
<!-- This is an HTML5 tag for the main menu - this is for support staff -->
<div id="menu">

    <!-- Primary Menu Container -->
    <ul>

        <!-- >> Primary Menu Item. Make sure the first item (li) has class = first
             >> Always put style="z-index:9;" in the first href and decrement with every
             new primary menu item.
             >> The first item should always have empty span tags (used for styling and
               spacing
         -->

        <!-- Make sure that there is an empty submenu container for every primary menuitem WITHOUT submenu items -->
        <li @if ($this->active=='act_home' || $this->active=='') class='first active' @elseif class='first' @endif >
        <a href="{{$base_url}}pafupi/main/" style="z-index:9;">
            <span></span>home</a><ul><li>&nbsp;</li></ul>
        <li>
            @if($this->session->userdata('username'))
        <li @if ($this->active=='act_account') class='active'  elseif @endif >
        <a href="{{$base_url}}pafupi/main/receive_starterpack" style="z-index:8;">
           SP Management</a>
        <ul>
            <li><a href="{{$base_url}}pafupi/main/receive_starterpack">
                    Receive Starterpacks</a></li>
            <li><a href="{{$base_url}}pafupi/main/assign_starterpack">
                    Assign Starterpacks</a></li>
            <li><a href="{{$base_url}}pafupi/main/view_assigned_starterpacks">
                   Assigned Starterpacks</a></li>

        </ul>
        <li>

        <!--<li @if ($this->active=='act_kyc') class='active' @endif >
        <a href="{{$base_url}}pafupi/main/view_customers" style="z-index:7;">
            <span></span>KYC management</a>
                <ul><li>&nbsp;</li></ul>
            <!--<ul>
                <li><a href="{{$base_url}}pafupi/main/view_customers">
                        Receive Starterpacks</a></li>
                <li><a href="{{$base_url}}pafupi/main/upload_kyc_image">
                        Assign Starterpacks</a></li>
                <li><a href="{{$base_url}}pafupi/main/view_assigned_starterpacks">
                        Assigned Starterpacks</a></li>
            </ul>
        <li>-->


        <li @if ($this->active=='act_password') class='active'  elseif @endif >
        <a href="{{$base_url}}pafupi/main/password_reset" style="z-index:6;">
            Change Password</a><ul><li>&nbsp;</li></ul>
        <li>


            @endif
    </ul>
</div>
<!-- -------------------------------------- -->



@endif
    <!-- Submenu Container -->
    <div id="submenu">
    </div>

    <!-- Information Bar -->
    <div id="information-bar">
        <div id="date">{{date('l, jS F Y')}} @if($this->session->userdata('username'))
                                                 |  {{$this->session->userdata('fullname')}}, {{$this->session->userdata('role')}}@{{$this->session->userdata('branch_name')}}
                                             @endif</div>
        <!--<div id="user"><strong>{{$this->session->userdata('fullname')}} |
                {{$this->session->userdata('role')}}
            </strong></div>-->
    </div>

    <div id="screen-title">{{str_replace("_", " ", $this->name); /* Replace all underscores with spaces */}}

    </div>








<!--{{

//options for the registration supervisor

}}-->

@if($this->role_id == 3)

    <script type="text/javascript">



    </script>

    <!-- This is an HTML5 tag for the main menu -->
    <div id="menu">

        <!-- Primary Menu Container -->
        <ul>

            <!-- >> Primary Menu Item. Make sure the first item (li) has class = first
                 >> Always put style="z-index:9;" in the first href and decrement with every
                 new primary menu item.
                 >> The first item should always have empty span tags (used for styling and
                   spacing            
             -->

            <!-- Make sure that there is an empty submenu container for every primary menuitem WITHOUT submenu items -->
            <li @if ($this->active=='act_home') class='first active' @elseif class='first' @endif >
                <a href="{{$base_url}}eazymobile/main/home/act_home/Home" style="z-index:11;">
                    <span></span>home</a><ul><li>&nbsp;</li></ul>
            <li>

                <!-- Secondary Menu Container -->
               {{ // Checks if $active (passed from your main controller) is set to that page
                // If it is, it sets that main menu item to class=active
                }}
            <li  @if ($this->active=='act_cus_man') class='active' @elseif @endif>
                <!--
                // Passes three arguments to maincontroller/main:
                // >> page = the page to appear on the primary navigation link e.g. user_management
                // >> active page = the page that is currently active (with the first three letters seperated with an underscore)
                // as the format e.g act_use_man
                // >> sub page name = name of the page to appear as string title (capitalized and seperated with an underscore
                // ** All primary links will map to the FIRST secondary link of that menu item
                // ** All secondary links will make their parent (primary links) active
                -->
                <a href="{{$base_url}}eazymobile/main/sync_accounts/act_cus_man/Sync_Accounts"
                   style="z-index:10;">Customer Management</a>
                <ul>

                    <li><a href="{{$base_url}}eazymobile/main/sync_accounts/act_cus_man/Sync_Accounts">
                            Sync EazyMobile accounts with T24 records</a></li>

                    <li><a href="{{$base_url}}eazymobile/main/lock_accounts/act_cus_man/Lock_Accounts">
                            Lock accounts</a></li>
                    <li><a href="{{$base_url}}eazymobile/main/unlock_accounts/act_cus_man/Unlock_Accounts">
                            Unlock accounts</a></li>

                </ul>
            <li>

            <li @if ($this->active=='act_cus_workflow') class='active' @elseif @endif>

                @if($menu_notifications > 0)
                    <a id="workflow" href="{{$base_url}}eazymobile/main/workflow/act_cus_workflow/Registration_Workflow"
                       style="z-index:9;">Workflow Management <span id="count" class="counter">{{ $menu_notifications}}</span></a>
                    <ul>

                        <li><a href="{{$base_url}}eazymobile/main/workflow/act_cus_workflow/Registration_Workflow">
                                Registration Workflow</a></li>

                        <li><a href="{{$base_url}}eazymobile/main/workflow_pin/act_cus_workflow/PIN_Reset_Workflow">
                                PIN Reset Workflow</a></li>

                    </ul>



               @endif



                @if($menu_notifications == 0)

                    <a id="workflow" href="{{$base_url}}eazymobile/main/workflow/act_cus_workflow/Registration_Workflow"
                       style="z-index:9;">Workflow Management  </a>

                    <ul>

                        <li><a href="{{$base_url}}'eazymobile/main/workflow/act_cus_workflow/Registration_Workflow">
                                Registration Workflow</a></li>

                        <li><a href="{{$base_url}}eazymobile/main/workflow_pin/act_cus_workflow/PIN_Reset_Workflow">
                                PIN Reset Workflow</a></li>

                    </ul>

                @endif

            <li>

            <li @if ($this->active=='act_password') class='active' @elseif @endif >
                <a href="{{site_url('eazymobile/main/password/act_password/Change_Password')}}" style="z-index:5;">
                    Change Password</a><ul><li>&nbsp;</li></ul>
            <li>
        </ul>
    </div>
    <!-- -------------------------------------- -->

    <!-- Submenu Container -->
    <div id="submenu">

    </div>
    <!-- Information Bar -->
    <div id="information-bar">
        <div id="date">{{date('l, jS F Y')}}</div>
        <div id="user"><strong>{{$this->session->userdata('fullname')}} |
                {{$this->session->userdata('role')}}

            </strong></div>
    </div>

    <div id="screen-title">{{str_replace("_", " ", $this->name); /* Replace all underscores with spaces */ }}</div>

@endif
