@include('header')
@include('menu')
<script>
    function generate(type,content) {
        var n = noty({
            text        : content,
            type        : type,
            dismissQueue: true,
            layout      : 'top',
            theme       : 'defaultTheme',
            maxVisible  : 10,
            buttons: [
                {addClass: 'btn btn-primary', text: 'Ok', onClick: function($noty) {
                    $noty.close();
                }
                }
            ]
        });
        console.log('html: ' + n.options.id);
    }

    function generateAll(type,data) {

        generate(type,data);
    }
    $(document).ready(function(){
        var conf = false;
        $('.role').live('change',function (e){
            e.preventDefault();
            var role = $(this).val();
            if(role == 3){
                alert(role)
                $('#device_label').show();
                $('.device').show();
                $('.device').addClass("validate[required]");
            }
            else{
                $('#device_label').hide();
                $('.device').hide();
                $('.device').removeClass("validate[required]");
            }

        })
        $("form#create").validationEngine('attach', {
            onValidationComplete: function(form, status){
                $(":submit").hide();
                if(status==true){
                    dataForm = form.serialize();

                    url = "{{$base_url}}pafupi/main/create_user";
                    $('div#feed').html('').show();
                    $.ajax({
                        "type":"POST",
                        "url":url,
                        "data":dataForm,
                        "dataType":"json",
                        "success":function(data){
                            if(data !=0){
                                var result;
                                result ="<h4>user account created</h4><br />";
                                result +="for "+data.fullname+" as "+data.username;


                                generateAll('success',result);
                                $(".clearing") .text('');
                                //$(":submit") .text('Create');
                                //$('div#feed').fadeOut(10000);
                            }
                            else
                            {
                                generateAll('error',"user creation failed");

                            }
                        },
                        "error" : function(){

                        }
                    });

                }

                $(":submit").show().delay(10000);
            }

        });

        $('#feed').on('click', function(){
            $('#feed').hide();
        })


    });
    </script>


<div class="section">


    <div id="body">
        <div align="center">

            <form  id="create" method="post">
              <table class="form-container" cellspacing="0" cellpadding="0" border="0">
                <tr><td><div class="user-info-header"><p>Create User</p></div></td></tr>
                  <tr>
                      <td>
                          <div class="inputs">
                                    <label for="fname" >First Name</label><br />

                                    <label for="lname">Last Name</label><br />

                                    <label for="lname">Email Address</label><br />

                                    <label for="role">Role</label><br />

                                    <label for="branch">Branch</label><br />
                                    <label for="device" style="display:none" id="device_label">Device number</label><br />
                          </div>
                          <div class="inputs">
                                        <input type="text" name="fname" class="clearing validate[required] textbox" /><br />
                                        <input type="text" name="lname" class=" clearing validate[required] textbox" /><br />
                                        <input type="text" name="email" class="clearing validate[custom[email]] textbox" /><br />
                                        <select name="role" class="validate[required] styled-select option role">
                                            @foreach ($roles as $role)
                                            <option value="{{ $role['RoleID'] }}">{{ $role['RoleName'] }}</option>
                                            @endforeach
                                        </select><br />

                                        <select name="branch" class="validate[required] styled-select option">
                                            @foreach ($branches as $branch)
                                            <option value="{{ $branch['branch_id'] }}">{{ $branch['branch_name'] }}</option>
                                            @endforeach
                                        </select><br />
                                       <input type="text" name="device" style="display:none" class="clearing textbox device" /><br />
                                       <input type="submit" class="btn-primary submit" value="Create"/>
                          </div>


                      </td>
                  </tr>

              </table>
            </form>
        </div>
    </div>


</div>

@include('footer')