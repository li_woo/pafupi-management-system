@include('header')
@include('menu')
<script>
    function generateA(layout,type,content) {
        var n = noty({
            text        : content,
            type        : type,
            layout      : layout,
            dismissQueue: true,
            theme       : 'defaultTheme',
            maxVisible  : 10,
            buttons: [
                {addClass: 'btn btn-primary', text: 'Ok', onClick: function($noty) {
                    $noty.close();
                }
                }
            ]
        });
        console.log('html: ' + n.options.id);
    }
    function generateFeedback(layout,type,data) {

        generateA(layout,type,data);
    }



    $(document).ready(function (){
        @if($feedback)
            generateFeedback('top','warning',"{{$feedback}}");
        @endif

        $('.branch').change(function(event){
            $('table').find('tr.rows').hide();
            $('tr.no_account').remove();

            var fil = $(this).val();
            var num = 0;
            $('table tr.rows').each(function(){
                coldata=$(this).children().eq(3);

                if(fil.toUpperCase().trim() == coldata.text().toUpperCase().trim())
                {
                    $(this).show();
                    num++;

                }

            });
            if(num < 1){
                $('tbody.main')
                    .append(" <tr class='no_account'><td>&nbsp;</td><td>&nbsp;</td><td><p style='color: red'>No accounts</p></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>");

            }
            else{
                $('tr.no_account').remove();
            }

            event.preventDefault();
        });






        $('.case').bind('change', function (){
            if  (this.checked) {

                $('.lock-disable').show();

            }
            else {
                var  checkboxes = $('.pafupi_tabl').find('.case' );
                for(var i=0; i< checkboxes.length;i++){
                    if  (checkboxes[i].checked) {

                        $('.lock-disable').show();

                        break;
                    }
                    else {


                        $('.lock-disable').hide();

                    }
                }
                //$('.delete-disabled').hide();
                //$('.note').show();
            }
        })
        $('.lock-disable').hide();
        $('#selectall').bind('change', function (){

            var  checkboxes = $('.pafupi_tabl').find('.case' );
            if  (this.checked) {
                checkboxes.attr('checked', 'true');
                $('.lock-disable').show();
                $('.note').hide();
            }
            else {

                checkboxes.attr('checked', false);
                $('.lock-disable').hide();
                $('.note').show();
            };
        });


        //sorting
        $('.tbl-titles td').each(function(column) {

            $(this).hover(
                function(){
                    $(this).addClass('hover');
                },
                function(){
                    $(this).removeClass('hover');
                }
            );

            $(this).click(function(){

                if($(this).is('.asc'))
                {
                    $(this).removeClass('asc');
                    $(this).addClass('desc');
                    $('.tbl-titles td').find('img').remove();
                    $(this).append("<img src='{{$base_url}}assets/images/down.png' />");
                    sortdir=-1;
                }
                else
                {
                    $(this).addClass('asc');
                    $(this).removeClass('desc');
                    $('.tbl-titles td').find('img').remove();
                    $(this).append("<img src='{{$base_url}}assets/images/up.png' />");
                    sortdir=1;
                }
                $(this).siblings().removeClass('asc');
                $(this).siblings().removeClass('desc');

                var rec=$('table').find('tr.rows').get();

                rec.sort(function(a, b) {
                    var val1 = $(a).children('td').eq(column).text().toUpperCase();

                    var val2 = $(b).children('td').eq(column).text().toUpperCase();
                    return (val1 < val2) ? -sortdir : (val1 > val2) ? sortdir : 0;
                });
                $('table').find('tr.rows').remove();
                $.each(rec, function(index, row) {

                    $('tbody.main').append(row);
                    console.log(row);
                });
            });
        });



        function paginate(){
            $('table.paginate tr.pp').remove();
            var rows=$('table').find('tr.rows').length;

            var no_rec_per_page=5;
            var no_pages= Math.ceil(rows/no_rec_per_page);
            var $pagenumbers=$('<tr id="pp"></tr>');

            for(i=0;i<no_pages;i++)
            {
                $('<td class="pagination-item page"><a href="#" class="clickable">'+(i+1)+'</a></td>').appendTo($pagenumbers);
            }

            $pagenumbers.insertAfter('table.paginate');

            $('.page').hover(
                function(){
                    $(this).addClass('hover pagination-item-current');
                },
                function(){
                    $(this).removeClass('hover pagination-item-current');
                }
            );
            $('.page').bind('hover', function(){

            })
            $('table').find('tr.rows').hide();
            var tr=$('table tr.rows');
            $(tr[0]).show();if(rows >= 2){$(tr[1]).show();}if(rows >= 3){$(tr[2]).show();}if(rows >= 4){$(tr[3]).show();}if(rows >= 5){$(tr[4]).show();}
            $('a.clickable').click(function(event){
                event.preventDefault();
                $('table').find('tr.rows').hide();
                for(var i=($(this).text()-1)*no_rec_per_page;
                    i<=$(this).text()*no_rec_per_page-1;
                    i++)
                {
                    $(tr[i]).show();
                }
            });
        }

@if($search_feedback)
            paginate();
 @endif
    })
</script>
<style>
    .hover{
        cursor: pointer;
    }
    .asc{
        background:url('{{$base_url}}assets/images/up.png') no-repeat; padding-left:20px;
    }

    .desc{
        background:url('{{$base_url}}assets/images/down.png') no-repeat; padding-left:20px;
    }

</style>
<div class="section">


    <div id="body">
        <div align="center">

            <p style="color: red">
                @if($search_feedback)
                {{$search_feedback}}<br />
                @endif

            </p>
            <div class="pafupi_tabl" >
                @if (count($accounts) >=1)
                <table border="0" cellpadding="0" cellspacing="0" class="full-tbl-container">
                    <tr>
                        <td colspan="7" class="tbl-header">
                            <form id="search" action="{{$base_url}}pafupi/main/view_preopen" method="post">
                                <table>
                                    <tr>
                                        <!--<td width="50"></td>
                                        <td width="250"><input type="text" id="param" name="param" placeholder="Search account" class="validate[required]"/></td>
                                        <input type="hidden" name="search" value="s" />
                                        <td width=""><button type="submit" class="btn btn-primary">Search</button></td>-->

                                        <td>
                                            <label for="branch">filter by branch</label>
                                            <select name="branch"  class="validate[required] styled-select option branch">
                                            @foreach ($branches as $branch)
                                            <option value="{{ $branch['branch_id'] }}">{{ $branch['branch_name'] }}</option>
                                            @endforeach

                                             </select>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </td>
                    </tr>
                    <form id="search" action="{{$base_url}}pafupi/main/view_preopen" method="post">
                        <input type="hidden" name="link_card" value="yes" />

                        <tr class="tbl-titles">
                            <td>&nbsp;</td>
                            <td>
                                Account Number
                            </td>
                            <td >
                                Branch Name
                            </td>
                            <!--<td>&nbsp;</td>-->
                            <td>
                                Date Created
                            </td>
                            <td>
                                Opened By
                            </td>
                        </tr>
                        <tbody class="table_body main">
                        @foreach ($accounts as $account)
                        <tr class="tbl-rows rows">
                            <td>
                                <input type="checkbox" class="case" value="{{$account['acc_no']}}"
                                       name="selected_accounts[]" id="selected_accounts[]" />
                            </td>
                            <td >
                                {{ $account['acc_no'] }}
                            </td>
                            <td class="branch-name">
                                {{ $account['branch_name'] }}
                            </td>
                            <td style="display:none" class="branch_id">
                                {{ $account['branch_id'] }}
                            </td>
                            <td>
                                {{ date('d-m-Y H:i',strtotime($account['date_created'])) }}
                            </td>
                            <td>
                                {{ $account['FullName'] }}
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                        <tr id="select_all">

                            <td colspan="1" class="tbl-footer-checks" >


                                    <input type="checkbox" id="selectall" />



                            </td>
                            <td colspan="6" class="tbl-footer">
                                <div class="tbl-options">
                                    <button type="submit" style="display:none"  class="btn btn-danger lock-disable">Link to card</button>
                                </div>
                                <div class="tbl-pagination">

                                    <table class="paginate" cellspacing="0" cellpadding="0" border="0">
                                        <tr class="pp">
                                            {{$this->pagination->create_links()}}
                                        </tr>
                                    </table>
                                </div>
                            </td>
                    </form>
                </table>
                <span>
                    @if(isset($action))
                      @if($action =='carding')
                          <a href="{{$base_url}}pafupi/main/view_to_card?act=carding" class="btn btn-primary download">Link all to cards</a>
                      @elseif($action=='linking')
                          <a href="{{$base_url}}pafupi/main/view_to_link?act=linking" class="btn btn-primary download">Link all to books</a>
                      @endif
                    @endif
                </span>
                @else
                No accounts to show
                @endif
            </div><br />


        </div>
    </div>

</div>
@include('footer')