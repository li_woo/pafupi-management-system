@include('header')
@include('menu')
<script>
    function generate(type,content) {
        var n = noty({
            text        : content,
            type        : type,
            dismissQueue: true,
            layout      : 'top',
            theme       : 'defaultTheme',
            maxVisible  : 10,
            buttons: [
                {addClass: 'btn btn-primary', text: 'Ok', onClick: function($noty) {
                    $noty.close();
                }
                }]
        });

    }

    function generateAll(type,data) {

        generate(type,data);
    }

    $(document).ready(function (){
        $('.lock-disable').hide();$('.note').show();
        $('.case').bind('change', function (){
            if  (this.checked) {

                $('.lock-disable').show();
                $('.note').hide();
            }
            else {
                var  checkboxes = $('.pafupi_tabl').find('.case' );
                for(var i=0; i< checkboxes.length;i++){
                    if  (checkboxes[i].checked) {

                        $('.lock-disable').show();
                        $('.note').hide();
                        break;
                    }
                    else {


                        $('.lock-disable').hide();
                        $('.note').show();
                    }
                }
                //$('.delete-disabled').hide();
                //$('.note').show();
            }
        })
        //$('.lock-disable').hide();
        $('#selectall').bind('change', function (){

            var  checkboxes = $('.pafupi_tabl').find('.case' );
            if  (this.checked) {
                checkboxes.attr('checked', 'true');
                $('.lock-disable').show();
                $('.note').hide();
            }
            else {

                checkboxes.attr('checked', false);
                $('.lock-disable').hide();
                $('.note').show();
            };
        });







        function paginate(){
            $('table.paginate tr.pp').remove();
            var rows=$('table').find('tr.rows').length;

            var no_rec_per_page=5;
            var no_pages= Math.ceil(rows/no_rec_per_page);
            var $pagenumbers=$('<tr id="pp"></tr>');

            for(i=0;i<no_pages;i++)
            {
                $('<td class="pagination-item page"><a href="#" class="clickable">'+(i+1)+'</a></td>').appendTo($pagenumbers);
            }

            $pagenumbers.appendTo('table.paginate');

            $('.page').hover(
                function(){
                    $(this).addClass('hover pagination-item-current');
                },
                function(){
                    $(this).removeClass('hover pagination-item-current');
                }
            );
            $('.page').bind('hover', function(){

            })
            $('table').find('tr.rows').hide();
            var tr=$('table tr.rows');
            $(tr[0]).show();if(rows >= 2){$(tr[1]).show();}if(rows >= 3){$(tr[2]).show();}if(rows >= 4){$(tr[3]).show();}if(rows >= 5){$(tr[4]).show();}
            $('a.clickable').click(function(event){
                event.preventDefault();
                $('table').find('tr.rows').hide();
                for(var i=($(this).text()-1)*no_rec_per_page;
                    i<=$(this).text()*no_rec_per_page-1;
                    i++)
                {
                    $(tr[i]).show();
                }
            });
        }

@if($search_feedback)
            paginate();
 @endif
    })
</script>
<div class="section">


    <div id="body">
        <div align="center">
            <p style="color: red">
                @for ($i = 0; $i <count($feedback); $i++)
                {{$feedback[$i]}} <br />
                @endfor
            </p>
            <p style="color: red">
                @if($search_feedback)
                {{$search_feedback}}<br />
                @endif

            </p>
            <div class="pafupi_tabl" >
                @if (count($accounts) >=1)
                <table border="0" cellpadding="0" cellspacing="0" class="full-tbl-container">
                    <tr>
                        <td colspan="7" class="tbl-header">
                            <form id="search" action="{{$base_url}}pafupi/main/receive_starterpack" method="post">
                                <table>
                                    <tr>
                                        <td width="50"></td>
                                        <td width="">Select the account</td>
                                        <td width="400"><input type="text" id="param" name="param" placeholder="Search account" class="validate[required]"/></td>
                                        <input type="hidden" name="search" value="s" />
                                        <td width=""><button type="submit" class="btn btn-primary">Search</button></td>
                                    </tr>
                                </table>
                            </form>
                        </td>
                    </tr>
                    <form id="search" action="{{$base_url}}pafupi/main/receive_starterpack" method="post">
                        <input type="hidden" name="receive" value="yes" />

                    <tr class="tbl-titles">
                        <td>&nbsp;</td>
                        <td >
                            SP Number
                        </td>
                        <td>
                            Dispatched By
                        </td>
                        <td>
                            Date Dispatched
                        </td>



                    </tr>
                    @foreach ($accounts as $account)
                    <tr  class="tbl-rows rows">
                        <td>
                            <input type="checkbox" class="case" value="{{$account['acc_no']}}"
                                   name="selected_accounts[]" id="selected_accounts[]" />
                        </td>
                        <td >
                            {{ $account['starter_id'] }}
                        </td>
                        <td >
                            {{ $account['dispatched_by'] }}
                        </td>
                        <td>
                            {{ date('d-m-Y',strtotime($account['dispatch_date'])) }}
                        </td>


                    </tr>
                    @endforeach
                    <tr id="select_all">
                        <td colspan="1" class="tbl-footer-checks" >


                            <input type="checkbox" id="selectall" />



                        </td>

                        <td colspan="7" class="tbl-footer">
                            <div class="tbl-options">
                                <button type="submit"  class="btn btn-danger lock-disable">Receive Selected</button>
                            </div>
                            <div class="tbl-pagination">

                                <table  class="paginate" cellspacing="0" cellpadding="0" border="0">
                                    <tr class="pp">
                                        {{$this->pagination->create_links()}}
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>

                    <!--<p class="note">No other user profiles are eligible for locking. <a href="{{$base_url}}pafupi/main/create_user">Would you like to register a new user profile?</a></p> <br />-->
                </form>
                </table>

                @else
                No Accounts to receive
                @endif
            </div><br />


        </div>
    </div>

</div>
@include('footer')