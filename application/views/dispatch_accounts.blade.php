@include('header')
@include('menu')
<script>
    function generate(type,content) {
        var n = noty({
            text        : content,
            type        : type,
            dismissQueue: true,
            layout      : 'top',
            theme       : 'defaultTheme',
            maxVisible  : 10,
            buttons: [
                {addClass: 'btn btn-primary', text: 'Ok', onClick: function($noty) {
                    $noty.close();
                }
                }]
        });

    }

    function generateAll(type,data) {

        generate(type,data);
    }

    $(document).ready(function (){
        $('.lock-disable').hide();$('.note').show();
        $('.case').bind('change', function (){
            if  (this.checked) {

                $('.lock-disable').show();
                $('.note').hide();
            }
            else {
                var  checkboxes = $('.pafupi_tabl').find('.case' );
                for(var i=0; i< checkboxes.length;i++){
                    if  (checkboxes[i].checked) {

                        $('.lock-disable').show();
                        $('.note').hide();
                        break;
                    }
                    else {


                        $('.lock-disable').hide();
                        $('.note').show();
                    }
                }
                //$('.delete-disabled').hide();
                //$('.note').show();
            }
        })
        //$('.lock-disable').hide();
        $('#selectall').bind('change', function (){

            var  checkboxes = $('.pafupi_tabl').find('.case' );
            if  (this.checked) {
                checkboxes.attr('checked', 'true');
                $('.lock-disable').show();
                $('.note').hide();
            }
            else {

                checkboxes.attr('checked', false);
                $('.lock-disable').hide();
                $('.note').show();
            };
        });



        $( "#lock_users").submit(function(event){
            event.preventDefault();

            confirm_lock();




        })

        //sorting
        $('.tbl-titles td').each(function(column) {
            $(this).hover(
                function(){
                    $(this).addClass('hover');

                },
                function(){
                    $(this).removeClass('hover');
                }
            );

            $(this).click(function(){
                if($(this).is('.asc'))
                {
                    $(this).removeClass('asc');
                    $(this).addClass('desc');
                    $('.tbl-titles td').find('img').remove();
                    $(this).append("<img src='{{$base_url}}assets/images/down.png' />");
                    sortdir=-1;
                }
                else
                {
                    $(this).addClass('asc');
                    $(this).removeClass('desc');
                    $('.tbl-titles td').find('img').remove();
                    $(this).append("<img src='{{$base_url}}assets/images/up.png' />");
                    sortdir=1;
                }
                $(this).siblings().removeClass('asc');
                $(this).siblings().removeClass('desc');

                var rec=$('table').find('tr.rows').get();

                rec.sort(function(a, b) {
                    var val1 = $(a).children('td').eq(column).text().toUpperCase();

                    var val2 = $(b).children('td').eq(column).text().toUpperCase();
                    return (val1 < val2) ? -sortdir : (val1 > val2) ? sortdir : 0;
                });
                $('table').find('tr.rows').remove();
                $.each(rec, function(index, row) {

                    $('tbody.main').append(row);
                    console.log(row);
                });
            });
        });




        function paginate(){
            $('table.paginate tr.pp').remove();
            var rows=$('table').find('tr.rows').length;

            var no_rec_per_page=5;
            var no_pages= Math.ceil(rows/no_rec_per_page);
            var $pagenumbers=$('<tr id="pp"></tr>');

            for(i=0;i<no_pages;i++)
            {
                $('<td class="pagination-item page"><a href="#" class="clickable">'+(i+1)+'</a></td>').appendTo($pagenumbers);
            }

            $pagenumbers.insertAfter('table.paginate');

            $('.page').hover(
                function(){
                    $(this).addClass('hover pagination-item-current');
                },
                function(){
                    $(this).removeClass('hover pagination-item-current');
                }
            );
            $('.page').bind('hover', function(){

            })
            $('table').find('tr.rows').hide();
            var tr=$('table tr.rows');
            $(tr[0]).show();if(rows >= 2){$(tr[1]).show();}if(rows >= 3){$(tr[2]).show();}if(rows >= 4){$(tr[3]).show();}if(rows >= 5){$(tr[4]).show();}
            $('a.clickable').click(function(event){
                event.preventDefault();
                $('table').find('tr.rows').hide();
                for(var i=($(this).text()-1)*no_rec_per_page;
                    i<=$(this).text()*no_rec_per_page-1;
                    i++)
                {
                    $(tr[i]).show();
                }
            });
        }

@if($search_feedback)
            paginate();
 @endif
    })
</script>
<style>
    .hover{
        cursor: pointer;
    }
    .asc{
        background:url('{{$base_url}}assets/images/up.png') no-repeat; padding-left:20px;
    }

    .desc{
        background:url('{{$base_url}}assets/images/down.png') no-repeat; padding-left:20px;
    }

</style>
<div class="section">


    <div id="body">
        <div align="center">
            <p style="color: red">
                @for ($i = 0; $i <count($feedback); $i++)
                {{$feedback[$i]}} <br />
                @endfor
            </p>
            <p style="color: red">
                @if($search_feedback)
                {{$search_feedback}}<br />
                @endif

            </p>
            <div class="pafupi_tabl" >
                @if (count($accounts) >=1)
                <table border="0" cellpadding="0" cellspacing="0" class="full-tbl-container">
                    <tr>
                        <td colspan="7" class="tbl-header">
                            <form id="search" action="{{$base_url}}pafupi/main/dispatch_account" method="post">
                                <table>
                                    <tr>
                                        <td width="50"></td>
                                        <td width="">Select the users you would like to lock</td>
                                        <td width="400"><input type="text" id="param" name="param" placeholder="Search user" class="validate[required]"/></td>
                                        <input type="hidden" name="search" value="s" />
                                        <td width=""><button type="submit" class="btn btn-primary">Search</button></td>
                                    </tr>
                                </table>
                            </form>
                        </td>
                    </tr>
                    <form id="dispatch_form" action="{{$base_url}}pafupi/main/dispatch_account" method="post">
                        <input type="hidden" name="dispatch" value="yes" />

                        <tr class="tbl-titles">
                            <td>&nbsp;</td>
                            <td>
                                Account Number
                            </td>
                            <td>
                                Card Number
                            </td>
                            <td >
                                Branch Name
                            </td>
                            <td>
                                Date Created
                            </td>
                            <td>
                                Opened By
                            </td>

                        </tr>
                        <tbody class="main">
                        @foreach ($accounts as $account)
                        <tr  class="tbl-rows rows">
                            <td>
                                <input type="checkbox" class="case" value="{{$account['acc_no']}}"
                                       name="selected_accounts[]" id="selected_accounts[]" />
                            </td>
                            <td >
                                {{ $account['acc_no'] }}
                            </td>
                            <td >
                                {{ $account['card_no'] }}
                            </td>
                            <td>
                                {{ $account['branch_name'] }}
                            </td>
                            <td>
                                {{ date('d-m-Y',strtotime($account['date_created'])) }}
                            </td>
                            <td>
                                {{ $account['FullName'] }}
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                        <tr id="select_all">

                            <td colspan="1" class="tbl-footer-checks" >


                                <input type="checkbox" id="selectall" />



                            </td>
                            <td colspan="6" class="tbl-footer">
                                <div class="tbl-options">
                                    <button type="submit" style="display:none"  class="btn btn-danger lock-disable">Dispatch Selected</button>
                                </div>
                                <div class="tbl-pagination">

                                    <table class="paginate" cellspacing="0" cellpadding="0" border="0">
                                        <tr class="pp">
                                            {{$this->pagination->create_links()}}
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </form>
                    <!--<p class="note">No other user profiles are eligible for locking. <a href="{{$base_url}}pafupi/main/create_user">Would you like to register a new user profile?</a></p> <br />-->
                </table>

                @else
                No Accounts to show
                @endif
            </div><br />


        </div>
    </div>

</div>
@include('footer')