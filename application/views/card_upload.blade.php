@include('header')
@include('menu')
<div class="section">
    <div align="center">


        @if($errors)
        <h4>Feedback</h4>
          <span style="color: red">
                @if(@$errors['database'])
                    @foreach($errors['database'] as $error)
                    {{$error}}<br />
                    @endforeach
                @endif
                @foreach($errors as $error)
                   @if(isset($error['row'])){{$error['row']}}<br />  @endif
                   @if(isset($error['cell']))

                       {{$error['cell']}}}<br />
                   @endif
                    @if(isset($error['error'])){{$error['error']}}
                      <div align="left" style="width:100px" id="progressF" class="jquery-ui-like"><div></div></div>
                      <script>
                          progressBar({{$errors['percent']}}, $('#progressF'));
                      </script>
                      <br />
                      @endif
                       @if(isset($error))

                       {{$error}}<br />
                   @endif
                @endforeach


        </span>
        @endif

    </div>
    <form action="{{$base_url}}pafupi/main/upload_card_file" enctype="multipart/form-data" method="post">
        <table class="form-container" cellspacing="0" cellpadding="0" border="0">
            <tr><td><div class="user-info-header"><p>Upload card excel file</p></div></td></tr>
            <tr>
                <td>
                    <div class="inputs">
                        <label for="excel">Select excel file</label><br />

                    </div>
                    <div class="inputs">
                        <input type="file" name="userfile" multiple /><br />
                        <button type="submit" class="btn-primary submit" >Upload</button>
                    </div>
                </td>
            </tr>
        </table>
    </form>


</div>
</div>


</div>

@include('footer')