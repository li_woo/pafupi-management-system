<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Account_model extends CI_Model {
    public function __construct(){
        parent::__construct();
        $this->load->database();
        //$this->load->library('common_functions');

    }
    public function test(){
        $result = $this->db->query("SELECT getdate()");
        var_dump($result);exit;
    }
    public function getAccountBookPins($acc_no=false){

        if($acc_no){
            $sql = 'SELECT [trans_id]
                     ,[trans_pin]
                      ,[trans_pins].[book_no]
                FROM [PAFUPINBS].[dbo].[trans_pins]
                INNER JOIN [PAFUPINBS].[dbo].[booklet]
                ON [trans_pins].[book_no] = [booklet].[book_no]
                WHERE [booklet].[acc_no]=?';
            $result = $this->db->query($sql,array($acc_no));
            return $result->result_array();
        }
        else{
            $sql = 'SELECT [trans_id]
                     ,[trans_pin]
                      ,[trans_pins].[book_no]
                FROM [PAFUPINBS].[dbo].[trans_pins]
                INNER JOIN [PAFUPINBS].[dbo].[booklet]
                ON [trans_pins].[book_no] = [booklet].[book_no]
               ';
            $result = $this->db->query($sql);
            return $result->result_array();
        }
    }
    public function getReferalPins($acc_no){
        $sql = 'SELECT [coupon_no]
                  ,[referal_coupons].[referal_no]
                  ,[coupon_id]
              FROM [PAFUPINBS].[dbo].[referal_coupons]
              INNER JOIN [PAFUPINBS].[dbo].[referal_book]
              ON [referal_coupons].[referal_no] = [referal_book].[referal_no]
              WHERE [referal_book].[acc_no]=?';
        $result = $this->db->query($sql,array($acc_no));
        return $result->result_array();
    }

    public function getBranches($branch_id=false){
        if($branch_id){
            //$query = $this->db->get_where('branches', array('branch_id' => $branch_id));
            $q = 'SELECT [branch_name],[branch_id]
		             FROM [PAFUPINBS].[dbo].[branches]
					 WHERE [branch_id] = ?';

            $result = $this->db->query($q,array($branch_id));
            return $result->result_array();
        }
        else{
            $q = 'SELECT [branch_name],[branch_id]
		          FROM [PAFUPINBS].[dbo].[branches]';
            $result = $this->db->query($q);
            return $result->result_array();
        }
    }
    public function saveAccount($data){

        $feedback = array();
        $account = array();
        $username =  $this->session->userdata('username');
        $user_id =  $this->session->userdata('user_id');
        //$data['branch_id'] = $this->session->userdata('branch');
        $this->db->trans_start();
        $sql_create_account = 'INSERT INTO [PAFUPINBS].[dbo].[PAFUPINBS_account]
		                       ([acc_no],[branch_id],[opened_by],[customer_id])
							   VALUES(?,?,?,?)';
        $sql_create_customer = 'INSERT INTO [PAFUPINBS].[dbo].[Customer]
                               ([CustomerNumber])
                               VALUES(?)';
        //change
        $data['customer_id'] =  substr($data['account_number'],3,6);
        $this->db->query($sql_create_account,array($data['account_number'],$data['branch_id'],$user_id,$data['customer_id']));
        $this->db->query($sql_create_customer,array(substr($data['account_number'],3,6)));
        $this->db->trans_complete();
        $result = $this->db->trans_status();
        if($result){

            array_push($account,array('account' =>'account no '.$data['account_number'].' created'));
            $sql_select = 'SELECT [acc_no],[branch_id],[date_created],[opened_by]
		                   FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					       WHERE [acc_no] = ?';

            $result = $this->db->query($sql_select,array($data['account_number']));
            foreach ($result->result() as $row)
            {
                $cust_id = substr($row->acc_no,4,6);
                $done = $this->makeBooklet($row->acc_no,$cust_id);
                $done2 = $this->makeCoupon($row->acc_no,$cust_id);
                $this->log_activity('Account creation', $username,$row->acc_no);
                $sql_account = 'INSERT INTO [PAFUPINBS].[dbo].[account_status]
		                    ([acc_no],[dispatch_flag],[lock_status],[acc_status])
							 VALUES(?,?,?,?)';
                $result =$this->db->query($sql_account,array($row->acc_no,false,false,'precarded'));
                if($result){
                    $this->log_activity('Account creation-status inserted', $username,$row->acc_no);
                }
                else{
                    $this->log_activity('Account creation-status insertion failed', $username,$row->acc_no);
                }
                array_push($account,$done);
                //return $done;
            }
            return true;
        }
        else{
            return false;
            //array_push($account,array('account' =>'account no '.$data['account_number'].' NOT saved in database'));
        }
        //array_push($feedback,$account);

        return true;
    }
    public function accountExists($acc_no){
        $sql_select = 'SELECT COUNT([acc_no]) as total
		             FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					 WHERE [acc_no] = ?';

        $query = $this->db->query($sql_select,array($acc_no));
        foreach($query->result() as $row){
            if($row->total >=1){
                return true;
            }
            else{
                return false;
            }
        }

    }
    public function get_total_accounts(){
        $count = 0;
        $sql_account = 'SELECT COUNT(*) as [total_accounts]
		             FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]';
        $result = $this->db->query($sql_account);

        foreach($result->result() as $row)
        {

            $count = $row->total_accounts;

        }
        return $count;
    }
    public function getFullyCount($percentage=false){
        $count = 0;
        $sql_account = 'SELECT COUNT([PAFUPINBS_account].[acc_no]) as [fully_accounts]
		              FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
                                         INNER JOIN [PAFUPINBS].[dbo].[branches]
                                         ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_card]
                                         ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [PAFUPINBS].[dbo].[Customer]
                                         ON [PAFUPINBS_account].[customer_id] = [Customer].[CustomerNumber]
                                          INNER JOIN [PAFUPINBS].[dbo].[CustomerProfile]
                                         ON [PAFUPINBS_account].[customer_id] = [CustomerProfile].[CustomerID]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_status]
                                         ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_mobility]
                                         ON [PAFUPINBS_account].[acc_no] = [account_mobility].[acc_no]
                                         INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                                         ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
                                         ';

        $result = $this->db->query($sql_account);
        foreach($result->result() as $row)
        {

            $count = $row->fully_accounts;

        }
        $total_accounts = $this->get_total_accounts();
        if($percentage){
            if($total_accounts==0) $total_accounts=1;
            $percentage_fully = round(($count/$total_accounts) * 100,0);

            return $percentage_fully;
        }
        else{
            return $count;
        }
    }
    public function getCardedCount($percentage=false){
        $count = 0;
        $sql_account = 'SELECT COUNT([PAFUPINBS_account].[acc_no]) as [carded_accounts]
		              FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					 INNER JOIN [PAFUPINBS].[dbo].[branches]
					 ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [PAFUPINBS].[dbo].[account_card]
					 ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
					 INNER JOIN [PAFUPINBS].[dbo].[account_status]
                     ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                     INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                     ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
					 WHERE [account_status].[acc_status]=?';

        $result = $this->db->query($sql_account,array('carded'));
        foreach($result->result() as $row)
        {

            $count = $row->carded_accounts;

        }
        $total_accounts = $this->get_total_accounts();
        if($percentage){
            if($total_accounts==0) $total_accounts=1;
            $percentage_carded = round(($count/$total_accounts) * 100,0);

            return $percentage_carded;
        }
        else{
            return $count;
        }
    }
    /*
     * get all the accounts with real cards...accounts with a starterpack_id
     */
    public function getWithCardAccounts($user_id=false,$acc_no=false){
        if($user_id && !$acc_no){

            $sql_account = 'SELECT [PAFUPINBS_account].*,[branches].[branch_name],[account_card].[card_no],[account_status].[acc_status],[account_card].[starter_id],[lock_status]
		             FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					 INNER JOIN [PAFUPINBS].[dbo].[branches]
					 ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [PAFUPINBS].[dbo].[account_card]
					 ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
					 INNER JOIN [account_status]
					 ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
					 WHERE [account_card].[starter_id] !=? AND [account_status].[dispatch_flag]=? AND [PAFUPINBS_account].[opened_by]=?';

            $query = $this->db->query($sql_account,array(NULL,FALSE,$user_id));

            return $query->result_array();
        }
        else if($user_id && $acc_no){

            $sql_account = 'SELECT [PAFUPINBS_account].*,[branches].[branch_name],[account_card].[card_no],[account_status].[acc_status],[account_card].[starter_id],[lock_status]
		             FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					 INNER JOIN [PAFUPINBS].[dbo].[branches]
					 ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [PAFUPINBS].[dbo].[account_card]
					 ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
					 INNER JOIN [account_status]
					 ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
					 WHERE [account_card].[starter_id] !=? AND [PAFUPINBS_account][acc_no]=? AND [account_status].[dispatch_flag]=? AND [PAFUPINBS_account].[opened_by]=?';

            $query = $this->db->query($sql_account,array(NULL,FALSE,$acc_no,FALSE,$user_id));

            return $query->result_array();
        }
        else if(!$user_id && $acc_no){

            $sql_account = 'SELECT [PAFUPINBS_account].*,[branches].[branch_name],[account_card].[card_no],[account_status].[acc_status],[account_card].[starter_id],[lock_status]
		             FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					 INNER JOIN [PAFUPINBS].[dbo].[branches]
					 ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [PAFUPINBS].[dbo].[account_card]
					 ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
					 INNER JOIN [account_status]
					 ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
					 WHERE [account_card].[starter_id] !=? AND [account_status].[dispatch_flag]=? AND [PAFUPINBS_account][acc_no]=? ';

            $query = $this->db->query($sql_account,array(NULL,FALSE,$acc_no));

            return $query->result_array();
        }
        else{

            $sql_account = 'SELECT [PAFUPINBS_account].*,[branches].[branch_name],[account_card].[card_no],[account_status].[acc_status],[account_card].[starter_id],[lock_status]
		             FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					 INNER JOIN [PAFUPINBS].[dbo].[branches]
					 ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [PAFUPINBS].[dbo].[account_card]
					 ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
					 INNER JOIN [account_status]
					 ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
					 WHERE [account_card].[starter_id] !=NULL AND [account_status].[dispatch_flag]=FALSE';

            $query = $this->db->query($sql_account,array(NULL,FALSE));

            return $query->result_array();

        }
    }
    /*
     * gets all accounts with card data from Fincard but without starterpack_id
     */
    public function getCardAccounts($user_id=false,$acc_no=false){
        if($user_id && !$acc_no){

            $sql_account = 'SELECT [PAFUPINBS_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName],[book_no],[referal_no]
		             FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					 INNER JOIN [PAFUPINBS].[dbo].[branches]
					 ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [PAFUPINBS].[dbo].[account_card]
					 ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
					 INNER JOIN [PAFUPINBS].[dbo].[booklet]
                     ON [PAFUPINBS_account].[acc_no] = [booklet].[acc_no]
                     INNER JOIN [PAFUPINBS].[dbo].[referal_book]
                     ON [PAFUPINBS_account].[acc_no] = [referal_book].[acc_no]
					 INNER JOIN [PAFUPINBS].[dbo].[account_status]
                     ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                     INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                     ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
					 WHERE [PAFUPINBS_account].[opened_by]=? AND [account_status].[acc_status]=?';

            $query = $this->db->query($sql_account,array($user_id,'carded'));

            return $query->result_array();
        }
        else if($user_id && $acc_no){

            $sql_account = 'SELECT [PAFUPINBS_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName]
		             FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					 INNER JOIN [PAFUPINBS].[dbo].[branches]
					 ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [PAFUPINBS].[dbo].[account_card]
					 ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
					 INNER JOIN [PAFUPINBS].[dbo].[account_status]
                     ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                     INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                     ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
					 WHERE [PAFUPINBS_account].[opened_by]=? AND [PAFUPINBS_account].[acc_no]=? AND [account_status].[acc_status]=?';

            $query = $this->db->query($sql_account,array($user_id,$acc_no,'carded'));

            return $query->result_array();
        }
        else if(!$user_id && $acc_no){

            $sql_account = 'SELECT [PAFUPINBS_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName]
		             FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					 INNER JOIN [PAFUPINBS].[dbo].[branches]
					 ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [PAFUPINBS].[dbo].[account_card]
					 ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
					 INNER JOIN [PAFUPINBS].[dbo].[account_status]
                     ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                     INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                     ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
					 WHERE [PAFUPINBS_account].[acc_no]=? AND [account_status].[acc_status]=?';

            $query = $this->db->query($sql_account,array($acc_no,'carded'));

            return $query->result_array();
        }
        else{

            $sql_account = 'SELECT [PAFUPINBS_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName]
		             FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					 INNER JOIN [PAFUPINBS].[dbo].[branches]
					 ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [PAFUPINBS].[dbo].[account_card]
					 ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
					 INNER JOIN [PAFUPINBS].[dbo].[account_status]
                     ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                     INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                     ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
					 WHERE [account_status].[acc_status]=?
					 ';

            $query = $this->db->query($sql_account,'carded');

            return $query->result_array();
        }
    }
    /*get accounts from prelinkedy paginate */
    public function get_all_carded_accounts($limit,$start,$username=false)
    {
        //get the username
        if(!$username){
            $sql = "SELECT * FROM (
                                      SELECT row_number() OVER (ORDER BY [PAFUPINBS_account].acc_no) AS rownum,[PAFUPINBS_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName],[referal_no],[book_no]
                                         FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
                                         INNER JOIN [PAFUPINBS].[dbo].[branches]
                                         ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_card]
                                         ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [PAFUPINBS].[dbo].[booklet]
                                         ON [PAFUPINBS_account].[acc_no] = [booklet].[acc_no]
                                         INNER JOIN [PAFUPINBS].[dbo].[referal_book]
                                         ON [PAFUPINBS_account].[acc_no] = [referal_book].[acc_no]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_status]
                                         ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                                         INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                                         ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
                                            WHERE [account_status].[acc_status]=?



                           ) AS A

                           WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1
                           ";


            $result = $this->db->query($sql,array('carded'));
        }
        else{
            $sql = "SELECT * FROM (
                                      SELECT row_number() OVER (ORDER BY [PAFUPINBS_account].acc_no) AS rownum,[PAFUPINBS_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName]
                                         FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
                                         INNER JOIN [PAFUPINBS].[dbo].[branches]
                                         ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_card]
                                         ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_status]
                                         ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                                         INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                                         ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
                                            WHERE [account_status].[acc_status]=? AND [UserProfile].[Username]=?



                           ) AS A

                           WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1
                           ";


            $result = $this->db->query($sql,array('carded',$username));
        }
        //return the query result
        return $result->result_array();

    }
    /*get accounts from prelinked paginate */
    public function get_all_prelinked_accounts($limit,$start,$username=false)
    {
        //get the username
        if(!$username){
            $sql = "SELECT * FROM (
                                      SELECT row_number() OVER (ORDER BY [PAFUPINBS_account].acc_no) AS rownum,[PAFUPINBS_account].*,[branches].[branch_name],[Username],[FullName]
                                            FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
                                            INNER JOIN [PAFUPINBS].[dbo].[branches]
                                            ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
                                            INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                                            ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
                                            INNER JOIN [PAFUPINBS].[dbo].[account_status]
                                            ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                                            WHERE [account_status].[acc_status]=?



                           ) AS A

                           WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1
                           ";


            $result = $this->db->query($sql,array('precarded'));
        }
        else{
            $sql = "SELECT * FROM (
                                      SELECT row_number() OVER (ORDER BY [PAFUPINBS_account].acc_no) AS rownum,[PAFUPINBS_account].*,[branches].[branch_name],[Username]
                                            FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
                                            INNER JOIN [PAFUPINBS].[dbo].[branches]
                                            ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
                                            INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                                            ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
                                            INNER JOIN [PAFUPINBS].[dbo].[account_status]
                                            ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                                            WHERE [account_status].[acc_status]=? AND [UserProfile].[Username]=?



                           ) AS A

                           WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1
                           ";


            $result = $this->db->query($sql,array('precarded',$username));
        }
        //return the query result
        return $result->result_array();

    }
    /*get accounts from locked paginate */
    public function get_all_locked_accounts($limit,$start,$username=false)
    {
        //get the username
        if(!$username){
            $sql = "SELECT * FROM (
                                      SELECT row_number() OVER (ORDER BY [PAFUPINBS_account].acc_no) AS rownum,[PAFUPINBS_account].*,[branches].[branch_name],[Username],[FullName]
                                            FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
                                            INNER JOIN [PAFUPINBS].[dbo].[branches]
                                            ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
                                            INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                                            ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
                                            INNER JOIN [PAFUPINBS].[dbo].[account_status]
                                            ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                                            WHERE [account_status].[lock_status]=?



                           ) AS A

                           WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1
                           ";


            $result = $this->db->query($sql,true);
        }
        else{
            $sql = "SELECT * FROM (
                                      SELECT row_number() OVER (ORDER BY [PAFUPINBS_account].acc_no) AS rownum,[PAFUPINBS_account].*,[branches].[branch_name],[Username]
                                            FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
                                            INNER JOIN [PAFUPINBS].[dbo].[branches]
                                            ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
                                            INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                                            ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
                                            INNER JOIN [PAFUPINBS].[dbo].[account_status]
                                            ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                                            WHERE [account_status].[lock_status]=? AND [UserProfile].[Username]=?



                           ) AS A

                           WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1
                           ";


            $result = $this->db->query($sql,array(true,$username));
        }
        //return the query result
        return $result->result_array();

    }
    /*get accounts from non locked paginate */
    public function get_all_nonlocked_accounts($limit,$start,$username=false)
    {
        //get the username
        if(!$username){
            $sql = "SELECT * FROM (
                                      SELECT row_number() OVER (ORDER BY [PAFUPINBS_account].acc_no) AS rownum,[PAFUPINBS_account].*,[branches].[branch_name],[Username],[FullName]
                                            FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
                                            INNER JOIN [PAFUPINBS].[dbo].[branches]
                                            ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
                                            INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                                            ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
                                            INNER JOIN [PAFUPINBS].[dbo].[account_status]
                                            ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                                            WHERE [account_status].[lock_status]=?



                           ) AS A

                           WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1
                           ";


            $result = $this->db->query($sql,array(false));
        }
        else{
            $sql = "SELECT * FROM (
                                      SELECT row_number() OVER (ORDER BY [PAFUPINBS_account].acc_no) AS rownum,[PAFUPINBS_account].*,[branches].[branch_name],[Username]
                                            FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
                                            INNER JOIN [PAFUPINBS].[dbo].[branches]
                                            ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
                                            INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                                            ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
                                            INNER JOIN [PAFUPINBS].[dbo].[account_status]
                                            ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                                            WHERE [account_status].[lock_status]=? AND [UserProfile].[Username]=?



                           ) AS A

                           WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1
                           ";


            $result = $this->db->query($sql,array(false,$username));
        }
        //return the query result
        return $result->result_array();

    }

    /*get accounts from starterpack paginate */
    public function get_all_starterpack_accounts($limit,$start,$username=false)
    {

        if(!$username){
            $sql = "SELECT * FROM (
                                  SELECT row_number() OVER (ORDER BY [PAFUPINBS_account].acc_no) AS rownum,[PAFUPINBS_account].*,[branches].[branch_name],[Username]
                                        FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
                                        INNER JOIN [PAFUPINBS].[dbo].[branches]
                                        ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
                                        INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                                        ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
                                        INNER JOIN [PAFUPINBS].[dbo].[account_status]
                                        ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                                        WHERE [account_status].[acc_status]=?



                       ) AS A

                       WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1
                       ";


            $result = $this->db->query($sql,array('starterpack'));
        }
        else{
            $sql = "SELECT * FROM (
                          SELECT row_number() OVER (ORDER BY [PAFUPINBS_account].acc_no) AS rownum,[PAFUPINBS_account].*,[branches].[branch_name],[Username]
                                FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					            INNER JOIN [PAFUPINBS].[dbo].[branches]
					            ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					            INNER JOIN [PAFUPINBS].dbo.[UserProfile]
					            ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
					            INNER JOIN [PAFUPINBS].[dbo].[account_status]
					            ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
					            WHERE [account_status].[acc_status]=? AND [UserProfile].[Username] = ?



			   ) AS A

			   WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1
			   ";


            $result = $this->db->query($sql,array('starterpack',$username));

        }
        //return the query result
        return $result->result_array();

    }
    /*get accounts all dispatched accounts for paginating */
    public function get_all_dispatched_accounts($limit,$start,$username=false){
        //get the username
        //$username = $this->session->userdata('username');

        if(!$username){
            $sql = "SELECT * FROM (
                          SELECT row_number() OVER (ORDER BY [PAFUPINBS_account].acc_no) AS rownum,[PAFUPINBS_account].*,[branches].[branch_name],[Username],[FullName],[dispatched_by],[AOC],[card_no]
                                FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					            INNER JOIN [PAFUPINBS].[dbo].[branches]
					            ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					            INNER JOIN [PAFUPINBS].dbo.[UserProfile]
					            ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
					            INNER JOIN [PAFUPINBS].[dbo].[account_status]
					            ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
					            INNER JOIN [PAFUPINBS].[dbo].[account_card]
					            ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
					            INNER JOIN [PAFUPINBS].[dbo].[account_mobility]
					            ON [PAFUPINBS_account].[acc_no] = [account_mobility].[acc_no]




			   ) AS A

			   WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1
			   ";


            $result = $this->db->query($sql);
        }
        else{
            $sql = "SELECT * FROM (
                          SELECT row_number() OVER (ORDER BY [PAFUPINBS_account].acc_no) AS rownum,[PAFUPINBS_account].*,[branches].[branch_name],[Username],[dispatched_by],[AOC]
                                FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					            INNER JOIN [PAFUPINBS].[dbo].[branches]
					            ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					            INNER JOIN [PAFUPINBS].dbo.[UserProfile]
					            ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
					            INNER JOIN [PAFUPINBS].[dbo].[account_status]
					            ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
					            INNER JOIN [PAFUPINBS].[dbo].[account_mobility]
					            ON [PAFUPINBS_account].[acc_no] = [account_mobility].[acc_no]
                                WHERE [UserProfile].[Username] =?



			   ) AS A

			   WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1
			   ";


            $result = $this->db->query($sql,array($username));

        }
        //return the query result
        return $result->result_array();

    }
    /*get accounts all dispatched accounts for paginating */
    public function get_all_nondispatched_accounts($limit,$start,$username=false){
        //get the username
        //$username = $this->session->userdata('username');

        if(!$username){
            $sql = "SELECT * FROM (
                          SELECT row_number() OVER (ORDER BY [PAFUPINBS_account].acc_no) AS rownum,[PAFUPINBS_account].*,[branches].[branch_name],[Username],[FullName],[card_no]
                                FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					            INNER JOIN [PAFUPINBS].[dbo].[branches]
					            ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					            INNER JOIN [PAFUPINBS].dbo.[UserProfile]
					            ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
					            INNER JOIN [PAFUPINBS].[dbo].[account_status]
					            ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
					             INNER JOIN [PAFUPINBS].[dbo].[account_card]
					            ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
					            WHERE [account_status].[dispatch_flag]=?




			   ) AS A

			   WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1
			   ";


            $result = $this->db->query($sql,array(false));
        }
        else{
            $sql = "SELECT * FROM (
                          SELECT row_number() OVER (ORDER BY [PAFUPINBS_account].acc_no) AS rownum,[PAFUPINBS_account].*,[branches].[branch_name],[Username]
                                FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					            INNER JOIN [PAFUPINBS].[dbo].[branches]
					            ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					            INNER JOIN [PAFUPINBS].dbo.[UserProfile]
					            ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
					            INNER JOIN [PAFUPINBS].[dbo].[account_status]
					            ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                                WHERE [UserProfile].[Username] =? AND [account_status].[dispatch_flag]=?



			   ) AS A

			   WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1
			   ";


            $result = $this->db->query($sql,array($username,false));

        }
        //return the query result
        return $result->result_array();

    }
    /* get accounts paginate */
    public function get_all_accounts($limit,$start,$username=false)
    {

        if($username){
            $sql = "SELECT * FROM (
                          SELECT row_number() OVER (ORDER BY acc_no) AS rownum,[PAFUPINBS_account].*,[branches].[branch_name],[Username]
                                FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					            INNER JOIN [PAFUPINBS].[dbo].[branches]
					            ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					            INNER JOIN [PAFUPINBS].dbo.[UserProfile]
					            ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]



			   ) AS A

			   WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1
			   ";


            $result = $this->db->query($sql);
        }
        else{
            $sql = "SELECT * FROM (
                          SELECT row_number() OVER (ORDER BY acc_no) AS rownum,[PAFUPINBS_account].*,[branches].[branch_name],[Username]
                                FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					            INNER JOIN [PAFUPINBS].[dbo].[branches]
					            ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					            INNER JOIN [PAFUPINBS].dbo.[UserProfile]
					            ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
					            WHERE [UserProfile].[Username]= ?



			   ) AS A

			   WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1
			   ";


            $result = $this->db->query($sql,array($username));
        }
        //return the query result
        return $result->result_array();

    }
    /*
     * gets preopened accounts
     */
    public function getPreAccount($user_id=false,$acc_no=false){
        if($user_id && !$acc_no){

            $sql_account = 'SELECT [PAFUPINBS_account].*,[branches].[branch_name],[FullName],[referal_no],[book_no]
		             FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					 INNER JOIN [PAFUPINBS].[dbo].[branches]
					 ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [PAFUPINBS].[dbo].[account_status]
					 ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
					 INNER JOIN [PAFUPINBS].[dbo].[referal_book]
					 ON [PAFUPINBS_account].[acc_no] = [referal_book].[acc_no]
					 INNER JOIN [PAFUPINBS].[dbo].[booklet]
					 ON [PAFUPINBS_account].[acc_no] = [booklet].[acc_no]
					 INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                     ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
					 WHERE [PAFUPINBS_account].[opened_by]=? AND [account_status].[acc_status]=?';

            $query = $this->db->query($sql_account,array($user_id,'precarded'));

            return $query->result_array();
        }
        else if($user_id && $acc_no){

            $sql_account = 'SELECT [PAFUPINBS_account].*,[branches].[branch_name]
		             FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					 INNER JOIN [PAFUPINBS].[dbo].[branches]
					 ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [PAFUPINBS].[dbo].[account_status]
					 ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
					 WHERE [PAFUPINBS_account].[opened_by]=$user_id AND [PAFUPINBS_account].[acc_no]=? AND [account_status].[acc_status]=?';

            $query = $this->db->query($sql_account,array($user_id,$acc_no,'precarded'));

            return $query->result_array();
        }
        else if(!$user_id && $acc_no){

            $sql_account = 'SELECT [PAFUPINBS_account].*,[branches].[branch_name]
		             FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					 INNER JOIN [PAFUPINBS].[dbo].[branches]
					 ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [PAFUPINBS].[dbo].[account_status]
					 ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
					 WHERE [PAFUPINBS_account].[acc_no]=? AND AND [account_status].[acc_status]=?';

            $query = $this->db->query($sql_account,array($acc_no,'precarded'));

            return $query->result_array();
        }
        else{

            $sql_account = 'SELECT [PAFUPINBS_account].*,[branches].[branch_name]
		             FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					 INNER JOIN [PAFUPINBS].[dbo].[branches]
					 ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [PAFUPINBS].[dbo].[account_status]
					 ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]';

            $query = $this->db->query($sql_account);

            return $query->result_array();
        }

    }
    public function getLockedAccounts($user_id=false,$acc_no=false){
        if($user_id && !$acc_no){

            $sql_account = 'SELECT [PAFUPINBS_account].*,[branches].[branch_name],[acc_status]
		             FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					 INNER JOIN [PAFUPINBS].[dbo].[branches]
					 ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [PAFUPINBS].[dbo].[account_status]
					 ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
					 WHERE [PAFUPINBS_account].[opened_by]=? AND  [account_status].[lock_status]=? AND [account_status].[acc_status]=?';

            $query = $this->db->query($sql_account,array($user_id,true,'starterpack'));

            return $query->result_array();
        }
        if($user_id && $acc_no){

            $sql_account = 'SELECT [PAFUPINBS_account].*,[branches].[branch_name],[acc_status]
		             FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					 INNER JOIN [PAFUPINBS].[dbo].[branches]
					 ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [PAFUPINBS].[dbo].[account_status]
					 ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
					 WHERE [PAFUPINBS_account].[opened_by]=? AND [PAFUPINBS_account].[acc_no] = ?  AND [account_status].[lock_status]=? AND [account_status].[acc_status]=?';

            $query = $this->db->query($sql_account,array($user_id,$acc_no,true,'starterpack'));

            return $query->result_array();
        }
        elseif(!$user_id && $acc_no){
            $sql_account = 'SELECT [PAFUPINBS_account].*,[branches].[branch_name],[acc_status]
		             FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					 INNER JOIN [PAFUPINBS].[dbo].[branches]
					 ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [PAFUPINBS].[dbo].[account_status]
					 ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
					 WHERE [PAFUPINBS_account].[acc_no] = ? AND [account_status].[lock_status]=? AND [account_status].[acc_status]=?';
            $query = $this->db->query($sql_account,array($acc_no,true,'starterpack'));

            return $query->result_array();
        }
        else{
            $sql_account = 'SELECT [PAFUPINBS_account].*,[branches].[branch_name],[acc_status]
		             FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					 INNER JOIN [PAFUPINBS].[dbo].[branches]
					 ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [PAFUPINBS].[dbo].[account_status]
					 ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
					 WHERE [account_status].[lock_status]=? AND [account_status].[acc_status]=?';
            $query = $this->db->query($sql_account,array(true,'starterpack'));

            return $query->result_array();
        }
    }
    public function getNonLockedAccounts($user_id=false,$acc_no=false){
        if($user_id && !$acc_no){

            $sql_account = 'SELECT [PAFUPINBS_account].*,[branches].[branch_name]
		             FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					 INNER JOIN [PAFUPINBS].[dbo].[branches]
					 ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [PAFUPINBS].[dbo].[account_status]
					 ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
					 WHERE [PAFUPINBS_account].[opened_by]=? AND  [account_status].[lock_status]=? AND [account_status].[acc_status]=?';

            $query = $this->db->query($sql_account,array($user_id,false,'starterpack'));

            return $query->result_array();
        }
        if($user_id && $acc_no){

            $sql_account = 'SELECT [PAFUPINBS_account].*,[branches].[branch_name]
		             FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					 INNER JOIN [PAFUPINBS].[dbo].[branches]
					 ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [PAFUPINBS].[dbo].[account_status]
					 ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
					 WHERE [PAFUPINBS_account].[opened_by]=? AND [PAFUPINBS_account].[acc_no] = ?  AND [account_status].[lock_status]=? AND [account_status].[acc_status]=?';

            $query = $this->db->query($sql_account,array($user_id,$acc_no,false,'starterpack'));

            return $query->result_array();
        }
        elseif(!$user_id && $acc_no){
            $sql_account = 'SELECT [PAFUPINBS_account].*,[branches].[branch_name]
		             FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					 INNER JOIN [PAFUPINBS].[dbo].[branches]
					 ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [PAFUPINBS].[dbo].[account_status]
					 ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
					 WHERE [PAFUPINBS_account].[acc_no] = ? AND [account_status].[lock_status]=? AND [account_status].[acc_status]=?';
            $query = $this->db->query($sql_account,array($acc_no,false,'starterpack'));

            return $query->result_array();
        }
        else{
            $sql_account = 'SELECT [PAFUPINBS_account].*,[branches].[branch_name]
		             FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					 INNER JOIN [PAFUPINBS].[dbo].[branches]
					 ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [PAFUPINBS].[dbo].[account_status]
					 ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
					 WHERE [account_status].[lock_status]=? AND [account_status].[acc_status]=?';
            $query = $this->db->query($sql_account,array(false,'starterpack'));

            return $query->result_array();
        }
    }
    public function getDispatchedAccounts($user_id=false,$acc_no=false){
        if($user_id && !$acc_no){

            $sql_account = 'SELECT [PAFUPINBS_account].*,[branches].[branch_name],[dispatched_by],[acc_status]
		             FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					 INNER JOIN [PAFUPINBS].[dbo].[branches]
					 ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [PAFUPINBS].[dbo].[account_status]
					 ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
					 INNER JOIN [PAFUPINBS].[dbo].[account_mobility]
					 ON [PAFUPINBS_account].[acc_no] = [account_mobility].[acc_no]
					 WHERE [PAFUPINBS_account].[opened_by]=? AND  [account_status].[dispatch_flag]=?';

            $query = $this->db->query($sql_account,array($user_id,true));

            return $query->result_array();
        }
        if($user_id && $acc_no){

            $sql_account = 'SELECT [PAFUPINBS_account].*,[branches].[branch_name],[dispatched_by],[acc_status]
		             FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					 INNER JOIN [PAFUPINBS].[dbo].[branches]
					 ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [PAFUPINBS].[dbo].[account_status]
					 ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
					  INNER JOIN [PAFUPINBS].[dbo].[account_mobility]
					 ON [PAFUPINBS_account].[acc_no] = [account_mobility].[acc_no]
					 WHERE [PAFUPINBS_account].[opened_by]=? AND [PAFUPINBS_account].[acc_no] = ?  AND [account_status].[dispatch_flag]=?';

            $query = $this->db->query($sql_account,array($user_id,$acc_no,true));

            return $query->result_array();
        }
        elseif(!$user_id && $acc_no){
            $sql_account = 'SELECT [PAFUPINBS_account].*,[branches].[branch_name],[dispatched_by],[acc_status]
		             FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					 INNER JOIN [PAFUPINBS].[dbo].[branches]
					 ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [PAFUPINBS].[dbo].[account_status]
					 ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
					  INNER JOIN [PAFUPINBS].[dbo].[account_mobility]
					 ON [PAFUPINBS_account].[acc_no] = [account_mobility].[acc_no]
					 WHERE [PAFUPINBS_account].[acc_no] = ? AND [account_status].[dispatch_flag]=?';
            $query = $this->db->query($sql_account,array($acc_no,true));

            return $query->result_array();
        }
        else{
            $sql_account = 'SELECT [PAFUPINBS_account].*,[branches].[branch_name],[dispatched_by],[acc_status]
		             FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					 INNER JOIN [PAFUPINBS].[dbo].[branches]
					 ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [PAFUPINBS].[dbo].[account_status]
					 ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
					  INNER JOIN [PAFUPINBS].[dbo].[account_mobility]
					 ON [PAFUPINBS_account].[acc_no] = [account_mobility].[acc_no]
					 WHERE [account_status].[dispatch_flag]=?';
            $query = $this->db->query($sql_account,array(true));

            return $query->result_array();
        }
    }
    public function getNotDispatchedAccounts($user_id=false,$acc_no=false){
        if($user_id && !$acc_no){

            $sql_account = 'SELECT [PAFUPINBS_account].*,[branches].[branch_name],[card_no]
		             FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					 INNER JOIN [PAFUPINBS].[dbo].[branches]
					 ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [PAFUPINBS].[dbo].[account_status]
					 ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
					 INNER JOIN [PAFUPINBS].[dbo].[account_card]
					 ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
					 WHERE [PAFUPINBS_account].[opened_by]=? AND [account_status].[dispatch_flag]=?';

            $query = $this->db->query($sql_account,array($user_id,false));

            return $query->result_array();
        }
        if($user_id && $acc_no){

            $sql_account = 'SELECT [PAFUPINBS_account].*,[branches].[branch_name]
		             FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					 INNER JOIN [PAFUPINBS].[dbo].[branches]
					 ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [PAFUPINBS].[dbo].[account_status]
					 ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
					 INNER JOIN [PAFUPINBS].[dbo].[account_card]
					 ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
					 WHERE [PAFUPINBS_account].[opened_by]=? AND [PAFUPINBS_account].[acc_no] = ?  AND [account_status].[dispatch_flag]=?';

            $query = $this->db->query($sql_account,array($user_id,$acc_no,false));

            return $query->result_array();
        }
        elseif(!$user_id && $acc_no){
            $sql_account = 'SELECT [PAFUPINBS_account].*,[branches].[branch_name]
		             FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					 INNER JOIN [PAFUPINBS].[dbo].[branches]
					 ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [PAFUPINBS].[dbo].[account_status]
					 ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
					 INNER JOIN [PAFUPINBS].[dbo].[account_card]
					 ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
					 WHERE [PAFUPINBS_account].[acc_no] = ? AND [account_status].[dispatch_flag]=?';
            $query = $this->db->query($sql_account,array($acc_no,false));

            return $query->result_array();
        }
        else{
            $sql_account = 'SELECT [PAFUPINBS_account].*,[branches].[branch_name]
		             FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					 INNER JOIN [PAFUPINBS].[dbo].[branches]
					 ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [PAFUPINBS].[dbo].[account_status]
					 ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
					 INNER JOIN [PAFUPINBS].[dbo].[account_card]
					 ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
					 WHERE [account_status].[dispatch_flag]=?';
            $query = $this->db->query($sql_account,array(false));

            return $query->result_array();
        }
    }
    public function getAccountsWithAOC($user_id=false,$acc_no=false){
        if($user_id && !$acc_no){

            $sql_account = 'SELECT [PAFUPINBS_account].*,[branches].[branch_name],[dispatched_by],[AOC],[acc_status]
		             FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					 INNER JOIN [PAFUPINBS].[dbo].[branches]
					 ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [PAFUPINBS].[dbo].[account_status]
					 ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
					 INNER JOIN [PAFUPINBS].[dbo].[account_mobility]
					 ON [PAFUPINBS_account].[acc_no] = [account_mobility].[acc_no]
					 WHERE [PAFUPINBS_account].[opened_by]=? AND  [account_mobility].[AOC] !=?';

            $query = $this->db->query($sql_account,array($user_id,NULL));
        }
        if($user_id && $acc_no){

            $sql_account = 'SELECT [PAFUPINBS_account].*,[branches].[branch_name],[dispatched_by],[AOC],[acc_status]
		             FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					 INNER JOIN [PAFUPINBS].[dbo].[branches]
					 ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [PAFUPINBS].[dbo].[account_status]
					 ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
					  INNER JOIN [PAFUPINBS].[dbo].[account_mobility]
					 ON [PAFUPINBS_account].[acc_no] = [account_mobility].[acc_no]
					 WHERE [PAFUPINBS_account].[opened_by]=? AND [PAFUPINBS_account].[acc_no] = ?  AND [account_mobility].[AOC] !=?';

            $query = $this->db->query($sql_account,array($user_id,$acc_no,true));

            return $query->result_array();
        }
        elseif(!$user_id && $acc_no){
            $sql_account = 'SELECT [PAFUPINBS_account].*,[branches].[branch_name],[dispatched_by],[AOC],[acc_status]
		             FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					 INNER JOIN [PAFUPINBS].[dbo].[branches]
					 ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [PAFUPINBS].[dbo].[account_status]
					 ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
					  INNER JOIN [PAFUPINBS].[dbo].[account_mobility]
					 ON [PAFUPINBS_account].[acc_no] = [account_mobility].[acc_no]
					 WHERE [PAFUPINBS_account].[acc_no] = ? AND [account_mobility].[AOC] !=?';
            $query = $this->db->query($sql_account,array($acc_no,NULL));

            return $query->result_array();
        }
        else{
            $sql_account = 'SELECT [PAFUPINBS_account].*,[branches].[branch_name],[dispatched_by],[AOC],[acc_status]
		             FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					 INNER JOIN [PAFUPINBS].[dbo].[branches]
					 ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [PAFUPINBS].[dbo].[account_status]
					 ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
					  INNER JOIN [PAFUPINBS].[dbo].[account_mobility]
					 ON [PAFUPINBS_account].[acc_no] = [account_mobility].[acc_no]
					 WHERE [account_mobility].[AOC] !=?';
            $query = $this->db->query($sql_account,array(NULL));

            return $query->result_array();
        }
    }
    public function getBookPins($book_no=false){
        if($book_no){
            $sql_account = 'SELECT *
		                    FROM [PAFUPINBS].[dbo].[trans_pins]
		                    WHERE [book_no] =?';

            $query = $this->db->query($sql_account,array($book_no));
            return $query->result_array();
        }
        else{

            $sql_account = 'SELECT *
		             FROM [PAFUPINBS].[dbo].[trans_pins]';

            $query = $this->db->query($sql_account);
            return $query->result_array();
        }
    }
    public function saveUploadData($data){

        $sql_upload = 'INSERT INTO [PAFUPINBS].[dbo].[UploadData]
		                    ([upload_name],[uploaded_by],[error_type])
							 VALUES(?,?,?)';
        $result=$this->db->query($sql_upload,array($data['upload_name'],$data['uploaded_by'],$data['error_type']));
        if($result){
            return true;
        }
        else
            return false;
    }
    public function makeCoupon($acc_no,$cust_id){
        $errors = array();
        $username = $this->session->userdata('username');
        $referal_no = $cust_id.rand(0,9).rand(0,9).rand(0,9).rand(0,9);
        $data = array('referal_no' => $referal_no,'acc_no' => $acc_no);
        $sql_upload = 'INSERT INTO [PAFUPINBS].[dbo].[referal_book]
		                    ([referal_no],[acc_no])
							 VALUES(?,?)';
        $result=$this->db->query($sql_upload,array($referal_no,$acc_no));
        if($result){
            $this->log_activity('Referal creation', $username,$acc_no);
            array_push($errors,array('booklet' => 'coupon number '.$referal_no.' created for '.$acc_no));
            $error_pins = array();
            $num = 0;
            for($i=0;$i<10;$i++){

                $sql_upload = 'INSERT INTO [PAFUPINBS].[dbo].[referal_coupons]
		                    ([referal_no],[coupon_no])
							 VALUES(?,?)';
                $done=$this->db->query($sql_upload,array($referal_no,$this->generatePin()));
                if($done){
                    $num++;
                }

            }
            if($num < 10){
                $num2 = $num;
                for($i=0;$i<(10-$num2);$i++){

                    $sql_upload = 'INSERT INTO [PAFUPINBS].[dbo].[referal_coupon]
		                    ([referal_no],[referal])
							 VALUES(?,?)';
                    $done=$this->db->query($sql_upload,array($referal_no,$this->generatePin()));
                    if($done){

                        $num ++;
                    }

                }
            }
            array_push($error_pins,$num." coupons created for referal ".$referal_no." for ".$acc_no);
            array_push($errors,array('pins' => $error_pins));

        }
        else{
            array_push($errors,array('referal' => 'referal number '.$referal_no.' NOT created'));
        }
        return $errors;
    }
    public function makeBooklet($acc_no,$cust_id){
        $errors = array();
        $username = $this->session->userdata('username');
        $book_no = $cust_id.rand(0,9).rand(0,9).rand(0,9).rand(0,9);
        $data = array('book_no' => $book_no,'acc_no' => $acc_no);
        $sql_upload = 'INSERT INTO [PAFUPINBS].[dbo].[booklet]
		                    ([book_no],[acc_no])
							 VALUES(?,?)';
        $result=$this->db->query($sql_upload,array($book_no,$acc_no));
        if($result){
            $this->log_activity('Booklet creation', $username,$acc_no);
            array_push($errors,array('booklet' => 'booklet number '.$book_no.' created for '.$acc_no));
            $error_pins = array();
            $num = 0;
            for($i=0;$i<50;$i++){

                $sql_upload = 'INSERT INTO [PAFUPINBS].[dbo].[trans_pins]
		                    ([book_no],[trans_pin])
							 VALUES(?,?)';
                $done=$this->db->query($sql_upload,array($book_no,$this->generatePin()));
                if($done){
                    $num++;
                }

            }
            if($num < 50){
                $num2 = $num;
                for($i=0;$i<(50-$num2);$i++){

                    $sql_upload = 'INSERT INTO [PAFUPINBS].[dbo].[trans_pins]
		                    ([book_no],[trans_pin])
							 VALUES(?,?)';
                    $done=$this->db->query($sql_upload,array($book_no,$this->generatePin()));
                    if($done){

                        $num ++;
                    }

                }
            }
            array_push($error_pins,$num." pins created for booklet ".$book_no." for ".$acc_no);
            array_push($errors,array('pins' => $error_pins));

        }
        else{
            array_push($errors,array('booklet' => 'booklet number '.$book_no.' NOT created'));
        }
        return $errors;
    }
    public function checkAccount($data){
        $sql = 'SELECT [PAFUPINBS_account].[acc_no]
		             FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					 INNER JOIN [PAFUPINBS].[dbo].[branches]
					 ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [PAFUPINBS].[dbo].[account_card]
					 ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
					 INNER JOIN [PAFUPINBS].[dbo].[booklet]
                     ON [PAFUPINBS_account].[acc_no] = [booklet].[acc_no]
                     INNER JOIN [PAFUPINBS].[dbo].[referal_book]
                     ON [PAFUPINBS_account].[acc_no] = [referal_book].[acc_no]
					 INNER JOIN [PAFUPINBS].[dbo].[account_status]
                     ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                     INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                     ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
					 WHERE [PAFUPINBS_account].[acc_no]=? AND [PAFUPINBS_account].[branch_id]=? AND [booklet].[book_no]=? AND [referal_book]=? AND [account_card].[card_no] =?';
        $result = $this->db->query($sql,array($data['acc_no'],$data['branch_id'],$data['book_no'],$data['referal_no'],$data['card_no']))->result_array();
        $count = count($result);
        if($count > 0){
            return true;
        }
        else{
            return false;
        }
    }
    /*
     * update uccounts with starterpack IDs
     */
    public function saveStarterIds($data){
        $errors = array();
        $username = $this->session->userdata('username');
        $today_date = Common_Tasks::generate_date();
        foreach($data as $d){
            if($this->accountExists($d['acc_no']) && $this->checkAccount($data)){

                $where = "acc_no =".$d['acc_no'];
                $acc_no =$d['acc_no'];
                unset($d['acc_no']);

                $sql_lock = 'UPDATE [PAFUPINBS].[dbo].[account_card]
						 SET [starter_id] = ?,[starter_date]=?
			             WHERE [acc_no] = ?';

                $done =$this->db->query($sql_lock,array($d['starter_id'],$today_date,$acc_no));
                if(!$done)
                    array_push($errors,"card number not saved for account no ".$acc_no);
                else{
                    $sql_lock = 'UPDATE [PAFUPINBS].[dbo].[account_status]
						 SET [acc_status] = ?
			             WHERE [acc_no] = ?';

                    $done =$this->db->query($sql_lock,array('starterpack',$acc_no));
                    array_push($errors,"card number successfully saved for account no ".$acc_no);
                    $this->log_activity('ATM card number successfully saved.Starterpack ID made', $username,$acc_no);

                }
            }
            else{
                array_push($errors,"account no ".$acc_no." does not exist or data doesnt match account");
            }

        }
        return $errors;
    }
    public function cardNumberExists($card_no){
        $sql_select = 'SELECT COUNT([card_no]) as total
		             FROM [PAFUPINBS].[dbo].[account_card]
					 WHERE [card_no] = ?';

        $query = $this->db->query($sql_select,array($card_no));
        foreach($query->result() as $row){
            if($row->total >=1){
                return true;
            }
            else{
                return false;
            }
        }

    }

    /*
     * update accounts with data from Fincard
     */
    public function saveCardNumbers($data){
        $errors = array();
        $username = $this->session->userdata('username');
        $today_date = Common_Tasks::generate_date();
        foreach($data as $d){
            if($this->accountExists($d['acc_no'])){

                //$d['starter_id'] = $this->generateStarterId();
                // $sql_create_user = 'INSERT INTO [PAFUPINBS].[dbo].[account_card]
                // ([card_no],[acc_no],[starter_id])
                //VALUES(?,?,?)';
                if(!$this->cardNumberExists($d['card_no'])){
                    $sql_create_user = 'INSERT INTO [PAFUPINBS].[dbo].[account_card]
		                    ([card_no],[acc_no],[card_date])
							 VALUES(?,?,?)';


                    $done = $this->db->query($sql_create_user,array($d['card_no'],$d['acc_no'],$today_date));
                    if(!$done)
                        array_push($errors,"card number not saved for account no ".$d['acc_no']);
                    else{
                        $this->log_activity('card number successfully saved', $username,$d['acc_no']);
                        array_push($errors,"card number successfully saved for account no ".$d['acc_no']);
                        $sql_update = 'UPDATE [PAFUPINBS].[dbo].[account_status]
                                   SET [acc_status] = ?
                                   WHERE [account_status].[acc_no]=?';
                        $done = $this->db->query($sql_update,array('carded',$d['acc_no']));

                    }
                }
                else{
                    array_push($errors,"card number already there for account no ".$d['acc_no']);
                }
            }


            else{
                array_push($errors,"account no ".$d['acc_no']." does not exist");
            }

        }
        array_push($errors,"no data was passed for saving");
        return $errors;
    }
    public function removeAccount($data){
        $done = array();
        $username =  $this->session->userdata('username');
        $fullname =  $this->session->userdata('fullname');
        for($i=0;$i<count($data);$i++){
            $acc_no = $data[$i];
            $sql_delete = 'DELETE FROM [PAFUPINBS].[dbo].[PAFUPINBS_account] ';
            $sql_delete.= 'WHERE [acc_no] = ? ';

            $deleted = $this->db->query($sql_delete,array($acc_no));
            if($deleted){
                array_push($done,'account no '.$data[$i].' deleted');
                $this->log_activity('Account deleted by '.$fullname, $username,$data[$i]);
            }
            else{
                array_push($done,'account no '.$data[$i].' deletion failed');
                $this->log_activity('Account deletion failed. Action by '.$fullname, $username,$data[$i]);
            }
        }
        return $done;
    }
    public function regeneratePins($data){
        $book_no =false;

        for($i=0;$i<count($data);$i++){
            $acc_no = $data[$i];
            $sql_select = 'SELECT [book_no]
		             FROM [PAFUPINBS].[dbo].[booklet]
					 WHERE [acc_no] = ?';

            $query = $this->db->query($sql_select,array($acc_no));

            if($query->num_rows >= 1){
                foreach($query->result() as $row){

                    $book_no =  $row->book_no;


                }
            }
            else{
                return false;
            }
            if($book_no){

                $sql_delete = 'DELETE FROM [PAFUPINBS].[dbo].[trans_pins] ';
                $sql_delete.= 'WHERE [book_no] = ? ';

                $deleted = $this->db->query($sql_delete,array($book_no));
                if(true || $deleted ){
                    $num = 0;
                    for($i=0;$i<50;$i++){

                        $sql_upload = 'INSERT INTO [PAFUPINBS].[dbo].[trans_pins]
		                    ([book_no],[trans_pin])
							 VALUES(?,?)';
                        $done=$this->db->query($sql_upload,array($book_no,$this->generatePin()));
                        if($done){
                            $num++;
                        }

                    }
                    $username = $this->session->userdata('username');
                    $this->log_activity('Pins regerated',$username,$acc_no);
                    return $num;
                }
                else{
                    return false;
                }
            }
            else
                return false;
        }
        return false;
    }
    public function removeBooklet($book_no){

        $sql_delete = 'DELETE FROM [PAFUPINBS].[dbo].[booklet] ';
        $sql_delete.= 'WHERE [book_no] = ? ';

        $deleted = $this->db->query($sql_delete,array($book_no));
    }
    public function generatePin(){
        //$pins = array();

        $rand = rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9);

        return $rand;
    }
    function getFive($var){
        $five ="";
        $alpha = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
        if($var == 0){
            for($i=0;$i<5;$i++){
                $ran = rand(0,1);
                if($ran == 0){
                    $five .=$alpha[rand(0,25)];
                }
                else{
                    $five .= rand(0,9);
                }
            }
        }
        else{
            for($i=0;$i<5;$i++){
                $ran = rand(0,1);
                if($ran == 1){
                    $five .=$alpha[rand(0,25)];
                }
                else{
                    $five .= rand(0,9);
                }
            }
        }
        return $five;
    }
    public function starterIdExists($starter_id){
        $sql_select = 'SELECT [starter_id]
		             FROM [PAFUPINBS].[dbo].[account_card]
					 WHERE [starter_id] = ?';

        $query = $this->db->query($sql_select,array($starter_id));
        foreach($query->result() as $row){
            if($row->starter_id == $starter_id){
                return true;
            }
        }
        return false;
    }
    public function generateStarterId(){
        $starter_id = $this->getFive(rand(0,1))."-".$this->getFive(rand(0,1))."-".$this->getFive(rand(0,1))."-".$this->getFive(rand(0,1))."-".$this->getFive(rand(0,1));
        $is_there = true;
        while($is_there){
            $starter = $this->starterIdExists($starter_id);
            if(!$starter){
                $is_there = false;
                return $starter_id;
            }
        }
    }
    public function log_activity($activity, $username,$account_id) {

        //generate a date
        $today_date = Common_Tasks::generate_date();

        $activity_id = mt_rand() + round(microtime()*100);

        //get user details
        $user_details = $this->get_details($username);

        $user_details['FullName'] = Validation::sanitise_input($user_details['FullName']);

        //get the ip address
        $ip_address = Network::get_ip_address();

        $sql_activity = 'INSERT INTO [PAFUPINBS].[dbo].[AccountActivity] ';
        $sql_activity.= 'VALUES(?,?,?,?,?,?,?,?,?) ';

        $this->db->query($sql_activity,array($activity_id,$today_date,$username,
            $user_details['Branch'],$account_id,$ip_address,$user_details['FullName'],$user_details['RoleName'],$activity));

    }
    public function get_details($username)
    {

        //this method returns a user's details
        $user_details = array();
        $sql_details = 'SELECT [RoleName], [Username], [UserProfile].[RoleID], [Branch],';
        $sql_details.= '[UserProfile].[UserID],[FullName], [LastPasswordChange], CONVERT(VARCHAR(19),LastLogin) AS [LastLogin] ';
        $sql_details.= 'FROM [PAFUPINBS].[dbo].[UserRole] ';
        $sql_details.= 'INNER JOIN [PAFUPINBS].[dbo].[UserProfile] ';
        $sql_details.= 'ON [UserRole].[RoleID] = [UserProfile].[RoleID] ';
        $sql_details.= 'WHERE [Username] = ? ';

        $result = $this->db->query($sql_details,array($username));

        foreach($result->result() as $row)
        {

            $user_details['RoleName']           = $row->RoleName;
            $user_details['Username']           = $row->Username;
            $user_details['UserRoleID']         = $row->RoleID;
            $user_details['UserID']             = $row->UserID;
            $user_details['FullName']           = $row->FullName;
            $user_details['LastPasswordChange'] = $row->LastPasswordChange;
            $user_details['LastLogin']          = strtotime($row->LastLogin);
            $user_details['Branch']             = $row->Branch;

        }

        return $user_details;

    }
    public function lockAccount($data){
        $done = array();
        $username =  $this->session->userdata('username');
        $fullname =  $this->session->userdata('fullname');
        $today_date = Common_Tasks::generate_date();
        for($i=0;$i<count($data);$i++){

            $sql_lock = 'UPDATE [PAFUPINBS].[dbo].[account_status]
						 SET [lock_status] = ?,[lock_date]=?
			             WHERE [acc_no] = ?';

            $result =$this->db->query($sql_lock,array(true,$today_date,$data[$i]));
            if($result){
                array_push($done,'account no '.$data[$i].' locked');
                $this->log_activity('Account locked by '.$fullname, $username,$data[$i]);
            }
            else{
                array_push($done,'account no '.$data[$i].' locking failed');
                $this->log_activity('Account locking failed by '.$fullname, $username,$data[$i]);
            }
        }
        return $done;
    }
    public function unlockAccount($data){
        $done = array();
        $username =  $this->session->userdata('username');
        $fullname =  $this->session->userdata('fullname');
        $today_date = Common_Tasks::generate_date();
        for($i=0;$i<count($data);$i++){

            $sql_lock = 'UPDATE [PAFUPINBS].[dbo].[account_status]
						 SET [lock_status] = ?,[unlock_date]=?
			             WHERE [acc_no] = ?';

            $result =$this->db->query($sql_lock,array(false,$today_date,$data[$i]));
            if($result){
                array_push($done,'account no '.$data[$i].' unlocked');
                $this->log_activity('Account unlocked by '.$fullname, $username,$data[$i]);
            }
            else{
                array_push($done,'account no '.$data[$i].' unlocking failed');
                $this->log_activity('Account unlocking failed by '.$fullname, $username,$data[$i]);
            }
        }
        return $done;
    }
    public function dispatchAccount($data){
        $done = array();
        $username =  $this->session->userdata('username');
        $fullname =  $this->session->userdata('fullname');
        $today_date = Common_Tasks::generate_date();
        for($i=0;$i<count($data);$i++){

            $sql_lock = 'UPDATE [PAFUPINBS].[dbo].[account_status]
						 SET [dispatch_flag] = ?
			             WHERE [acc_no] = ?';

            $result =$this->db->query($sql_lock,array(true,$data[$i]));
            if($result){
                $sql_account = 'INSERT INTO [PAFUPINBS].[dbo].[account_mobility]
		                    ([acc_no],[dispatched_by],[AOC],[dispatch_date])
							 VALUES(?,?,?,?)';
                $dis_insert =$this->db->query($sql_account,array($data[$i],$fullname,"",$today_date));
                if($dis_insert){
                    array_push($done,'account no '.$data[$i].' dispatched');
                    $this->log_activity('Account dispatched by '.$fullname, $username,$data[$i]);
                }
                else{
                    $sql_lock = 'UPDATE [PAFUPINBS].[dbo].[account_status]
						 SET [dispatch_flag] = ?
			             WHERE [acc_no] = ?';

                    $result =$this->db->query($sql_lock,array(false,$data[$i]));
                    array_push($done,'account no '.$data[$i].' dispatching failed');
                    $this->log_activity('Account dispatching failed by '.$fullname, $username,$data[$i]);
                }

            }
            else{
                array_push($done,'account no '.$data[$i].' dispatching failed');
                $this->log_activity('Account dispatching failed by '.$fullname, $username,$data[$i]);
            }
        }
        return $done;
    }
    public function search_dispatched($parameter){
        $parameter = "%" . $parameter . "%";
        $sql = ' SELECT [PAFUPINBS_account].*,[branches].[branch_name],[Username],[FullName],[dispatched_by],[AOC],[card_no]
                                FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					            INNER JOIN [PAFUPINBS].[dbo].[branches]
					            ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					            INNER JOIN [PAFUPINBS].dbo.[UserProfile]
					            ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
					            INNER JOIN [PAFUPINBS].[dbo].[account_status]
					            ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
					            INNER JOIN [PAFUPINBS].[dbo].[account_card]
					            ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
					            INNER JOIN [PAFUPINBS].[dbo].[account_mobility]
					            ON [PAFUPINBS_account].[acc_no] = [account_mobility].[acc_no]
					            WHERE [PAFUPINBS_account].[opened_by] LIKE ?
					            OR [UserProfile].[FullName] LIKE ?
					            OR [account_mobility].[dispatched_by] LIKE ?
					            OR [account_mobility].[AOC] LIKE ?
					            OR [branches].[branch_name] LIKE ?
					            OR [UserProfile].[Username] LIKE ?';
        $data['result'] = $this->db->query($sql,array($parameter,$parameter,$parameter,$parameter,$parameter,$parameter))->result_array();

        $count = ' SELECT COUNT(*) AS [Total]
					         FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					            INNER JOIN [PAFUPINBS].[dbo].[branches]
					            ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					            INNER JOIN [PAFUPINBS].dbo.[UserProfile]
					            ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
					            INNER JOIN [PAFUPINBS].[dbo].[account_status]
					            ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
					            INNER JOIN [PAFUPINBS].[dbo].[account_card]
					            ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
					            INNER JOIN [PAFUPINBS].[dbo].[account_mobility]
					            ON [PAFUPINBS_account].[acc_no] = [account_mobility].[acc_no]
					            WHERE [PAFUPINBS_account].[opened_by] LIKE ?
					            OR [UserProfile].[FullName] LIKE ?
					            OR [account_mobility].[dispatched_by] LIKE ?
					            OR [account_mobility].[AOC] LIKE ?
					            OR [branches].[branch_name] LIKE ?
					            OR [UserProfile].[Username] LIKE ?';
        $result = $this->db->query($count,array($parameter,$parameter,$parameter,$parameter,$parameter,$parameter));

        foreach($result->result() as $row) {

            $total =  $row->Total;

        }

        $data['total'] = $total;

        return $data;

    }
    public function search_precarded($limit=false,$start=false,$parameter){
        $parameter = "%" . $parameter . "%";

        if($limit){

            $sql = "SELECT * FROM (
                     SELECT row_number() OVER (ORDER BY [PAFUPINBS_account].acc_no) AS rownum,
                     [PAFUPINBS_account].*,[branches].[branch_name],[UserProfile].[FullName]
		             FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					 INNER JOIN [PAFUPINBS].[dbo].[branches]
					 ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [PAFUPINBS].[dbo].[account_status]
                     ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                     INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                     ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
					 WHERE [account_status].[acc_status]=?
					 AND ([PAFUPINBS_account].[opened_by] LIKE ?
					 OR [UserProfile].[FullName] LIKE ?
					 OR [branches].[branch_name] LIKE ?
					 OR [UserProfile].[Username] LIKE ?)

					    ) AS A
					     WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1" ;
            $data['result'] = $this->db->query($sql,array('precarded',$parameter,$parameter,$parameter,$parameter))->result_array();
        }
        else{

            $sql = 'SELECT [PAFUPINBS_account].*,[branches].[branch_name],[UserProfile].[FullName]
		             FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					 INNER JOIN [PAFUPINBS].[dbo].[branches]
					 ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [PAFUPINBS].[dbo].[account_status]
                     ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                     INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                     ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
					 WHERE [account_status].[acc_status]=?
					 AND ([PAFUPINBS_account].[opened_by] LIKE ?
					 OR [UserProfile].[FullName] LIKE ?
					 OR [branches].[branch_name] LIKE ?
					 OR [UserProfile].[Username] LIKE ?)' ;
            $data['result'] = $this->db->query($sql,array('precarded',$parameter,$parameter,$parameter,$parameter))->result_array();
        }
        $count = ' SELECT COUNT(*) AS [Total]
					FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
					 INNER JOIN [PAFUPINBS].[dbo].[branches]
					 ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [PAFUPINBS].[dbo].[account_status]
                     ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                     INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                      ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
					 WHERE [account_status].[acc_status]=?
					 AND ([PAFUPINBS_account].[opened_by] LIKE ?
					 OR [UserProfile].[FullName] LIKE ?
					 OR [branches].[branch_name] LIKE ?
					 OR [UserProfile].[Username] LIKE ?)';
        $result = $this->db->query($count,array('precarded',$parameter,$parameter,$parameter,$parameter));

        foreach($result->result() as $row) {

            $total =  $row->Total;

        }

        $data['total'] = $total;

        return $data;

    }
    /* public function getStarterPacks(){

         $sql = 'SELECT [PAFUPINBS_account].*,[branches].[branch_name],[Username]
                                         FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
                                         INNER JOIN [PAFUPINBS].[dbo].[branches]
                                         ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                                         ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_status]
                                         ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                                         WHERE [account_status].[acc_status]=?';
         $result = $this->db->query($sql,array('starterpack'))->result_array();
         return $result;

     }*/
    public function search_starter_pack($parameter){
        $parameter = "%" . $parameter . "%";
        $sql = 'SELECT [PAFUPINBS_account].acc_no) AS rownum,[PAFUPINBS_account].*,[branches].[branch_name],[Username]
                                        FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
                                        INNER JOIN [PAFUPINBS].[dbo].[branches]
                                        ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
                                        INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                                        ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
                                        INNER JOIN [PAFUPINBS].[dbo].[account_status]
                                        ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                                        WHERE [account_status].[acc_status]=?
					 AND ([PAFUPINBS_account].[opened_by] LIKE ?
					 OR [UserProfile].[FullName] LIKE ?
					 OR [branches].[branch_name] LIKE ?
					 OR [UserProfile].[Username] LIKE ?)'					 ;
        $data['result'] = $this->db->query($sql,array('starterpack',$parameter,$parameter,$parameter,$parameter))->result_array();

        $count = ' SELECT COUNT(*) AS [Total]
					FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
                                        INNER JOIN [PAFUPINBS].[dbo].[branches]
                                        ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
                                        INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                                        ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
                                        INNER JOIN [PAFUPINBS].[dbo].[account_status]
                                        ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                                        WHERE [account_status].[acc_status]=?
					 AND ([PAFUPINBS_account].[opened_by] LIKE ?
					 OR [UserProfile].[FullName] LIKE ?
					 OR [branches].[branch_name] LIKE ?
					 OR [UserProfile].[Username] LIKE ?)';
        $result = $this->db->query($count,array('starterpack',$parameter,$parameter,$parameter,$parameter));

        foreach($result->result() as $row) {

            $total =  $row->Total;

        }

        $data['total'] = $total;

        return $data;
    }
    public function search_undispatched($parameter){
        $parameter = "%" . $parameter . "%";
        $sql = 'SELECT [PAFUPINBS_account].*,[branches].[branch_name],[Username],[FullName],[card_no]
                FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
			    INNER JOIN [PAFUPINBS].[dbo].[branches]
			    ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
			    INNER JOIN [PAFUPINBS].dbo.[UserProfile]
			    ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
			    INNER JOIN [PAFUPINBS].[dbo].[account_status]
				ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
			    INNER JOIN [PAFUPINBS].[dbo].[account_card]
				ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
			    WHERE [account_status].[dispatch_flag]=?
					 AND ([PAFUPINBS_account].[opened_by] LIKE ?
					 OR [UserProfile].[FullName] LIKE ?
					 OR [branches].[branch_name] LIKE ?
					 OR [UserProfile].[Username] LIKE ?)'					 ;
        $data['result'] = $this->db->query($sql,array(false,$parameter,$parameter,$parameter,$parameter))->result_array();

        $count = ' SELECT COUNT(*) AS [Total]
					FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
			    INNER JOIN [PAFUPINBS].[dbo].[branches]
			    ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
			    INNER JOIN [PAFUPINBS].dbo.[UserProfile]
			    ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
			    INNER JOIN [PAFUPINBS].[dbo].[account_status]
				ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
			    INNER JOIN [PAFUPINBS].[dbo].[account_card]
				ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
			    WHERE [account_status].[dispatch_flag]=?
					 AND ([PAFUPINBS_account].[opened_by] LIKE ?
					 OR [UserProfile].[FullName] LIKE ?
					 OR [branches].[branch_name] LIKE ?
					 OR [UserProfile].[Username] LIKE ?)';
        $result = $this->db->query($count,array(false,$parameter,$parameter,$parameter,$parameter));

        foreach($result->result() as $row) {

            $total =  $row->Total;

        }

        $data['total'] = $total;

        return $data;
    }
    public function search_unlocked($parameter){
        $parameter = "%" . $parameter . "%";
        $sql = 'SELECT [PAFUPINBS_account].*,
                [branches].[branch_name],[Username],[FullName]
                FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
                INNER JOIN [PAFUPINBS].[dbo].[branches]
                ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
                INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
                INNER JOIN [PAFUPINBS].[dbo].[account_status]
                ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                WHERE [account_status].[lock_status]=?
			    AND ([PAFUPINBS_account].[opened_by] LIKE ?
			    OR [UserProfile].[FullName] LIKE ?
			    OR [branches].[branch_name] LIKE ?
			    OR [UserProfile].[Username] LIKE ?)';
        $data['result'] = $this->db->query($sql,array(false,$parameter,$parameter,$parameter,$parameter))->result_array();

        $count = ' SELECT COUNT(*) AS [Total]
				 FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
                INNER JOIN [PAFUPINBS].[dbo].[branches]
                ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
                INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
                INNER JOIN [PAFUPINBS].[dbo].[account_status]
                ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                WHERE [account_status].[lock_status]=?
			    AND ([PAFUPINBS_account].[opened_by] LIKE ?
			    OR [UserProfile].[FullName] LIKE ?
			    OR [branches].[branch_name] LIKE ?
			    OR [UserProfile].[Username] LIKE ?)';
        $result = $this->db->query($count,array(false,$parameter,$parameter,$parameter,$parameter));

        foreach($result->result() as $row) {

            $total =  $row->Total;

        }

        $data['total'] = $total;

        return $data;
    }
    public function search_locked($parameter){
        $parameter = "%" . $parameter . "%";
        $sql = 'SELECT [PAFUPINBS_account].*,
                [branches].[branch_name],[Username],[FullName]
                FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
                INNER JOIN [PAFUPINBS].[dbo].[branches]
                ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
                INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
                INNER JOIN [PAFUPINBS].[dbo].[account_status]
                ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                WHERE [account_status].[lock_status]=?
			    AND ([PAFUPINBS_account].[opened_by] LIKE ?
			    OR [UserProfile].[FullName] LIKE ?
			    OR [branches].[branch_name] LIKE ?
			    OR [UserProfile].[Username] LIKE ?)';
        $data['result'] = $this->db->query($sql,array(true,$parameter,$parameter,$parameter,$parameter))->result_array();

        $count = ' SELECT COUNT(*) AS [Total]
				 FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
                INNER JOIN [PAFUPINBS].[dbo].[branches]
                ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
                INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
                INNER JOIN [PAFUPINBS].[dbo].[account_status]
                ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                WHERE [account_status].[lock_status]=?
			    AND ([PAFUPINBS_account].[opened_by] LIKE ?
			    OR [UserProfile].[FullName] LIKE ?
			    OR [branches].[branch_name] LIKE ?
			    OR [UserProfile].[Username] LIKE ?)';
        $result = $this->db->query($count,array(true,$parameter,$parameter,$parameter,$parameter));

        foreach($result->result() as $row) {

            $total =  $row->Total;

        }

        $data['total'] = $total;

        return $data;
    }
    public function search_carded($parameter){
        $parameter = "%" . $parameter . "%";
        $sql = 'SELECT [PAFUPINBS_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName],[acc_status],[referal_no],[book_no]
                                         FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
                                         INNER JOIN [PAFUPINBS].[dbo].[branches]
                                         ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_card]
                                         ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [PAFUPINBS].[dbo].[booklet]
                                         ON [PAFUPINBS_account].[acc_no] = [booklet].[acc_no]
                                         INNER JOIN [PAFUPINBS].[dbo].[referal_book]
                                         ON [PAFUPINBS_account].[acc_no] = [referal_book].[acc_no]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_status]
                                         ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                                         INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                                         ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
					 WHERE [account_status].[acc_status]=?
					 AND ([PAFUPINBS_account].[opened_by] LIKE ?
					 OR [UserProfile].[FullName] LIKE ?
					 OR [branches].[branch_name] LIKE ?
					 OR [UserProfile].[Username] LIKE ?)'					 ;
        $data['result'] = $this->db->query($sql,array('carded',$parameter,$parameter,$parameter,$parameter))->result_array();

        $count = ' SELECT COUNT(*) AS [Total] FROM
					 [PAFUPINBS].[dbo].[PAFUPINBS_account]
                                         INNER JOIN [PAFUPINBS].[dbo].[branches]
                                         ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_card]
                                         ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_status]
                                         ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                                         INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                                         ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
					 WHERE [account_status].[acc_status]=?
					 AND [PAFUPINBS_account].[opened_by] LIKE ?
					 OR [UserProfile].[FullName] LIKE ?
					 OR [branches].[branch_name] LIKE ?
					 OR [UserProfile].[Username] LIKE ?';
        $result = $this->db->query($count,array('carded',$parameter,$parameter,$parameter,$parameter));

        foreach($result->result() as $row) {

            $total =  $row->Total;

        }

        $data['total'] = $total;

        return $data;

    }
    public function getAccountsWithCustomer($start=false,$limit=false){
        if($limit){
            $sql = "SELECT * FROM (SELECT [PAFUPINBS_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName],[acc_status]
                                         ,[received_date],[AOC],[dispatched_by]
                                         FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
                                         INNER JOIN [PAFUPINBS].[dbo].[branches]
                                         ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_card]
                                         ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [PAFUPINBS].[dbo].[Customer]
                                         ON [PAFUPINBS_account].[customer_id] = [Customer].[CustomerNumber]
                                         INNER JOIN [PAFUPINBS].[dbo].[CustomerAccount]
                                         ON [PAFUPINBS_account].[customer_id] = [CustomerAccount].[CustomerID]
                                         INNER JOIN [PAFUPINBS].[dbo].[CustomerProfile]
                                         ON [PAFUPINBS_account].[customer_id] = [CustomerProfile].[CustomerID]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_status]
                                         ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_mobility]
                                         ON [PAFUPINBS_account].[acc_no] = [account_mobility].[acc_no]
                                         INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                                         ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
                                         ) AS A
					     WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1" ;
            $data['result'] = $this->db->query($sql)->result_array();
        }
        else{
            $sql = "SELECT [PAFUPINBS_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName],[acc_status]
                                         ,[received_date],[AOC],[dispatched_by]
                                         FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
                                         INNER JOIN [PAFUPINBS].[dbo].[branches]
                                         ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_card]
                                         ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [PAFUPINBS].[dbo].[Customer]
                                         ON [PAFUPINBS_account].[customer_id] = [Customer].[CustomerNumber]
                                         INNER JOIN [PAFUPINBS].[dbo].[CustomerAccount]
                                         ON [PAFUPINBS_account].[customer_id] = [CustomerAccount].[CustomerID]
                                         INNER JOIN [PAFUPINBS].[dbo].[CustomerProfile]
                                         ON [PAFUPINBS_account].[customer_id] = [CustomerProfile].[CustomerID]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_status]
                                         ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_mobility]
                                         ON [PAFUPINBS_account].[acc_no] = [account_mobility].[acc_no]
                                         INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                                         ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
                                         " ;
            $data['result'] = $this->db->query($sql)->result_array();

        }
        $sql = "SELECT COUNT([PAFUPINBS_account].[acc_no]) as Total
                                         FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
                                         INNER JOIN [PAFUPINBS].[dbo].[branches]
                                         ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_card]
                                         ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [PAFUPINBS].[dbo].[Customer]
                                         ON [PAFUPINBS_account].[customer_id] = [Customer].[CustomerNumber]
                                         INNER JOIN [PAFUPINBS].[dbo].[CustomerAccount]
                                         ON [PAFUPINBS_account].[customer_id] = [CustomerAccount].[CustomerID]
                                         INNER JOIN [PAFUPINBS].[dbo].[CustomerProfile]
                                         ON [PAFUPINBS_account].[customer_id] = [CustomerProfile].[CustomerID]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_status]
                                         ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_mobility]
                                         ON [PAFUPINBS_account].[acc_no] = [account_mobility].[acc_no]
                                         INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                                         ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
                                         " ;
        $result = $this->db->query($sql);
        foreach($result->result() as $row) {

            $total =  $row->Total;

        }

        $data['total'] = $total;

        return $data;
    }
    public function search_with_customer($parameter){
        $parameter = "%" . $parameter . "%";
        $sql = "SELECT [PAFUPINBS_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName],[acc_status]
                                         ,[received_date],[AOC],[dispatched_by]
                                         FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
                                         INNER JOIN [PAFUPINBS].[dbo].[branches]
                                         ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_card]
                                         ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [PAFUPINBS].[dbo].[Customer]
                                         ON [PAFUPINBS_account].[customer_id] = [Customer].[CustomerNumber]
                                         INNER JOIN [PAFUPINBS].[dbo].[CustomerAccount]
                                         ON [PAFUPINBS_account].[customer_id] = [CustomerAccount].[CustomerID]
                                         INNER JOIN [PAFUPINBS].[dbo].[CustomerProfile]
                                         ON [PAFUPINBS_account].[customer_id] = [CustomerProfile].[CustomerID]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_status]
                                         ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_mobility]
                                         ON [PAFUPINBS_account].[acc_no] = [account_mobility].[acc_no]
                                         INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                                         ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
                                         WHERE [PAFUPINBS_account].[opened_by] LIKE ?
                                         OR [UserProfile].[FullName] LIKE ?
                                         OR [branches].[branch_name] LIKE ?
                                         OR [UserProfile].[Username] LIKE ?
                                         OR [account_mobility].[AOC] LIKE ?
                                         OR [account_mobility].[dispatched_by] LIKE ?
                                         OR [Customer].[CustomerSurname] LIKE ?
                                         OR [Customer].[CustomerFirstName] LIKE ?
                                         OR [CustomerProfile].[CustomerVillage] LIKE ?
                                         OR [CustomerProfile].[CustomerTraditionalAuthority] LIKE ?
                                         " ;
        $data['result'] = $this->db->query($sql,array($parameter,$parameter,$parameter,$parameter,$parameter,$parameter,$parameter,$parameter,$parameter,$parameter))->result_array();
        $data['total'] = count($data['result']);

        return $data;

    }
    //for branch admin

    public function getDispatchedByBranch($branch_id,$count_acc=false){
        $sql = 'SELECT [PAFUPINBS_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName],[acc_status]
                                         ,[dispatched_by],[dispatch_date]
                                         FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
                                         INNER JOIN [PAFUPINBS].[dbo].[branches]
                                         ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_card]
                                         ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_status]
                                         ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                                          INNER JOIN [PAFUPINBS].[dbo].[account_mobility]
                                         ON [PAFUPINBS_account].[acc_no] = [account_mobility].[acc_no]
                                         INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                                         ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
                                         WHERE [PAFUPINBS_account].[branch_id] = ? AND [account_status].[dispatch_flag] = ?';
        $result = $this->db->query($sql,array($branch_id,true))->result_array();
        $count = count($result);

        if($count_acc){
            return $count;
        }
        else{
            return $result;
        }

    }
    public function getReceivedByBranch($branch_id,$percentage=false,$count_acc= false){
        $sql = 'SELECT [PAFUPINBS_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName],[acc_status]
                                         ,[received_date]
                                         FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
                                         INNER JOIN [PAFUPINBS].[dbo].[branches]
                                         ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_card]
                                         ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_status]
                                         ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                                          INNER JOIN [PAFUPINBS].[dbo].[account_mobility]
                                         ON [PAFUPINBS_account].[acc_no] = [account_mobility].[acc_no]
                                         INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                                         ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
                                         WHERE [PAFUPINBS_account].[branch_id] = ? AND [account_mobility].[received] = ?';
        $result = $this->db->query($sql,array($branch_id,true))->result_array();
        $count = count($result);

        if($percentage){
            $total_accounts = $this->getDispatchedByBranch($branch_id,true);
            if($total_accounts==0) $total_accounts=1;
            $percentage_received = round(($count/$total_accounts) * 100,0);

            return $percentage_received;
        }
        elseif($count_acc){
            return $count;
        }
        else{
            return $result;
        }

    }
    public function get_all_assigned_by_branch($branch_id,$limit=false,$start=false){
        $sql = "SELECT * FROM (
                                         SELECT row_number() OVER (ORDER BY [PAFUPINBS_account].acc_no) AS rownum,
                                         [PAFUPINBS_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName],[acc_status]
                                         ,[received_date],[AOC],[starter_id],[dispatch_date]
                                         FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
                                         INNER JOIN [PAFUPINBS].[dbo].[branches]
                                         ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_card]
                                         ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_status]
                                         ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                                          INNER JOIN [PAFUPINBS].[dbo].[account_mobility]
                                         ON [PAFUPINBS_account].[acc_no] = [account_mobility].[acc_no]
                                         INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                                         ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
                                         WHERE [PAFUPINBS_account].[branch_id] = ? AND [account_mobility].[assigned_flag] = ?
                          )
                           AS A
					     WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1";
        $data['result'] = $this->db->query($sql,array($branch_id,true))->result_array();

        $sql = "SELECT COUNT(*) as [Total]
                                         FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
                                         INNER JOIN [PAFUPINBS].[dbo].[branches]
                                         ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_card]
                                         ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_status]
                                         ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_mobility]
                                         ON [PAFUPINBS_account].[acc_no] = [account_mobility].[acc_no]
                                         INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                                         ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
                                         WHERE [PAFUPINBS_account].[branch_id] = ? AND [account_mobility].[assigned_flag] = ?";
        $result = $this->db->query($sql,array($branch_id,true));
        foreach($result->result() as $row) {

            $total =  $row->Total;

        }

        $data['total'] = $total;

        return $data;
    }
    public function  search_dispatched_not_received($branch_id=false,$parameter){
        $parameter = "%" . $parameter . "%";
        $sql = "SELECT [PAFUPINBS_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName],[acc_status]
                                         ,[received_date],[AOC],[starter_id],[dispatch_date],[dispatched_by]
                                         FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
                                         INNER JOIN [PAFUPINBS].[dbo].[branches]
                                         ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_card]
                                         ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_status]
                                         ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                                          INNER JOIN [PAFUPINBS].[dbo].[account_mobility]
                                         ON [PAFUPINBS_account].[acc_no] = [account_mobility].[acc_no]
                                         INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                                         ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
                                         WHERE [PAFUPINBS_account].[branch_id] = ?
                                         AND [account_status].[dispatch_flag] =?
                                         AND [account_mobility].[assigned_flag] = ?
                                         AND [account_mobility].[received] = ?
                                         AND ( [UserProfile].[FullName] LIKE ?
                                         OR [UserProfile].[Username] LIKE ?
                                         OR [PAFUPINBS_account].[acc_no] LIKE ?
                                         OR [account_card].[starter_id] LIKE ?
                                         OR [account_mobility].[dispatched_by] LIKE ? )";
        $data['result'] = $this->db->query($sql,array($branch_id,true,false,false,$parameter,$parameter,$parameter,$parameter,$parameter))->result_array();
        $data['total'] = count($data['result']);

        return $data;
    }

    public function get_all_dispatched_not_received_by_branch($branch_id,$limit=false,$start=false,$count=false){
        if($limit){
            $sql = "SELECT * FROM (
                                         SELECT row_number() OVER (ORDER BY [PAFUPINBS_account].acc_no) AS rownum,
                                         [PAFUPINBS_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName],[acc_status]
                                         ,[received_date],[AOC],[starter_id],[dispatch_date],[dispatched_by]
                                         FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
                                         INNER JOIN [PAFUPINBS].[dbo].[branches]
                                         ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_card]
                                         ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_status]
                                         ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                                          INNER JOIN [PAFUPINBS].[dbo].[account_mobility]
                                         ON [PAFUPINBS_account].[acc_no] = [account_mobility].[acc_no]
                                         INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                                         ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
                                         WHERE [PAFUPINBS_account].[branch_id] = ? AND [account_status].[dispatch_flag] =? AND [account_mobility].[assigned_flag] = ? AND [account_mobility].[received] = ?
                          )
                           AS A
					     WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1";
            $data['result'] = $this->db->query($sql,array($branch_id,true,false,false))->result_array();
        }
        $sql = "SELECT COUNT(*) as [Total]
                                         FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
                                         INNER JOIN [PAFUPINBS].[dbo].[branches]
                                         ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_card]
                                         ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_status]
                                         ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_mobility]
                                         ON [PAFUPINBS_account].[acc_no] = [account_mobility].[acc_no]
                                         INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                                         ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
                                         WHERE [PAFUPINBS_account].[branch_id] = ?
                                         AND [account_status].[dispatch_flag] =? AND [account_mobility].[assigned_flag] = ?
                                         AND [account_mobility].[received] = ?";
        $result = $this->db->query($sql,array($branch_id,true,true,false));
        foreach($result->result() as $row) {

            $total =  $row->Total;
            if($count){
                return $total;
            }
        }

        $data['total'] = $total;

        return $data;
    }
    public function  search_dispatched_not_assigned($branch_id=false,$parameter){
        $parameter = "%" . $parameter . "%";
        $sql = 'SELECT [PAFUPINBS_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName],[acc_status]
                                         ,[received_date],[AOC],[starter_id],[dispatch_date]
                                         FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
                                         INNER JOIN [PAFUPINBS].[dbo].[branches]
                                         ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_card]
                                         ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_status]
                                         ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                                          INNER JOIN [PAFUPINBS].[dbo].[account_mobility]
                                         ON [PAFUPINBS_account].[acc_no] = [account_mobility].[acc_no]
                                         INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                                         ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
                                         WHERE [PAFUPINBS_account].[branch_id] = ? AND [account_status].[dispatch_flag] =? AND [account_mobility].[assigned_flag] = ? AND [account_mobility].[received] = ?
                                         AND (OR [UserProfile].[FullName] LIKE ?
                                         OR [UserProfile].[Username] LIKE ?
                                         OR [PAFUPINBS_account].[acc_no] LIKE ?
                                         OR [PAFUPINBS_account].[starter_id] LIKE ?
                                         OR [account_mobility].[dispatched_by] LIKE ?)';
        $data['result'] = $this->db->query($sql,array($branch_id,true,false,true,$parameter,$parameter,$parameter,$parameter,$parameter))->result_array();
        $data['total'] = count($data['result']);

        return $data;
    }
    public function get_all_dispatched_not_assigned_by_branch($branch_id,$limit=false,$start=false,$count=false){
        if($limit){
            $sql = "SELECT * FROM (
                                         SELECT row_number() OVER (ORDER BY [PAFUPINBS_account].acc_no) AS rownum,
                                         [PAFUPINBS_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName],[acc_status]
                                         ,[received_date],[AOC],[starter_id],[dispatch_date]
                                         FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
                                         INNER JOIN [PAFUPINBS].[dbo].[branches]
                                         ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_card]
                                         ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_status]
                                         ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                                          INNER JOIN [PAFUPINBS].[dbo].[account_mobility]
                                         ON [PAFUPINBS_account].[acc_no] = [account_mobility].[acc_no]
                                         INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                                         ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
                                         WHERE [PAFUPINBS_account].[branch_id] = ? AND [account_status].[dispatch_flag] =? AND [account_mobility].[assigned_flag] = ? AND [account_mobility].[received] = ?
                          )
                           AS A
					     WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1";
            $data['result'] = $this->db->query($sql,array($branch_id,true,false,true))->result_array();
        }
        $sql = "SELECT COUNT(*) as [Total]
                                         FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
                                         INNER JOIN [PAFUPINBS].[dbo].[branches]
                                         ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_card]
                                         ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_status]
                                         ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_mobility]
                                         ON [PAFUPINBS_account].[acc_no] = [account_mobility].[acc_no]
                                         INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                                         ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
                                         WHERE [PAFUPINBS_account].[branch_id] = ?
                                         AND [account_status].[dispatch_flag] =? AND [account_mobility].[assigned_flag] = ?
                                         AND [account_mobility].[received] = ?";
        $result = $this->db->query($sql,array($branch_id,true,true,true));
        foreach($result->result() as $row) {

            $total =  $row->Total;
            if($count){
                return $total;
            }
        }

        $data['total'] = $total;

        return $data;
    }

    public function search_assigned($branch_id=false,$parameter){
        $parameter = "%" . $parameter . "%";
        if($branch_id){
            $sql = 'SELECT [PAFUPINBS_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName],[acc_status]
                                         ,[received_date],[AOC],[starter_id],[dispatch_date]
                                         FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
                                         INNER JOIN [PAFUPINBS].[dbo].[branches]
                                         ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_card]
                                         ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_status]
                                         ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                                          INNER JOIN [PAFUPINBS].[dbo].[account_mobility]
                                         ON [PAFUPINBS_account].[acc_no] = [account_mobility].[acc_no]
                                         INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                                         ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
                                         WHERE [PAFUPINBS_account].[branch_id] = ? AND [account_mobility].[assigned_flag] = ?
                                         AND ([account_mobility].[AOC] LIKE ? OR
                                         [account_mobility].[dispatched_by] LIKE ? OR
                                         [UserProfile].[FullName] LIKE ? OR
                                         [branches].[branch_name] LIKE ? OR
                                         [account_status].[acc_status] LIKE ?
                                         )
                ';
            $data['result'] = $this->db->query($sql,array($branch_id,true,$parameter,$parameter,$parameter,$parameter,$parameter))->result_array();
            $data['total'] = count($data['result']);
        }
        else{
            $sql = "SELECT [PAFUPINBS_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName],[acc_status]
                                         ,[received_date],[AOC],[starter_id],[dispatch_date]
                                         FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
                                         INNER JOIN [PAFUPINBS].[dbo].[branches]
                                         ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_card]
                                         ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_status]
                                         ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                                          INNER JOIN [PAFUPINBS].[dbo].[account_mobility]
                                         ON [PAFUPINBS_account].[acc_no] = [account_mobility].[acc_no]
                                         INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                                         ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
                                         WHERE [account_mobility].[assigned_flag] = ?
                                         AND ([account_mobility].[AOC] LIKE ? OR
                                         [account_mobility].[dispatched_by] LIKE ? OR
                                         [UserProfile].[FullName] LIKE ? OR
                                         [branches].[branch_name] LIKE ? OR
                                         [account_status].[acc_status] LIKE ?
                                         )
                ";
            $data['result'] = $this->db->query($sql,array(true,$parameter,$parameter,$parameter,$parameter,$parameter))->result_array();
            $data['total'] = count($data['result']);

        }
        return $data;
    }
    public function getAssignedByBranch($branch_id,$percentage=false,$count_acc= false){
        $sql = 'SELECT [PAFUPINBS_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName],[acc_status]
                                         ,[received_date],[AOC],[starter_id],[dispatch_date]
                                         FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
                                         INNER JOIN [PAFUPINBS].[dbo].[branches]
                                         ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_card]
                                         ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_status]
                                         ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                                          INNER JOIN [PAFUPINBS].[dbo].[account_mobility]
                                         ON [PAFUPINBS_account].[acc_no] = [account_mobility].[acc_no]
                                         INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                                         ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
                                         WHERE [PAFUPINBS_account].[branch_id] = ? AND [account_mobility].[assigned_flag] = ?
                ';
        $result = $this->db->query($sql,array($branch_id,true))->result_array();
        $count = count($result);

        if($percentage){
            $total_accounts = $this->getDispatchedByBranch($branch_id,true);
            if($total_accounts==0) $total_accounts=1;
            $percentage_received = round(($count/$total_accounts) * 100,0);

            return $percentage_received;
        }
        elseif($count_acc){
            return $count;
        }
        else{
            return $result;
        }
    }
    public function getFullyByBranch($branch_id,$percentage=false,$count_acc= false){
        $sql = 'SELECT [PAFUPINBS_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName],[acc_status]
                                         ,[received_date]
                                         FROM [PAFUPINBS].[dbo].[PAFUPINBS_account]
                                         INNER JOIN [PAFUPINBS].[dbo].[branches]
                                         ON [PAFUPINBS_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_card]
                                         ON [PAFUPINBS_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [PAFUPINBS].[dbo].[Customer]
                                         ON [PAFUPINBS_account].[customer_id] = [Customer].[CustomerNumber]
                                          INNER JOIN [PAFUPINBS].[dbo].[CustomerProfile]
                                         ON [PAFUPINBS_account].[customer_id] = [CustomerProfile].[CustomerID]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_status]
                                         ON [PAFUPINBS_account].[acc_no] = [account_status].[acc_no]
                                         INNER JOIN [PAFUPINBS].[dbo].[account_mobility]
                                         ON [PAFUPINBS_account].[acc_no] = [account_mobility].[acc_no]
                                         INNER JOIN [PAFUPINBS].dbo.[UserProfile]
                                         ON [PAFUPINBS_account].[opened_by] = [UserProfile].[UserID]
                                         WHERE [PAFUPINBS_account].[branch_id] = ?';
        $result = $this->db->query($sql,array($branch_id))->result_array();
        $count = count($result);

        if($percentage){
            $total_accounts = $this->getDispatchedByBranch($branch_id,true);
            if($total_accounts==0) $total_accounts=1;
            $percentage_received = round(($count/$total_accounts) * 100,0);

            return $percentage_received;
        }
        elseif($count_acc){
            return $count;
        }
        else{
            return $result;
        }
    }
    public function receiveStarterPack($data){
        $result = array();
        for($i = 0; $i<count($data);$i++){
            $today_date = Common_Tasks::generate_date();
            $sql = "UPDATE [PAFUPINBS].[dbo].[account_mobility]
						 SET [received_date] = ?,[received]=?
			             WHERE [acc_no] = ?";
            $done = $this->db->query($sql, array($today_date,true,$data[$i]));
            if($done){
                $username =  $this->session->userdata('username');
                $this->log_activity('Starterpack received', $username,$data[$i]);
                $result[$i] = "received starterpack with account ".$data[$i];
            }
            else{
                $username =  $this->session->userdata('username');
                $this->log_activity('Error receiving starterpack', $username,$data[$i]);
                $result[$i] = "failed to receive starterpack with account ".$data[$i];
            }

        }
        return $result;
    }
    public function getOSSC($branch_id, $search=false,$ossc=false){
        if(!$search){
            $sql = "SELECT [Username]
                      ,[FullName]
                      ,[Branch]
                      ,[UserID]
                      ,[Status]
                      ,[CreatedBy]
                      ,[ActivatedBy]
                      ,[RoleID]
                      ,[PasswordExpiry]
                      ,[LastPasswordChange]
                      ,[FirstLogin]
                      ,[EmailAddress]
                  FROM [PAFUPINBS].[dbo].[UserProfile] WHERE [branch] = ? AND [Status] =? AND RoleID=?";
            $result = $this->db->query($sql,array($branch_id,'A',3))->result_array();
            return $result;
        }
        else{
            $ossc = "%" . $ossc . "%";
            $sql = "SELECT [Username]
                      ,[FullName]
                      ,[Branch]
                      ,[UserID]
                      ,[Status]
                      ,[CreatedBy]
                      ,[ActivatedBy]
                      ,[RoleID]
                      ,[PasswordExpiry]
                      ,[LastPasswordChange]
                      ,[FirstLogin]
                      ,[EmailAddress]
                  FROM [PAFUPINBS].[dbo].[UserProfile] WHERE [branch] = ? AND [Status] =? AND [RoleID]=? AND [FullName]=?";
            $result = $this->db->query($sql,array($branch_id,'A',3,$ossc))->result_array();
            if(count($result) >=1)
                return true;
            else
                return false;
        }
    }
    public function assignStarterPack($data){
        $today_date = Common_Tasks::generate_date();
        $username =  $this->session->userdata('username');
        $sql = "UPDATE [PAFUPINBS].[dbo].[account_mobility]
						 SET [assigned_date] = ?,[AOC]=?,[assigned_flag]=?
			             WHERE [acc_no] = ?";
        $done = $this->db->query($sql, array($today_date,$data['ossc'],true,$data['acc_no']));
        if($done){
            $this->log_activity('starterpack assigned to '.$data['ossc'], $username,$data['acc_no']);
            return true;
        }
        else{
            $this->log_activity('failed to assign starterpack to '.$data['ossc'], $username,$data['acc_no']);
            return false;
        }
    }
}
