<?php
/** ----------------------------------------------------------------------------------------------------------
 *	@file <Authentication.php>
 *   @version 1.0
 *	@description : This model handles authentication related tasks
 *	@author : Vitu Mhone( modified by Compiler) <vmhone@nbsmw.com>
 * -------------------------------------------------------------------------------------------------------------*/
?>

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH.'libraries/Security.php';
require_once APPPATH.'libraries/Common_Tasks.php';
require_once APPPATH.'libraries/Validation.php';

Class Authentication extends CI_Model {

    public function __construct()
    {

        //call the parent constructor before anything else
        parent::__construct();

        //initialise connection to the database
        $this->load->database();

        //load the session library
        $this->load->library('session');

    }

    public function change_flag($username)
    {

        //change the login flag to true upon login on
        $sql_login = 'UPDATE [pafupi].[dbo].[UserProfile]
		              SET [LoginFlag] = ?
					  WHERE [Username] = ?';

        $this->db->query($sql_login,array('Online',$username));


    }

    public function get_branch_code($branch)
    {

        //this method returns a branch code given a branch

        //now lets generate a branch code

        /*

        The company codes are below

        Blantyre        : MW0010020
        Capital City    : MW0010070
        Ginnery Corner  : MW0010025
        Haille Selassie : MW0010080
        Head Office     : MW0010001
        Karonga         : MW0010065
        Kasungu         : MW0010075
        Limbe           : MW0010040
        Mangochi        : MW0010045
        Mchinji         : MW0010052
        Mulanje         : MW0010055
        Mzuzu           : MW0010030
        Old Town        : MW0010050
        Zomba           : MW0010060

        */

        switch($branch)
        {

            case 'Blantyre Branch':
                $company_code = 'MW0010020';
                break;

            case 'Capital City Branch':
                $company_code = 'MW0010070';
                break;

            case 'Ginnery Corner Branch':
                $company_code = 'MW0010025';
                break;

            case 'Haille Selassie Branch':
                $company_code = 'MW0010080';
                break;

            case 'Head Office':
                $company_code = 'MW0010001';
                break;

            case 'Karonga Branch':
                $company_code = 'MW0010065';
                break;

            case 'Kasungu Branch':
                $company_code = 'MW0010075';
                break;

            case 'Limbe Branch':
                $company_code = 'MW0010040';
                break;

            case 'Mangochi Branch':
                $company_code = 'MW0010045';
                break;

            case 'Mchinji Branch':
                $company_code = 'MW0010052';
                break;

            case 'Mulanje Branch':
                $company_code = 'MW0010055';
                break;

            case 'Mzuzu Branch':
                $company_code = 'MW0010030';
                break;

            case 'Old Town Branch':
                $company_code = 'MW0010050';
                break;

            case 'Zomba Branch':
                $company_code = 'MW0010060';
                break;

            default:
                $company_code = 'None';
        }

        return $company_code;


    }

    public function get_branch($branch)
    {

        //this method returns a branch code given a branch

        //now lets generate a branch code

        /*

        The company codes are below

        Blantyre        : MW0010020
        Capital City    : MW0010070
        Ginnery Corner  : MW0010025
        Haille Selassie : MW0010080
        Head Office     : MW0010001
        Karonga         : MW0010065
        Kasungu         : MW0010075
        Limbe           : MW0010040
        Mangochi        : MW0010045
        Mchinji         : MW0010052
        Mulanje         : MW0010055
        Mzuzu           : MW0010030
        Old Town        : MW0010050
        Zomba           : MW0010060

        */

        switch($branch)
        {

            case 'MW0010020':
                $company_code = 'Blantyre Branch';
                break;

            case 'MW0010070':
                $company_code = 'Capital City Branch';
                break;

            case 'MW0010025':
                $company_code = 'Ginnery Corner Branch';
                break;

            case 'MW0010080':
                $company_code = 'Haille Selassie Branch';
                break;

            case 'MW0010001':
                $company_code = 'Head Office';
                break;

            case 'MW0010065':
                $company_code = 'Karonga Branch';
                break;

            case 'MW0010075':
                $company_code = 'Kasungu Branch';
                break;

            case 'MW0010040':
                $company_code = 'Limbe Branch';
                break;

            case 'MW0010045':
                $company_code = 'Mangonchi Branch';
                break;

            case 'MW0010052':
                $company_code = 'Mchinji Branch';
                break;

            case 'MW0010055':
                $company_code = 'Mulanje Branch';
                break;

            case 'MW0010030':
                $company_code = 'Mzuzu Branch';
                break;

            case 'MW0010050':
                $company_code = 'Old Town Branch';
                break;

            case 'MW0010060':
                $company_code = 'Zomba Branch';
                break;

            default:
                $company_code = 'None';
        }

        return $company_code;


    }
    public function get_branch_region($branch)
    {

        //this method returns a branch code given a branch

        //now lets generate a branch code

        /*

        The company codes are below

        Blantyre        : MW0010020
        Capital City    : MW0010070
        Ginnery Corner  : MW0010025
        Haille Selassie : MW0010080
        Head Office     : MW0010001
        Karonga         : MW0010065
        Kasungu         : MW0010075
        Limbe           : MW0010040
        Mangochi        : MW0010045
        Mchinji         : MW0010052
        Mulanje         : MW0010055
        Mzuzu           : MW0010030
        Old Town        : MW0010050
        Zomba           : MW0010060

        */

        switch($branch)
        {

            case 'MW0010020':
                $company_code = 'S';
                break;

            case 'MW0010070':
                $company_code = 'C';
                break;

            case 'MW0010025':
                $company_code = 'S';
                break;

            case 'MW0010080':
                $company_code = 'S';
                break;

            case 'MW0010001':
                $company_code = 'S';
                break;

            case 'MW0010065':
                $company_code = 'N';
                break;

            case 'MW0010075':
                $company_code = 'C';
                break;

            case 'MW0010040':
                $company_code = 'S';
                break;

            case 'MW0010045':
                $company_code = 'S';
                break;

            case 'MW0010052':
                $company_code = 'C';
                break;

            case 'MW0010055':
                $company_code = 'S';
                break;

            case 'MW0010030':
                $company_code = 'N';
                break;

            case 'MW0010050':
                $company_code = 'C';
                break;

            case 'MW0010060':
                $company_code = 'S';
                break;

            default:
                $company_code = 'S';
        }

        return $company_code;


    }
    public function check_login($user,$pwd)
    {

        $count = 0;

        $username_db = NULL;
        $password_db = NULL;

        //get the username and password passed to the function
        //hash the password
        $username = strtolower(Validation::sanitise_input($user));
        $password = Security::hash_password($pwd);

        $sql_authenticate = 'SELECT [Username], [Password], [Status], [PasswordExpiry] ';
        $sql_authenticate.= 'FROM [pafupi].[dbo].[UserProfile] ';
        $sql_authenticate.= 'WHERE [Username] = ? AND [Password] = ? ';

        $result = $this->db->query($sql_authenticate,array($username,$password));

        foreach($result->result() as $row)
        {

            $count++;
            $username_db      = $row->Username;
            $password_db      = $row->Password;
            $password_expiry  = "'" . $row->PasswordExpiry . "'";

        }

        if(!empty($username_db))
        {

            foreach($result->result() as $row)
            {
                //trimming whitespaces :)
                $status = trim($row->Status);

            }

            $today_date = Common_Tasks::generate_date();
            $sql_date   = "'" . $today_date . "'";

            //check	if the password has expired
            $sql_pwd_check = "SELECT datediff(day, $sql_date, $password_expiry) AS [DaysToExpiry] ";
            $sql_pwd_check.= "FROM [pafupi].[dbo].[UserProfile] ";
            $sql_pwd_check.= "WHERE [Username] = ? ";

            $result = $this->db->query($sql_pwd_check,array($username));

            foreach($result->result() as $row) {

                $days_to_expiry = $row->DaysToExpiry;

            }

            if($days_to_expiry < 0) {

                //password has expired
                if($status != 'L')
                {

                    //the password has expired, so lets update the password status to Expired
                    $update_account_status = 'UPDATE [pafupi].[dbo].[UserProfile] ';
                    $update_account_status.= 'SET [Status] = ? ';
                    $update_account_status.= 'WHERE [Username] = ? ';

                    $this->db->query($update_account_status,array('E',$username));

                    //set the session variable to indicate the password has expired
                    $this->session->set_userdata('status',1);

                }

            }


            $this->log_activity('User Login', $username_db);

        }

        //lets check if the username and password on the form matched the credentials on the database
        //if the username and password matched and the account is not locked out, return A
        if((strcmp($username,$username_db) == 0 && ($status == 'A') && (strcmp($password,$password_db)==0)))
        {

            //change the login status to online by calling the change flag method
            $this->change_flag($username);
            return 'A';

        }

        //if the credentials are correct but the account is locked
        else if((strcmp($username,$username_db) == 0) && ($status == 'L') && (strcmp($password,$password_db)==0))
        {

            return 'L';

        }

        //if the credentials are correct but the account is inactive
        else if((strcmp($username,$username_db) == 0) && ($status == 'I') && (strcmp($password,$password_db)==0))
        {

            return 'I';

        }

        //if the credentials are correct but the account password has expired
        else if((strcmp($username,$username_db) == 0) && ($status == 'E') && (strcmp($password,$password_db)==0))
        {

            return 'E';

        }

        else if((strcmp($username,$username_db) != 0) && (strcmp($password,$password_db)!=0))
        {

            //the wrong credentials were provided, so return W
            return 'W';

        }

    }

    public function log_activity($activity, $username) {

        //generate a date
        $today_date = Common_Tasks::generate_date();

        $activity_id = mt_rand() + round(microtime()*100);

        //get user details
        $user_details = $this->get_details($username);

        $user_details['FullName'] = Validation::sanitise_input($user_details['FullName']);

        //get the ip address
        $ip_address = Network::get_ip_address();

        $sql_activity = 'INSERT INTO [pafupi].[dbo].[UserActivity] ';
        $sql_activity.= 'VALUES(?,?,?,?,?,?,?,?) ';

        $this->db->query($sql_activity,array($activity_id,$today_date,$username,$user_details['FullName'],
            $user_details['RoleName'],$user_details['Branch'],$activity,$ip_address));

    }

    public function first_login($username)
    {

        //this method checks whether the user is logging in for the first time
        //the first login column is simply check for either a zero or a one
        $sql_login_check = 'SELECT COUNT([Username]) AS [login_check] ';
        $sql_login_check.= 'FROM [pafupi].[dbo].[UserProfile] ';
        $sql_login_check.= 'WHERE [FirstLogin] = ? ';
        $sql_login_check.= 'AND [Username] = ? ';

        $result = $this->db->query($sql_login_check,array(1,$username));

        foreach($result->result() as $row)
        {

            $status = $row->login_check;

        }

        if($status == 1)
        {
            //this is a first login
            return true;

        }

        else

            //this is not a first login
            return false;

    }

    public function get_details($username)
    {

        //this method returns a user's details
        $sql_details = 'SELECT [RoleName], [Username], [UserProfile].[RoleID], [Branch],';
        $sql_details.= '[UserProfile].[UserID],[FullName], [LastPasswordChange], CONVERT(VARCHAR(19),LastLogin) AS [LastLogin] ';
        $sql_details.= 'FROM [pafupi].[dbo].[UserRole] ';
        $sql_details.= 'INNER JOIN [pafupi].[dbo].[UserProfile] ';
        $sql_details.= 'ON [UserRole].[RoleID] = [UserProfile].[RoleID] ';
        $sql_details.= 'WHERE [Username] = ? ';

        $result = $this->db->query($sql_details,array($username));

        foreach($result->result() as $row)
        {

            $user_details['RoleName']           = $row->RoleName;
            $user_details['Username']           = $row->Username;
            $user_details['UserRoleID']         = $row->RoleID;
            $user_details['UserID']             = $row->UserID;
            $user_details['FullName']           = $row->FullName;
            $user_details['LastPasswordChange'] = $row->LastPasswordChange;
            $user_details['LastLogin']          = strtotime($row->LastLogin);
            $user_details['Branch']             = $row->Branch;

        }

        return $user_details;

    }

    public function update_login($username)
    {

        //generate a date
        $date = Common_Tasks::generate_date();

        //this method updates the timestamp of a user's last login
        //plus the Login flag
        //upon logging out
        $sql_update = 'UPDATE [pafupi].[dbo].[UserProfile]
		               SET [LastLogin] = ?,
					   [LoginFlag] = ?
					   WHERE [Username] = ?';

        $this->db->query($sql_update,array($date,'Offline',$username));

        $this->log_activity('User Logout', $username);

    }

    public function flag_change($username)
    {

        //this method updates the login flag
        //upon logout
        $sql_logout = 'UPDATE [pafupi].[dbo].[UserProfile]
		               SET [LoginFlag] = ?
					   WHERE [Username] = ?';

        $this->db->query($sql_logout,array('Offline',$username));

    }

    public function change_password($username)
    {

        //this method updates a user's password
        //first get the current password and hash it
        $pwd = $this->security->xss_clean($this->input->post('current_password',TRUE));

        //get the new password
        $new_password = $this->security->xss_clean($this->input->post('new_password',TRUE));

        //hash the current password
        $password = Security::hash_password($pwd);

        //fetch the existing hashed password from the database
        $sql_password = 'SELECT [Password]
		                 FROM [pafupi].[dbo].[UserProfile]
						 WHERE [Username] = ?';

        $result = $this->db->query($sql_password,array($username));

        foreach($result->result() as $row)
        {

            $password_db = $row->Password;

        }

        //now compare the password from the form with the password
        //in the database
        $result_compare = strcmp($password,$password_db);

        //hash the new password
        $hashed_new_password = Security::hash_password($new_password);

        //compare the new password with the current one
        $new_current = strcmp($hashed_new_password,$password_db);

        //before we change the password, lets check to see that the new provided password is not
        //the same as the one in the database. basically preventing the user from re-using
        //the same password
        if($new_current != 0 && $result_compare == 0)
        {

            //if the old and the new password are different lets move to the next step

            //generate a UNIX timestamp
            $time = date('Y-m-d H:i:s');

            //we should also generate a new expiry date for the password.
            //lets set it to 90 days

            $expiry_date = date('Y-m-d',strtotime('+90 days'));

            $sql_update_password = 'UPDATE [pafupi].[dbo].[UserProfile]
                                    SET [Password] = ?,
                                    [LastPasswordChange] = ?,
                                    [PasswordExpiry] = ?,
                                    [Status] = ?,
                                    [FirstLogin] = ?
                                    WHERE [Username] = ?';

            $this->db->query($sql_update_password,array($hashed_new_password,$time,$expiry_date,'A',0,$username));

            //clear the session variable
            $this->session->set_userdata('status','');

            //update the last password change variable
            $this->session->set_userdata('last_pwd_change',$time);

            return 1;
        }

        elseif($new_current == 0 && $result_compare == 0)
        {

            //this is to prevent users reusing passwords
            return 2;


        }

        elseif($new_current != 0 && $result_compare != 0)
        {

            //the password change failed, so just return false
            return 0;

        }

    }

}

/* End of file Authentication.php */
/* Location: ./application/models/Authentication.php */
