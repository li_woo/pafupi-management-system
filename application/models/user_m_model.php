<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH.'libraries/Security.php';
require_once APPPATH.'libraries/Network.php';
require_once APPPATH.'libraries/Common_Tasks.php';
require_once APPPATH.'libraries/Validation.php';
require_once APPPATH.'models/Authentication.php';

class User_m_model extends CI_Model {
    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        //$this->load->model('authentication');
        //$this->load->library('common_functions');

    }
    public function getRoles(){
        $q = 'SELECT [RoleName],[RoleID]
		          FROM [pafupi].[dbo].[UserRole]';
        $result = $this->db->query($q);
        return $result->result_array();
    }
    public function createUser($data){
        //this method creates a user

        //get the values from the form
        $firstname = ucwords(strtolower(Validation::sanitise_input($data['fname'])));
        $lastname  = ucwords(strtolower(Validation::sanitise_input($data['lname'])));

        $full_name      = $firstname . ' ' . $lastname;
        $password       = Security::hash_password(strtolower($firstname.$lastname));
        $branch         = $data['branch'];
        $role           = $data['role'];
        if($role == 3){
            $device = @$data['device'];
        }
        $username_admin = $this->session->userdata('username');

        //generate a user_id
        $user_id = mt_rand() + round(microtime()*100);

        //generate a date
        $date = Common_Tasks::generate_date();

        //expire the account password already
        $expiry_date = date('Y-m-d H:i:s',strtotime('-3 days'));

        $status = 'A';

        //generate a username from the first and last name
        $first_fragment  = trim(strtolower(substr($firstname,0,1)));
        $second_fragment = trim(strtolower($lastname));
        $username        = $first_fragment . $second_fragment;
        $username        = Validation::sanitise_input($username,TRUE);

        //verify if the username may already exist by calling the check_user method
        $result_check = $this->check_user($username);

        //generate a UNIX timestamp
        $time = date('Y-m-d H:i:s');

        if($result_check == 1)
        {

            //the username exists, append a number to it
            $username = $username . '_' .  mt_rand(1,99);

        }
        if($role == 3){
            //some SQL to insert the record into the db
            $sql_create_user = 'INSERT INTO [pafupi].[dbo].[UserProfile]
		                    ([UserID],[FullName],[Username],[Password],[Branch],
		                     [RoleID],[Status],[PasswordExpiry],[CreatedBy],[ActivatedBy],[LastPasswordChange],[FirstLogin])
							 VALUES(?,?,?,?,?,?,?,?,?,?,?,?)';
            $sql_ossc = "INSERT INTO [pafupi].[dbo].[ossc_details]
                        ([user_id] ,[device_number])
                         VALUES(?,?)";
            $this->db->trans_start();
            $this->db->query($sql_create_user,array($user_id,$full_name,$username,$password,$branch,
                $role,$status,$expiry_date,$username_admin,$username_admin,$time,'0'));
            $this->db->query($sql_create_user,array($user_id,$device));
            $this->db->trans_complete();

            if ($this->db->trans_status()== TRUE){
               $result = true;
            }
            else
                 $result = false;
        }
        else{
        //some SQL to insert the record into the db
        $sql_create_user = 'INSERT INTO [pafupi].[dbo].[UserProfile]
		                    ([UserID],[FullName],[Username],[Password],[Branch],
		                     [RoleID],[Status],[PasswordExpiry],[CreatedBy],[ActivatedBy],[LastPasswordChange],[FirstLogin])
							 VALUES(?,?,?,?,?,?,?,?,?,?,?,?)';

        $result = $this->db->query($sql_create_user,array($user_id,$full_name,$username,$password,$branch,
            $role,$status,$expiry_date,$username_admin,$username_admin,$time,'1'));
        }
        if($result){
        $role_title    = $this->get_user_type($role);
        $user_activity = 'User Creation : ';
        $user_activity.= $username . ' (' . $branch . ') as ' .  $role_title;

        Authentication::log_activity($user_activity, $username_admin);

        //return the fullname, username, and branch of the created user
        $user_details = array();

        $user_details['username'] = $username;
        $user_details['fullname'] = $full_name;
        $user_details['branch']   = $branch;

        return $user_details;
        }
        else{
            return false;
        }

    }
    public function get_user_type($role_id)
    {
        //this method returns the role title of the user given an ID

        switch($role_id)
        {

            case 1:
                $role_title = 'Support Staff';
                break;

            case 2:
                $role_title = 'Branch Administrator';
                break;

            case 3:
                $role_title = 'OSSC';
                break;

            default:
                $role_title = 'Unknown Role';

        }

        return $role_title;

    }
    private function check_user($username)
    {
        //initialise count to zero, it will check how many times
        //the username was found
        $count = 0;

        //this method checks whether a particular username already exists in the database
        $sql_check = 'SELECT COUNT ([Username]) AS [user_count]
					  FROM [pafupi].[dbo].[UserProfile]
		              WHERE [Username] = ?';

        $result = $this->db->query($sql_check,array($username));

        foreach($result->result() as $row)
        {

            $count = $row->user_count;

        }

        if ($count > 0)
        {

            //if the username was found return a 1
            return 1;
        }

        else
        {

            //if the username was not found, return a 0
            return 0;
        }

    }
    public function get_all_unlocked_users($limit,$start)
    {
        //get the username
        //$username = $this->session->userdata('username');

        //this method retrieves users and facilitates pagination
        //get all the unlocked users
        $sql = "SELECT * FROM (
                          SELECT row_number() OVER (ORDER BY FullName) AS rownum, [Username],[Branch],[FullName],
                                [UserProfile].[RoleID], [UserID],
                                CONVERT(VARCHAR(19),LastLogin) AS [LastLogin],[LastPasswordChange],
								[RoleName],[UserProfile].[Status]
                                FROM [pafupi].[dbo].[UserProfile]
								INNER JOIN [pafupi].[dbo].[UserRole]
								ON [UserRole].[RoleID] = [UserProfile].[RoleID]
								WHERE ([UserProfile].[Status] = ?
								OR [UserProfile].[Status] = ?)

			   ) AS A
               WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1";


        $result = $this->db->query($sql,array(/*$username,*/'A','E'));

        //return the query result
        return $result->result_array();

    }
    public function get_no_users()
    {
        //this method fetches the total number of users
        //registered on the system

        //lets initialise count to 0, it will hold the total
        //number of users
        $count = 0;

        //get the username
        $username = $this->session->userdata('username');

        //some SQL to find the number of users
        $sql_num_users = "SELECT COUNT([Username]) AS [user_count] FROM [pafupi].[dbo].[UserProfile]
						  INNER JOIN [pafupi].[dbo].[UserRole]
						  ON [UserProfile].[RoleID] = [UserRole].[RoleID]";

        $result = $this->db->query($sql_num_users);

        foreach($result->result() as $row)
        {

            $count = $row->user_count;

        }

        return $count;

    }
    public function get_all_users($limit,$start)
    {
        //get the username
        $username = $this->session->userdata('username');

        //this method retrieves users and facilitates pagination
        //this method also excludes all users that have performed some sort of activity on the system
        $sql = "SELECT * FROM (
                          SELECT row_number() OVER (ORDER BY FullName) AS rownum, [Username],[Branch],[FullName],
                                [pafupi].[dbo].[UserProfile].[RoleID],[branch_name],[UserID], CONVERT(VARCHAR(19),LastLogin) AS [LastLogin],
                                [LastPasswordChange],[RoleName]
                                FROM [pafupi].[dbo].[UserProfile]
								INNER JOIN [pafupi].[dbo].[UserRole]
								ON [pafupi].[dbo].[UserProfile].[RoleID] = [pafupi].[dbo].[UserRole].[RoleID]
								INNER JOIN [pafupi].[dbo].[Branches]
					            ON [UserProfile].[Branch] = [Branches].[branch_id]


			   ) AS A

			   WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1
			   ";


        $result = $this->db->query($sql);

        //return the query result
        return $result->result_array();

    }
    public function get_all_locked_users($limit,$start)
    {
        //get the username
        $username = $this->session->userdata('username');

        //this method retrieves users and facilitates pagination
        //get all the unlocked users
        $sql = "SELECT * FROM (
                          SELECT row_number() OVER (ORDER BY FullName) AS rownum, [Username],[Branch],[FullName],
                                [UserProfile].[RoleID], [UserID],[branch_name],
                                CONVERT(VARCHAR(19),LastLogin) AS [LastLogin],[LastPasswordChange],
								[RoleName],[UserProfile].[Status]
                                FROM  [pafupi].[dbo].[UserProfile]
								INNER JOIN [pafupi].[dbo].[UserRole]
								ON [UserRole].[RoleID] = [UserProfile].[RoleID]
								 INNER JOIN [pafupi].[dbo].[Branches]
				                ON [UserProfile].[Branch] = [Branches].[branch_id]
								WHERE  [UserProfile].[Status] = ?

			   ) AS A
               WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1";


        $result = $this->db->query($sql,array(/*$username,*/'L'));

        //return the query result
        return $result->result_array();

    }
    public function get_users_to_modify($limit,$start)
    {

        //this method gets the full list of users who can have their profiles modified

        //this method retrieves users and facilitates pagination
        //get all the unlocked users
        $sql = "SELECT * FROM (
                          SELECT row_number() OVER (ORDER BY FullName) AS rownum, [Username],[Branch],[branch_name],[FullName],
                                [RoleID], [UserID],[RoleName],
                                CONVERT(VARCHAR(19),LastLogin) AS [LastLogin], [LastPasswordChange],
								[RoleName],[UserProfile].[Status]
                                FROM  [pafupi].[dbo].[UserProfile]
								INNER JOIN [pafupi].[dbo].[UserRole]
								ON [UserRole].[RoleID] = [UserProfile].[RoleID]
								INNER JOIN [pafupi].[dbo].[Branches]
				                ON [UserProfile].[Branch] = [Branches].[branch_id]

			   ) AS A
               WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1";


        $result = $this->db->query($sql);

        //return the query result
        return $result;

    }
    public function get_no_users_modify()
    {
        //this method fetches the total number of users
        //whose accounts are eligible for modification

        //lets initialise count to 0, it will hold the total
        //number of users
        $count = 0;


        //some SQL to find the number of users
        $sql_num_users = 'SELECT COUNT([Username]) AS [user_count] ';
        $sql_num_users.= 'FROM [pafupi].[dbo].[UserProfile] ';

        $result = $this->db->query($sql_num_users);

        //now loop thru the result set
        foreach($result->result() as $row)
        {

            $count = $row->user_count;
        }

        //return count to the calling method
        return $count;

    }
    public function delete_users($selected_users)
    {


        //this method deletes one or more users from the system

        //get the length of the array
        $array_length = count($selected_users);

        $username_admin = $this->session->userdata('username');

        $username = NULL;
        $branch   = NULL;
        $deleted  = array();
        //lets get the username and branch of the user we would like to delete
        for($i = 0; $i < $array_length; $i++)
        {

            $sql_select = 'SELECT [Username],[Branch] ';
            $sql_select.= 'FROM [pafupi].[dbo].[UserProfile] ';
            $sql_select.= 'WHERE [UserID] = ? ';

            $result = $this->db->query($sql_select,array($selected_users[$i]));

            foreach($result->result() as $row)
            {

                $username = $row->Username;
                $branch   = $row->Branch;

            }

            //generate a log message
            $log_message = 'User Deletion : ' . $username;
            $log_message.= ' (' . $branch . ')';

            //$this->Authentication->log_activity($log_message, $username_admin);

            //delete the user from the system
            $sql_delete = 'DELETE FROM [pafupi].[dbo].[UserProfile] ';
            $sql_delete.= 'WHERE [UserID] = ? ';

            $result =$this->db->query($sql_delete,array($selected_users[$i]));
            if($result){
                array_push($deleted,$selected_users[$i]);
            }


        }
        return $deleted;
    }
    public function lock_users($selected_users)
    {
        //get the length of the array
        $array_length = count($selected_users);

        //get the username
        $username = $this->session->userdata('username');
        $locked = array();
        //generate a date
        $date = Common_Tasks::generate_date();

        for($i = 0; $i < $array_length; $i++)
        {

            $sql_lock = 'UPDATE [pafupi].[dbo].[UserProfile]
						 SET [Status] = ?,
						 [LockDate] = ?,
						 [LockedBy] = ?
			             WHERE [UserID] = ?';

            $result =$this->db->query($sql_lock,array('L',$date,$username,$selected_users[$i]));
            if($result){
                array_push($locked,$selected_users[$i]);
            }
        }

        return $locked;
    }
    public function unlock_users($selected_users)
    {

        //get the length of the array
        $array_length = count($selected_users);
        $unlocked = array();
        //get the username
        $username = $this->session->userdata('username');

        //generate a date
        $date = Common_Tasks::generate_date();

        for($i = 0; $i < $array_length; $i++)
        {

            $sql_unlock = 'UPDATE [pafupi].[dbo].[UserProfile]
						   SET [Status] = ?,
						   [UnlockDate] = ?,
						   [UnlockedBy] = ?
			               WHERE [UserID] = ?';


            $result = $this->db->query($sql_unlock,array('A',$date,$username,$selected_users[$i]));

            if($result){
                array_push($unlocked,$selected_users[$i]);
            }
        }
        return $unlocked;
    }
    public function reset_password($user_id,$new_password)
    {

        //this method resets a user's password upon admin request
        //hash the new password
        $hashed_password = Security::hash_password($new_password);

        //generate a UNIX timestamp
        $time =  date('Y-m-d H:i:s');

        $sql_reset = 'UPDATE [pafupi].[dbo].[UserProfile]
		              SET [Password] = ?,
					  [LastPasswordChange] = ?,
					  [FirstLogin] = ?
					  WHERE [UserID] = ?';


        $update = $this->db->query($sql_reset,array($hashed_password,$time,0,$user_id));

        //get the userole, username and branch based on user_id
        $sql_details = 'SELECT [Username],[Branch],[RoleName] ';
        $sql_details.= 'FROM [pafupi].[dbo].[UserProfile] ';
        $sql_details.= 'INNER JOIN [pafupi].[dbo].[UserRole] ';
        $sql_details.= 'ON [UserProfile].[RoleID] = [UserRole].[RoleID] ';
        $sql_details.= 'WHERE [UserProfile].[UserID] = ? ';

        $result = $this->db->query($sql_details,array($user_id));

        foreach($result->result() as $row)
        {

            $username = $row->Username;
            $branch   = $row->Branch;

        }

        //log the password change
        $username_admin = $this->session->userdata('username');
        $user_activity  = 'Password reset : ';
        $user_activity .= $username . ' ' . $branch;

        Authentication::log_activity($user_activity, $username_admin);
        return $update;

    }
    public function get_total_users()
    {

        //this method returns the total number of users on the system
        $count = 0;

        $sql_total = 'SELECT COUNT([Status]) AS [total_users] ';
        $sql_total.= 'FROM [pafupi].[dbo].[UserProfile] ';

        //return the total number of users
        $result = $this->db->query($sql_total);

        foreach($result->result() as $row)
        {

            $count = $row->total_users;

        }


        return $count;
    }
    public function get_locked_users($percentage=false)
    {

        $count = 0;

        //this method returns the percentage of locked users
        $sql_locked = 'SELECT COUNT([Status]) AS [locked_users] ';
        $sql_locked.= 'FROM [pafupi].[dbo].[UserProfile] ';
        $sql_locked.= 'WHERE [Status] = ?';

        //get the total number of locked users
        $result = $this->db->query($sql_locked,'L');

        foreach($result->result() as $row)
        {

            $count = $row->locked_users;

        }

        //get total number of users
        $total = $this->get_total_users();

        //now get the percentage
        if($percentage){
            $percentage_locked = round(($count/$total) * 100,0);

            return $percentage_locked;
        }
        else{
            return $count;
        }

    }
    public function get_unlocked_users($percentage=false)
    {

        $count = 0;

        //this method returns the percentage of unlocked users
        $sql_unlocked = 'SELECT COUNT([Status]) AS [unlocked_users] ';
        $sql_unlocked.= 'FROM [pafupi].[dbo].[UserProfile] ';
        $sql_unlocked.= 'WHERE [Status] = ? ';

        //get the total number of unlocked users
        $result = $this->db->query($sql_unlocked,'A');

        foreach($result->result() as $row)
        {

            $count = $row->unlocked_users;

        }

        //get total number of users
        $total = $this->get_total_users();

        //now get the percentage
        if($percentage){
            $percentage_unlocked = round(($count/$total) * 100,0);

            return $percentage_unlocked;
        }
        else{
            return $count;
        }

    }
    public function get_expired_passwords($percentage=false)
    {

        $count = 0;

        //this method returns the percentage of expired passwords
        $sql_locked = 'SELECT COUNT([Status]) AS [expired_passwords] ';
        $sql_locked.= 'FROM [pafupi].[dbo].[UserProfile] ';
        $sql_locked.= 'WHERE [Status] = ? ';

        //get the total number of unlocked users
        $result = $this->db->query($sql_locked,'E');

        foreach($result->result() as $row)
        {

            $count = $row->expired_passwords;

        }

        //get total number of users
        $total = $this->get_total_users();
        if($percentage){
            //now get the percentage
            $percentage_expired = round(($count/$total) * 100);

            return $percentage_expired;
        }
        else{
            return $total;
        }

    }
    public function get_user_info($user_id)
    {

        //this method retrieves a user's details given an ID

        //some sql to retrieve user data
        $sql_user = 'SELECT [FullName],[UserName],[UserID],CONVERT(VARCHAR(19),LastLogin) AS [LastLogin],[Branch],[RoleName],[branch_name],[UserProfile].[RoleID]
		             FROM [pafupi].[dbo].[UserProfile]
					 INNER JOIN [pafupi].[dbo].[UserRole]
					 ON [UserRole].[RoleID] = [UserProfile].[RoleID]
					 INNER JOIN [pafupi].[dbo].[Branches]
					 ON [UserProfile].[Branch] = [Branches].[branch_id]
					 WHERE [UserID] = ?';

        $result = $this->db->query($sql_user,array($user_id));

        return $result->result_array();

    }
    public function search_lock($parameter)
    {

        $parameter = "%" . $parameter . "%";

        $username = $this->session->userdata('username');

        $search = 'SELECT [Username],[Branch],[FullName],
                  [UserRole].[RoleID], [UserID],
                  CONVERT(VARCHAR(19),LastLogin) AS [LastLogin],[LastPasswordChange],
                  [RoleName],[UserProfile].[Status]
                  FROM [pafupi].[dbo].[UserProfile]
                  INNER JOIN [pafupi].[dbo].[UserRole]
                  ON [UserRole].[RoleID] = [UserProfile].[RoleID]
                  WHERE [Username] != ?
                  AND [UserProfile].[Status] = ?
                  AND ([Username] LIKE ?
                  OR [FullName] LIKE ?
                  OR [Branch] LIKE ?)';

        $data['result'] = $this->db->query($search,array($username,'L',$parameter,$parameter,$parameter))->result_array();

        //do a count
        $count  = 'SELECT COUNT(*) AS [Total]
                   FROM [pafupi].[dbo].[UserProfile]
                   INNER JOIN [pafupi].[dbo].[UserRole]
                   ON [UserRole].[RoleID] = [UserProfile].[RoleID]
                   WHERE [Username] != ?
                   AND [UserProfile].[Status] = ?
                   AND ([Username] LIKE ?
                   OR [FullName] LIKE ?
                   OR [Branch] LIKE ?)';

        $result = $this->db->query($count,array($username,'L',$parameter,$parameter,$parameter));

        foreach($result->result() as $row) {

            $total =  $row->Total;

        }

        $data['total'] = $total;

        return $data;


    }
    public function search_unlock($parameter)
    {

        $parameter = "%" . $parameter . "%";

        $username = $this->session->userdata('username');

        $search = 'SELECT [Username],[Branch],[FullName],[branch_id],[RoleName],
                  [UserProfile].[RoleID], [UserID],
                  CONVERT(VARCHAR(19),LastLogin) AS [LastLogin],[LastPasswordChange],
                  [RoleName],[UserProfile].[Status]
                  FROM [pafupi].[dbo].[UserProfile]
                  INNER JOIN [pafupi].[dbo].[UserRole]
                  ON [UserRole].[RoleID] = [UserProfile].[RoleID]
                  INNER JOIN [pafupi].[dbo].[Branches]
				  ON [UserProfile].[Branch] = [Branches].[branch_id]
                  WHERE  [UserProfile].[Status] = ?
                  AND ([Username] LIKE ?
                  OR [FullName] LIKE ?
                  OR [Branch] LIKE ?)';

        $data['result'] = $this->db->query($search,array(/*$username,*/'A',$parameter,$parameter,$parameter))->result_array();

        //do a count
        $count  = 'SELECT COUNT(*) AS [Total]
                   FROM [pafupi].[dbo].[UserProfile]
                   INNER JOIN [pafupi].[dbo].[UserRole]
                   ON [UserRole].[RoleID] = [UserProfile].[RoleID]
                   INNER JOIN [pafupi].[dbo].[Branches]
				   ON [UserProfile].[Branch] = [Branches].[branch_id]
                   WHERE  [UserProfile].[Status] = ?
                   AND ([Username] LIKE ?
                   OR [FullName] LIKE ?
                   OR [Branch] LIKE ?)';

        $result = $this->db->query($count,array(/*$username,*/'A',$parameter,$parameter,$parameter));

        foreach($result->result() as $row) {

            $total =  $row->Total;

        }

        $data['total'] = $total;

        return $data;


    }
    public function search_modify($parameter) {


        $parameter = "%" . $parameter . "%";
        $data = array();
        $username = $this->session->userdata('username');

        $search = 'SELECT [Username],[Branch],[FullName],[branch_name],[RoleName],
                  [UserProfile].[RoleID], [UserID],
                  CONVERT(VARCHAR(19),LastLogin) AS [LastLogin],[LastPasswordChange],
                  [RoleName],[UserProfile].[Status]
                  FROM [pafupi].[dbo].[UserProfile]
                  INNER JOIN [pafupi].[dbo].[UserRole]
                  ON [UserRole].[RoleID] = [UserProfile].[RoleID]
                  INNER JOIN [pafupi].[dbo].[Branches]
					 ON [UserProfile].[Branch] = [Branches].[branch_id]
                  WHERE [Username] LIKE ?
                  OR [FullName] LIKE ?
                  OR [Branch] LIKE ?';

        $data['result'] = $this->db->query($search,array(/*$username,*/$parameter,$parameter,$parameter))->result_array();

        //do a count
        $count  = 'SELECT COUNT(*) AS [Total]
                   FROM [pafupi].[dbo].[UserProfile]
                   INNER JOIN [pafupi].[dbo].[UserRole]
                   ON [UserRole].[RoleID] = [UserProfile].[RoleID]
                   INNER JOIN [pafupi].[dbo].[Branches]
					 ON [UserProfile].[Branch] = [Branches].[branch_id]
                   WHERE [Username] LIKE ?
                   OR [FullName] LIKE ?
                   OR [Branch] LIKE ?';

        $result = $this->db->query($count,array(/*$username,*/$parameter,$parameter,$parameter));

        foreach($result->result() as $row) {

            $total =  $row->Total;

        }

        $data['total'] = $total;

        return $data;


    }


    public function update_user($user_id,$data)
    {

        //this method updates a user account
        //get the data sent from the form
        //$user_id = $data['user_id'];
        $username_admin = $this->session->userdata('username');
        //sanitize input
        $firstname = ucwords(strtolower(Validation::sanitise_input($data['fname'])));
        $lastname  = ucwords(strtolower(Validation::sanitise_input($data['lname'])));

        $full_name = $firstname . ' ' . $lastname;
        $branch    = $data['branch'];
        $user_role = $data['user_role'];

        //generate a username from the first and last name
        $first_fragment  = trim(strtolower(substr($firstname,0,1)));
        $second_fragment = trim(strtolower($lastname));
        $username        = $first_fragment . $second_fragment;
        $username        = Validation::sanitise_input($username,TRUE);

        $sql_update_user = 'UPDATE [pafupi].[dbo].[UserProfile]
		                    SET [FullName] = ?,
							[Branch] = ?,
							[RoleID] = ?
							WHERE [UserID] = ?';

        $result = $this->db->query($sql_update_user,array($full_name,$branch,$user_role,$user_id));

        //logging
        $username_admin = $this->session->userdata('username');
        $user_activity  = 'User Modification : ';
        $user_activity .= $username . ' ' . $branch;

        Authentication::log_activity($user_activity, $username_admin);
        return $result;
    }
    public function get_details($username)
    {

        //this method returns a user's details
        $sql_details = 'SELECT [RoleName], [Username], [UserProfile].[RoleID], [Branch],';
        $sql_details.= '[UserProfile].[UserID],[FullName], [LastPasswordChange], CONVERT(VARCHAR(19),LastLogin) AS [LastLogin] ';
        $sql_details.= 'FROM [pafupi].[dbo].[UserRole] ';
        $sql_details.= 'INNER JOIN [pafupi].[dbo].[UserProfile] ';
        $sql_details.= 'ON [UserRole].[RoleID] = [UserProfile].[RoleID] ';
        $sql_details.= 'WHERE [Username] = ? ';

        $result = $this->db->query($sql_details,array($username));

        foreach($result->result() as $row)
        {

            $user_details['RoleName']           = $row->RoleName;
            $user_details['Username']           = $row->Username;
            $user_details['UserRoleID']         = $row->RoleID;
            $user_details['UserID']             = $row->UserID;
            $user_details['FullName']           = $row->FullName;
            $user_details['LastPasswordChange'] = $row->LastPasswordChange;
            $user_details['LastLogin']          = strtotime($row->LastLogin);
            $user_details['Branch']             = $row->Branch;

        }

        return $user_details;

    }
    function __destruct() {
        $this->db->close();
    }
}