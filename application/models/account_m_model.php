<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Account_m_model extends CI_Model {
    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        //$this->load->library('common_functions');

    }
    public function test(){
        $result = $this->db->query("SELECT getdate()");
        var_dump($result);exit;
    }
    public function assignOTP(){
        try{
        $accounts = $this->getToLink();
        $booklets = $this->get_active_otp_books();
        $referals = $this->get_active_referal_books();
        if(count($booklets) < 1){
            return;
        }
        if(count($referals) < 1){
            return;
        }
        $num = 0;
        for($i=0;$i<count($accounts);$i++){

            if($accounts[$i]['acc_status'] == 'carded'){
                //$status = 'starterpack';
                $status = 'linked';
            }
            elseif($accounts[$i]['acc_status'] == 'precarded'){
                $status = 'linked';
            }
            else{
               continue;
            }

            $sql_book_update = 'UPDATE [pafupi].[dbo].[booklet]
								 SET [acc_no]=?
								 WHERE [book_no]=?
								 ';


            $sql_referal_update = 'UPDATE [pafupi].[dbo].[referal_book]
						 SET [acc_no]=?
						 WHERE [referal_no] =?';

            $sql_account_status = 'UPDATE [pafupi].[dbo].[account_status]
						 SET [acc_status] = ? WHERE [acc_no]=?';


            $this->db->trans_start();
            $this->db->query($sql_referal_update,array($accounts[$i]['acc_no'],$referals[$i]['referal_no']));
            $this->db->query($sql_book_update,array($accounts[$i]['acc_no'],$booklets[$i]['book_no']));
            $this->db->query($sql_account_status,array($status,$accounts[$i]['acc_no']));
            $this->db->trans_complete();

            if ($this->db->trans_status()== TRUE){
                $num++;
            }
            else{

            }
        }

        return $num;
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function updateOTP($data,$valid_books){
        $errors = array();
        $num_otp_book = 0;
        try{
        if(count($valid_books) < 1){
            array_push($errors,"No valid books");
            return $errors;
        }
        for($i=0;$i<count($valid_books);$i++){
            $num = 0;
            foreach($data as $d){
                if($this->checkOTPValid(@$d['trans_pin'],$valid_books[$i])){
                    $num++;

                }
            }
            if($num == 50){

                $sql_book_update = 'UPDATE [pafupi].[dbo].[booklet]
								 SET [active]=?
								 WHERE [book_no]=?
								 ';

                $result = $this->db->query($sql_book_update,array(true,$valid_books[$i]));


                if ($result){
                    array_push($errors,"OTP Book number ".$valid_books[$i]." successfully updated ");

                    $num_otp_book++;

                }
                else{
                    array_push($errors,"OTP Book number ".$valid_books[$i]." NOT successfully updated ");

                }

            }
            else{
                array_push($errors,"OTP Book number ".$valid_books[$i]." is invalid ");
            }
        }

        $saved['books']=$num_otp_book;
        $saved['accounts'] = $this->assignOTP();
        array_push($errors,$saved);

        return $errors;
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function updateReferals($data,$valid_refs){
        $errors = array();
        $num_otp_book = 0;
        try{
        if(count($valid_refs) < 1 ){
            array_push($errors,"No valid books");
            return $errors;
        }

        $num_ref_ =0;
        $saved = array();
        for($i=0;$i<count($valid_refs);$i++){
            $num = 0;
            foreach($data as $d){
                if( $this->checkRefValid(@$d['referal_coupons'],$valid_refs[$i])){
                    $num++;
                }
            }
            if($num ==10){
                ;                $sql_referal_update = 'UPDATE [pafupi].[dbo].[referal_book]
						 SET [active]=?
						 WHERE [referal_no] =?';
                $result = $this->db->query($sql_referal_update,array(true,$valid_refs[$i]));
                if ($result){
                    array_push($errors,"Referal Book number ".$valid_refs[$i]." successfully updated ");
                    $num_ref_++;
                }
                else{
                    array_push($errors,"Referal Book number ".$valid_refs[$i]." NOT successfully updated ");

                }

            }
            else{
                array_push($errors,"Referal Book number ".$valid_refs[$i]." is invalid ");
            }
        }
        $saved['refs'] = $num_ref_;
        $saved['accounts'] = $this->assignOTP();
        array_push($errors,$saved);

        return $errors;
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function checkOTPValid($otp_no,$book_no){
        $sql = 'SELECT COUNT(*) as count FROM [pafupi].[dbo].[trans_pins]
                WHERE [trans_pin]=? AND [book_no]=?';
        $result = $this->db->query($sql,array($otp_no,$book_no));
        $count = 0;
        foreach($result->result() as $row){
            $count = $row->count;
        }

        if($count>=1){
            return true;
        }
        else{
            return false;
        }

    }
    public function checkRefValid($coupon_no,$ref_book_no){
        $sql = 'SELECT COUNT(*) as count FROM [pafupi].[dbo].[referal_coupons]
                WHERE [coupon_no]=? AND [referal_no]=?';
        $result = $this->db->query($sql,array($coupon_no,$ref_book_no));
        $count = 0;
        foreach($result->result() as $row){
            $count = $row->count;
        }

        if($count>=1){
            return true;
        }
        else{
            return false;
        }

    }
    public function linkBooks($data){
        $num = 0;
        $done = array();
        try{
        if(!$data){
            $accounts = $this->getToLink();

            for($i=0;$i<count($accounts);$i++){
                $data[$i] = $accounts[$i]['acc_no'];
            }
        }
        //become carded, active set to true $data[$i]
        $unassigned_books = $this->getUnassignedBooklets();
        $unassigned_refs = $this->getUnassignedReferals();
        if(count($data) < 1){
            array_push($done,'no available accounts'); return 'no available accounts';}
        if(count($unassigned_refs) < 1){
            array_push($done,'no available referrals'); return 'no available referrals';}
        if(count($unassigned_books) < 1){
            array_push($done,'no  available booklets'); return 'no available booklets';}
        $counting = max(count($unassigned_books),count($unassigned_refs));
        if($counting < 1){
            array_push($done,'no  available booklets'); return 'no available booklets';}
        for($i=0;$i<$counting;$i++){
            //get all unassigned booklets

            //get unassigned referals
            //$unassigned_books = $this->getUnassignedBooklets();
            //$unassigned_refs = $this->getUnassignedReferals();

            if(count($unassigned_refs) < 1){
                array_push($done,'no available referrals');break; }
            if(count($unassigned_books) < 1){
                array_push($done,'no available booklets');break; }
            if(count($unassigned_books) > 0 &&  count($unassigned_refs) > 0 ){
                $sql_book_update = 'UPDATE [pafupi].[dbo].[booklet]
						 SET [acc_no] = ?,[active]=?
						 WHERE [book_no]=?
			             ';
                $sql_referal_update = 'UPDATE [pafupi].[dbo].[referal_book]
						 SET [acc_no] = ?,[active]=?
						 WHERE [referal_no] =?
			             ';
                $sql_account_status = 'UPDATE [pafupi].[dbo].[account_status]
						 SET [acc_status] = ? WHERE [acc_no]=?';
                $this->db->trans_start();
                $this->db->query($sql_book_update,array($data[$i],true,$unassigned_books[$i]['book_no']));
                $this->db->query($sql_referal_update,array($data[$i],true,$unassigned_refs[$i]['referal_no']));
                $this->db->query($sql_account_status,array('linked',$data[$i]));
                $this->db->trans_complete();

                if ($this->db->trans_status()== TRUE){
                    //array_push($done,'account no '.$data[$i].' linked to to booklet '.$unassigned_books[$i]['book_no']." and referal ".$unassigned_refs[$i]['referal_no']);
                    $num++;
                }
                else{
                    array_push($done,'account no '.$data[$i].' failed');
                }
            }
            else{
                array_push($done,'no available booklets or referrals');
            }

        }
        //array_push($done,$num." account(s) successfully assigned");
        $res = $num." account(s) successfully assigned";

        return $res;}
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function getUnassignedBooklets(){
        $sql = 'SELECT [book_no] FROM [pafupi].[dbo].[booklet]
                WHERE [acc_no]=? AND [active]=?';
        $result = $this->db->query($sql,array('0',false))->result_array();
        $this->db->close();
        return $result;
    }
    public function getUnassignedReferals(){
        $sql = 'SELECT [referal_no] FROM [pafupi].[dbo].[referal_book]
                WHERE [acc_no]=? AND [active]=?';
        $result = $this->db->query($sql,array('0',false))->result_array();

        return $result;
    }
    public function getAccountBookPins($acc_no=false){
        try{
        if($acc_no){
            $sql = 'SELECT [trans_id]
                     ,[trans_pin]
                      ,[trans_pins].[book_no]
                FROM [pafupi].[dbo].[trans_pins]
                INNER JOIN [pafupi].[dbo].[booklet]
                ON [trans_pins].[book_no] = [booklet].[book_no]
                INNER JOIN [pafupi].[dbo].[account_status]
                ON [booklet].[acc_no] = [account_status].[acc_no]
                WHERE [acc_status]=? AND [booklet].[active]=? AND [booklet].[acc_no]=?
                ';
            $result = $this->db->query($sql,array('precarded',false,'0'));
            return $result->result_array();
        }
        else{


            $sql = 'SELECT [trans_id]
                     ,[trans_pin]
                      ,[trans_pins].[book_no]
                FROM [pafupi].[dbo].[trans_pins]
                INNER JOIN [pafupi].[dbo].[booklet]
                ON [trans_pins].[book_no] = [booklet].[book_no]
                WHERE [booklet].[acc_no]=? AND [booklet].[active]=?
               ';
            $result = $this->db->query($sql,array('0',false));

            return $result->reslult_array();
        }
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function getReferalPins($acc_no =false){
        try{
        if($acc_no){
            $sql = 'SELECT [coupon_no]
                  ,[referal_coupons].[referal_no]
                  ,[coupon_id]
              FROM [pafupi].[dbo].[referal_coupons]
              INNER JOIN [pafupi].[dbo].[referal_book]
              ON [referal_coupons].[referal_no] = [referal_book].[referal_no]
              INNER JOIN [pafupi].[dbo].[account_status]
              ON [referal_book].[acc_no] = [account_status].[acc_no]
              WHERE  [referal_book].[acc_no]=? AND [referal_book].[active]=?
              ';
            $result = $this->db->query($sql,array('0',false));
            return $result->result_array();
        }
        else{
            $sql = 'SELECT [coupon_no]
                  ,[referal_coupons].[referal_no]
                  ,[coupon_id]
              FROM [pafupi].[dbo].[referal_coupons]
              INNER JOIN [pafupi].[dbo].[referal_book]
              ON [referal_coupons].[referal_no] = [referal_book].[referal_no]
              WHERE  [referal_book].[acc_no]=? AND [referal_book].[active]=?
             ';
            $result = $this->db->query($sql,array('0',false));

            return $result->result_array();
        }
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function getBranches($branch_id=false){
        if($branch_id){
            //$query = $this->db->get_where('branches', array('branch_id' => $branch_id));
            $q = 'SELECT [branch_name],[branch_id]
		             FROM [pafupi].[dbo].[branches]
					 WHERE [branch_id] = ?';

            $result = $this->db->query($q,array($branch_id));
            return $result->result_array();
        }
        else{
            $q = 'SELECT [branch_name],[branch_id]
		          FROM [pafupi].[dbo].[branches]';
            $result = $this->db->query($q);

            return $result->result_array();
        }
    }
    public function saveAccount($data){

        $feedback = array();
        $account = array();
        $username =  $this->session->userdata('username');
        $user_id =  $this->session->userdata('user_id');
        //$data['branch_id'] = $this->session->userdata('branch');
        try{
        $this->db->trans_start();
        $sql_create_account = 'INSERT INTO [pafupi].[dbo].[pafupi_account]
		                       ([starter_id],[acc_no],[branch_id],[opened_by],[customer_id])
							   VALUES(?,?,?,?,?)';
        $sql_create_customer = 'INSERT INTO [pafupi].[dbo].[Customer]
                               ([CustomerID])
                               VALUES(?)';
        $sql_create_customer_profile = 'INSERT INTO [pafupi].[dbo].[CustomerProfile]
                               ([CustomerID])
                               VALUES(?)';
        $sql_create_customer_account = 'INSERT INTO [pafupi].[dbo].[CustomerAccount]
                               ([CustomerID])
                               VALUES(?)';
        //change
        //$data['customer_id'] =  substr($data['account_number'],3,6);
        $this->db->query($sql_create_customer,array($data['customer_id']));
        $this->db->query($sql_create_customer_profile,array($data['customer_id']));
        $this->db->query($sql_create_customer_account,array($data['customer_id']));
        $this->db->query($sql_create_account,array($this->generateStarterId(),$data['account_number'],$data['branch_id'],$user_id,$data['customer_id']));

        $this->db->trans_complete();
        $result = $this->db->trans_status();
        if($result){

            array_push($account,array('account' =>'account no '.$data['account_number'].' created'));
            $sql_select = 'SELECT [acc_no],[branch_id],[date_created],[opened_by]
		                   FROM [pafupi].[dbo].[pafupi_account]
					       WHERE [acc_no] = ?';

            $result = $this->db->query($sql_select,array($data['account_number']));
            foreach ($result->result() as $row)
            {
                $cust_id = substr($row->acc_no,4,6);
                //$done = $this->makeBooklet($row->acc_no,$cust_id);
                //$done2 = $this->makeCoupon($row->acc_no,$cust_id);
                $this->log_activity('Account creation', $username,$row->acc_no);
                $sql_account = 'INSERT INTO [pafupi].[dbo].[account_status]
		                    ([acc_no],[dispatch_flag],[lock_status],[acc_status])
							 VALUES(?,?,?,?)';
                $result =$this->db->query($sql_account,array($row->acc_no,false,false,'precarded'));
                if($result){
                    $this->log_activity('Account creation-status inserted', $username,$row->acc_no);
                }
                else{
                    $this->log_activity('Account creation-status insertion failed', $username,$row->acc_no);
                }
                //array_push($account,$done);
                //return $done;
            }
            return true;
        }
        else{
            return false;
            //array_push($account,array('account' =>'account no '.$data['account_number'].' NOT saved in database'));
        }
        //array_push($feedback,$account);

        return true;
        }
        catch(Exception $e){
           return false;
        }
    }
    public function accountExists($acc_no){
        $sql_select = 'SELECT COUNT([acc_no]) as total
		             FROM [pafupi].[dbo].[pafupi_account]
					 WHERE [acc_no] = ?';

        $query = $this->db->query($sql_select,array($acc_no));
        foreach($query->result() as $row){
            if($row->total >=1){
                return true;
            }
            else{
                return false;
            }
        }

        return false;
    }
    public function get_total_accounts(){
        $count = 0;
        try{
        $sql_account = 'SELECT COUNT(*) as [total_accounts]
		             FROM [pafupi].[dbo].[pafupi_account]';
        $result = $this->db->query($sql_account);
        $count = 0;
        foreach($result->result() as $row)
        {

            $count = $row->total_accounts;

        }

        return $count;
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function getFullyCount($percentage=false){
        $count = 0;
        try{
        $sql_account = 'SELECT COUNT([pafupi_account].[acc_no]) as [fully_accounts]
		              FROM [pafupi].[dbo].[pafupi_account]
                                         INNER JOIN [pafupi].[dbo].[branches]
                                         ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [pafupi].[dbo].[account_card]
                                         ON [pafupi_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[Customer]
                                         ON [pafupi_account].[customer_id] = [Customer].[CustomerID]
                                          INNER JOIN [pafupi].[dbo].[CustomerProfile]
                                         ON [pafupi_account].[customer_id] = [CustomerProfile].[CustomerID]
                                         INNER JOIN [pafupi].[dbo].[account_status]
                                         ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[account_mobility]
                                         ON [pafupi_account].[acc_no] = [account_mobility].[acc_no]
                                         INNER JOIN [pafupi].dbo.[UserProfile]
                                         ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                                         WHERE [CustomerProfile].[CustomerGender] !=?';

        $result = $this->db->query($sql_account,array('NULL'));
        foreach($result->result() as $row)
        {

            $count = $row->fully_accounts;

        }
        $total_accounts = $this->get_total_accounts();
        $this->db->close();
        if($percentage){
            if($total_accounts==0) $total_accounts=1;
            $percentage_fully = round(($count/$total_accounts) * 100,0);

            return $percentage_fully;
        }
        else{
            return $count;
        }
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function getCardedCount($percentage=false){
        $count = 0;
        try{
        $sql_account = 'SELECT COUNT([pafupi_account].[acc_no]) as [carded_accounts]
		              FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[account_card]
					 ON [pafupi_account].[acc_no] = [account_card].[acc_no]
					 INNER JOIN [pafupi].[dbo].[account_status]
                     ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                     INNER JOIN [pafupi].dbo.[UserProfile]
                     ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
					 WHERE [account_status].[acc_status]=?';

        $result = $this->db->query($sql_account,array('carded'));
        foreach($result->result() as $row)
        {

            $count = $row->carded_accounts;

        }
        $total_accounts = $this->get_total_accounts();
        $this->db->close();
        if($percentage){
            if($total_accounts==0) $total_accounts=1;
            $percentage_carded = round(($count/$total_accounts) * 100,0);

            return $percentage_carded;
        }
        else{
            return $count;
        }
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function getWithCardAccounts($user_id=false,$acc_no=false){
        try{
        if($user_id && !$acc_no){

            $sql_account = 'SELECT [pafupi_account].*,[branches].[branch_name],[account_card].[card_no],[account_status].[acc_status],[lock_status]
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[account_card]
					 ON [pafupi_account].[acc_no] = [account_card].[acc_no]
					 INNER JOIN [account_status]
					 ON [pafupi_account].[acc_no] = [account_status].[acc_no]
					 WHERE [account_status].[acc_status] =? AND [account_status].[dispatch_flag]=? AND [pafupi_account].[opened_by]=?';

            $query = $this->db->query($sql_account,array('carded',FALSE,$user_id));

            return $query->result_array();
        }
        else if($user_id && $acc_no){

            $sql_account = 'SELECT [pafupi_account].*,[branches].[branch_name],[account_card].[card_no],[account_status].[acc_status],[lock_status]
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[account_card]
					 ON [pafupi_account].[acc_no] = [account_card].[acc_no]
					 INNER JOIN [account_status]
					 ON [pafupi_account].[acc_no] = [account_status].[acc_no]
					 WHERE [account_status].[acc_status] =? AND [pafupi_account][acc_no]=? AND [account_status].[dispatch_flag]=? AND [pafupi_account].[opened_by]=?';

            $query = $this->db->query($sql_account,array('carded',FALSE,$acc_no,FALSE,$user_id));

            return $query->result_array();
        }
        else if(!$user_id && $acc_no){

            $sql_account = 'SELECT [pafupi_account].*,[branches].[branch_name],[account_card].[card_no],[account_status].[acc_status],[lock_status]
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[account_card]
					 ON [pafupi_account].[acc_no] = [account_card].[acc_no]
					 INNER JOIN [account_status]
					 ON [pafupi_account].[acc_no] = [account_status].[acc_no]
					 WHERE [account_status].[acc_status] =? AND [account_status].[dispatch_flag]=? AND [pafupi_account][acc_no]=? ';

            $query = $this->db->query($sql_account,array('carded',FALSE,$acc_no));

            return $query->result_array();
        }
        else{

            $sql_account = 'SELECT [pafupi_account].*,[branches].[branch_name],[account_card].[card_no],[account_status].[acc_status],[lock_status]
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[account_card]
					 ON [pafupi_account].[acc_no] = [account_card].[acc_no]
					 INNER JOIN [account_status]
					 ON [pafupi_account].[acc_no] = [account_status].[acc_no]
					 WHERE [account_status].[acc_status] !=NULL AND [account_status].[dispatch_flag]=FALSE';

            $query = $this->db->query($sql_account,array('carded',FALSE));

            return $query->result_array();

        }
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function getCardAccounts($user_id=false,$acc_no=false){
        try{
        if($user_id && !$acc_no){

            $sql_account = 'SELECT [pafupi_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName],[book_no],[referal_no]
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[account_card]
					 ON [pafupi_account].[acc_no] = [account_card].[acc_no]
					 INNER JOIN [pafupi].[dbo].[booklet]
                     ON [pafupi_account].[acc_no] = [booklet].[acc_no]
                     INNER JOIN [pafupi].[dbo].[referal_book]
                     ON [pafupi_account].[acc_no] = [referal_book].[acc_no]
					 INNER JOIN [pafupi].[dbo].[account_status]
                     ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                     INNER JOIN [pafupi].dbo.[UserProfile]
                     ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
					 WHERE [pafupi_account].[opened_by]=? AND [account_status].[acc_status]=?';

            $query = $this->db->query($sql_account,array($user_id,'carded'));

            return $query->result_array();
        }
        else if($user_id && $acc_no){

            $sql_account = 'SELECT [pafupi_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName]
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[account_card]
					 ON [pafupi_account].[acc_no] = [account_card].[acc_no]
					 INNER JOIN [pafupi].[dbo].[account_status]
                     ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                     INNER JOIN [pafupi].dbo.[UserProfile]
                     ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
					 WHERE [pafupi_account].[opened_by]=? AND [pafupi_account].[acc_no]=? AND [account_status].[acc_status]=?';

            $query = $this->db->query($sql_account,array($user_id,$acc_no,'carded'));

            return $query->result_array();
        }
        else if(!$user_id && $acc_no){

            $sql_account = 'SELECT [pafupi_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName]
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[account_card]
					 ON [pafupi_account].[acc_no] = [account_card].[acc_no]
					 INNER JOIN [pafupi].[dbo].[account_status]
                     ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                     INNER JOIN [pafupi].dbo.[UserProfile]
                     ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
					 WHERE [pafupi_account].[acc_no]=? AND [account_status].[acc_status]=?';

            $query = $this->db->query($sql_account,array($acc_no,'carded'));
                    return $query->result_array();
        }
        else{

            $sql_account = 'SELECT [pafupi_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName]
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[account_card]
					 ON [pafupi_account].[acc_no] = [account_card].[acc_no]
					 INNER JOIN [pafupi].[dbo].[account_status]
                     ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                     INNER JOIN [pafupi].dbo.[UserProfile]
                     ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
					 WHERE [account_status].[acc_status]=?
					 ';

            $query = $this->db->query($sql_account,'carded');
            return $query->result_array();
        }
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }

    } /*get accounts from pre-linked paginate */
    public function get_all_carded_accounts($limit,$start,$username=false)
    {
        try{
        //get the username
        if(!$username){
            $sql = "SELECT * FROM (
                                      SELECT row_number() OVER (ORDER BY [pafupi_account].acc_no) AS rownum,[pafupi_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName],[referal_no],[book_no]
                                         FROM [pafupi].[dbo].[pafupi_account]
                                         INNER JOIN [pafupi].[dbo].[branches]
                                         ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [pafupi].[dbo].[account_card]
                                         ON [pafupi_account].[acc_no] = [account_card].[acc_no]
                                        
                                         INNER JOIN [pafupi].[dbo].[account_status]
                                         ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                         INNER JOIN [pafupi].dbo.[UserProfile]
                                         ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                                            WHERE [account_status].[acc_status]=?



                           ) AS A

                           WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1
                           ";


            $result = $this->db->query($sql,array('carded'));
        }
        else{
            $sql = "SELECT * FROM (
                                      SELECT row_number() OVER (ORDER BY [pafupi_account].acc_no) AS rownum,[pafupi_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName]
                                         FROM [pafupi].[dbo].[pafupi_account]
                                         INNER JOIN [pafupi].[dbo].[branches]
                                         ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [pafupi].[dbo].[account_card]
                                         ON [pafupi_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[account_status]
                                         ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                         INNER JOIN [pafupi].dbo.[UserProfile]
                                         ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                                            WHERE [account_status].[acc_status]=? AND [UserProfile].[Username]=?



                           ) AS A

                           WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1
                           ";


            $result = $this->db->query($sql,array('carded',$username));
        }
        //return the query resul
        $this->db->close();
        return $result->result_array();
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    } /*get accounts from linked paginate */
    public function get_all_linked_accounts($limit,$start,$username=false)
    {
        //get the username
        try{
        if(!$username){
            $sql = "SELECT * FROM (
                                      SELECT row_number() OVER (ORDER BY [pafupi_account].acc_no) AS rownum,[pafupi_account].*,[branches].[branch_name],[Username],[FullName],[book_no],[referal_no],[card_no]
                                            FROM [pafupi].[dbo].[pafupi_account]
                                            INNER JOIN [pafupi].[dbo].[branches]
                                            ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                            INNER JOIN [pafupi].dbo.[UserProfile]
                                            ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                                             INNER JOIN [pafupi].[dbo].[booklet]
                                             ON [pafupi_account].[acc_no] = [booklet].[acc_no]
                                             INNER JOIN [pafupi].[dbo].[referal_book]
                                             ON [pafupi_account].[acc_no] = [referal_book].[acc_no]
                                             INNER JOIN [pafupi].[dbo].[account_card]
                                            ON [pafupi_account].[acc_no] = [account_card].[acc_no]
                                            INNER JOIN [pafupi].[dbo].[account_status]
                                            ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                            WHERE [account_status].[acc_status]=?


                           ) AS A

                           WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1
                           ";


            $result = $this->db->query($sql,array('linked'));
        }
        else{
            $sql = "SELECT * FROM (
                                      SELECT row_number() OVER (ORDER BY [pafupi_account].acc_no) AS rownum,[pafupi_account].*,[branches].[branch_name],
                                      [Username],[book_no],[referal_no],[card_no]
                                            FROM [pafupi].[dbo].[pafupi_account]
                                            INNER JOIN [pafupi].[dbo].[branches]
                                            ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                            INNER JOIN [pafupi].[dbo].[account_card]
                                             ON [pafupi_account].[acc_no] = [account_card].[acc_no]
                                            INNER JOIN [pafupi].[dbo].[account_status]
                                             INNER JOIN [pafupi].[dbo].[booklet]
                                             ON [pafupi_account].[acc_no] = [booklet].[acc_no]
                                             INNER JOIN [pafupi].[dbo].[referal_book]
                                             ON [pafupi_account].[acc_no] = [referal_book].[acc_no]
                                            INNER JOIN [pafupi].dbo.[UserProfile]
                                            ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                                            INNER JOIN [pafupi].[dbo].[account_status]
                                            ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                            WHERE [account_status].[acc_status]=? AND [UserProfile].[Username]=?

                           ) AS A

                           WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1
                           ";


            $result = $this->db->query($sql,array('linked',$username));
        }
        $this->db->close();
        //return the query result
        return $result->result_array();
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function get_all_prelinked_accounts($limit,$start,$username=false)
    {
        //get the username
        try{
        if(!$username){
            $sql = "SELECT * FROM (
                                      SELECT row_number() OVER (ORDER BY [pafupi_account].acc_no) AS rownum,[pafupi_account].*,[branches].[branch_name],[Username],[FullName]
                                            FROM [pafupi].[dbo].[pafupi_account]
                                            INNER JOIN [pafupi].[dbo].[branches]
                                            ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                            INNER JOIN [pafupi].dbo.[UserProfile]
                                            ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                                            INNER JOIN [pafupi].[dbo].[account_status]
                                            ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                            WHERE [account_status].[acc_status]=?



                           ) AS A

                           WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1
                           ";


            $result = $this->db->query($sql,array('precarded'));
        }
        else{
            $sql = "SELECT * FROM (
                                      SELECT row_number() OVER (ORDER BY [pafupi_account].acc_no) AS rownum,[pafupi_account].*,[branches].[branch_name],[Username]
                                            FROM [pafupi].[dbo].[pafupi_account]
                                            INNER JOIN [pafupi].[dbo].[branches]
                                            ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                            INNER JOIN [pafupi].dbo.[UserProfile]
                                            ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                                            INNER JOIN [pafupi].[dbo].[account_status]
                                            ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                            WHERE [account_status].[acc_status]=? AND [UserProfile].[Username]=?



                           ) AS A

                           WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1
                           ";


            $result = $this->db->query($sql,array('precarded',$username));
        }
        //return the query result
        return $result->result_array();
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }/*get accounts from locked paginate */
    public function get_all_locked_accounts($limit,$start,$username=false)
    {
        //get the username
        try{
        if(!$username){
            $sql = "SELECT * FROM (
                                      SELECT row_number() OVER (ORDER BY [pafupi_account].acc_no) AS rownum,[pafupi_account].*,[branches].[branch_name],[Username],[FullName]
                                            FROM [pafupi].[dbo].[pafupi_account]
                                            INNER JOIN [pafupi].[dbo].[branches]
                                            ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                            INNER JOIN [pafupi].dbo.[UserProfile]
                                            ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                                            INNER JOIN [pafupi].[dbo].[account_status]
                                            ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                            
                                            INNER JOIN [pafupi].[dbo].[account_card]
                                            ON [pafupi_account].[acc_no] = [account_card].[acc_no]
                                            INNER JOIN [pafupi].[dbo].[booklet]
                                            ON [pafupi_account].[acc_no] = [booklet].[acc_no]
                                            INNER JOIN [pafupi].[dbo].[referal_book]
                                            ON [pafupi_account].[acc_no] = [referal_book].[acc_no]
                                            
                                            WHERE [account_status].[lock_status]=?



                           ) AS A

                           WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1
                           ";


            $result = $this->db->query($sql,true);
        }
        else{
            $sql = "SELECT * FROM (
                                      SELECT row_number() OVER (ORDER BY [pafupi_account].acc_no) AS rownum,[pafupi_account].*,[branches].[branch_name],[Username]
                                            FROM [pafupi].[dbo].[pafupi_account]
                                            INNER JOIN [pafupi].[dbo].[branches]
                                            ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                            INNER JOIN [pafupi].dbo.[UserProfile]
                                            ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                                            INNER JOIN [pafupi].[dbo].[account_status]
                                            ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                            
											INNER JOIN [pafupi].[dbo].[account_card]
                                            ON [pafupi_account].[acc_no] = [account_card].[acc_no]
                                            INNER JOIN [pafupi].[dbo].[booklet]
                                            ON [pafupi_account].[acc_no] = [booklet].[acc_no]
                                            INNER JOIN [pafupi].[dbo].[referal_book]
                                            ON [pafupi_account].[acc_no] = [referal_book].[acc_no]
											
											
                                            WHERE [account_status].[lock_status]=? AND [UserProfile].[Username]=?



                           ) AS A

                           WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1
                           ";


            $result = $this->db->query($sql,array(true,$username));
        }
        //return the query result
        return $result->result_array();
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }

    } /*get accounts from non locked paginate */
    public function get_all_nonlocked_accounts($limit,$start,$username=false)
    {
        //get the username
        try{
        if(!$username){
            $sql = "SELECT * FROM (
                                      SELECT row_number() OVER (ORDER BY [pafupi_account].acc_no) AS rownum,[pafupi_account].*,[branches].[branch_name],[Username],[FullName]
                                            FROM [pafupi].[dbo].[pafupi_account]
                                            INNER JOIN [pafupi].[dbo].[branches]
                                            ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                            INNER JOIN [pafupi].dbo.[UserProfile]
                                            ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                                            INNER JOIN [pafupi].[dbo].[account_status]
                                            ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                            
                                            INNER JOIN [pafupi].[dbo].[account_card]
                                            ON [pafupi_account].[acc_no] = [account_card].[acc_no]
                                            INNER JOIN [pafupi].[dbo].[booklet]
                                            ON [pafupi_account].[acc_no] = [booklet].[acc_no]
                                            INNER JOIN [pafupi].[dbo].[referal_book]
                                            ON [pafupi_account].[acc_no] = [referal_book].[acc_no]
                                            
                                            WHERE [account_status].[lock_status]=?



                           ) AS A

                           WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1
                           ";


            $result = $this->db->query($sql,array(false));
        }
        else{
            $sql = "SELECT * FROM (
                                      SELECT row_number() OVER (ORDER BY [pafupi_account].acc_no) AS rownum,[pafupi_account].*,[branches].[branch_name],[Username]
                                            FROM [pafupi].[dbo].[pafupi_account]
                                            INNER JOIN [pafupi].[dbo].[branches]
                                            ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                            INNER JOIN [pafupi].dbo.[UserProfile]
                                            ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                                            INNER JOIN [pafupi].[dbo].[account_status]
                                            ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                            
											INNER JOIN [pafupi].[dbo].[account_card]
                                            ON [pafupi_account].[acc_no] = [account_card].[acc_no]
                                            INNER JOIN [pafupi].[dbo].[booklet]
                                            ON [pafupi_account].[acc_no] = [booklet].[acc_no]
                                            INNER JOIN [pafupi].[dbo].[referal_book]
                                            ON [pafupi_account].[acc_no] = [referal_book].[acc_no]
											
                                            WHERE [account_status].[lock_status]=? AND [UserProfile].[Username]=?



                           ) AS A

                           WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1
                           ";


            $result = $this->db->query($sql,array(false,$username));
        }
        //return the query result
        return $result->result_array();
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    } /*get accounts from starter-pack paginate */
    public function get_all_starterpack_accounts($limit,$start,$username=false)
    {
        try{
        if(!$username){
            $sql = "SELECT * FROM (
                                  SELECT row_number() OVER (ORDER BY [pafupi_account].acc_no) AS rownum,[pafupi_account].*,[branches].[branch_name],[Username]
                                        FROM [pafupi].[dbo].[pafupi_account]
                                        INNER JOIN [pafupi].[dbo].[branches]
                                        ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                        INNER JOIN [pafupi].dbo.[UserProfile]
                                        ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                                        INNER JOIN [pafupi].[dbo].[account_status]
                                        ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                        WHERE [account_status].[acc_status]=?



                       ) AS A

                       WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1
                       ";


            $result = $this->db->query($sql,array('starterpack'));
        }
        else{
            $sql = "SELECT * FROM (
                          SELECT row_number() OVER (ORDER BY [pafupi_account].acc_no) AS rownum,[pafupi_account].*,[branches].[branch_name],[Username]
                                FROM [pafupi].[dbo].[pafupi_account]
					            INNER JOIN [pafupi].[dbo].[branches]
					            ON [pafupi_account].[branch_id] = [branches].[branch_id]
					            INNER JOIN [pafupi].dbo.[UserProfile]
					            ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
					            INNER JOIN [pafupi].[dbo].[account_status]
					            ON [pafupi_account].[acc_no] = [account_status].[acc_no]
					            WHERE [account_status].[acc_status]=? AND [UserProfile].[Username] = ?

			   ) AS A

			   WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1
			   ";


            $result = $this->db->query($sql,array('starterpack',$username));

        }
        //return the query result
        return $result->result_array();
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    } /*get accounts all dispatched accounts for paginating */
    public function get_all_dispatched_accounts($limit,$start,$username=false){
        //get the username
        //$username = $this->session->userdata('username');
        try{
        if(!$username){
            $sql = "SELECT * FROM (
                          SELECT row_number() OVER (ORDER BY [pafupi_account].acc_no) AS rownum,[pafupi_account].*,[branches].[branch_name],[Username],[FullName],[dispatched_by],[AOC],[card_no]
                                FROM [pafupi].[dbo].[pafupi_account]
					            INNER JOIN [pafupi].[dbo].[branches]
					            ON [pafupi_account].[branch_id] = [branches].[branch_id]
					            INNER JOIN [pafupi].dbo.[UserProfile]
					            ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
					            INNER JOIN [pafupi].[dbo].[account_status]
					            ON [pafupi_account].[acc_no] = [account_status].[acc_no]
					            INNER JOIN [pafupi].[dbo].[account_card]
					            ON [pafupi_account].[acc_no] = [account_card].[acc_no]
					            INNER JOIN [pafupi].[dbo].[account_mobility]
					            ON [pafupi_account].[acc_no] = [account_mobility].[acc_no]
					            WHERE [account_status].[dispatch_flag]=?




			   ) AS A

			   WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1
			   ";


            $result = $this->db->query($sql,array(true));
        }
        else{
            $sql = "SELECT * FROM (
                          SELECT row_number() OVER (ORDER BY [pafupi_account].acc_no) AS rownum,[pafupi_account].*,[branches].[branch_name],[Username],[dispatched_by],[AOC]
                                FROM [pafupi].[dbo].[pafupi_account]
					            INNER JOIN [pafupi].[dbo].[branches]
					            ON [pafupi_account].[branch_id] = [branches].[branch_id]
					            INNER JOIN [pafupi].dbo.[UserProfile]
					            ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
					            INNER JOIN [pafupi].[dbo].[account_status]
					            ON [pafupi_account].[acc_no] = [account_status].[acc_no]
					            INNER JOIN [pafupi].[dbo].[account_mobility]
					            ON [pafupi_account].[acc_no] = [account_mobility].[acc_no]
                                WHERE [UserProfile].[Username] =? AND [account_status].[dispatch_flag]=?



			   ) AS A

			   WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1
			   ";


            $result = $this->db->query($sql,array($username,true));

        }
        //return the query result
        return $result->result_array();
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }

    } /*get accounts all dispatched accounts for paginating */
    public function get_all_nondispatched_accounts($limit,$start,$username=false){
        //get the username
        //$username = $this->session->userdata('username');
        try{
        if(!$username){
            $sql = "SELECT * FROM (
                          SELECT row_number() OVER (ORDER BY [pafupi_account].acc_no) AS rownum,[pafupi_account].*,[branches].[branch_name],[Username],[FullName],[card_no]
                                FROM [pafupi].[dbo].[pafupi_account]
					            INNER JOIN [pafupi].[dbo].[branches]
					            ON [pafupi_account].[branch_id] = [branches].[branch_id]
					            INNER JOIN [pafupi].dbo.[UserProfile]
					            ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
					            INNER JOIN [pafupi].[dbo].[account_status]
					            ON [pafupi_account].[acc_no] = [account_status].[acc_no]
					             INNER JOIN [pafupi].[dbo].[account_card]
					            ON [pafupi_account].[acc_no] = [account_card].[acc_no]
					            WHERE [account_status].[dispatch_flag]=? AND [account_status].[acc_status] =?




			   ) AS A

			   WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1
			   ";


            $result = $this->db->query($sql,array(false,'starterpack'));
        }
        else{
            $sql = "SELECT * FROM (
                          SELECT row_number() OVER (ORDER BY [pafupi_account].acc_no) AS rownum,[pafupi_account].*,[branches].[branch_name],[Username]
                                FROM [pafupi].[dbo].[pafupi_account]
					            INNER JOIN [pafupi].[dbo].[branches]
					            ON [pafupi_account].[branch_id] = [branches].[branch_id]
					            INNER JOIN [pafupi].dbo.[UserProfile]
					            ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
					            INNER JOIN [pafupi].[dbo].[account_status]
					            ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                WHERE [UserProfile].[Username] =? AND [account_status].[dispatch_flag]=?  AND [account_status].[acc_status] =?



			   ) AS A

			   WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1
			   ";


            $result = $this->db->query($sql,array($username,false,'starterpack'));

        }
        //return the query result
        return $result->result_array();
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    } /* get accounts paginate */
    public function get_all_accounts($limit,$start,$username=false)
    {
        try{
        if($username){
            $sql = "SELECT * FROM (
                          SELECT row_number() OVER (ORDER BY acc_no) AS rownum,[pafupi_account].*,[branches].[branch_name],[Username]
                                FROM [pafupi].[dbo].[pafupi_account]
					            INNER JOIN [pafupi].[dbo].[branches]
					            ON [pafupi_account].[branch_id] = [branches].[branch_id]
					            INNER JOIN [pafupi].dbo.[UserProfile]
					            ON [pafupi_account].[opened_by] = [UserProfile].[UserID]



			   ) AS A

			   WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1
			   ";


            $result = $this->db->query($sql);
        }
        else{
            $sql = "SELECT * FROM (
                          SELECT row_number() OVER (ORDER BY acc_no) AS rownum,[pafupi_account].*,[branches].[branch_name],[Username]
                                FROM [pafupi].[dbo].[pafupi_account]
					            INNER JOIN [pafupi].[dbo].[branches]
					            ON [pafupi_account].[branch_id] = [branches].[branch_id]
					            INNER JOIN [pafupi].dbo.[UserProfile]
					            ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
					            WHERE [UserProfile].[Username]= ?



			   ) AS A

			   WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1
			   ";


            $result = $this->db->query($sql,array($username));
        }
        //return the query result
        return $result->result_array();
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }    /*
     * gets preopened accounts
     */
    public function getLinkedAccount($user_id=false,$acc_no=false,$branch_id=false){
        if($user_id && !$acc_no){

            $sql_account = 'SELECT [pafupi_account].*,[branches].[branch_name],[FullName],[referal_no],[book_no],[card_no]
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[referal_book]
					 ON [pafupi_account].[acc_no] = [referal_book].[acc_no]
					 INNER JOIN [pafupi].[dbo].[account_status]
					 ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                     INNER JOIN [pafupi].[dbo].[account_card]
                      ON [pafupi_account].[acc_no] = [account_card].[acc_no]
					 INNER JOIN [pafupi].[dbo].[booklet]
					 ON [pafupi_account].[acc_no] = [booklet].[acc_no]
					 INNER JOIN [pafupi].dbo.[UserProfile]
                     ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
					 WHERE [pafupi_account].[opened_by]=? AND [account_status].[acc_status]=?';

            $query = $this->db->query($sql_account,array($user_id,'linked'));

            return $query->result_array();
        }
        else if($branch_id){
            $sql_account = 'SELECT [pafupi_account].*,[branches].[branch_name],[FullName],[referal_no],[book_no],[card_no]
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[account_status]
					 ON [pafupi_account].[acc_no] = [account_status].[acc_no]
					 INNER JOIN [pafupi].[dbo].[referal_book]
					 ON [pafupi_account].[acc_no] = [referal_book].[acc_no]
					 INNER JOIN [pafupi].[dbo].[booklet]
					 ON [pafupi_account].[acc_no] = [booklet].[acc_no]
					 INNER JOIN [pafupi].[dbo].[account_card]
                     ON [pafupi_account].[acc_no] = [account_card].[acc_no]
					 INNER JOIN [pafupi].dbo.[UserProfile]
                     ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
					 WHERE [pafupi_account].[branch_id]=? AND [account_status].[acc_status]=?';

            $query = $this->db->query($sql_account,array($branch_id,'linked'));

            return $query->result_array();

        }
        else if($user_id && $acc_no){

            $sql_account = 'SELECT [pafupi_account].*,[branches].[branch_name],[book_no],[referal_no],[card_no]
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					  INNER JOIN [pafupi].[dbo].[booklet]
                     ON [pafupi_account].[acc_no] = [booklet].[acc_no]
                     INNER JOIN [pafupi].[dbo].[referal_book]
                     ON [pafupi_account].[acc_no] = [referal_book].[acc_no]
					 INNER JOIN [pafupi].[dbo].[account_status]
					 ON [pafupi_account].[acc_no] = [account_status].[acc_no]
					 INNER JOIN [pafupi].[dbo].[account_card]
                      ON [pafupi_account].[acc_no] = [account_card].[acc_no]
					 WHERE [pafupi_account].[opened_by]=$user_id AND [pafupi_account].[acc_no]=? AND [account_status].[acc_status]=?';

            $query = $this->db->query($sql_account,array($user_id,$acc_no,'linked'));

            return $query->result_array();
        }
        else if(!$user_id && $acc_no){

            $sql_account = 'SELECT [pafupi_account].*,[branches].[branch_name],[book_no],[referal_no],[card_no]
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					  INNER JOIN [pafupi].[dbo].[booklet]
                     ON [pafupi_account].[acc_no] = [booklet].[acc_no]
                     INNER JOIN [pafupi].[dbo].[referal_book]
                     ON [pafupi_account].[acc_no] = [referal_book].[acc_no]
					 INNER JOIN [pafupi].[dbo].[account_status]
					 ON [pafupi_account].[acc_no] = [account_status].[acc_no]
					 INNER JOIN [pafupi].[dbo].[account_card]
                      ON [pafupi_account].[acc_no] = [account_card].[acc_no]
					 WHERE [pafupi_account].[acc_no]=?  AND [account_status].[acc_status]=?';

            $query = $this->db->query($sql_account,array($acc_no,'linked'));

            return $query->result_array();
        }
        else{

            $sql_account = 'SELECT [pafupi_account].*,[branches].[branch_name],[book_no],[referal_no],[card_no]
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					  INNER JOIN [pafupi].[dbo].[booklet]
                     ON [pafupi_account].[acc_no] = [booklet].[acc_no]
                     INNER JOIN [pafupi].[dbo].[referal_book]
                     ON [pafupi_account].[acc_no] = [referal_book].[acc_no]
					 INNER JOIN [pafupi].[dbo].[account_status]
					 ON [pafupi_account].[acc_no] = [account_status].[acc_no]
					 INNER JOIN [pafupi].[dbo].[account_card]
                     ON [pafupi_account].[acc_no] = [account_card].[acc_no]
					 [account_status].[acc_status]=?';

            $query = $this->db->query($sql_account,array('linked'));

            return $query->result_array();
        }

    }    /*
     * gets preopened accounts
     */
    public function getPreAccount($user_id=false,$acc_no=false,$branch_id=false){
        try{
        if($user_id && !$acc_no){

            $sql_account = 'SELECT [pafupi_account].*,[branches].[branch_name],[FullName],[referal_no],[book_no]
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[account_status]
					 ON [pafupi_account].[acc_no] = [account_status].[acc_no]

					 INNER JOIN [pafupi].dbo.[UserProfile]
                     ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
					 WHERE [pafupi_account].[opened_by]=? AND [account_status].[acc_status]=?';

            /* INNER JOIN [pafupi].[dbo].[referal_book]
            ON [pafupi_account].[acc_no] = [referal_book].[acc_no]
            INNER JOIN [pafupi].[dbo].[booklet]
            ON [pafupi_account].[acc_no] = [booklet].[acc_no]*/

            $query = $this->db->query($sql_account,array($user_id,'precarded'));

            return $query->result_array();
        }
        else if($branch_id){
            $sql_account = 'SELECT [pafupi_account].*,[branches].[branch_name],[FullName]
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[account_status]
					 ON [pafupi_account].[acc_no] = [account_status].[acc_no]
					 INNER JOIN [pafupi].dbo.[UserProfile]
                     ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
					 WHERE [pafupi_account].[branch_id]=? AND [account_status].[acc_status]=?';

            $query = $this->db->query($sql_account,array($branch_id,'precarded'));

            return $query->result_array();

        }
        else if($user_id && $acc_no){

            $sql_account = 'SELECT [pafupi_account].*,[branches].[branch_name]
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[account_status]
					 ON [pafupi_account].[acc_no] = [account_status].[acc_no]
					 WHERE [pafupi_account].[opened_by]=$user_id AND [pafupi_account].[acc_no]=? AND [account_status].[acc_status]=?';

            $query = $this->db->query($sql_account,array($user_id,$acc_no,'precarded'));

            return $query->result_array();
        }
        else if(!$user_id && $acc_no){

            $sql_account = 'SELECT [pafupi_account].*,[branches].[branch_name]
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[account_status]
					 ON [pafupi_account].[acc_no] = [account_status].[acc_no]
					 WHERE [pafupi_account].[acc_no]=? AND AND [account_status].[acc_status]=?';

            $query = $this->db->query($sql_account,array($acc_no,'precarded'));

            return $query->result_array();
        }
        else{

            $sql_account = 'SELECT [pafupi_account].*,[branches].[branch_name]
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[account_status]
					 ON [pafupi_account].[acc_no] = [account_status].[acc_no]
					 WHERE  [account_status].[acc_status]=?';

            $query = $this->db->query($sql_account,array('precarded'));

            return $query->result_array();
        }
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function getLockedAccounts($user_id=false,$acc_no=false){
        try{
        if($user_id && !$acc_no){

            $sql_account = 'SELECT [pafupi_account].*,[branches].[branch_name],[acc_status]
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[account_status]
					 ON [pafupi_account].[acc_no] = [account_status].[acc_no]
					 
					 INNER JOIN [pafupi].[dbo].[account_card]
                     ON [pafupi_account].[acc_no] = [account_card].[acc_no]
                     INNER JOIN [pafupi].[dbo].[booklet]
                     ON [pafupi_account].[acc_no] = [booklet].[acc_no]
                     INNER JOIN [pafupi].[dbo].[referal_book]
                     ON [pafupi_account].[acc_no] = [referal_book].[acc_no]
                                            
					 WHERE [pafupi_account].[opened_by]=? AND  [account_status].[lock_status]=? AND [account_status].[acc_status]=?';

            $query = $this->db->query($sql_account,array($user_id,true,'starterpack'));

            return $query->result_array();
        }
        if($user_id && $acc_no){

            $sql_account = 'SELECT [pafupi_account].*,[branches].[branch_name],[acc_status]
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[account_status]
					 ON [pafupi_account].[acc_no] = [account_status].[acc_no]
					 
					 INNER JOIN [pafupi].[dbo].[account_card]
                     ON [pafupi_account].[acc_no] = [account_card].[acc_no]
                     INNER JOIN [pafupi].[dbo].[booklet]
                     ON [pafupi_account].[acc_no] = [booklet].[acc_no]
                     INNER JOIN [pafupi].[dbo].[referal_book]
                     ON [pafupi_account].[acc_no] = [referal_book].[acc_no]
					 
					 WHERE [pafupi_account].[opened_by]=? AND [pafupi_account].[acc_no] = ?  AND [account_status].[lock_status]=? AND [account_status].[acc_status]=?';

            $query = $this->db->query($sql_account,array($user_id,$acc_no,true,'starterpack'));

            return $query->result_array();
        }
        elseif(!$user_id && $acc_no){
            $sql_account = 'SELECT [pafupi_account].*,[branches].[branch_name],[acc_status]
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[account_status]
					 ON [pafupi_account].[acc_no] = [account_status].[acc_no]
					 
					 INNER JOIN [pafupi].[dbo].[account_card]
                     ON [pafupi_account].[acc_no] = [account_card].[acc_no]
                     INNER JOIN [pafupi].[dbo].[booklet]
                     ON [pafupi_account].[acc_no] = [booklet].[acc_no]
                     INNER JOIN [pafupi].[dbo].[referal_book]
                     ON [pafupi_account].[acc_no] = [referal_book].[acc_no]
					 
					 
					 WHERE [pafupi_account].[acc_no] = ? AND [account_status].[lock_status]=? AND [account_status].[acc_status]=?';
            $query = $this->db->query($sql_account,array($acc_no,true,'starterpack'));

            return $query->result_array();
        }
        else{
            $sql_account = 'SELECT [pafupi_account].*,[branches].[branch_name],[acc_status]
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[account_status]
					 ON [pafupi_account].[acc_no] = [account_status].[acc_no]
					 WHERE [account_status].[lock_status]=? AND [account_status].[acc_status]=?';
            $query = $this->db->query($sql_account,array(true,'starterpack'));

            return $query->result_array();
        }
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function getNonLockedAccounts($user_id=false,$acc_no=false){
        try{
        if($user_id && !$acc_no){

            $sql_account = 'SELECT [pafupi_account].*,[branches].[branch_name]
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[account_status]
					 ON [pafupi_account].[acc_no] = [account_status].[acc_no]
					 WHERE [pafupi_account].[opened_by]=? AND  [account_status].[lock_status]=? AND [account_status].[acc_status]=?';

            $query = $this->db->query($sql_account,array($user_id,false,'starterpack'));

            return $query->result_array();
        }
        if($user_id && $acc_no){

            $sql_account = 'SELECT [pafupi_account].*,[branches].[branch_name]
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[account_status]
					 ON [pafupi_account].[acc_no] = [account_status].[acc_no]
					 WHERE [pafupi_account].[opened_by]=? AND [pafupi_account].[acc_no] = ?  AND [account_status].[lock_status]=? AND [account_status].[acc_status]=?';

            $query = $this->db->query($sql_account,array($user_id,$acc_no,false,'starterpack'));

            return $query->result_array();
        }
        elseif(!$user_id && $acc_no){
            $sql_account = 'SELECT [pafupi_account].*,[branches].[branch_name]
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[account_status]
					 ON [pafupi_account].[acc_no] = [account_status].[acc_no]
					 WHERE [pafupi_account].[acc_no] = ? AND [account_status].[lock_status]=? AND [account_status].[acc_status]=?';
            $query = $this->db->query($sql_account,array($acc_no,false,'starterpack'));

            return $query->result_array();
        }
        else{
            $sql_account = 'SELECT [pafupi_account].*,[branches].[branch_name]
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[account_status]
					 ON [pafupi_account].[acc_no] = [account_status].[acc_no]
					 WHERE [account_status].[lock_status]=? AND [account_status].[acc_status]=?';
            $query = $this->db->query($sql_account,array(false,'starterpack'));

            return $query->result_array();
        }
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function getDispatchedAccounts($user_id=false,$acc_no=false){
        try{
        if($user_id && !$acc_no){

            $sql_account = 'SELECT [pafupi_account].*,[branches].[branch_name],[dispatched_by],[acc_status]
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[account_status]
					 ON [pafupi_account].[acc_no] = [account_status].[acc_no]
					 INNER JOIN [pafupi].[dbo].[account_mobility]
					 ON [pafupi_account].[acc_no] = [account_mobility].[acc_no]
					 WHERE [pafupi_account].[opened_by]=? AND  [account_status].[dispatch_flag]=?';

            $query = $this->db->query($sql_account,array($user_id,true));

            return $query->result_array();
        }
        if($user_id && $acc_no){

            $sql_account = 'SELECT [pafupi_account].*,[branches].[branch_name],[dispatched_by],[acc_status]
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[account_status]
					 ON [pafupi_account].[acc_no] = [account_status].[acc_no]
					  INNER JOIN [pafupi].[dbo].[account_mobility]
					 ON [pafupi_account].[acc_no] = [account_mobility].[acc_no]
					 WHERE [pafupi_account].[opened_by]=? AND [pafupi_account].[acc_no] = ?  AND [account_status].[dispatch_flag]=?';

            $query = $this->db->query($sql_account,array($user_id,$acc_no,true));

            return $query->result_array();
        }
        elseif(!$user_id && $acc_no){
            $sql_account = 'SELECT [pafupi_account].*,[branches].[branch_name],[dispatched_by],[acc_status]
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[account_status]
					 ON [pafupi_account].[acc_no] = [account_status].[acc_no]
					  INNER JOIN [pafupi].[dbo].[account_mobility]
					 ON [pafupi_account].[acc_no] = [account_mobility].[acc_no]
					 WHERE [pafupi_account].[acc_no] = ? AND [account_status].[dispatch_flag]=?';
            $query = $this->db->query($sql_account,array($acc_no,true));

            return $query->result_array();
        }
        else{
            $sql_account = 'SELECT [pafupi_account].*,[branches].[branch_name],[dispatched_by],[acc_status]
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[account_status]
					 ON [pafupi_account].[acc_no] = [account_status].[acc_no]
					  INNER JOIN [pafupi].[dbo].[account_mobility]
					 ON [pafupi_account].[acc_no] = [account_mobility].[acc_no]
					 WHERE [account_status].[dispatch_flag]=?';
            $query = $this->db->query($sql_account,array(true));

            return $query->result_array();
        }
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function getNotDispatchedAccounts($user_id=false,$acc_no=false){
        try{
        if($user_id && !$acc_no){

            $sql_account = 'SELECT [pafupi_account].*,[branches].[branch_name],[card_no]
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[account_status]
					 ON [pafupi_account].[acc_no] = [account_status].[acc_no]
					 INNER JOIN [pafupi].[dbo].[account_card]
					 ON [pafupi_account].[acc_no] = [account_card].[acc_no]
					 WHERE [pafupi_account].[opened_by]=? AND [account_status].[dispatch_flag]=? AND [account_status].[acc_status]=?';

            $query = $this->db->query($sql_account,array($user_id,false,'starterpack'));

            return $query->result_array();
        }
        if($user_id && $acc_no){

            $sql_account = 'SELECT [pafupi_account].*,[branches].[branch_name]
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[account_status]
					 ON [pafupi_account].[acc_no] = [account_status].[acc_no]
					 INNER JOIN [pafupi].[dbo].[account_card]
					 ON [pafupi_account].[acc_no] = [account_card].[acc_no]
					 WHERE [pafupi_account].[opened_by]=? AND [pafupi_account].[acc_no] = ?  AND [account_status].[dispatch_flag]=?';

            $query = $this->db->query($sql_account,array($user_id,$acc_no,false));

            return $query->result_array();
        }
        elseif(!$user_id && $acc_no){
            $sql_account = 'SELECT [pafupi_account].*,[branches].[branch_name]
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[account_status]
					 ON [pafupi_account].[acc_no] = [account_status].[acc_no]
					 INNER JOIN [pafupi].[dbo].[account_card]
					 ON [pafupi_account].[acc_no] = [account_card].[acc_no]
					 WHERE [pafupi_account].[acc_no] = ? AND [account_status].[dispatch_flag]=?';
            $query = $this->db->query($sql_account,array($acc_no,false));

            return $query->result_array();
        }
        else{
            $sql_account = 'SELECT [pafupi_account].*,[branches].[branch_name]
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[account_status]
					 ON [pafupi_account].[acc_no] = [account_status].[acc_no]
					 INNER JOIN [pafupi].[dbo].[account_card]
					 ON [pafupi_account].[acc_no] = [account_card].[acc_no]
					 WHERE [account_status].[dispatch_flag]=?';
            $query = $this->db->query($sql_account,array(false));

            return $query->result_array();
        }
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function getAccountsWithAOC($user_id=false,$acc_no=false){
        try{
        if($user_id && !$acc_no){

            $sql_account = 'SELECT [pafupi_account].*,[branches].[branch_name],[dispatched_by],[AOC],[acc_status]
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[account_status]
					 ON [pafupi_account].[acc_no] = [account_status].[acc_no]
					 INNER JOIN [pafupi].[dbo].[account_mobility]
					 ON [pafupi_account].[acc_no] = [account_mobility].[acc_no]
					 WHERE [pafupi_account].[opened_by]=? AND  [account_mobility].[AOC] !=?';

            $query = $this->db->query($sql_account,array($user_id,NULL));
        }
        if($user_id && $acc_no){

            $sql_account = 'SELECT [pafupi_account].*,[branches].[branch_name],[dispatched_by],[AOC],[acc_status]
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[account_status]
					 ON [pafupi_account].[acc_no] = [account_status].[acc_no]
					  INNER JOIN [pafupi].[dbo].[account_mobility]
					 ON [pafupi_account].[acc_no] = [account_mobility].[acc_no]
					 WHERE [pafupi_account].[opened_by]=? AND [pafupi_account].[acc_no] = ?  AND [account_mobility].[AOC] !=?';

            $query = $this->db->query($sql_account,array($user_id,$acc_no,true));

            return $query->result_array();
        }
        elseif(!$user_id && $acc_no){
            $sql_account = 'SELECT [pafupi_account].*,[branches].[branch_name],[dispatched_by],[AOC],[acc_status]
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[account_status]
					 ON [pafupi_account].[acc_no] = [account_status].[acc_no]
					  INNER JOIN [pafupi].[dbo].[account_mobility]
					 ON [pafupi_account].[acc_no] = [account_mobility].[acc_no]
					 WHERE [pafupi_account].[acc_no] = ? AND [account_mobility].[AOC] !=?';
            $query = $this->db->query($sql_account,array($acc_no,NULL));

            return $query->result_array();
        }
        else{
            $sql_account = 'SELECT [pafupi_account].*,[branches].[branch_name],[dispatched_by],[AOC],[acc_status]
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[account_status]
					 ON [pafupi_account].[acc_no] = [account_status].[acc_no]
					  INNER JOIN [pafupi].[dbo].[account_mobility]
					 ON [pafupi_account].[acc_no] = [account_mobility].[acc_no]
					 WHERE [account_mobility].[AOC] !=?';
            $query = $this->db->query($sql_account,array(NULL));

            return $query->result_array();
        }
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function getBookPins($book_no=false){
        try{
        if($book_no){
            $sql_account = 'SELECT *
		                    FROM [pafupi].[dbo].[trans_pins]
		                    WHERE [book_no] =?';

            $query = $this->db->query($sql_account,array($book_no));
            return $query->result_array();
        }
        else{

            $sql_account = 'SELECT *
		             FROM [pafupi].[dbo].[trans_pins]';

            $query = $this->db->query($sql_account);
            return $query->result_array();
        }
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function saveUploadData($data){
        try{
        $sql_upload = 'INSERT INTO [pafupi].[dbo].[upload_data]
		                    ([upload_name],[uploaded_by],[error_type])
							 VALUES(?,?,?)';
        $result=$this->db->query($sql_upload,array($data['upload_name'],$data['uploaded_by'],$data['error_type']));
        if($result){
            return true;
        }
        else
            return false;
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function checkReferalNo($ref_no){
        $sql = 'SELECT COUNT([referal_no]) as count FROM [pafupi].[dbo].[referal_book]
                WHERE [referal_no]=?';
        $result = $this->db->query($sql,array($ref_no));
        $count = 0;
        foreach($result->result()as $row){
            $count = $row->count;
        }
        if($count>=1){
            return true;
        }
        else{
            return false;
        }

    }
    public function generateReferal(){
        return mt_rand(0,9).mt_rand(0,9).mt_rand(0,9).mt_rand(0,9).mt_rand(0,9).mt_rand(0,9).mt_rand(0,9).mt_rand(0,9).mt_rand(0,9);
    }
    public function checkRefNo($coupon_no,$referal_no){

        /*$sql = 'SELECT COUNT([coupon_no]) as count FROM [pafupi].[dbo].[referal_coupons]
                WHERE [coupon_no]=? AND [referal_no]';*/
        $sql = 'EXEC pafupi.dbo.CheckRefNumber @RefNumber = ?, ';
        $sql.= '@BookNumber = ?';
        $result = $this->db->query($sql,array($coupon_no,$referal_no));
        $count = 0;
        foreach($result->result() as $row){
            $count = $row->IsPinUsed;
        }
        if($count>=1){
            return true;
        }
        else{
            return false;
        }
        $result = $this->db->query($sql,array($coupon_no,$referal_no));
        $count = 0;
        foreach($result->result() as $row){
            $count = $row->count;
        }
        if($count>=1){
            return true;
        }
        else{
            return false;
        }

    }
    public function makeCoupon($acc_no,$cust_id=false){
        $errors = array();
        try{
        $username = $this->session->userdata('username');
        $fullname = $this->session->userdata('fullname');
        $today_date = Common_Tasks::generate_date();
        $referal_no =  "RB".date('m',time()).date('y',time()).mt_rand(0,9).mt_rand(0,9).mt_rand(0,9).mt_rand(0,9).mt_rand(0,9);
        while($this->checkReferalNo($referal_no)){
            $referal_no = "RB".date('m',time()).date('y',time()).mt_rand(0,9).mt_rand(0,9).mt_rand(0,9).mt_rand(0,9).mt_rand(0,9);
        }
        $data = array('referal_no' => $referal_no,'acc_no' => $acc_no);
        $sql_upload = 'INSERT INTO [pafupi].[dbo].[referal_book]
		                    ([referal_no],[acc_no],[date_created],[created_by])
							 VALUES(?,?,?,?)';
        $result=$this->db->query($sql_upload,array($referal_no,$acc_no,$today_date,$fullname));
        if($result){
            $this->log_activity('Referal creation', $username,$acc_no);
            array_push($errors,array('booklet' => 'coupon number '.$referal_no.' created for '.$acc_no));
            $error_pins = array();
            $num = 0;
            for($i=0;$i<10;$i++){

                $sql_upload = 'INSERT INTO [pafupi].[dbo].[referal_coupons]
		                    ([referal_no],[coupon_no])
							 VALUES(?,?)';
                $ref_no = $this->generateReferal();
                while($this->checkRefNo($ref_no,$referal_no)){
                    $ref_no = $this->generateReferal();
                }
                $done=$this->db->query($sql_upload,array($referal_no,$ref_no));
                if($done){
                    $num++;
                }

            }
            if($num < 10){
                $num2 = $num;
                for($i=0;$i<(10-$num2);$i++){

                    $sql_upload = 'INSERT INTO [pafupi].[dbo].[referal_coupon]
		                    ([referal_no],[referal])
							 VALUES(?,?)';
                    $ref_no = $this->generateReferal();
                    while($this->checkRefNo($ref_no,$referal_no)){
                        $ref_no = $this->generateReferal();
                    }
                    $done=$this->db->query($sql_upload,array($referal_no,$ref_no));
                    if($done){

                        $num ++;
                    }

                }
            }
            array_push($error_pins,$num." coupons created for referal ".$referal_no." for ".$acc_no);
            array_push($errors,array('pins' => $error_pins));

        }
        else{
            array_push($errors,array('referal' => 'referal number '.$referal_no.' NOT created'));
        }
        return $result;
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function checkBookNo($book_no){
        /*$sql = 'SELECT COUNT([book_no]) as total_count FROM [pafupi].[dbo].[booklet]
                WHERE [book_no]=?';*/

        $sql = 'EXEC pafupi.dbo.CheckBookExits @BookNumber = ? ';

        $result = $this->db->query($sql,array($book_no));
        $count = 0;
        foreach($result->result() as $row){
            $count = $row->IsPinUsed;
        }
        if($count>=1){
            return true;
        }
        else{
            return false;
        }

    }
    public function insertBookNo($book_no){
        /*$sql = 'SELECT COUNT([book_no]) as count FROM [pafupi].[dbo].[booklet]
                WHERE [book_no]=?';*/
        try{
        $sql = 'EXEC pafupi.dbo.CheckBookNumber @BookNumber = ? ';

        $result = $this->db->query($sql,array($book_no));
        $count = 0;
        foreach($result->result() as $row){
            $count = $row->success;
        }
        if($count>=1){
            return true;
        }
        else{
            return false;
        }
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function checkOTPNo($otp_no,$book_no){
        /*$sql = 'SELECT COUNT([trans_pin]) as count FROM [pafupi].[dbo].[trans_pins]
                WHERE [trans_pin]=? AND [book_no] =?';*/
        $sql = 'EXEC pafupi.dbo.CheckOTPNumber @OTPNumber = ?, ';
        $sql.= '@BookNumber = ?';
        $result = $this->db->query($sql,array($otp_no,$book_no));
        $count = 0;
        foreach($result->result() as $row){
            $count = $row->IsPinUsed;
        }
        if($count>=1){
            return true;
        }
        else{
            return false;
        }

    }
    public function makeBooklet($acc_no,$cust_id=false){
        $errors = array();
        try{
        $username = $this->session->userdata('username');
        $fullname = $this->session->userdata('fullname');
        $today_date = Common_Tasks::generate_date();
        $book_no = "BP".date('m',time()).date('y',time()).mt_rand(0,9).mt_rand(0,9).mt_rand(0,9).mt_rand(0,9).mt_rand(0,9);
        while($this->checkBookNo($book_no)){
            $book_no = "BP".date('m',time()).date('y',time()).mt_rand(0,9).mt_rand(0,9).mt_rand(0,9).mt_rand(0,9).mt_rand(0,9);
        }

        //$acc_no = 1;
        $data = array('book_no' => $book_no,'acc_no' => $acc_no);

        $sql_upload = 'INSERT INTO [pafupi].[dbo].[booklet]
		                    ([book_no],[acc_no],[date_created],[created_by])
							 VALUES(?,?,?,?)';
        $result=$this->db->query($sql_upload,array($book_no,$acc_no,$today_date,$fullname));

        if($result){
            $result = $this->insertBookNo($book_no);
            if(!$result){
                $sql = 'DELETE FROM [pafupi].[dbo].[booklet]
                        WHERE [book_no] = ?';
                $done=$this->db->query($sql,$book_no);
            }
        }
        /*if(false){
            $this->log_activity('Booklet creation', $username,$acc_no);
            array_push($errors,array('booklet' => 'booklet number '.$book_no.' created for '.$acc_no));
            $error_pins = array();
            $num = 0;
            for($i=0;$i<50;$i++){

                $sql_upload = 'EXEC pafupi.dbo.InsertOTPNumber @BookNumber=?';
                $done= $this->db->query($sql_upload,array($book_no));
                if($done){
                    $count = 0;
                    foreach($done->result() as $row){
                        $count = $row->success;
                    }
                    if($count ==1)
                    $num++;
                }

            }
            if($num < 50){
                $num2 = $num;
                for($i=0;$i<(50-$num2);$i++){
                   .....................
                   /* $sql_upload = 'INSERT INTO [pafupi].[dbo].[trans_pins]
		                    ([book_no],[trans_pin])
							 VALUES(?,?)';
                    $otp = $this->generatePin();
                    while($this->checkOTPNo($otp,$book_no)){
                        $otp = $this->generatePin();
                    }...............................
                    $sql_upload = 'EXEC pafupi.dbo.InsertOTPNumber @BookNumber=?';
                    $done= $this->db->query($sql_upload,array($book_no));
                    if($done){
                        $count = 0;
                        foreach($done->result() as $row){
                            $count = $row->success;
                        }
                        if($count ==1)
                            $num++;
                    }

                }
            }
            if($num < 50){
                $sql = 'DELETE FROM [pafupi].[dbo].[booklet]
                        WHERE [book_no] = ?';
                $done=$this->db->query($sql,$book_no);
            }

            array_push($error_pins,$num." pins created for booklet ".$book_no." for ".$acc_no);
            array_push($errors,array('pins' => $error_pins));

        }
        else{
            array_push($errors,array('booklet' => 'booklet number '.$book_no.' NOT created'));
        }*/
        return $result;
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function checkAccount($data){
        $sql = 'SELECT COUNT([pafupi_account].[acc_no]) as count
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[account_card]
					 ON [pafupi_account].[acc_no] = [account_card].[acc_no]
					 INNER JOIN [pafupi].[dbo].[booklet]
                     ON [pafupi_account].[acc_no] = [booklet].[acc_no]
                     INNER JOIN [pafupi].[dbo].[referal_book]
                     ON [pafupi_account].[acc_no] = [referal_book].[acc_no]
					 INNER JOIN [pafupi].[dbo].[account_status]
                     ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                     INNER JOIN [pafupi].dbo.[UserProfile]
                     ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
					 WHERE [pafupi_account].[acc_no]=? AND [pafupi_account].[branch_id]=? AND [booklet].[book_no]=? AND [referal_book]=? AND [account_card].[card_no] =?';
        $result = $this->db->query($sql,array($data['acc_no'],$data['branch_id'],$data['book_no'],$data['referal_no'],$data['card_no']));
        $count = 0;
        foreach($result->result() as $row){
            $count = $row->count;
        }
        if($count>=1){
            return true;
        }
        else{
            return false;
        }
    }
    public function saveStarterIds($data){
        $errors = array();
        $username = $this->session->userdata('username');
        $today_date = Common_Tasks::generate_date();
        foreach($data as $d){
            if($this->accountExists($d['acc_no']) && $this->checkAccount($data)){

                $where = "acc_no =".$d['acc_no'];
                $acc_no =$d['acc_no'];
                unset($d['acc_no']);

                $sql_lock = 'UPDATE [pafupi].[dbo].[account_card]
						 SET [starter_id] = ?,[starter_date]=?
			             WHERE [acc_no] = ?';

                $done =$this->db->query($sql_lock,array($d['starter_id'],$today_date,$acc_no));
                if(!$done)
                    array_push($errors,"card number not saved for account no ".$acc_no);
                else{
                    $sql_lock = 'UPDATE [pafupi].[dbo].[account_status]
						 SET [acc_status] = ?
			             WHERE [acc_no] = ?';

                    $done =$this->db->query($sql_lock,array('starterpack',$acc_no));
                    array_push($errors,"card number successfully saved for account no ".$acc_no);
                    $this->log_activity('ATM card number successfully saved.Starterpack ID made', $username,$acc_no);

                }
            }
            else{
                array_push($errors,"account no ".$acc_no." does not exist or data doesnt match account");
            }

        }

        return $errors;
    }
    public function cardNumberExists($card_no){
        $sql_select = 'SELECT COUNT([card_no]) as total
		             FROM [pafupi].[dbo].[account_card]
					 WHERE [card_no] = ?';

        $query = $this->db->query($sql_select,array($card_no));
        foreach($query->result() as $row){
            if($row->total >=1){
                return true;
            }
            else{
                return false;
            }
        }

    }    /*
     * update accounts with data from Fincard
     */
    public function saveCardNumbers($data=false){
        $errors = array();
        $cards = 0;
        try{
        if(!$data){
            $data = array();
            $accounts = $this->getToCard();
            foreach($accounts as $account){
                array_push($data,$account['acc_no']);
            }
        }
        $username = $this->session->userdata('username');
        $today_date = Common_Tasks::generate_date();
        $card = array();
        //check fincard
        try{

            $db = $this->config->item('db_connection'); if(!$db){
                return "Error connecting to Fincard";
            }
            $sql = "SELECT account_id, maint_card.pan,maint_account.account_sys_id as sys_id FROM maint_account
                LEFT JOIN maint_card_acct ON maint_account.account_sys_id = maint_card_acct.account_sys_id
                LEFT JOIN maint_card ON maint_card.card_id = maint_card_acct.card_id
                WHERE maint_card.card_holder_initial ='PAFUPI'";
            $stmt = $db->prepare($sql);
            $pafupi = 'PAFUPI';
            //$stmt->bindParam(1,$pafupi, PDO::PARAM_STR, 5);
            //$stmt = $db->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll();
            if($result){
                $num = 0;
                foreach($result as $r)
                {
                    $card[$num]['card_no'] = $r['pan'];
					$card[$num]['sys_id'] = $r['sys_id'];
                    $num++;
                }

            }
            else{

            }

        }
        catch(Exception $e){
            array_push($errors,"error connecting to fincard ");
            return "Error connecting to fincard ";
        }
        //insert account_card

        if(!empty($card)){
            //$accounts  = $this->getLinkedAccount();
            $count = 0;
            foreach($card as $c){
                //$acc = $accounts[$count]['acc_no'];
                if($this->accountExists($data[$count])){


                    if($accounts[$count]['acc_status'] == 'linked'){
                        //$status = 'starterpack';
                        $status = 'carded';
                    }
                    elseif($accounts[$count]['acc_status'] == 'precarded'){
                        $status = 'carded';
                    }
                    else{
                        continue;
                    }

                    if(!$this->cardNumberExists($c['card_no'])){
                        
                        if(true){
                            $fin_updated = false;
                            try{
                                $db = $this->config->item('db_connection');
                                $db = $this->config->item('db_connection'); if(!$db){
                                    return "Error connecting to Fincard";
                                }
                                /*$sql = "UPDATE [dbo].[maint_account]
                                        SET [account_id] = :account_id
                                        WHERE account_sys_id =
                                       (SELECT account_sys_id FROM maint_card_acct
                                        LEFT JOIN maint_card ON maint_card_acct.card_id = maint_card.card_id
                                        WHERE maint_card.pan = :card_no )";*/
                                $sql = "UPDATE [maint_account]
                                SET account_id =$data[$count]
                                WHERE len(account_id) =4 AND account_sys_id=". $c['sys_id'];
                                $stmt = $db->prepare($sql);
                               // $stmt->bindParam(':account_id',$data[$count]);
                                //$len_ = 4;
                                // $stmt->bindParam(':len_',$len_);
                                //$stmt->bindParam(':sys_id',$c['sys_id']);
                                $fin_updated = $stmt->execute(); }
                            catch(Exception $e){

                            }
                            if($fin_updated){
                                $this->log_activity('card number successfully saved', $username,$data[$count]);
                                array_push($errors,"card number successfully saved for account no ".$data[$count]);
                                $sql_update = 'UPDATE [pafupi].[dbo].[account_status]
                                   SET [acc_status] = ?
                                   WHERE [account_status].[acc_no]=?';
                                $done = $this->db->query($sql_update,array($status,$data[$count]));
								$sql_create_user = 'INSERT INTO [pafupi].[dbo].[account_card]
			                    ([card_no],[acc_no],[card_date])
								 VALUES(?,?,?)';	
	
	                            $done = $this->db->query($sql_create_user,array($c['card_no'],$data[$count],$today_date));
	
	                            if(!$done)
	                              array_push($errors,"card number not saved for account no ".$data[$count]);
								else
                                  $cards++;
								
                            }
                            else{
                                $sql = 'DELETE FROM [pafupi].[dbo].[account_card] WHERE [acc_no] =?';
                                $done = $this->db->query($sql,array($data[$count]));
                            }

                        }
                    }
                    else{
                        array_push($errors,"card number already there for account no ".$data[$count]);
                    }
                }


                else{
                    array_push($errors,"account no ".$data[$count]." does not exist");
                }
                $count++;
            }
        }
        else{
            array_push($errors,"no data was passed for saving");
        }
        return $cards. " account successfully updated";
        }
        catch(Exception $e){
            redirect("");
        }
    }
    public function removeAccount($data){
        $done = array();
        $username =  $this->session->userdata('username');
        $fullname =  $this->session->userdata('fullname');
        for($i=0;$i<count($data);$i++){
            $acc_no = $data[$i];
            $sql_delete = 'DELETE FROM [pafupi].[dbo].[pafupi_account] ';
            $sql_delete.= 'WHERE [acc_no] = ? ';

            $deleted = $this->db->query($sql_delete,array($acc_no));
            if($deleted){
                array_push($done,'account no '.$data[$i].' deleted');
                $this->log_activity('Account deleted by '.$fullname, $username,$data[$i]);
            }
            else{
                array_push($done,'account no '.$data[$i].' deletion failed');
                $this->log_activity('Account deletion failed. Action by '.$fullname, $username,$data[$i]);
            }
        }
        return $done;
    }
    public function regeneratePins($data){
        $book_no =false;
        try{
        for($i=0;$i<count($data);$i++){
            $acc_no = $data[$i];
            $sql_select = 'SELECT [book_no]
		             FROM [pafupi].[dbo].[booklet]
					 WHERE [acc_no] = ?';

            $query = $this->db->query($sql_select,array($acc_no));

            if($query->num_rows >= 1){
                foreach($query->result() as $row){

                    $book_no =  $row->book_no;


                }
            }
            else{
                return false;
            }
            if($book_no){

                $sql_delete = 'DELETE FROM [pafupi].[dbo].[trans_pins] ';
                $sql_delete.= 'WHERE [book_no] = ? ';

                $deleted = $this->db->query($sql_delete,array($book_no));
                if(true || $deleted ){
                    $num = 0;
                    for($i=0;$i<50;$i++){

                        $sql_upload = 'INSERT INTO [pafupi].[dbo].[trans_pins]
		                    ([book_no],[trans_pin])
							 VALUES(?,?)';
                        $done=$this->db->query($sql_upload,array($book_no,$this->generatePin()));
                        if($done){
                            $num++;
                        }

                    }
                    $username = $this->session->userdata('username');
                    $this->log_activity('Pins regerated',$username,$acc_no);
                    return $num;
                }
                else{
                    return false;
                }
            }
            else
                return false;
        }
        return false;
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function removeBooklet($book_no){

        $sql_delete = 'DELETE FROM [pafupi].[dbo].[booklet] ';
        $sql_delete.= 'WHERE [book_no] = ? ';

        $deleted = $this->db->query($sql_delete,array($book_no));
    }
    public function generatePin(){
        //$pins = array();

        $rand =  mt_rand(0,9).mt_rand(0,9).mt_rand(0,9).mt_rand(0,9).mt_rand(0,9).mt_rand(0,9).mt_rand(0,9).mt_rand(0,9).mt_rand(0,9);

        return $rand;
    }
    public function getFive($var=false){
        /* $five ="";
         $alpha = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
         if($var == 0){
             for($i=0;$i<5;$i++){
                 $ran = mt_rand(0,1);
                 if($ran == 0){
                     $five .=$alpha[mt_rand(0,25)];
                 }
                 else{
                     $five .= mt_rand(0,9);
                 }
             }
         }
         else{
             for($i=0;$i<5;$i++){
                 $ran = mt_rand(0,1);
                 if($ran == 1){
                     $five .=$alpha[mt_rand(0,25)];
                 }
                 else{
                     $five .= mt_rand(0,9);
                 }
             }
         }
         return $five;*/
        $sql = "SELECT TOP 1 [starter_id] as id FROM [pafupi].[dbo].[pafupi_account] ORDER BY [date_created] DESC";
        $result = $this->db->query($sql);
        $id = 0;
        foreach($result->result() as $row){
            $id = $row->id;
        }
        if(substr($id,0,9) == date("Y-md")){
            $before = substr($id,10,4);
            $starter_id = date("Y-md")."-".($before+1);
        }
        elseif($id == 0){
            $before = 1000;
            $starter_id = date("Y-md")."-".$before;
        }
        else{
            $before = 1000;
            $starter_id = date("Y-mmd")."-".$before;
        }
        return $starter_id;
    }
    public function starterIdExists($starter_id){
        $sql_select = 'SELECT [starter_id]
		             FROM [pafupi].[dbo].[pafupi_account]
					 WHERE [starter_id] = ?';

        $query = $this->db->query($sql_select,array($starter_id));
        foreach($query->result() as $row){
            if($row->starter_id == $starter_id){
                return true;
            }
        }
        return false;
    }
    public function generateStarterId(){
        $starter_id = $this->getFive();//mt_rand(0,1))."-".$this->getFive(mt_rand(0,1))."-".$this->getFive(mt_rand(0,1))."-".$this->getFive(mt_rand(0,1))."-".$this->getFive(mt_rand(0,1));
        $is_there = true;
        //while($is_there){
        $starter = $this->starterIdExists($starter_id);
        if(!$starter){
            $is_there = false;
            return $starter_id;
        }
        else{
            return false;
        }
        // }
    }
    public function log_activity($activity, $username,$account_id) {

        //generate a date
        $today_date = Common_Tasks::generate_date();

        $activity_id = mt_rand() + round(microtime()*100);

        //get user details
        $user_details = $this->get_details($username);

        $user_details['FullName'] = Validation::sanitise_input($user_details['FullName']);

        //get the ip address
        $ip_address = Network::get_ip_address();

        $sql_activity = 'INSERT INTO [pafupi].[dbo].[AccountActivity] ';
        $sql_activity.= 'VALUES(?,?,?,?,?,?,?,?,?) ';

        $this->db->query($sql_activity,array($activity_id,$today_date,$username,
            $user_details['Branch'],$account_id,$ip_address,$user_details['FullName'],$user_details['RoleName'],$activity));

    }
    public function get_details($username)
    {

        //this method returns a user's details
        $sql_details = 'SELECT [RoleName], [Username], [UserProfile].[RoleID], [Branch],';
        $sql_details.= '[UserProfile].[UserID],[FullName], [LastPasswordChange], CONVERT(VARCHAR(19),LastLogin) AS [LastLogin] ';
        $sql_details.= 'FROM [pafupi].[dbo].[UserRole] ';
        $sql_details.= 'INNER JOIN [pafupi].[dbo].[UserProfile] ';
        $sql_details.= 'ON [UserRole].[RoleID] = [UserProfile].[RoleID] ';
        $sql_details.= 'WHERE [Username] = ? ';

        $result = $this->db->query($sql_details,array($username));

        foreach($result->result() as $row)
        {

            $user_details['RoleName']           = $row->RoleName;
            $user_details['Username']           = $row->Username;
            $user_details['UserRoleID']         = $row->RoleID;
            $user_details['UserID']             = $row->UserID;
            $user_details['FullName']           = $row->FullName;
            $user_details['LastPasswordChange'] = $row->LastPasswordChange;
            $user_details['LastLogin']          = strtotime($row->LastLogin);
            $user_details['Branch']             = $row->Branch;

        }

        return $user_details;

    }
    public function lockAccount($data){
        $done = array();
        try{
        $username =  $this->session->userdata('username');
        $fullname =  $this->session->userdata('fullname');
        $today_date = Common_Tasks::generate_date();
        for($i=0;$i<count($data);$i++){

            $sql_lock = 'UPDATE [pafupi].[dbo].[account_status]
						 SET [lock_status] = ?,[lock_date]=?
			             WHERE [acc_no] = ?';

            $result =$this->db->query($sql_lock,array(true,$today_date,$data[$i]));
            if($result){
                array_push($done,'account no '.$data[$i].' locked');
                $this->log_activity('Account locked by '.$fullname, $username,$data[$i]);
            }
            else{
                array_push($done,'account no '.$data[$i].' locking failed');
                $this->log_activity('Account locking failed by '.$fullname, $username,$data[$i]);
            }
        }
        return $done;
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function unlockAccount($data){
        try{
        $done = array();
        $username =  $this->session->userdata('username');
        $fullname =  $this->session->userdata('fullname');
        $today_date = Common_Tasks::generate_date();
        for($i=0;$i<count($data);$i++){

            $sql_lock = 'UPDATE [pafupi].[dbo].[account_status]
						 SET [lock_status] = ?,[unlock_date]=?
			             WHERE [acc_no] = ?';

            $result =$this->db->query($sql_lock,array(false,$today_date,$data[$i]));
            if($result){
                array_push($done,'account no '.$data[$i].' unlocked');
                $this->log_activity('Account unlocked by '.$fullname, $username,$data[$i]);
            }
            else{
                array_push($done,'account no '.$data[$i].' unlocking failed');
                $this->log_activity('Account unlocking failed by '.$fullname, $username,$data[$i]);
            }
        }
        return $done;
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function dispatchAccount($data){
        $done = array();
        $username =  $this->session->userdata('username');
        $fullname =  $this->session->userdata('fullname');
        $today_date = Common_Tasks::generate_date();
        try{
        for($i=0;$i<count($data);$i++){

            $sql_lock = 'UPDATE [pafupi].[dbo].[account_status]
						 SET [dispatch_flag] = ?
			             WHERE [acc_no] = ?';

            $result =$this->db->query($sql_lock,array(true,$data[$i]));
            if($result){
                $sql_account = 'INSERT INTO [pafupi].[dbo].[account_mobility]
		                    ([acc_no],[dispatched_by],[AOC],[dispatch_date])
							 VALUES(?,?,?,?)';
                $dis_insert =$this->db->query($sql_account,array($data[$i],$fullname,"",$today_date));
                if($dis_insert){
                    array_push($done,'account no '.$data[$i].' dispatched');
                    $this->log_activity('Account dispatched by '.$fullname, $username,$data[$i]);
                }
                else{
                    $sql_lock = 'UPDATE [pafupi].[dbo].[account_status]
						 SET [dispatch_flag] = ?
			             WHERE [acc_no] = ?';

                    $result =$this->db->query($sql_lock,array(false,$data[$i]));
                    array_push($done,'account no '.$data[$i].' dispatching failed');
                    $this->log_activity('Account dispatching failed by '.$fullname, $username,$data[$i]);
                }

            }
            else{
                array_push($done,'account no '.$data[$i].' dispatching failed');
                $this->log_activity('Account dispatching failed by '.$fullname, $username,$data[$i]);
            }
        }
        return $done;
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function search_dispatched($parameter){
        try{
        $parameter = "%" . $parameter . "%";
        $sql = ' SELECT [pafupi_account].*,[branches].[branch_name],[Username],[FullName],[dispatched_by],[AOC],[card_no]
                                FROM [pafupi].[dbo].[pafupi_account]
					            INNER JOIN [pafupi].[dbo].[branches]
					            ON [pafupi_account].[branch_id] = [branches].[branch_id]
					            INNER JOIN [pafupi].dbo.[UserProfile]
					            ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
					            INNER JOIN [pafupi].[dbo].[account_status]
					            ON [pafupi_account].[acc_no] = [account_status].[acc_no]
					            INNER JOIN [pafupi].[dbo].[account_card]
					            ON [pafupi_account].[acc_no] = [account_card].[acc_no]
					            INNER JOIN [pafupi].[dbo].[account_mobility]
					            ON [pafupi_account].[acc_no] = [account_mobility].[acc_no]
					            WHERE [account_status].[dispatch_flag] =? AND ([pafupi_account].[opened_by] LIKE ?
					            OR [UserProfile].[FullName] LIKE ?
					            OR [account_mobility].[dispatched_by] LIKE ?
					            OR [account_mobility].[AOC] LIKE ?
					            OR [branches].[branch_name] LIKE ?
					            OR [UserProfile].[Username] LIKE ?)';
        $data['result'] = $this->db->query($sql,array(true,$parameter,$parameter,$parameter,$parameter,$parameter,$parameter))->result_array();

        $count = ' SELECT COUNT(*) AS [Total]
					         FROM [pafupi].[dbo].[pafupi_account]
					            INNER JOIN [pafupi].[dbo].[branches]
					            ON [pafupi_account].[branch_id] = [branches].[branch_id]
					            INNER JOIN [pafupi].dbo.[UserProfile]
					            ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
					            INNER JOIN [pafupi].[dbo].[account_status]
					            ON [pafupi_account].[acc_no] = [account_status].[acc_no]
					            INNER JOIN [pafupi].[dbo].[account_card]
					            ON [pafupi_account].[acc_no] = [account_card].[acc_no]
					            INNER JOIN [pafupi].[dbo].[account_mobility]
					            ON [pafupi_account].[acc_no] = [account_mobility].[acc_no]
					            WHERE [account_status].[dispatch_flag]=? AND ([pafupi_account].[opened_by] LIKE ?
					            OR [UserProfile].[FullName] LIKE ?
					            OR [account_mobility].[dispatched_by] LIKE ?
					            OR [account_mobility].[AOC] LIKE ?
					            OR [branches].[branch_name] LIKE ?
					            OR [UserProfile].[Username] LIKE ?)';
        $result = $this->db->query($count,array(true,$parameter,$parameter,$parameter,$parameter,$parameter,$parameter));

        foreach($result->result() as $row) {

            $total =  $row->Total;

        }

        $data['total'] = $total;

        return $data;
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function search_precarded($limit=false,$start=false,$parameter){
        $parameter = "%" . $parameter . "%";
        try{
        if($limit){

            $sql = "SELECT * FROM (
                     SELECT row_number() OVER (ORDER BY [pafupi_account].acc_no) AS rownum,
                     [pafupi_account].*,[branches].[branch_name],[UserProfile].[FullName]
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[account_status]
                     ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                     INNER JOIN [pafupi].dbo.[UserProfile]
                     ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
					 WHERE [account_status].[acc_status]=?
					 AND ([pafupi_account].[opened_by] LIKE ?
					 OR [UserProfile].[FullName] LIKE ?
					 OR [branches].[branch_name] LIKE ?
					 OR [UserProfile].[Username] LIKE ?)

					    ) AS A
					     WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1" ;
            $data['result'] = $this->db->query($sql,array('precarded',$parameter,$parameter,$parameter,$parameter))->result_array();
        }
        else{

            $sql = 'SELECT [pafupi_account].*,[branches].[branch_name],[UserProfile].[FullName]
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[account_status]
                     ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                     INNER JOIN [pafupi].dbo.[UserProfile]
                     ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
					 WHERE [account_status].[acc_status]=?
					 AND ([pafupi_account].[opened_by] LIKE ?
					 OR [UserProfile].[FullName] LIKE ?
					 OR [branches].[branch_name] LIKE ?
					 OR [UserProfile].[Username] LIKE ?)' ;
            $data['result'] = $this->db->query($sql,array('precarded',$parameter,$parameter,$parameter,$parameter))->result_array();
        }
        $count = ' SELECT COUNT(*) AS [Total]
					FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[account_status]
                     ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                     INNER JOIN [pafupi].dbo.[UserProfile]
                      ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
					 WHERE [account_status].[acc_status]=?
					 AND ([pafupi_account].[opened_by] LIKE ?
					 OR [UserProfile].[FullName] LIKE ?
					 OR [branches].[branch_name] LIKE ?
					 OR [UserProfile].[Username] LIKE ?)';
        $result = $this->db->query($count,array('precarded',$parameter,$parameter,$parameter,$parameter));

        foreach($result->result() as $row) {

            $total =  $row->Total;

        }

        $data['total'] = $total;

        return $data;
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }

    }
    public function search_linked($limit=false,$start=false,$parameter){
        $parameter = "%" . $parameter . "%";
        try{
        if($limit){

            $sql = "SELECT * FROM (
                     SELECT row_number() OVER (ORDER BY [pafupi_account].acc_no) AS rownum,
                     [pafupi_account].*,[branches].[branch_name],[UserProfile].[FullName],[book_no],[referal_no],[card_no]
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[account_status]
                     ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                     INNER JOIN [pafupi].[dbo].[account_card]
                       ON [pafupi_account].[acc_no] = [account_card].[acc_no]
                     INNER JOIN [pafupi].[dbo].[booklet]
                     ON [pafupi_account].[acc_no] = [booklet].[acc_no]
                     INNER JOIN [pafupi].[dbo].[referal_book]
                     ON [pafupi_account].[acc_no] = [referal_book].[acc_no]
                     INNER JOIN [pafupi].dbo.[UserProfile]
                     ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
					 WHERE [account_status].[acc_status]=?
					 AND ([pafupi_account].[opened_by] LIKE ?
					 OR [UserProfile].[FullName] LIKE ?
					 OR [branches].[branch_name] LIKE ?
					 OR [UserProfile].[Username] LIKE ?)

					    ) AS A
					     WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1" ;
            $data['result'] = $this->db->query($sql,array('linked',$parameter,$parameter,$parameter,$parameter))->result_array();
        }
        else{

            $sql = 'SELECT [pafupi_account].*,[branches].[branch_name],[UserProfile].[FullName],[book_no],[referal_no],[card_no]
		             FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[account_status]
                     ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                     INNER JOIN [pafupi].[dbo].[account_card]
                     ON [pafupi_account].[acc_no] = [account_card].[acc_no]
                      INNER JOIN [pafupi].[dbo].[booklet]
                     ON [pafupi_account].[acc_no] = [booklet].[acc_no]
                     INNER JOIN [pafupi].[dbo].[referal_book]
                     ON [pafupi_account].[acc_no] = [referal_book].[acc_no]
                     INNER JOIN [pafupi].dbo.[UserProfile]
                     ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
					 WHERE [account_status].[acc_status]=?
					 AND ([pafupi_account].[opened_by] LIKE ?
					 OR [UserProfile].[FullName] LIKE ?
					 OR [branches].[branch_name] LIKE ?
					 OR [UserProfile].[Username] LIKE ?)' ;
            $data['result'] = $this->db->query($sql,array('linked',$parameter,$parameter,$parameter,$parameter))->result_array();
        }
        $count = ' SELECT COUNT(*) AS [Total]
					FROM [pafupi].[dbo].[pafupi_account]
					 INNER JOIN [pafupi].[dbo].[branches]
					 ON [pafupi_account].[branch_id] = [branches].[branch_id]
					 INNER JOIN [pafupi].[dbo].[account_status]
                     ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                     INNER JOIN [pafupi].[dbo].[account_card]
                      ON [pafupi_account].[acc_no] = [account_card].[acc_no]
                     INNER JOIN [pafupi].dbo.[UserProfile]
                      INNER JOIN [pafupi].[dbo].[booklet]
                     ON [pafupi_account].[acc_no] = [booklet].[acc_no]
                     INNER JOIN [pafupi].[dbo].[referal_book]
                     ON [pafupi_account].[acc_no] = [referal_book].[acc_no]
                      ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
					 WHERE [account_status].[acc_status]=?
					 AND ([pafupi_account].[opened_by] LIKE ?
					 OR [UserProfile].[FullName] LIKE ?
					 OR [branches].[branch_name] LIKE ?
					 OR [UserProfile].[Username] LIKE ?)';
        $result = $this->db->query($count,array('linked',$parameter,$parameter,$parameter,$parameter));

        foreach($result->result() as $row) {

            $total =  $row->Total;

        }

        $data['total'] = $total;

        return $data;
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }

    } /* public function getStarterPacks(){

         $sql = 'SELECT [pafupi_account].*,[branches].[branch_name],[Username]
                                         FROM [pafupi].[dbo].[pafupi_account]
                                         INNER JOIN [pafupi].[dbo].[branches]
                                         ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [pafupi].dbo.[UserProfile]
                                         ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                                         INNER JOIN [pafupi].[dbo].[account_status]
                                         ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                         WHERE [account_status].[acc_status]=?';
         $result = $this->db->query($sql,array('starterpack'))->result_array();
         return $result;

     }*/
    public function search_starter_pack($parameter){
        $parameter = "%" . $parameter . "%";
        try{
        $sql = 'SELECT [pafupi_account].acc_no) AS rownum,[pafupi_account].*,[branches].[branch_name],[Username]
                                        FROM [pafupi].[dbo].[pafupi_account]
                                        INNER JOIN [pafupi].[dbo].[branches]
                                        ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                        INNER JOIN [pafupi].dbo.[UserProfile]
                                        ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                                        INNER JOIN [pafupi].[dbo].[account_status]
                                        ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                        WHERE [account_status].[acc_status]=?
					 AND ([pafupi_account].[opened_by] LIKE ?
					 OR [UserProfile].[FullName] LIKE ?
					 OR [branches].[branch_name] LIKE ?
					 OR [UserProfile].[Username] LIKE ?)'					 ;
        $data['result'] = $this->db->query($sql,array('starterpack',$parameter,$parameter,$parameter,$parameter))->result_array();

        $count = ' SELECT COUNT(*) AS [Total]
					FROM [pafupi].[dbo].[pafupi_account]
                                        INNER JOIN [pafupi].[dbo].[branches]
                                        ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                        INNER JOIN [pafupi].dbo.[UserProfile]
                                        ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                                        INNER JOIN [pafupi].[dbo].[account_status]
                                        ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                        WHERE [account_status].[acc_status]=?
					 AND ([pafupi_account].[opened_by] LIKE ?
					 OR [UserProfile].[FullName] LIKE ?
					 OR [branches].[branch_name] LIKE ?
					 OR [UserProfile].[Username] LIKE ?)';
        $result = $this->db->query($count,array('starterpack',$parameter,$parameter,$parameter,$parameter));

        foreach($result->result() as $row) {

            $total =  $row->Total;

        }

        $data['total'] = $total;

        return $data;
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function search_undispatched($parameter){
        $parameter = "%" . $parameter . "%";
        try{
        $sql = 'SELECT [pafupi_account].*,[branches].[branch_name],[Username],[FullName],[card_no]
                FROM [pafupi].[dbo].[pafupi_account]
			    INNER JOIN [pafupi].[dbo].[branches]
			    ON [pafupi_account].[branch_id] = [branches].[branch_id]
			    INNER JOIN [pafupi].dbo.[UserProfile]
			    ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
			    INNER JOIN [pafupi].[dbo].[account_status]
				ON [pafupi_account].[acc_no] = [account_status].[acc_no]
			    INNER JOIN [pafupi].[dbo].[account_card]
				ON [pafupi_account].[acc_no] = [account_card].[acc_no]
				INNER JOIN [pafupi].[dbo].[booklet]
				ON ON [pafupi_account].[acc_no] = [booklet].[acc_no]
				INNER JOIN [pafupi].[dbo].[referal_book]
				ON ON [pafupi_account].[acc_no] = [referal_book].[acc_no]
			    WHERE [account_status].[dispatch_flag]=?
					 AND ([pafupi_account].[opened_by] LIKE ?
					 OR [UserProfile].[FullName] LIKE ?
					 OR [branches].[branch_name] LIKE ?
					 OR [UserProfile].[Username] LIKE ?)'					 ;
        $data['result'] = $this->db->query($sql,array(false,$parameter,$parameter,$parameter,$parameter))->result_array();

        $count = ' SELECT COUNT(*) AS [Total]
					FROM [pafupi].[dbo].[pafupi_account]
			    INNER JOIN [pafupi].[dbo].[branches]
			    ON [pafupi_account].[branch_id] = [branches].[branch_id]
			    INNER JOIN [pafupi].dbo.[UserProfile]
			    ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
			    INNER JOIN [pafupi].[dbo].[account_status]
				ON [pafupi_account].[acc_no] = [account_status].[acc_no]
			   
				ON [pafupi_account].[acc_no] = [account_card].[acc_no]
				INNER JOIN [pafupi].[dbo].[booklet]
				ON ON [pafupi_account].[acc_no] = [booklet].[acc_no]
				INNER JOIN [pafupi].[dbo].[referal_book]
				ON ON [pafupi_account].[acc_no] = [referal_book].[acc_no]
				
			    WHERE [account_status].[dispatch_flag]=?
					 AND ([pafupi_account].[opened_by] LIKE ?
					 OR [UserProfile].[FullName] LIKE ?
					 OR [branches].[branch_name] LIKE ?
					 OR [UserProfile].[Username] LIKE ?)';
        $result = $this->db->query($count,array(false,$parameter,$parameter,$parameter,$parameter));

        foreach($result->result() as $row) {

            $total =  $row->Total;

        }

        $data['total'] = $total;

        return $data;
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function search_unlocked($parameter){
        $parameter = "%" . $parameter . "%";
        try{
        $sql = 'SELECT [pafupi_account].*,
                [branches].[branch_name],[Username],[FullName]
                FROM [pafupi].[dbo].[pafupi_account]
                INNER JOIN [pafupi].[dbo].[branches]
                ON [pafupi_account].[branch_id] = [branches].[branch_id]
                INNER JOIN [pafupi].dbo.[UserProfile]
                ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                INNER JOIN [pafupi].[dbo].[account_status]
                ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                
                INNER JOIN [pafupi].[dbo].[account_card]
				ON [pafupi_account].[acc_no] = [account_card].[acc_no]
				INNER JOIN [pafupi].[dbo].[booklet]
				ON ON [pafupi_account].[acc_no] = [booklet].[acc_no]
				INNER JOIN [pafupi].[dbo].[referal_book]
				ON ON [pafupi_account].[acc_no] = [referal_book].[acc_no]
				
                WHERE [account_status].[lock_status]=?
			    AND ([pafupi_account].[opened_by] LIKE ?
			    OR [UserProfile].[FullName] LIKE ?
			    OR [branches].[branch_name] LIKE ?
			    OR [UserProfile].[Username] LIKE ?)';
        $data['result'] = $this->db->query($sql,array(false,$parameter,$parameter,$parameter,$parameter))->result_array();

        $count = ' SELECT COUNT(*) AS [Total]
				 FROM [pafupi].[dbo].[pafupi_account]
                INNER JOIN [pafupi].[dbo].[branches]
                ON [pafupi_account].[branch_id] = [branches].[branch_id]
                INNER JOIN [pafupi].dbo.[UserProfile]
                ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                INNER JOIN [pafupi].[dbo].[account_status]
                ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                
				INNER JOIN [pafupi].[dbo].[account_card]
				ON [pafupi_account].[acc_no] = [account_card].[acc_no]
				INNER JOIN [pafupi].[dbo].[booklet]
				ON ON [pafupi_account].[acc_no] = [booklet].[acc_no]
				INNER JOIN [pafupi].[dbo].[referal_book]
				ON ON [pafupi_account].[acc_no] = [referal_book].[acc_no]
				
                WHERE [account_status].[lock_status]=?
			    AND ([pafupi_account].[opened_by] LIKE ?
			    OR [UserProfile].[FullName] LIKE ?
			    OR [branches].[branch_name] LIKE ?
			    OR [UserProfile].[Username] LIKE ?)';
        $result = $this->db->query($count,array(false,$parameter,$parameter,$parameter,$parameter));

        foreach($result->result() as $row) {

            $total =  $row->Total;

        }

        $data['total'] = $total;

        return $data;
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function search_locked($parameter){
        $parameter = "%" . $parameter . "%";
        try{
        $sql = 'SELECT [pafupi_account].*,
                [branches].[branch_name],[Username],[FullName]
                FROM [pafupi].[dbo].[pafupi_account]
                INNER JOIN [pafupi].[dbo].[branches]
                ON [pafupi_account].[branch_id] = [branches].[branch_id]
                INNER JOIN [pafupi].dbo.[UserProfile]
                ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                INNER JOIN [pafupi].[dbo].[account_status]
                ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                
				INNER JOIN [pafupi].[dbo].[account_card]
				ON [pafupi_account].[acc_no] = [account_card].[acc_no]
				INNER JOIN [pafupi].[dbo].[booklet]
				ON ON [pafupi_account].[acc_no] = [booklet].[acc_no]
				INNER JOIN [pafupi].[dbo].[referal_book]
				ON ON [pafupi_account].[acc_no] = [referal_book].[acc_no]
				
				
                WHERE [account_status].[lock_status]=?
			    AND ([pafupi_account].[opened_by] LIKE ?
			    OR [UserProfile].[FullName] LIKE ?
			    OR [branches].[branch_name] LIKE ?
			    OR [UserProfile].[Username] LIKE ?)';
        $data['result'] = $this->db->query($sql,array(true,$parameter,$parameter,$parameter,$parameter))->result_array();

        $count = ' SELECT COUNT(*) AS [Total]
				 FROM [pafupi].[dbo].[pafupi_account]
                INNER JOIN [pafupi].[dbo].[branches]
                ON [pafupi_account].[branch_id] = [branches].[branch_id]
                INNER JOIN [pafupi].dbo.[UserProfile]
                ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                INNER JOIN [pafupi].[dbo].[account_status]
                ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                
				 INNER JOIN [pafupi].[dbo].[account_card]
				ON [pafupi_account].[acc_no] = [account_card].[acc_no]
				INNER JOIN [pafupi].[dbo].[booklet]
				ON ON [pafupi_account].[acc_no] = [booklet].[acc_no]
				INNER JOIN [pafupi].[dbo].[referal_book]
				ON ON [pafupi_account].[acc_no] = [referal_book].[acc_no]
				
				
                WHERE [account_status].[lock_status]=?
			    AND ([pafupi_account].[opened_by] LIKE ?
			    OR [UserProfile].[FullName] LIKE ?
			    OR [branches].[branch_name] LIKE ?
			    OR [UserProfile].[Username] LIKE ?)';
        $result = $this->db->query($count,array(true,$parameter,$parameter,$parameter,$parameter));

        foreach($result->result() as $row) {

            $total =  $row->Total;

        }

        $data['total'] = $total;

        return $data;
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function search_carded($parameter){
        $parameter = "%" . $parameter . "%";
        try{
        $sql = 'SELECT [pafupi_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName],[acc_status],[referal_no],[book_no]
                                         FROM [pafupi].[dbo].[pafupi_account]
                                         INNER JOIN [pafupi].[dbo].[branches]
                                         ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                        
                                         INNER JOIN [pafupi].[dbo].[account_status]
                                         ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                         INNER JOIN [pafupi].dbo.[UserProfile]
                                         ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
					 WHERE [account_status].[acc_status]=?
					 AND ([pafupi_account].[opened_by] LIKE ?
					 OR [UserProfile].[FullName] LIKE ?
					 OR [branches].[branch_name] LIKE ?
					 OR [UserProfile].[Username] LIKE ?)'					 ;
        $data['result'] = $this->db->query($sql,array('carded',$parameter,$parameter,$parameter,$parameter))->result_array();

        $count = ' SELECT COUNT(*) AS [Total] FROM
					 [pafupi].[dbo].[pafupi_account]
                                         INNER JOIN [pafupi].[dbo].[branches]
                                         ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [pafupi].[dbo].[account_card]
                                         ON [pafupi_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[account_status]
                                         ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                         INNER JOIN [pafupi].dbo.[UserProfile]
                                         ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
					 WHERE [account_status].[acc_status]=?
					 AND [pafupi_account].[opened_by] LIKE ?
					 OR [UserProfile].[FullName] LIKE ?
					 OR [branches].[branch_name] LIKE ?
					 OR [UserProfile].[Username] LIKE ?';
        $result = $this->db->query($count,array('carded',$parameter,$parameter,$parameter,$parameter));

        foreach($result->result() as $row) {

            $total =  $row->Total;

        }

        $data['total'] = $total;

        return $data;
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function getAccountsWithCustomer($start=false,$limit=false){
        try{
        if($limit){
            $sql = "SELECT * FROM (SELECT [pafupi_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName],[acc_status]
                                         ,[received_date],[AOC],[dispatched_by]
                                         FROM [pafupi].[dbo].[pafupi_account]
                                         INNER JOIN [pafupi].[dbo].[branches]
                                         ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [pafupi].[dbo].[account_card]
                                         ON [pafupi_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[Customer]
                                         ON [pafupi_account].[customer_id] = [Customer].[CustomerID]
                                         INNER JOIN [pafupi].[dbo].[CustomerAccount]
                                         ON [pafupi_account].[customer_id] = [CustomerAccount].[CustomerID]
                                         INNER JOIN [pafupi].[dbo].[CustomerProfile]
                                         ON [pafupi_account].[customer_id] = [CustomerProfile].[CustomerID]
                                         INNER JOIN [pafupi].[dbo].[account_status]
                                         ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[account_mobility]
                                         ON [pafupi_account].[acc_no] = [account_mobility].[acc_no]
                                         INNER JOIN [pafupi].dbo.[UserProfile]
                                         ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                                         WHERE CustomerProfile].[CustomerGender] !=?
                                         ) AS A
					     WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1" ;
            $data['result'] = $this->db->query($sql,array('NULL'))->result_array();
        }
        else{
            $sql = "SELECT [pafupi_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName],[acc_status]
                                         ,[received_date],[AOC],[dispatched_by]
                                         FROM [pafupi].[dbo].[pafupi_account]
                                         INNER JOIN [pafupi].[dbo].[branches]
                                         ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [pafupi].[dbo].[account_card]
                                         ON [pafupi_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[Customer]
                                         ON [pafupi_account].[customer_id] = [Customer].[CustomerID]
                                         INNER JOIN [pafupi].[dbo].[CustomerAccount]
                                         ON [pafupi_account].[customer_id] = [CustomerAccount].[CustomerID]
                                         INNER JOIN [pafupi].[dbo].[CustomerProfile]
                                         ON [pafupi_account].[customer_id] = [CustomerProfile].[CustomerID]
                                         INNER JOIN [pafupi].[dbo].[account_status]
                                         ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[account_mobility]
                                         ON [pafupi_account].[acc_no] = [account_mobility].[acc_no]
                                         INNER JOIN [pafupi].dbo.[UserProfile]
                                         ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                                          WHERE CustomerProfile].[CustomerGender] !=?
                                         " ;
            $data['result'] = $this->db->query($sql,array('NULL'))->result_array();

        }
        $sql = "SELECT COUNT([pafupi_account].[acc_no]) as Total
                                         FROM [pafupi].[dbo].[pafupi_account]
                                         INNER JOIN [pafupi].[dbo].[branches]
                                         ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [pafupi].[dbo].[account_card]
                                         ON [pafupi_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[Customer]
                                         ON [pafupi_account].[customer_id] = [Customer].[CustomerID]
                                         INNER JOIN [pafupi].[dbo].[CustomerAccount]
                                         ON [pafupi_account].[customer_id] = [CustomerAccount].[CustomerID]
                                         INNER JOIN [pafupi].[dbo].[CustomerProfile]
                                         ON [pafupi_account].[customer_id] = [CustomerProfile].[CustomerID]
                                         INNER JOIN [pafupi].[dbo].[account_status]
                                         ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[account_mobility]
                                         ON [pafupi_account].[acc_no] = [account_mobility].[acc_no]
                                         INNER JOIN [pafupi].dbo.[UserProfile]
                                         ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                                         WHERE CustomerProfile].[CustomerGender] !=?
                                         " ;
        $result = $this->db->query($sql,array('NULL'));
        foreach($result->result() as $row) {

            $total =  $row->Total;

        }

        $data['total'] = $total;

        return $data;
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function search_with_customer($parameter){
        $parameter = "%" . $parameter . "%";
        try{
        $sql = "SELECT [pafupi_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName],[acc_status]
                                         ,[received_date],[AOC],[dispatched_by]
                                         FROM [pafupi].[dbo].[pafupi_account]
                                         INNER JOIN [pafupi].[dbo].[branches]
                                         ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [pafupi].[dbo].[account_card]
                                         ON [pafupi_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[Customer]
                                         ON [pafupi_account].[customer_id] = [Customer].[CustomerID]
                                         INNER JOIN [pafupi].[dbo].[CustomerAccount]
                                         ON [pafupi_account].[customer_id] = [CustomerAccount].[CustomerID]
                                         INNER JOIN [pafupi].[dbo].[CustomerProfile]
                                         ON [pafupi_account].[customer_id] = [CustomerProfile].[CustomerID]
                                         INNER JOIN [pafupi].[dbo].[account_status]
                                         ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[account_mobility]
                                         ON [pafupi_account].[acc_no] = [account_mobility].[acc_no]
                                         INNER JOIN [pafupi].dbo.[UserProfile]
                                         ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                                         WHERE [pafupi_account].[opened_by] LIKE ?
                                         OR [UserProfile].[FullName] LIKE ?
                                         OR [branches].[branch_name] LIKE ?
                                         OR [UserProfile].[Username] LIKE ?
                                         OR [account_mobility].[AOC] LIKE ?
                                         OR [account_mobility].[dispatched_by] LIKE ?
                                         OR [Customer].[CustomerSurname] LIKE ?
                                         OR [Customer].[CustomerFirstName] LIKE ?
                                         OR [CustomerProfile].[CustomerVillage] LIKE ?
                                         OR [CustomerProfile].[CustomerTraditionalAuthority] LIKE ?
                                         " ;
        $data['result'] = $this->db->query($sql,array($parameter,$parameter,$parameter,$parameter,$parameter,$parameter,$parameter,$parameter,$parameter,$parameter))->result_array();
        $data['total'] = count($data['result']);

        return $data;
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }//for branch admin
    public function getDispatchedByBranch($branch_id,$count_acc=false){
        try{
        $sql = 'SELECT [pafupi_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName],[acc_status]
                                         ,[dispatched_by],[dispatch_date]
                                         FROM [pafupi].[dbo].[pafupi_account]
                                         INNER JOIN [pafupi].[dbo].[branches]
                                         ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [pafupi].[dbo].[account_card]
                                         ON [pafupi_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[account_status]
                                         ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                          INNER JOIN [pafupi].[dbo].[account_mobility]
                                         ON [pafupi_account].[acc_no] = [account_mobility].[acc_no]
                                         INNER JOIN [pafupi].dbo.[UserProfile]
                                         ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                                         WHERE [pafupi_account].[branch_id] = ? AND [account_status].[dispatch_flag] = ?';
        $result = $this->db->query($sql,array($branch_id,true))->result_array();
        $count = count($result);

        if($count_acc){
            return $count;
        }
        else{
            return $result;
        }
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function getReceivedByBranch($branch_id,$percentage=false,$count_acc= false){
        try{
        $sql = 'SELECT [pafupi_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName],[acc_status]
                                         ,[received_date]
                                         FROM [pafupi].[dbo].[pafupi_account]
                                         INNER JOIN [pafupi].[dbo].[branches]
                                         ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [pafupi].[dbo].[account_card]
                                         ON [pafupi_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[account_status]
                                         ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                          INNER JOIN [pafupi].[dbo].[account_mobility]
                                         ON [pafupi_account].[acc_no] = [account_mobility].[acc_no]
                                         INNER JOIN [pafupi].dbo.[UserProfile]
                                         ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                                         WHERE [pafupi_account].[branch_id] = ? AND [account_mobility].[received] = ?';
        $result = $this->db->query($sql,array($branch_id,true))->result_array();
        $count = count($result);

        if($percentage){
            $total_accounts = $this->getDispatchedByBranch($branch_id,true);
            if($total_accounts==0) $total_accounts=1;
            $percentage_received = round(($count/$total_accounts) * 100,0);

            return $percentage_received;
        }
        elseif($count_acc){
            return $count;
        }
        else{
            return $result;
        }
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function get_all_assigned_by_branch($branch_id,$limit=false,$start=false){
        try{
        $sql = "SELECT * FROM (
                                         SELECT row_number() OVER (ORDER BY [pafupi_account].acc_no) AS rownum,
                                         [pafupi_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName],[acc_status]
                                         ,[received_date],[AOC],[dispatch_date]
                                         FROM [pafupi].[dbo].[pafupi_account]
                                         INNER JOIN [pafupi].[dbo].[branches]
                                         ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [pafupi].[dbo].[account_card]
                                         ON [pafupi_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[account_status]
                                         ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                          INNER JOIN [pafupi].[dbo].[account_mobility]
                                         ON [pafupi_account].[acc_no] = [account_mobility].[acc_no]
                                         INNER JOIN [pafupi].dbo.[UserProfile]
                                         ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                                         WHERE [pafupi_account].[branch_id] = ? AND [account_mobility].[assigned_flag] = ?
                          )
                           AS A
					     WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1";
        $data['result'] = $this->db->query($sql,array($branch_id,true))->result_array();

        $sql = "SELECT COUNT(*) as [Total]
                                         FROM [pafupi].[dbo].[pafupi_account]
                                         INNER JOIN [pafupi].[dbo].[branches]
                                         ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [pafupi].[dbo].[account_card]
                                         ON [pafupi_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[account_status]
                                         ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[account_mobility]
                                         ON [pafupi_account].[acc_no] = [account_mobility].[acc_no]
                                         INNER JOIN [pafupi].dbo.[UserProfile]
                                         ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                                         WHERE [pafupi_account].[branch_id] = ? AND [account_mobility].[assigned_flag] = ?";
        $result = $this->db->query($sql,array($branch_id,true));
        foreach($result->result() as $row) {

            $total =  $row->Total;

        }

        $data['total'] = $total;

        return $data;
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function search_dispatched_not_received($branch_id=false,$parameter){
        $parameter = "%" . $parameter . "%";
        try{
        $sql = "SELECT [pafupi_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName],[acc_status]
                                         ,[received_date],[AOC],[starter_id],[dispatch_date],[dispatched_by]
                                         FROM [pafupi].[dbo].[pafupi_account]
                                         INNER JOIN [pafupi].[dbo].[branches]
                                         ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [pafupi].[dbo].[account_card]
                                         ON [pafupi_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[account_status]
                                         ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                          INNER JOIN [pafupi].[dbo].[account_mobility]
                                         ON [pafupi_account].[acc_no] = [account_mobility].[acc_no]
                                         INNER JOIN [pafupi].dbo.[UserProfile]
                                         ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                                         WHERE [pafupi_account].[branch_id] = ?
                                         AND [account_status].[dispatch_flag] =?
                                         AND [account_mobility].[assigned_flag] = ?
                                         AND [account_mobility].[received] = ?
                                         AND ( [UserProfile].[FullName] LIKE ?
                                         OR [UserProfile].[Username] LIKE ?
                                         OR [pafupi_account].[acc_no] LIKE ?
                                         OR [pafupi_account].[starter_id] LIKE ?
                                         OR [account_mobility].[dispatched_by] LIKE ? )";
        $data['result'] = $this->db->query($sql,array($branch_id,true,false,false,$parameter,$parameter,$parameter,$parameter,$parameter))->result_array();
        $data['total'] = count($data['result']);

        return $data;
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function get_all_dispatched_not_received_by_branch($branch_id,$limit=false,$start=false,$count=false){
        try{
        if($limit){
            $sql = "SELECT * FROM (
                                         SELECT row_number() OVER (ORDER BY [pafupi_account].acc_no) AS rownum,
                                         [pafupi_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName],[acc_status]
                                         ,[received_date],[AOC],[dispatch_date],[dispatched_by]
                                         FROM [pafupi].[dbo].[pafupi_account]
                                         INNER JOIN [pafupi].[dbo].[branches]
                                         ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [pafupi].[dbo].[account_card]
                                         ON [pafupi_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[account_status]
                                         ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                          INNER JOIN [pafupi].[dbo].[account_mobility]
                                         ON [pafupi_account].[acc_no] = [account_mobility].[acc_no]
                                         INNER JOIN [pafupi].dbo.[UserProfile]
                                         ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                                         WHERE [pafupi_account].[branch_id] = ? AND [account_status].[dispatch_flag] =? AND [account_mobility].[assigned_flag] = ? AND [account_mobility].[received] = ?
                          )
                           AS A
					     WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1";
            $data['result'] = $this->db->query($sql,array($branch_id,true,false,false))->result_array();
        }
        $sql = "SELECT COUNT(*) as [Total]
                                         FROM [pafupi].[dbo].[pafupi_account]
                                         INNER JOIN [pafupi].[dbo].[branches]
                                         ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [pafupi].[dbo].[account_card]
                                         ON [pafupi_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[account_status]
                                         ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[account_mobility]
                                         ON [pafupi_account].[acc_no] = [account_mobility].[acc_no]
                                         INNER JOIN [pafupi].dbo.[UserProfile]
                                         ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                                         WHERE [pafupi_account].[branch_id] = ?
                                         AND [account_status].[dispatch_flag] =? AND [account_mobility].[assigned_flag] = ?
                                         AND [account_mobility].[received] = ?";
        $result = $this->db->query($sql,array($branch_id,true,true,false));
        foreach($result->result() as $row) {

            $total =  $row->Total;
            if($count){
                return $total;
            }
        }

        $data['total'] = $total;

        return $data;
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function search_dispatched_not_assigned($branch_id=false,$parameter){
        $parameter = "%" . $parameter . "%";
        try{
        $sql = 'SELECT [pafupi_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName],[acc_status]
                                         ,[received_date],[AOC],[dispatch_date]
                                         FROM [pafupi].[dbo].[pafupi_account]
                                         INNER JOIN [pafupi].[dbo].[branches]
                                         ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [pafupi].[dbo].[account_card]
                                         ON [pafupi_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[account_status]
                                         ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                          INNER JOIN [pafupi].[dbo].[account_mobility]
                                         ON [pafupi_account].[acc_no] = [account_mobility].[acc_no]
                                         INNER JOIN [pafupi].dbo.[UserProfile]
                                         ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                                         WHERE [pafupi_account].[branch_id] = ? AND [account_status].[dispatch_flag] =? AND [account_mobility].[assigned_flag] = ? AND [account_mobility].[received] = ?
                                         AND (OR [UserProfile].[FullName] LIKE ?
                                         OR [UserProfile].[Username] LIKE ?
                                         OR [pafupi_account].[acc_no] LIKE ?
                                         OR [pafupi_account].[starter_id] LIKE ?
                                         OR [account_mobility].[dispatched_by] LIKE ?)';
        $data['result'] = $this->db->query($sql,array($branch_id,true,false,true,$parameter,$parameter,$parameter,$parameter,$parameter))->result_array();
        $data['total'] = count($data['result']);

        return $data;
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function get_all_dispatched_not_assigned_by_branch($branch_id,$limit=false,$start=false,$count=false){
        try{
        if($limit){
            $sql = "SELECT * FROM (
                                         SELECT row_number() OVER (ORDER BY [pafupi_account].acc_no) AS rownum,
                                         [pafupi_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName],[acc_status]
                                         ,[received_date],[AOC],[dispatch_date]
                                         FROM [pafupi].[dbo].[pafupi_account]
                                         INNER JOIN [pafupi].[dbo].[branches]
                                         ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [pafupi].[dbo].[account_card]
                                         ON [pafupi_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[account_status]
                                         ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                          INNER JOIN [pafupi].[dbo].[account_mobility]
                                         ON [pafupi_account].[acc_no] = [account_mobility].[acc_no]
                                         INNER JOIN [pafupi].dbo.[UserProfile]
                                         ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                                         WHERE [pafupi_account].[branch_id] = ? AND [account_status].[dispatch_flag] =? AND [account_mobility].[assigned_flag] = ? AND [account_mobility].[received] = ?
                          )
                           AS A
					     WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1";
            $data['result'] = $this->db->query($sql,array($branch_id,true,false,true))->result_array();
        }
        $sql = "SELECT COUNT(*) as [Total]
                                         FROM [pafupi].[dbo].[pafupi_account]
                                         INNER JOIN [pafupi].[dbo].[branches]
                                         ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [pafupi].[dbo].[account_card]
                                         ON [pafupi_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[account_status]
                                         ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[account_mobility]
                                         ON [pafupi_account].[acc_no] = [account_mobility].[acc_no]
                                         INNER JOIN [pafupi].dbo.[UserProfile]
                                         ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                                         WHERE [pafupi_account].[branch_id] = ?
                                         AND [account_status].[dispatch_flag] =? AND [account_mobility].[assigned_flag] = ?
                                         AND [account_mobility].[received] = ?";
        $result = $this->db->query($sql,array($branch_id,true,false,true));
        foreach($result->result() as $row) {

            $total =  $row->Total;
            if($count){
                return $total;
            }
        }

        $data['total'] = $total;

        return $data;
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function search_assigned($branch_id=false,$parameter){
        $parameter = "%" . $parameter . "%";
        try{
        if($branch_id){
            $sql = 'SELECT [pafupi_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName],[acc_status]
                                         ,[received_date],[AOC],[dispatch_date]
                                         FROM [pafupi].[dbo].[pafupi_account]
                                         INNER JOIN [pafupi].[dbo].[branches]
                                         ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [pafupi].[dbo].[account_card]
                                         ON [pafupi_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[account_status]
                                         ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                          INNER JOIN [pafupi].[dbo].[account_mobility]
                                         ON [pafupi_account].[acc_no] = [account_mobility].[acc_no]
                                         INNER JOIN [pafupi].dbo.[UserProfile]
                                         ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                                         WHERE [pafupi_account].[branch_id] = ? AND [account_mobility].[assigned_flag] = ?
                                         AND ([account_mobility].[AOC] LIKE ? OR
                                         [account_mobility].[dispatched_by] LIKE ? OR
                                         [UserProfile].[FullName] LIKE ? OR
                                         [branches].[branch_name] LIKE ? OR
                                         [account_status].[acc_status] LIKE ?
                                         )
                ';
            $data['result'] = $this->db->query($sql,array($branch_id,true,$parameter,$parameter,$parameter,$parameter,$parameter))->result_array();
            $data['total'] = count($data['result']);
        }
        else{
            $sql = "SELECT [pafupi_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName],[acc_status]
                                         ,[received_date],[AOC],[dispatch_date]
                                         FROM [pafupi].[dbo].[pafupi_account]
                                         INNER JOIN [pafupi].[dbo].[branches]
                                         ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [pafupi].[dbo].[account_card]
                                         ON [pafupi_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[account_status]
                                         ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                          INNER JOIN [pafupi].[dbo].[account_mobility]
                                         ON [pafupi_account].[acc_no] = [account_mobility].[acc_no]
                                         INNER JOIN [pafupi].dbo.[UserProfile]
                                         ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                                         WHERE [account_mobility].[assigned_flag] = ?
                                         AND ([account_mobility].[AOC] LIKE ? OR
                                         [account_mobility].[dispatched_by] LIKE ? OR
                                         [UserProfile].[FullName] LIKE ? OR
                                         [branches].[branch_name] LIKE ? OR
                                         [account_status].[acc_status] LIKE ?
                                         )
                ";
            $data['result'] = $this->db->query($sql,array(true,$parameter,$parameter,$parameter,$parameter,$parameter))->result_array();
            $data['total'] = count($data['result']);

        }
        return $data;
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function getAssignedByBranch($branch_id,$percentage=false,$count_acc= false){
        try{
        $sql = 'SELECT [pafupi_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName],[acc_status]
                                         ,[received_date],[AOC],[dispatch_date]
                                         FROM [pafupi].[dbo].[pafupi_account]
                                         INNER JOIN [pafupi].[dbo].[branches]
                                         ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [pafupi].[dbo].[account_card]
                                         ON [pafupi_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[account_status]
                                         ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                          INNER JOIN [pafupi].[dbo].[account_mobility]
                                         ON [pafupi_account].[acc_no] = [account_mobility].[acc_no]
                                         INNER JOIN [pafupi].dbo.[UserProfile]
                                         ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                                         WHERE [pafupi_account].[branch_id] = ? AND [account_mobility].[assigned_flag] = ?
                ';
        $result = $this->db->query($sql,array($branch_id,true))->result_array();
        $count = count($result);

        if($percentage){
            $total_accounts = $this->getDispatchedByBranch($branch_id,true);
            if($total_accounts==0) $total_accounts=1;
            $percentage_received = round(($count/$total_accounts) * 100,0);

            return $percentage_received;
        }
        elseif($count_acc){
            return $count;
        }
        else{
            return $result;
        }
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function getFullyByBranch($branch_id,$percentage=false,$count_acc= false){
        try{
        $sql = 'SELECT [pafupi_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName],[acc_status]
                                         ,[received_date]
                                         FROM [pafupi].[dbo].[pafupi_account]
                                         INNER JOIN [pafupi].[dbo].[branches]
                                         ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [pafupi].[dbo].[account_card]
                                         ON [pafupi_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[Customer]
                                         ON [pafupi_account].[customer_id] = [Customer].[CustomerID]
                                          INNER JOIN [pafupi].[dbo].[CustomerProfile]
                                         ON [pafupi_account].[customer_id] = [CustomerProfile].[CustomerID]
                                         INNER JOIN [pafupi].[dbo].[account_status]
                                         ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[account_mobility]
                                         ON [pafupi_account].[acc_no] = [account_mobility].[acc_no]
                                         INNER JOIN [pafupi].dbo.[UserProfile]
                                         ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                                         WHERE [pafupi_account].[branch_id] = ?';
        $result = $this->db->query($sql,array($branch_id))->result_array();
        $count = count($result);

        if($percentage){
            $total_accounts = $this->getDispatchedByBranch($branch_id,true);
            if($total_accounts==0) $total_accounts=1;
            $percentage_received = round(($count/$total_accounts) * 100,0);

            return $percentage_received;
        }
        elseif($count_acc){
            return $count;
        }
        else{
            return $result;
        }
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function receiveStarterPack($data){
        $result = array();
        try{
        for($i = 0; $i<count($data);$i++){
            $today_date = Common_Tasks::generate_date();
            $sql = "UPDATE [pafupi].[dbo].[account_mobility]
						 SET [received_date] = ?,[received]=?
			             WHERE [acc_no] = ?";
            $done = $this->db->query($sql, array($today_date,true,$data[$i]));
            if($done){
                $username =  $this->session->userdata('username');
                $this->log_activity('Starterpack received', $username,$data[$i]);
                $result[$i] = "received starterpack with account ".$data[$i];
            }
            else{
                $username =  $this->session->userdata('username');
                $this->log_activity('Error receiving starterpack', $username,$data[$i]);
                $result[$i] = "failed to receive starterpack with account ".$data[$i];
            }

        }
        return $result;
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function getOSSC($branch_id, $search=false,$ossc=false){
        try{
        if(!$search){
            $sql = "SELECT [Username]
                      ,[FullName]
                      ,[Branch]
                      ,[UserID]
                      ,[Status]
                      ,[CreatedBy]
                      ,[ActivatedBy]
                      ,[RoleID]
                      ,[PasswordExpiry]
                      ,[LastPasswordChange]
                      ,[FirstLogin]
                      ,[EmailAddress]
                  FROM [pafupi].[dbo].[UserProfile] WHERE [branch] = ? AND [Status] =? AND RoleID=?";
            $result = $this->db->query($sql,array($branch_id,'A',3))->result_array();
            return $result;
        }
        else{
            $ossc = "%" . $ossc . "%";
            $sql = "SELECT [Username]
                      ,[FullName]
                      ,[Branch]
                      ,[UserID]
                      ,[Status]
                      ,[CreatedBy]
                      ,[ActivatedBy]
                      ,[RoleID]
                      ,[PasswordExpiry]
                      ,[LastPasswordChange]
                      ,[FirstLogin]
                      ,[EmailAddress]
                  FROM [pafupi].[dbo].[UserProfile] WHERE [branch] = ? AND [Status] =? AND [RoleID]=? AND [FullName]=?";
            $result = $this->db->query($sql,array($branch_id,'A',3,$ossc))->result_array();
            if(count($result) >=1)
                return true;
            else
                return false;
        }
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function assignStarterPack($data){
        $today_date = Common_Tasks::generate_date();
        $username =  $this->session->userdata('username');
        try{
        $sql = "UPDATE [pafupi].[dbo].[account_mobility]
						 SET [assigned_date] = ?,[AOC]=?,[assigned_flag]=?
			             WHERE [acc_no] = ?";
        $done = $this->db->query($sql, array($today_date,$data['ossc'],true,$data['acc_no']));
        if($done){
            $this->log_activity('starterpack assigned to '.$data['ossc'], $username,$data['acc_no']);
            return true;
        }
        else{
            $this->log_activity('failed to assign starterpack to '.$data['ossc'], $username,$data['acc_no']);
            return false;
        }
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function search_carded_linked($parameter){
        $parameter = "%" . $parameter . "%";
        try{
        $sql = 'SELECT [pafupi_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName],[acc_status],[referal_no],[book_no]
                                         FROM [pafupi].[dbo].[pafupi_account]
                                         INNER JOIN [pafupi].[dbo].[branches]
                                         ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [pafupi].[dbo].[account_card]
                                         ON [pafupi_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[booklet]
                                         ON [pafupi_account].[acc_no] = [booklet].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[referal_book]
                                         ON [pafupi_account].[acc_no] = [referal_book].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[account_status]
                                         ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                         INNER JOIN [pafupi].dbo.[UserProfile]
                                         ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
					 AND ([pafupi_account].[opened_by] LIKE ?
					 OR [UserProfile].[FullName] LIKE ?
					 OR [branches].[branch_name] LIKE ?
					 OR [UserProfile].[Username] LIKE ?)'					 ;
        $data['result'] = $this->db->query($sql,array($parameter,$parameter,$parameter,$parameter))->result_array();

        $count = ' SELECT COUNT(*) AS [Total] FROM
					 [pafupi].[dbo].[pafupi_account]
                                         INNER JOIN [pafupi].[dbo].[branches]
                                         ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                         
										  INNER JOIN [pafupi].[dbo].[account_card]
										ON [pafupi_account].[acc_no] = [account_card].[acc_no]
										INNER JOIN [pafupi].[dbo].[booklet]
										ON ON [pafupi_account].[acc_no] = [booklet].[acc_no]
										INNER JOIN [pafupi].[dbo].[referal_book]
										ON ON [pafupi_account].[acc_no] = [referal_book].[acc_no]
										 
                                         INNER JOIN [pafupi].[dbo].[account_status]
                                         ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                         INNER JOIN [pafupi].dbo.[UserProfile]
                                         ON [pafupi_account].[opened_by] = [UserProfile].[UserID]

					 AND [pafupi_account].[opened_by] LIKE ?
					 OR [UserProfile].[FullName] LIKE ?
					 OR [branches].[branch_name] LIKE ?
					 OR [UserProfile].[Username] LIKE ?';
        $result = $this->db->query($count,array($parameter,$parameter,$parameter,$parameter));

        foreach($result->result() as $row) {

            $total =  $row->Total;

        }

        $data['total'] = $total;

        return $data;
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }

    }
    public function getCardedLinked(){
        try{
        $sql = 'SELECT [pafupi_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName],[acc_status],[referal_no],[book_no]
                                         FROM [pafupi].[dbo].[pafupi_account]
                                         INNER JOIN [pafupi].[dbo].[branches]
                                         ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [pafupi].[dbo].[account_card]
                                         ON [pafupi_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[booklet]
                                         ON [pafupi_account].[acc_no] = [booklet].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[referal_book]
                                         ON [pafupi_account].[acc_no] = [referal_book].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[account_status]
                                         ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                         INNER JOIN [pafupi].dbo.[UserProfile]
                                         ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                                         ';
        $result = $this->db->query($sql)->result_array();
        return $result;
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }

    }
    public function get_all_carded_linked($limit,$start,$username=false){
        //get the username
        try{
        if(!$username){
            $sql = "SELECT * FROM (
                                      SELECT row_number() OVER (ORDER BY [pafupi_account].acc_no) AS rownum,
                                      [pafupi_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName],[acc_status],[referal_no],[book_no]
                                         FROM [pafupi].[dbo].[pafupi_account]
                                         INNER JOIN [pafupi].[dbo].[branches]
                                         ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [pafupi].[dbo].[account_card]
                                         ON [pafupi_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[booklet]
                                         ON [pafupi_account].[acc_no] = [booklet].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[referal_book]
                                         ON [pafupi_account].[acc_no] = [referal_book].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[account_status]
                                         ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                         INNER JOIN [pafupi].dbo.[UserProfile]
                                         ON [pafupi_account].[opened_by] = [UserProfile].[UserID]


                           ) AS A

                           WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1
                           ";


            $result = $this->db->query($sql);
        }
        else{
            $sql = "SELECT * FROM (
                                      SELECT row_number() OVER (ORDER BY [pafupi_account].acc_no) AS rownum,
                                      [pafupi_account].*,[branches].[branch_name],[account_card].[card_no],[UserProfile].[FullName],[acc_status],[referal_no],[book_no]
                                         FROM [pafupi].[dbo].[pafupi_account]
                                         INNER JOIN [pafupi].[dbo].[branches]
                                         ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [pafupi].[dbo].[account_card]
                                         ON [pafupi_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[booklet]
                                         ON [pafupi_account].[acc_no] = [booklet].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[referal_book]
                                         ON [pafupi_account].[acc_no] = [referal_book].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[account_status]
                                         ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                         INNER JOIN [pafupi].dbo.[UserProfile]
                                         ON [pafupi_account].[opened_by] = [UserProfile].[UserID]



                           ) AS A

                           WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1
                           ";


            $result = $this->db->query($sql);
        }
        //return the query result
        return $result->result_array();
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function get_all_referal_books(){
        try{
        $sql = "SELECT  [acc_no]
                  ,[referal_no]
                  ,[ref_id]
                  ,[active]
                  ,[created_by]
                  ,[date_created]
               FROM [pafupi].[dbo].[referal_book] WHERE [active]=?";
        $result = $this->db->query($sql,array('0',false))->result_array();
        return $result;
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function get_active_referal_books(){
        try{
        $sql = "SELECT  [acc_no]
                  ,[referal_no]
                  ,[ref_id]
                  ,[active]
                  ,[created_by]
                  ,[date_created]
               FROM [pafupi].[dbo].[referal_book] WHERE [active]=?";
        $result = $this->db->query($sql,array(true))->result_array();
        return $result;
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function verifyStarterPack($data){
        try{
        $sql = "SELECT COUNT([pafupi_account].[acc_no]) as count
                                         FROM [pafupi].[dbo].[pafupi_account]
                                         INNER JOIN [pafupi].[dbo].[branches]
                                         ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                         INNER JOIN [pafupi].[dbo].[account_card]
                                         ON [pafupi_account].[acc_no] = [account_card].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[booklet]
                                         ON [pafupi_account].[acc_no] = [booklet].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[referal_book]
                                         ON [pafupi_account].[acc_no] = [referal_book].[acc_no]
                                         INNER JOIN [pafupi].[dbo].[account_status]
                                         ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                         INNER JOIN [pafupi].dbo.[UserProfile]
                                         ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                                         WHERE [pafupi_account].[acc_no]=?
                                         AND [book_no]=?
                                         AND [referal_no] =?
                                         AND [starter_id] =?
                                         AND [card_no] =?
                                         AND [acc_status] !=?";
        $result = $this->db->query($sql,array($data['acc_no'],$data['book_no'],$data['referal_no'],$data['starter_id'],$data['card_no'],'starterpack'));
        $count = 0;
        foreach($result->result() as $row){
            $count = $row->count;
        }
        if($count > 0){
            return true;
        }
        else{
            return false;
        }
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function checkStarterPack($data){
        try{
        $is_there = false;
        $username =  $this->session->userdata('username');
        $num = 0;
        for($i=0;$i<count($data);$i++){
            $is_there = $this->verifyStarterPack($data[$i]);
            if($is_there){
                $sql_update ="UPDATE [pafupi].[dbo].[account_status]
                            SET [acc_status] = ?
                            WHERE [acc_no] =?";
                $result = $this->db->query($sql_update,array('starterpack',$data[$i]['acc_no']));
                if($result){
                    $this->log_activity("Account". $data[$i]['acc_no'] ."status to starterpack", $username,$data[$i]['acc_no']);
                    $num++;
                }
                else{

                }
            }
            else{

            }
        }
        return $num." of ".count($data)." accounts verified";
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function get_active_otp_books(){
        $sql = "SELECT [book_no]
                      ,[acc_no],[date_created],[created_by]
                      ,[active]
                  FROM [pafupi].[dbo].[booklet]
                  WHERE [active]=?";
        $result = $this->db->query($sql,array(true))->result_array();
        return $result;

    }
    public function get_all_otp_books(){
        try{
        $sql = "SELECT [book_no]
                      ,[acc_no],[date_created],[created_by]
                      ,[active]
                  FROM [pafupi].[dbo].[booklet]
                  WHERE [acc_no] =? AND [active]=?";
        $result = $this->db->query($sql,array('0',false))->result_array();
        return $result;
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function updateDummyCard($date){
        try{
        $sql = "UPDATE [pafupi].[dbo].[dummy_accounts]
                SET [download_flag] =?
                 WHERE [date_created] = ?";
        $result = $this->db->query($sql,array(true,$date));
        return $result;
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function getDummyCard($branch_id=false){
        try{
        if(!$branch_id)
        $sql = "SELECT [dummy_accounts].*,[branches].[branch_name] as branch FROM [pafupi].[dbo].[dummy_accounts]
                INNER JOIN [pafupi].[dbo].[branches]
                ON [dummy_accounts].[branch_id] = [branches].[branch_id]
                WHERE [download_flag]=?";
        else
            $sql = "SELECT [dummy_accounts].*,[branches].[branch_name] as branch FROM [pafupi].[dbo].[dummy_accounts]
                INNER JOIN [pafupi].[dbo].[branches]
                ON [dummy_accounts].[branch_id] = [branches].[branch_id]
                WHERE [download_flag]=? AND [dummy_accounts].[branch_id]=?";
        $result = $this->db->query($sql,array(false,$branch_id))->result_array();
        return $result;
        }

        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function createDummyCard($num,$branch_id){
        try{
        $num_success = 0;
        $fullname =  $this->session->userdata('fullname');
        $today_date = Common_Tasks::generate_date();
        for($i=0;$i<$num;$i++){
            $sql = 'INSERT INTO [pafupi].[dbo].[dummy_accounts]
                ([branch_id],[created_by],[date_created])
                VALUES (?,?,?)';
            $result = $this->db->query($sql,array($branch_id,$fullname,$today_date));
            if($result)
                $num_success++;

        }
        return $num_success;
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function getToLink(){
        $sql = "  SELECT  [pafupi_account].[acc_no],[account_status].[acc_status]
                                            FROM [pafupi].[dbo].[pafupi_account]
                                            INNER JOIN [pafupi].[dbo].[branches]
                                            ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                            INNER JOIN [pafupi].dbo.[UserProfile]
                                            ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                                            INNER JOIN [pafupi].[dbo].[account_status]
                                            ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                            WHERE [account_status].[acc_status] !=? AND [account_status].[acc_status] !=?
                           ";


        $result = $this->db->query($sql,array('linked','starterpack'))->result_array();
        return $result;
    }
    public function get_all_to_link_accounts($limit=false,$start=false,$count=false)
    {
        try{
        if(!$count){
            $sql = "SELECT * FROM (
                                      SELECT row_number() OVER (ORDER BY [pafupi_account].acc_no) AS rownum,[pafupi_account].*,[branches].[branch_name],[Username],[FullName]
                                            FROM [pafupi].[dbo].[pafupi_account]
                                            INNER JOIN [pafupi].[dbo].[branches]
                                            ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                            INNER JOIN [pafupi].dbo.[UserProfile]
                                            ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                                            INNER JOIN [pafupi].[dbo].[account_status]
                                            ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                            WHERE [account_status].[acc_status] !=? AND [account_status].[acc_status] !=?


                           ) AS A

                           WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1
                           ";


            $result = $this->db->query($sql,array('linked','starterpack'));
        }
        else{
            $sql = "  SELECT                COUNT(*) as count
                                            FROM [pafupi].[dbo].[pafupi_account]
                                            INNER JOIN [pafupi].[dbo].[branches]
                                            ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                            INNER JOIN [pafupi].dbo.[UserProfile]
                                            ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                                            INNER JOIN [pafupi].[dbo].[account_status]
                                            ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                            WHERE [account_status].[acc_status] !=? AND [account_status].[acc_status] !=?
                           ";


            $result = $this->db->query($sql,array('linked','starterpack'));
            $count = 0;
            foreach($result->result() as $row){
                $count = $row->count;
                return $count;
            }
        }

        return $result->result_array();
        }
        catch(Exception $e){
            redirect("user/show_errors");
        }
    }
    public function getToCard(){
        $sql = "  SELECT    [pafupi_account].[acc_no],[account_status].[acc_status]
                                            FROM [pafupi].[dbo].[pafupi_account]
                                            INNER JOIN [pafupi].[dbo].[branches]
                                            ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                            INNER JOIN [pafupi].dbo.[UserProfile]
                                            ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                                            INNER JOIN [pafupi].[dbo].[account_status]
                                            ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                            WHERE [account_status].[acc_status] !=? AND [account_status].[acc_status] !=?
                           ";


        $result = $this->db->query($sql,array('carded','starterpack'))->result_array();
        return $result;
    }
    public function get_all_to_card_accounts($limit=false,$start=false,$count=false)
    {
       try{
        if(!$count){
            $sql = "SELECT * FROM (
                                      SELECT row_number() OVER (ORDER BY [pafupi_account].acc_no) AS rownum,[pafupi_account].*,[branches].[branch_name],[Username],[FullName]
                                            FROM [pafupi].[dbo].[pafupi_account]
                                            INNER JOIN [pafupi].[dbo].[branches]
                                            ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                            INNER JOIN [pafupi].dbo.[UserProfile]
                                            ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                                            INNER JOIN [pafupi].[dbo].[account_status]
                                            ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                            WHERE [account_status].[acc_status] !=? AND [account_status].[acc_status] !=?


                           ) AS A

                           WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1
                           ";


            $result = $this->db->query($sql,array('carded','starterpack'));
        }
        else{
                                            $sql = "  SELECT   COUNT(*) as count
                                            FROM [pafupi].[dbo].[pafupi_account]
                                            INNER JOIN [pafupi].[dbo].[branches]
                                            ON [pafupi_account].[branch_id] = [branches].[branch_id]
                                            INNER JOIN [pafupi].dbo.[UserProfile]
                                            ON [pafupi_account].[opened_by] = [UserProfile].[UserID]
                                            INNER JOIN [pafupi].[dbo].[account_status]
                                            ON [pafupi_account].[acc_no] = [account_status].[acc_no]
                                            WHERE [account_status].[acc_status] !=? AND [account_status].[acc_status] !=?
                           ";


            $result = $this->db->query($sql,array('carded','starterpack'));
            $count = 0;
            foreach($result->result() as $row){
                $count = $row->count;
                return $count;
            }
        }

        return $result->result_array();
       }
       catch(Exception $e){
           redirect("user/show_errors");
       }
    }
    function __destruct() {
        $this->db->close();
    }
}
