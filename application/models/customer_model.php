<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Customer_model extends CI_Model {
    public $branch_id;
    public function __construct(){
        parent::__construct();
        $this->load->database();
        //$this->load->library('common_functions');
        $this->branch_id = $this->session->userdata('branch');

    }
    public function saveFacePhoto($cust_id,$content){
        $content = $this->prepareImageDBString($content);
        $sql = 'UPDATE [pafupi].[dbo].CustomerProfile
                SET CustomerFacePhoto=?
                WHERE CustomerID=?';
        $done =$this->db->query($sql,array($content,$cust_id));
        return $done;

    }
    public function saveIDPhoto($cust_id,$content){
        $content = $this->prepareImageDBString($content);
        $sql = 'UPDATE [pafupi].[dbo].CustomerProfile
                SET CustomerIDPhoto=?
                WHERE CustomerID=?';
        $done =$this->db->query($sql,array($content,$cust_id));
        return $done;
    }
    function prepareImageDBString($filepath)
    {
        $out = 'null';
        $handle = @fopen($filepath, 'rb');
        if ($handle)
        {
            $content = @fread($handle, filesize($filepath));
            $content = bin2hex($content);
            @fclose($handle);
            $out = "0x".$content;
        }
        return $out;
    }
    public function getCustomer($customer_id=false,$parameter){
        if($customer_id){
            $sql = " SELECT                        [CustomerNumber]
                                                  ,[CustomerTitle]
                                                  ,[CustomerInitial]
                                                  ,[CustomerSurname]
                                                  ,[CustomerFirstName]
                                                  ,[CustomerIDType]
                                                  ,[CustomerIDNumber]
                                                  ,[CustomerIDExpiryDate]
                                                  ,[CustomerLegalEntity]
                                                  ,[CustomerID]
                                              FROM [pafupi].[dbo].[Customer]
                                              INNER JOIN [pafupi].[dbo].[pafupi_account]
                                              ON [Customer].[CustomerNumber] = [pafupi_account].[customer_id]
                                              WHERE [CustomerFirstName] !=? AND [CustomerSurname] !=? AND [CustomerNumber] =?
                                              AND [branch_id] = ?";
            $data['result'] = $this->db->query($sql,array('NULL','NULL',$customer_id,$this->branch_id))->result_array();
            $data['total'] = count($data['result']);
        }
        elseif($parameter){
            $parameter = "%" . $parameter . "%";
            $sql = " SELECT                        [CustomerNumber]
                                                  ,[CustomerTitle]
                                                  ,[CustomerInitial]
                                                  ,[CustomerSurname]
                                                  ,[CustomerFirstName]
                                                  ,[CustomerIDType]
                                                  ,[CustomerIDNumber]
                                                  ,[CustomerIDExpiryDate]
                                                  ,[CustomerLegalEntity]
                                                  ,[CustomerID]
                                              FROM [pafupi].[dbo].[Customer]
                                              INNER JOIN [pafupi].[dbo].[pafupi_account]
                                              ON [Customer].[CustomerNumber] = [pafupi_account].[customer_id]
                                              WHERE [CustomerFirstName] !=? AND [CustomerSurname] !=? AND [branch_id] = ?
                                              AND ( [CustomerFirstName] LIKE ? OR [CustomerSurname] LIKE ?)";
            $data['result'] = $this->db->query($sql,array('NULL','NULL',$this->branch_id,$parameter,$parameter))->result_array();
            $data['total'] = count($data['result']);
        }
        else{
            $sql = " SELECT                        [CustomerNumber]
                                                  ,[CustomerTitle]
                                                  ,[CustomerInitial]
                                                  ,[CustomerSurname]
                                                  ,[CustomerFirstName]
                                                  ,[CustomerIDType]
                                                  ,[CustomerIDNumber]
                                                  ,[CustomerIDExpiryDate]
                                                  ,[CustomerLegalEntity]
                                                  ,[CustomerID]
                                              FROM [pafupi].[dbo].[Customer]
                                              INNER JOIN [pafupi].[dbo].[pafupi_account]
                                              ON [Customer].[CustomerNumber] = [pafupi_account].[customer_id]
                                              WHERE [CustomerFirstName] !=? AND [CustomerSurname] !=? AND [branch_id] = ?";
            $data['result'] = $this->db->query($sql,array('NULL','NULL',$this->branch_id))->result_array();
            $data['total'] = count($data['result']);
        }
        return $data;
    }
    public function get_all_opened_customers($limit=false,$start=false,$count=false){
        if($limit){
            $sql = "SELECT * FROM (
                                         SELECT row_number() OVER (ORDER BY [Customer].CustomerID) AS rownum,
                                         [CustomerNumber]
                                                  ,[CustomerTitle]
                                                  ,[CustomerInitial]
                                                  ,[CustomerSurname]
                                                  ,[CustomerFirstName]
                                                  ,[CustomerIDType]
                                                  ,[CustomerIDNumber]
                                                  ,[CustomerIDExpiryDate]
                                                  ,[CustomerLegalEntity]
                                                  ,[CustomerID]
                                              FROM [pafupi].[dbo].[Customer]
                                              INNER JOIN [pafupi].[dbo].[pafupi_account]
                                              ON [Customer].[CustomerNumber] = [pafupi_account].[customer_id]
                                              WHERE [CustomerFirstName] !=? AND [CustomerSurname] !=? AND [branch_id] = ?
                          )
                           AS A
					     WHERE A.rownum BETWEEN ($start) AND ($start + $limit) - 1";
            $data['result'] = $this->db->query($sql,array('NULL','NULL',$this->branch_id))->result_array();
        }
                                $sql = "SELECT COUNT(*) as [Total]
                                        FROM [pafupi].[dbo].[Customer]
                                        INNER JOIN [pafupi].[dbo].[pafupi_account]
                                        ON [Customer].[CustomerNumber] = [pafupi_account].[customer_id]
                                        WHERE [CustomerFirstName] !=? AND [CustomerSurname] !=? AND [branch_id] = ?";
        $result = $this->db->query($sql,array('NULL','NULL',$this->branch_id));
        foreach($result->result() as $row) {

            $total =  $row->Total;
            if($count){
                return $total;
            }
        }

        $data['total'] = $total;

        return $data;
    }
}