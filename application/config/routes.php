<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "pafupi";
$route['404_override'] = 'uaer/show_errors';
$route['create_account_view'] = "account/createAccountView";//
$route['create_view'] = "account/createAccount";//
$route['view_excel'] = "create_excel/createDat";//
$route['view_dat/(:any)'] = "create_excel/createDat/$1";//
$route['upload_fin_file'] = "account/uploadCardFile";//
$route['upload_manu_file'] = "account/uploadManuFile";//
$route['pin_regen'] = "account/regeneratePins";//
$route['fin_file'] = "account/readCardExcel";

$route['download_book_pins_excel'] = "create_excel/createPinsExcel";
$route['download_referal_pins_excel'] = "create_excel/createReferalPinsExcel";
$route['download_otp_referal_excel'] = "create_excel/createOTPReferalExcel";
$route['download_referals'] = 'create_excel/createReferalExcel';
$route['download_book_pins'] = 'create_excel/createPinsExcel';
$route['card_linked_excel'] = 'create_excel/createCardLinkedExcel';

$route['view_prelinked']="account/viewPrelinked";//
$route['manu_file'] = "account/readManuExcel";
$route['get_otp'] = "create_excel/createPinsExcel";
$route['get_otp/(:num)'] = "create_excel/createPinsExcel/$1";
$route['card_accounts'] = "account/viewCardAccounts";//
$route['view_card_excel']="create_excel/createCardExcel";//
$route['real_card_excel'] = "account/createWithCardExcel";//
$route['view_card_real'] = "account/viewWithCardAccounts";//
$route['pafupi/login'] = "user/authenticate_user";
/* End of file routes.php */
/* Location: ./application/config/routes.php */